#! /usr/bin/env python
# BEGIN PROGRAM: QPEEK
# By: Bob Greschke
# Started: 2012.291
#   Q330/Nanometrics/Antelope/SeisComp data file reader and plotter.
################################################################
#
# modification
# version: 2020.220
# author: Maeva Pourpoint
#
# Updates to work under Python 3.
# Unit tests to ensure basic functionality.
# Code cleanup to conform to the PEP8 style guide.
# Directory cleanup (remove unused files introduced by Cookiecutter).
# Packaged with conda.
################################################################

import cProfile
import pstats
from calendar import monthcalendar, setfirstweekday
from copy import deepcopy
from fnmatch import fnmatch
from functools import wraps
from inspect import stack
from math import cos, sqrt
from os import W_OK, access, environ, getcwd, listdir, makedirs, remove, sep
from os.path import abspath, basename, dirname, exists, getsize, isdir, isfile
from struct import pack, unpack
from subprocess import call
from sys import argv, exit, platform, stdout, version_info
from time import gmtime, localtime, sleep, strftime, time
from warnings import filterwarnings


def profile(output_file=None, sort_by='cumulative', lines_to_print=None,
            strip_dirs=False):
    """A time profiler decorator.
    Inspired by and modified the profile decorator of Giampaolo Rodola:
    http://code.activestate.com/recipes/577817-profile-decorator/
    Args:
        output_file: str or None. Default is None
            Path of the output file. If only name of the file is given, it's
            saved in the current directory.
            If it's None, the name of the decorated function is used.
        sort_by: str or SortKey enum or tuple/list of str/SortKey enum
            Sorting criteria for the Stats object.
            For a list of valid string and SortKey refer to:
            https://docs.python.org/3/library/profile.html
        lines_to_print: int or None
            Number of lines to print. Default (None) is for all the lines.
            This is useful in reducing the size of the printout, especially
            that sorting by 'cumulative', the time consuming operations
            are printed toward the top of the file.
        strip_dirs: bool
            Whether to remove the leading path info from file names.
            This is also useful in reducing the size of the printout
    Returns:
        Profile of the decorated function
    """

    def inner(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            _output_file = output_file or func.__name__ + '.prof'
            pr = cProfile.Profile()
            pr.enable()
            retval = func(*args, **kwargs)
            pr.disable()
            pr.dump_stats(_output_file)

            with open(_output_file, 'w') as f:
                ps = pstats.Stats(pr, stream=f)
                if strip_dirs:
                    ps.strip_dirs()
                if isinstance(sort_by, (tuple, list)):
                    ps.sort_stats(*sort_by)
                else:
                    ps.sort_stats(sort_by)
                ps.print_stats(lines_to_print)
            return retval

        return wrapper

    return inner


PROGSystem = platform[:3].lower()
PROG_NAME = "QPEEK"
PROG_NAMELC = "qpeek"
PROG_VERSION = "2023.2.0.0"
PROG_LONGNAME = "Q330/Nanometrics Data File Reader/Plotter"
PROG_SETUPSVERS = "A"
PROG_CPSETUPSVERS = "A"

PROGKiosk = False
# If True the program will ignore the saved main display geometry information.
# Use this to force the main form to fit on the display.
PROGIgnoreGeometry = False
PROGIgnoreSetups = False
PROG_SETUPSUSECWD = False
for Arg in argv[1:]:
    if Arg == "-#":
        stdout.write("%s\n" % PROG_VERSION)
        exit(0)
    elif Arg == "-g":
        PROGIgnoreGeometry = True
    elif Arg == "-x":
        PROGIgnoreSetups = True

########################
# BEGIN: versionChecks()
# LIB:versionChecks():2019.214
#   Checks the current version of Python and sets up a couple of things for the
#   rest of the program to use.
#   Obviously this is not a real function. It's just a collection of things for
#   all programs to check and making it look like a library function makes it
#   easy to update everywhere.
#   This version is for Tkinter programs vs command line programs.
PROG_PYVERSION = "%d.%d.%d" % (
    version_info[0], version_info[1], version_info[2])
# PROG_PYVERSION = "4.0.0"
if PROG_PYVERSION.startswith("3"):
    PROG_PYVERS = 3
    from tkinter import *  # noqa: F403
    from tkinter.font import Font
    from urllib.request import urlopen
    # if PROG_NAME == "ITS":
    #     # This is a 'low-level' module but it is being used nicely.
    #     from _thread import interrupt_main, start_new_thread
    # if PROG_NAME == "WEBEDIT":
    #     from html.parser import HTMLParser
    #     # From Pillow. I don't know if Python Imaging Library will ever be
    #     # for Py3.
    #     from PIL import Image, ImageTk
    astring = str
    anint = int
    arange = range
    aninput = input
else:
    stdout.write("Unsupported Python version: %s\nStopping.\n" %
                 PROG_PYVERSION)
    exit(0)
# Nice. Right-click on a Mac using Anaconda Tkinter generates a <Button-2>
# event, instead of <Button-3>.
B2Glitch = False
if PROGSystem == "dar":
    B2Glitch = True
# These should be big enough for my programs, and not bigger than any system
# they run on can handle. These are around in some form on some systems with
# some versions, but with these figuring out what is where is moot.
maxInt = 1E100
maxFloat = 1.0E100
# END: versionChecks


filterwarnings("ignore")
setfirstweekday(6)

# This is way up here so StringVars and IntVars can be declared throughout the
# code.
Root = Tk()
Root.withdraw()

# For the program's forms, Text fields, buttons...
PROGBar = {}
PROGCan = {}
PROGEnt = {}
PROGFrm = {}
PROGMsg = {}
PROGTxt = {}

#####################
# BEGIN: option_add()
# LIB:option_add():2019.220
# A collection of setup items common to most of my programs.
# Where all of the vars for saving and loading setups are kept.
PROGSetups = []
# These may be altered by loading the setups. If they end up the same as
# PROGScreen<Height/Width>OrigNow then the program will know that it is
# running on the same screen as before.
PROGScreenHeightSaved = IntVar()
PROGScreenWidthSaved = IntVar()
PROGScreenHeightSaved.set(Root.winfo_screenheight())
PROGScreenWidthSaved.set(Root.winfo_screenwidth())
PROGSetups += ["PROGScreenHeightSaved", "PROGScreenWidthSaved"]
# Alter these if you want to fool the program into doing something on a small
# screen.
PROGScreenHeightNow = Root.winfo_screenheight()
PROGScreenWidthNow = Root.winfo_screenwidth()
# Fonts: a constant nagging problem, though less now. I've stopped trying
# to micromanage them and mostly let the user suffer with what the system
# decides to use.
# Some items (like ToolTips and Help text) may not want to have their fonts
# resizable. So in that case use these Orig fonts whose values do not get saved
# to the setups.
PROGOrigMonoFont = Text().cget("font")
PROGOrigPropFont = Entry().cget("font")
# Only two fonts. If something needs more it will have to modify these.
PROGMonoFont = Font(font=Text()["font"])
PROGMonoFontSize = IntVar()
PROGMonoFontSize.set(PROGMonoFont["size"])
# I think this is some damn Linux-Tcl/Tk bug of some kind, but CentOS7/Tk8.5
# (in 2018) reported a "size" of 0. Just set these to -12 if it happens, so
# resizing the font does something sensible. Same below.
if PROGMonoFontSize.get() == 0:
    PROGMonoFont["size"] = -12
    PROGMonoFontSize.set(-12)
# Entry() is used because it seems to be messed with less on different
# systems unlike Label() font which can be set to some bizarre stuff.
PROGPropFont = Font(font=Entry()["font"])
# Used by some plotting routines.
PROGPropFontHeight = PROGPropFont.metrics("ascent") + \
    PROGPropFont.metrics("descent")
PROGPropFontSize = IntVar()
PROGPropFontSize.set(PROGPropFont["size"])
if PROGPropFontSize.get() == 0:
    PROGPropFont["size"] = -12
    PROGPropFontSize.set(-12)
Root.option_add("*Font", PROGPropFont)
Root.option_add("*Text*Font", PROGMonoFont)
if PROGSystem == "dar":
    PROGSystemName = "Darwin"
elif PROGSystem == "lin":
    PROGSystemName = "Linux"
elif PROGSystem == "win":
    PROGSystemName = "Windows"
elif PROGSystem == "sun":
    PROGSystemName = "Sun"
else:
    PROGSystemName = "Unknown (%s)" % PROGSystem
# Depending on the Tkinter version or how it was compiled the scroll bars can
# get pretty narrow and hard to grab.
if Scrollbar().cget("width") < "16":
    Root.option_add("*Scrollbar*width", "16")
# Just using RGB for everything since some things don't handle color names
# correctly, like PIL on macOS doesn't handle "green" very well.
# b = dark blue, was the U value for years, but it can be hard to see, so U
#     was lightened up a bit.
# Orange should be #FF7F00, but #DD5F00 is easier to see on a white background
# and it still looks OK on a black background.
# Purple should be A020F0, but that was a little dark.
# "X" should not be used. Including X at the end of a passed color pair (or by
# itself) indicates that a Toplevel or dialog box should use grab_set_global()
# which is not a color.
Clr = {"B": "#000000", "C": "#00FFFF", "G": "#00FF00", "M": "#FF00FF",
       "R": "#FF0000", "O": "#FF7F00", "W": "#FFFFFF", "Y": "#FFFF00",
       "E": "#DFDFDF", "A": "#8F8F8F", "K": "#3F3F3F", "U": "#0070FF",
       "N": "#007F00", "S": "#7F0000", "y": "#7F7F00", "u": "#ADD8E6",
       "s": "#FA8072", "p": "#FFB6C1", "g": "#90EE90", "r": "#EFEFEF",
       "P": "#AA22FF", "b": "#0000FF"}
# This is just if the program wants to let the user know what the possibilities
# are.
ClrDesc = {"B": "black", "C": "cyan", "G": "green", "M": "magenta",
           "R": "red", "O": "orange", "W": "white", "Y": "yellow",
           "E": "light gray", "A": "gray", "K": "dark gray", "U": "blue",
           "N": "dark green", "S": "dark red", "y": "dark yellow",
           "u": "light blue", "s": "salmon", "p": "light pink",
           "g": "light green", "r": "very light gray", "P": "purple",
           "b": "dark blue"}
# Now things get ugly. cget("bg") can return a hex triplet, or a color word,
# or something like "systemWindowBody". All of my programs will attempt to use
# the Root background value as their default value. This will determine what
# needs to be done to get a hex triplet value for the Root background color.
Clr["D"] = Root.cget("background")
if Clr["D"].startswith("#"):
    pass
else:
    Value = Root.winfo_rgb(Clr["D"])
    if max(Value) < 256:
        Clr["D"] = "#%02X%02X%02X" % Value
    else:
        Clr["D"] = "#%04X%04X%04X" % Value
ClrDesc["D"] = "default"
# The color of a button (like a "Stop" button) may be checked to see if a
# command is still active. Specifically set this so things just work later on.
Root.option_add("*Button*background", Clr["D"])
Root.option_add("*Button*takeFocus", "0")
# Newer versions of Tkinter are setting this to 1. That's too small.
Root.option_add("*Button*borderWidth", "2")
Root.option_add("*Canvas*borderWidth", "0")
Root.option_add("*Canvas*highlightThickness", "0")
Root.option_add("*Checkbutton*anchor", "w")
Root.option_add("*Checkbutton*takeFocus", "0")
Root.option_add("*Checkbutton*borderWidth", "1")
Root.option_add("*Entry*background", Clr["W"])
Root.option_add("*Entry*foreground", Clr["B"])
Root.option_add("*Entry*highlightThickness", "2")
Root.option_add("*Entry*insertWidth", "3")
Root.option_add("*Entry*highlightColor", Clr["B"])
Root.option_add("*Entry*disabledBackground", Clr["D"])
Root.option_add("*Entry*disabledForeground", Clr["B"])
# Same as for Button.
Root.option_add("*Label*background", Clr["D"])
Root.option_add("*Listbox*background", Clr["W"])
Root.option_add("*Listbox*foreground", Clr["B"])
Root.option_add("*Listbox*selectBackground", Clr["G"])
Root.option_add("*Listbox*selectForeground", Clr["B"])
Root.option_add("*Listbox*takeFocus", "0")
Root.option_add("*Listbox*exportSelection", "0")
Root.option_add("*Radiobutton*takeFocus", "0")
Root.option_add("*Radiobutton*borderWidth", "1")
Root.option_add("*Scrollbar*takeFocus", "0")
# When the slider is really small this might help make it easier to see, but
# I don't know what the system might set for the slider color.
Root.option_add("*Scrollbar*troughColor", Clr["A"])
Root.option_add("*Text*background", Clr["W"])
Root.option_add("*Text*foreground", Clr["B"])
Root.option_add("*Text*takeFocus", "0")
Root.option_add("*Text*highlightThickness", "0")
Root.option_add("*Text*insertWidth", "0")
Root.option_add("*Text*width", "0")
Root.option_add("*Text*padX", "3")
Root.option_add("*Text*padY", "3")
# To control the color of the buttons better in X-Windows programs.
Root.option_add("*Button*activeBackground", Clr["D"])
Root.option_add("*Checkbutton*activeBackground", Clr["D"])
Root.option_add("*Radiobutton*activeBackground", Clr["D"])
# Used by various time functions.
# First day of the month for each non-leap year month MINUS 1. This will get
# subtracted from the DOY, so a DOY of 91, minus the first day of April 90
# (91-90) will leave the 1st of April. The 365 is the 1st of Jan of the next
# year.
PROG_FDOM = (0, 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365)
# Max days per month.
PROG_MAXDPMNLY = (0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
PROG_MAXDPMLY = (0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
# Not very friendly to other countries, but...
PROG_CALMON = ("", "JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE",
               "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER",
               "DECEMBER")
PROG_CALMONS = ("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
                "SEP", "OCT", "NOV", "DEC")
PROG_MONNUM = {"JAN": 1, "FEB": 2, "MAR": 3, "APR": 4, "MAY": 5, "JUN": 6,
               "JUL": 7, "AUG": 8, "SEP": 9, "OCT": 10, "NOV": 11, "DEC": 12}
# For use with the return of the calendar module weekday function.
PROG_DOW = ("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun")
# Stores the number of seconds to the beginning of a year so they don't have
# to be recalculated all the time.
Y2EPOCH = {}
# First characters that can be used to modifiy searches (only used in ITS so
# far).
PROG_SEARCHMODS = [">=", "<=", "!=", "!~", ">", "<", "=", "~", "_", "*"]
# A second set of help lines that describes error messages that may be seen by
# the user. It gets filled in by the headers of the modules that generate the
# messages, and the contents will be inserted in the regular help by
# formHELP().
HELPError = ""
# END: option_add

# Try to get the machine's network name. We'll put that in the Root.title().
PROGHostname = ""
try:
    from socket import gethostname
    PROGHostname = gethostname().split(".")[0]
except Exception:
    pass


# ==============================================
# BEGIN: ========== PROGRAM FUNCTIONS ==========
# ==============================================


################################
# BEGIN: beep(Howmany, e = None)
# LIB:beep():2018.235
#   Just rings the terminal bell the number of times requested.
# NEEDS: from time import sleep
#        updateMe()
PROGNoBeepingCRVar = IntVar()
PROGSetups += ["PROGNoBeepingCRVar"]


def beep(Howmany, e=None):
    if PROGNoBeepingCRVar.get() == 0:
        # In case someone passes something wild.
        if Howmany > 20:
            Howmany = 20
        for i in arange(0, Howmany):
            Root.bell()
            if i < Howmany - 1:
                updateMe(0)
                sleep(.15)
    return
# END: beep


##############################
# BEGIN: class BButton(Button)
# LIB:BButton():2009.239
#   A sub-class of Button() that adds a bit of additional color control and
#   that adds a space before and after the text on Windows systems, otherwise
#   the edge of the button is right at the edge of the text.
class BButton(Button):
    def __init__(self, master=None, **kw):
        if PROGSystem == "win" and "text" in kw:
            if kw["text"].find("\n") == -1:
                kw["text"] = " " + kw["text"] + " "
            else:
                # Add " " to each end of each line so all lines get centered.
                parts = kw["text"].split("\n")
                ntext = ""
                for part in parts:
                    ntext += " " + part + " \n"
                kw["text"] = ntext[:-1]
        # Some systems have the button change color when rolled over.
        if "bg" in kw:
            kw["activebackground"] = kw["bg"]
        if "fg" in kw:
            kw["activeforeground"] = kw["fg"]
        Button.__init__(self, master, **kw)
# END: BButton


#########################################
# BEGIN: buttonBG(Butt, Colr, State = "")
# LIB:buttonBG():2018.234
def buttonBG(Butt, Colr, State=""):
    # Try since this may get called without the button even existing.
    try:
        if isinstance(Butt, astring):
            # Set both items to keep the button from changing colors on *NIXs
            # when the mouse rolls over. Also only use the first character of
            # the passed value so we can pass bg/fg color pairs.
            # If the State is "" leave it alone, otherwise change it to what we
            # are told.
            if len(State) == 0:
                PROGButs[Butt].configure(bg=Clr[Colr[0]],
                                         activebackground=Clr[Colr[0]])
            else:
                PROGButs[Butt].configure(bg=Clr[Colr[0]],
                                         activebackground=Clr[Colr[0]],
                                         state=State)
        else:
            if len(State) == 0:
                Butt.configure(bg=Clr[Colr[0]],
                               activebackground=Clr[Colr[0]])
            else:
                Butt.configure(bg=Clr[Colr[0]],
                               activebackground=Clr[Colr[0]], state=State)
        updateMe(0)
    except Exception:
        pass
    return
# END: buttonBG


##########################
# BEGIN: busyCursor(OnOff)
# LIB:busyCursor():2018.236
# Needs PROGFrm, and updateMe().
DefCursor = Root.cget("cursor")


def busyCursor(OnOff):
    if OnOff == 0:
        TheCursor = DefCursor
    else:
        TheCursor = "watch"
    Root.config(cursor=TheCursor)
    for Fram in list(PROGFrm.values()):
        if Fram is not None:
            Fram.config(cursor=TheCursor)
    updateMe(0)
    return
# END: busyCursor


#################################################################
# BEGIN: canText(Can, Cx, Cy, Color, Str, Anchor = "w", Tag = "")
# LIB:canText():2018.234
#   Used to print text to a canvas such that the color of individual words
#   in a line can be changed. Use 0 for Cx to tell the routine to place this
#   Str at the end of the last Str passed.
CANTEXTLastX = 0
CANTEXTLastWidth = 0


def canText(Can, Cx, Cy, Color, Str, Anchor="w", Tag=""):
    global CANTEXTLastX
    global CANTEXTLastWidth
    if Cx == 0:
        Cx = CANTEXTLastX
    if isinstance(Color, astring):
        # This way it can be passed "W" or #000000.
        if Color.startswith("#") is False:
            FClr = Clr[Color[0]]
        else:
            FClr = Color
        if len(Tag) == 0:
            ID = Can.create_text(Cx, Cy, text=Str, fill=FClr,
                                 font=PROGPropFont, anchor=Anchor)
        else:
            ID = Can.create_text(Cx, Cy, text=Str, fill=FClr,
                                 font=PROGPropFont, anchor=Anchor, tags=Tag)
    # This may be an input from getAColor().
    elif isinstance(Color, tuple):
        if len(Tag) == 0:
            ID = Can.create_text(Cx, Cy, text=Str, fill=Color[0],
                                 font=PROGPropFont, anchor=Anchor)
        else:
            ID = Can.create_text(Cx, Cy, text=Str, fill=Color[0],
                                 font=PROGPropFont, anchor=Anchor, tags=Tag)
    else:
        if len(Tag) == 0:
            ID = Can.create_text(Cx, Cy, text=Str, fill="white",
                                 font=PROGPropFont, anchor=Anchor)
        else:
            ID = Can.create_text(Cx, Cy, text=Str, fill="white",
                                 font=PROGPropFont, anchor=Anchor, tags=Tag)
    L, T, R, B = Can.bbox(ID)
    # -1: I don't know if this is a Tkinter bug or if it just happens to be
    # specific to the font that is being used or what, but it has to be done.
    CANTEXTLastX = R - 1
    CANTEXTLastWidth = R - L
    return ID
# END: canText


###############################################################################
# BEGIN: center(Parent, TheFrame, Where, InOut, Show = True, CenterX = 0, \
#                CenterY = 0)
# LIB:center():2019.002
#   Where tells the function where in relation to the Parent TheFrame should
#   show up. Use the diagram below to figure out where things will end up.
#   Where can also be NX,SX,EX,etc. to force edge of the display checking for\
#   when you absolutely, positively don't want something coming up off the
#   edge of the display. "C" or "" can be used to put TheFrame in the center
#   of Parent.
#
#      +---------------+
#      | NW    N    NE |
#      |               |
#      | W     C     E |
#      |               |
#      | SW    S    SE |
#      +---------------+
#
#   Set Parent to None to use the whole display as the parent.
#   Set InOut to "I" or "O" to control if TheFrame shows "I"nside or "O"outside
#   the Parent (does not apply if the Parent is None).
#
#   CenterX and CenterY not equal to zero overrides everything.
#
def center(Parent, TheFrame, Where, InOut, Show=True, CenterX=0,
           CenterY=0):
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    if isinstance(TheFrame, astring):
        TheFrame = PROGFrm[TheFrame]
    # Size of the display(s). Still won't be good for dual displays, but...
    DW = PROGScreenWidthNow
    DH = PROGScreenHeightNow
    # Kiosk mode. Just take over the whole screen. Doesn't check for, but only
    # works for Root. The -30 is a fudge, because systems lie about their
    # height (but not their age).
    if Where == "K":
        Root.geometry("%dx%d+0+0" % (DW, DH - 30))
        Root.deiconify()
        Root.lift()
        updateMe(0)
        return
    # So all of the dimensions get updated.
    updateMe(0)
    FW = TheFrame.winfo_reqwidth()
    if TheFrame == Root:
        # Different systems have to be compensated for a little because of
        # differences in the reported heights (mostly title and menu bar
        # heights). Some systems include the height and some don't, and, of
        # course, it all depends on the font sizes, so there is little chance
        # of this fudge ever being 100% correct.
        if PROGSystem == "dar" or PROGSystem == "win":
            FH = TheFrame.winfo_reqheight()
        else:
            FH = TheFrame.winfo_reqheight() + 50
    else:
        FH = TheFrame.winfo_reqheight()
    # Find the center of the Parent.
    if CenterX == 0 and CenterY == 0:
        if Parent is None:
            PX = 0
            PY = 0
            PW = PROGScreenWidthNow
            PH = PROGScreenHeightNow
            # A PW of >2560 (the width of a 27" iMac) probably means the user
            # has two monitors. Tkinter just gets fed the total width and the
            # smallest display's height, so just set the size to 1024x768 and
            # then let the user resize and reposition as needed. It's what they
            # get for being so lucky.
            if PW > 2560:
                PW = 1024
                PH = 768
            CenterX = PW / 2
            CenterY = PH / 2 - 25
        elif Parent == Root:
            PX = Parent.winfo_x()
            PW = Parent.winfo_width()
            CenterX = PX + PW / 2
            # Macs, Linux and Suns think the top of the Root window is below
            # the title and menu bars.  Windows thinks the top of the window
            # is the top of the window, so adjust the window heights
            # accordingly to try and cover that up. Same problem as the title
            # and menu bars.
            if PROGSystem == "win":
                PY = Parent.winfo_y()
                PH = Parent.winfo_height()
            else:
                PY = Parent.winfo_y() - 50
                PH = Parent.winfo_height() + 50
            CenterY = PY + PH / 2
        else:
            PX = Parent.winfo_x()
            PW = Parent.winfo_width()
            CenterX = PX + PW / 2
            PY = Parent.winfo_y()
            PH = Parent.winfo_height()
            CenterY = PY + PH / 2
        # Can't put forms outside the whole display.
        if Parent is None or InOut == "I":
            InOut = 1
        else:
            InOut = -1
        HadX = False
        if Where.find("X") != -1:
            Where = Where.replace("X", "")
            HadX = True
        if Where == "C":
            XX = CenterX - FW / 2
            YY = CenterY - FH / 2
        elif Where == "N":
            XX = CenterX - FW / 2
            YY = PY + (50 * InOut)
        elif Where == "NE":
            XX = PX + PW - FW - (50 * InOut)
            YY = PY + (50 * InOut)
        elif Where == "E":
            XX = PX + PW - FW - (50 * InOut)
            YY = CenterY - TheFrame.winfo_reqheight() / 2
        elif Where == "SE":
            XX = PX + PW - FW - (50 * InOut)
            YY = PY + PH - FH - (50 * InOut)
        elif Where == "S":
            XX = CenterX - TheFrame.winfo_reqwidth() / 2
            YY = PY + PH - FH - (50 * InOut)
        elif Where == "SW":
            XX = PX + (50 * InOut)
            YY = PY + PH - FH - (50 * InOut)
        elif Where == "W":
            XX = PX + (50 * InOut)
            YY = CenterY - TheFrame.winfo_reqheight() / 2
        elif Where == "NW":
            XX = PX + (50 * InOut)
            YY = PY + (50 * InOut)
        # Try to make sure the system's title bar buttons are visible (which
        # may not always be functioning, but there you go).
        if HadX is True:
            # None are on the bottom.
            if (CenterY + FH / 2) > DH:
                YY = YY - ((CenterY + FH / 2) - DH + 20)
            # Never want things off the top.
            if YY < 0:
                YY = 10
            # But now it is OS-dependent.
            # Buttons in upper-left. Fix the right edge, but then check to see
            # if it needs to be moved back to the right.
            if PROGSystem == "dar" or PROGSystem == "sun":
                if (CenterX + FW / 2) > DW:
                    XX = XX - ((CenterX + FW / 2) - DW + 20)
                if XX < 0:
                    XX = 10
            # Opposite corner.
            elif PROGSystem == "lin" or PROGSystem == "win":
                if XX < 0:
                    XX = 10
                if (CenterX + FW / 2) > DW:
                    XX = XX - ((CenterX + FW / 2) - DW + 20)
        TheFrame.geometry("+%i+%i" % (XX, YY))
    else:
        # Just do what we're told.
        TheFrame.geometry("+%i+%i" % (CenterX - FW / 2, CenterY - FH / 2))
    if Show is True:
        TheFrame.deiconify()
        TheFrame.lift()
    updateMe(0)
    return
# END: center


# FINISHME - when formTMRNG comes on line. - should be QTMRNG for qpeek.
# logpeek has a formTMRNG.
###################################
# BEGIN: changeDateFormat(WhereMsg)
# FUNC:changeDateFormat():2018.310
#   Makes sure that the format of any data in the from/to fields matches the
#   format selected in the Options menu.
def changeDateFormat(WhereMsg):
    updateMe(0)
    # Format = OPTDateFormatRVar.get()
    for Fld in ("From", "To"):
        eval("%sDateVar" % Fld).set(
            eval("%sDateVar" % Fld).get().strip().upper())
        if len(eval("%sDateVar" % Fld).get()) != 0:
            Ret = dt2Time(0, 80, eval("%sDateVar" % Fld).get(), True)
            if Ret[0] != 0:
                setMsg(WhereMsg, Ret[1], "%s date: %s" % (Fld, Ret[2]), Ret[3])
                return False
            eval("%sDateVar" % Fld).set(Ret[1][:-13])
# For the time range form formTMRNG().
#    for Fld in ("From", "To"):
#        eval("TMRNG%sVar"%Fld).set(eval("TMRNG%sVar"% \
#                Fld).get().strip().upper())
#        if len(eval("TMRNG%sVar"%Fld).get()) != 0:
# convertDate is gone.
#            Ret = (Format, eval("TMRNG%sVar"%Fld).get())
#            if Ret[0] != 0:
#                setMsg("TMRNG", "%s field value bad."%Fld, 2)
#                return False
#            eval("TMRNG%sVar"%Fld).set(Ret[1])
    return True
# END: changeDateFormat


####################
# BEGIN: changeDir()
# FUNC:changeDir():2013.047
#   QPEEK just needs to reload the file list after an otherwise normal main
#   data directory change.
def changeDir():
    changeMainDirs(Root, "thedata", 1, None, "", "")
    loadSourceFiles(MFFiles)
    return
# END: changeDir


#####################################################################
# BEGIN: changeMainDirs(Parent, Which, Mode, Var, Title, WhichDir="")
# LIB:changeMainDirs():2018.234
# Needs PROGFrm, formMYDF(), and msgLn().
#   Mode = formMYDF() mode value (some callers may not want to allow directory
#          creation, for example).
#      1 = just picking
#      2 = picking and creating
#  D,W,M = may be added to the Mode for the main directories Default button
#          (see formMYDF()).
#   Var = if not None the selected directory will be placed there, instead of
#         one of the "main" Vars. May not be used in all programs.
#   Title = Will be used for the title of the form if Var is not None.
# WhichDir = If supplied this will be 'which directory?' was changed in the
#            change message when Which is "self".
#   Not all programs will use all items.
def changeMainDirs(Parent, Which, Mode, Var, Title, WhichDir=""):
    # The caller can pass either.
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    if Which == "theall":
        # Just use the directory as a starting point.
        Answer = formMYDF(Parent, Mode, "Pick A Main Directory For All",
                          PROGMsgsDirVar.get(), "")
        if len(Answer) == 0:
            return (1, "", "Nothing done.", 0, "")
        else:
            # Some of these may not exist in a program.
            try:
                PROGDataDirVar.set(Answer)
            except Exception:
                pass
            try:
                PROGMsgsDirVar.set(Answer)
            except Exception:
                pass
            try:
                PROGWorkDirVar.set(Answer)
            except Exception:
                pass
            return (0, "WB", "All main directories changed to\n   %s" %
                    Answer, 0, "")
    elif Which == "thedata":
        Answer = formMYDF(Parent, Mode, "Pick A Main Data Directory",
                          PROGDataDirVar.get(), "")
        if len(Answer) == 0:
            return (1, )
        elif Answer == PROGDataDirVar.get():
            return (0, "", "Main data directory unchanged.", 0, "")
        else:
            PROGDataDirVar.set(Answer)
            return (0, "WB", "Main Data directory changed to\n   %s" %
                    Answer, 0, "")
    elif Which == "themsgs":
        Answer = formMYDF(Parent, Mode, "Pick A Main Messages Directory",
                          PROGMsgsDirVar.get(), "")
        if len(Answer) == 0:
            return (1, )
        elif Answer == PROGMsgsDirVar.get():
            return (0, "", "Main messages directory unchanged.", 0, "")
        else:
            PROGMsgsDirVar.set(Answer)
            return (0, "WB", "Main Messages directory changed to\n   %s" %
                    Answer, 0, "")
    elif Which == "thework":
        Answer = formMYDF(Parent, Mode, "Pick A Main Work Directory",
                          PROGWorkDirVar.get(), "")
        if len(Answer) == 0:
            return (1, )
        elif Answer == PROGWorkDirVar.get():
            return (0, "", "Main work directory unchanged.", 0, "")
        else:
            PROGWorkDirVar.set(Answer)
            return (0, "WB", "Main work directory changed to\n   %s" %
                    Answer, 0, "")
    # Var and Title must be set for "self".
    elif Which == "self":
        Answer = formMYDF(Parent, Mode, Title, Var.get(), Title)
        if len(Answer) == 0:
            return (1, )
        elif Answer == Var.get():
            if len(WhichDir) == 0:
                return (0, "", "Directory unchanged.", 0, "")
            else:
                return (0, "", "%s directory unchanged." % WhichDir, 0, "")
        else:
            Var.set(Answer)
        if len(WhichDir) == 0:
            return (0, "WB", "Directory changed to\n   %s" % Answer, 0, "")
        else:
            return (0, "WB", "%s directory changed to\n   %s" %
                    (WhichDir, Answer), 0, "")
    return
###############################################################################
# BEGIN: changeMainDirsCmd(Parent, Which, Mode, Var, Title, WhichDir, e = None)
# FUNC:changeMainDirsCmd():2014.062


def changeMainDirsCmd(Parent, Which, Mode, Var, Title, WhichDir, e=None):
    Ret = changeMainDirs(Parent, Which, Mode, Var, Title, WhichDir)
    if Ret[0] == 0:
        # Some programs may not have a messages area.
        try:
            msgLn(0, Ret[1], Ret[2], True, Ret[3])
        except Exception:
            pass
    return
# END: changeMainDirs


#######################################
# BEGIN: checkForUpdates(Parent = Root)
# LIB:checkForUpdates():2019.260
#   Finds the "new"+PROG_NAMELC+".txt" file created by the program webvers at
#   the URL and checks to see if the version in that file matches the version
#   of this program.
VERS_DLALLOW = True
VERS_VERSURL = "http://www.passcal.nmt.edu/~bob/passoft/"
VERS_PARTS = 4
VERS_NAME = 0
VERS_VERS = 1
VERS_USIZ = 2
VERS_ZSIZ = 3


def checkForUpdates(Parent=Root):
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    formMYD(Parent, (), "", "CB", "", "Checking...")
    # Otherwise the menu doesn't go away on slow connections while url'ing.
    updateMe(0)
    # May prevent an ssl CERTIFICATE_VERIFY_FAILED error if things are not
    # right.
    # If this fails the urlopen() may too, so just go on.
    try:
        import ssl
        if (environ.get("PYTHONHTTPSVERIFY") is None or
                getattr(ssl, "_create_unverified_context", "") == ""):
            ssl._create_default_https_context = ssl._create_unverified_context
    except Exception:
        pass
    # Get the file that tells us about the current version on the server.
    # One line:  PROG; version; original size; compressed size
    try:
        Fp = urlopen(VERS_VERSURL + "new" + PROG_NAMELC + ".txt")
        Line = Fp.readlines()
        Fp.close()
        formMYDReturn("")
        # If nothing was returned Line will be [] and that will except, also do
        # the decode for Py3, otherwise it comes with a b' in front.
        Line = Line[0].decode("latin-1")
        # If the file doesn't exist you get something like
        #     <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
        # How unhandy.
        if Line.find("DOCTYPE") != -1:
            Line = ""
        # We won't say anything in particular.
        Error = ""
    except Exception as e:
        Line = ""
        Error = str(e)
    # If we didn't get this then there must have been a problem.
    if len(Line) == 0:
        formMYDReturn("")
        formMYD(Parent, (("(OK)", TOP, "ok"), ), "ok", "RW",
                "It's Probably The Net.",
                "There was an error obtaining the version information from"
                " PASSCAL.", Error, 2)
        return
    Parts2 = Line.split(";")
    Parts = []
    for Part in Parts2:
        Parts.append(Part.strip())
    Parts += (VERS_PARTS - len(Parts)) * [""]
    if PROG_VERSION < Parts[VERS_VERS]:
        # Some programs don't need to be professionally installed, some do.
        if VERS_DLALLOW is True:
            Answer = formMYD(Parent, (("Download New Version", TOP,
                                       "dlprod"), ("(Don't)", TOP, "dont"), ),
                             "dont", "YB", "Oh Oh...",
                             "This is an old version of %s.\nThe current "
                             "version generally available is %s." %
                             (PROG_NAME, Parts[VERS_VERS]), "", 2)
            if Answer == "dont":
                return
            if Answer == "dlprod":
                Ret = checkForUpdatesDownload(Parent, Answer, Parts)
            if Ret == "quit":
                progQuitter(True)
                return
        elif VERS_DLALLOW is False:
            Answer = formMYD(Parent, (("(OK)", TOP, "ok"), ), "ok",
                             "YB", "Tell Someone.",
                             "This is an old version of %s.\nThe current "
                             "version generally available is %s." %
                             (PROG_NAME, Parts[VERS_VERS]), "", 2)
            return
    elif PROG_VERSION == Parts[VERS_VERS]:
        Answer = formMYD(Parent, (("Download Anyway", TOP, "dlprod"),
                                  ("(OK)", TOP, "ok")),
                         "ok", "", "Good To Go.",
                         "This copy of %s is up to date." % PROG_NAME)
        if Answer == "ok":
            return
        if Answer == "dlprod":
            Ret = checkForUpdatesDownload(Parent, Answer, Parts)
        if Ret == "quit":
            progQuitter(True)
        return
    elif PROG_VERSION > Parts[VERS_VERS]:
        if VERS_DLALLOW is False:
            Answer = formMYD(Parent, (("(OK)", TOP, "ok"), ), "ok", "GB",
                             "All Right!",
                             "Congratulations! This is a newer version of %s "
                             "than is generally available. Everyone else "
                             "probably still has version %s." %
                             (PROG_NAME, Parts[VERS_VERS]))
        elif VERS_DLALLOW is True:
            Answer = formMYD(Parent,
                             (("Download Older Version", TOP, "dlprod"),
                              ("(No, Thanks.)", TOP, "ok"), ),
                             "ok", "GB", "All Right!!",
                             "Congratulations! This is a newer version of "
                             "%s than is generally available. Everyone else "
                             "probably still has version %s. You can download "
                             "and use the older version if you want." %
                             (PROG_NAME, Parts[VERS_VERS]))
        if Answer == "ok" or Answer == "keep":
            return
        if Answer == "dlprod":
            Ret = checkForUpdatesDownload(Parent, Answer, Parts)
        if Ret == "quit":
            progQuitter(True)
        return
    return
######################################################
# BEGIN: checkForUpdatesDownload(Parent, Which, Parts)
# FUNC:checkForUpdatesDownload():2019.028


def checkForUpdatesDownload(Parent, Which, Parts):
    formMYD(Parent, (), "", "CB", "", "Downloading...")
    ZSize = int(Parts[VERS_ZSIZ])
    try:
        if Which == "dlprod":
            GetFile = "new%s.zip" % PROG_NAMELC
            Fpr = urlopen(VERS_VERSURL + GetFile)
        # SetupsDir may not be the best place to put it, but at least it will
        # be consistent and not dependent on where the user was working
        # (like it was).
        # If a program does not use PROGSetupsDirVar it will be the current
        # working directory.
        SetupsDir = PROGSetupsDirVar.get()
        if len(SetupsDir) == 0:
            SetupsDir = "%s%s" % (abspath("."), sep)
        try:
            # Just open() so I don't interfere with what the OS wants to do.
            Fpw = open(SetupsDir + GetFile, "w")
        except Exception as e:
            formMYDReturn("")
            formMYD(Parent, (("(OK)", TOP, "ok"),), "ok", "RW", "Gasp!",
                    "Error downloading %s\n\n%s" % (GetFile, e))
            return
        DLSize = 0
        while True:
            if PROG_PYVERS == 3:
                Buffer = Fpr.read(20000).decode("latin-1")
            if len(Buffer) == 0:
                break
            Fpw.write(Buffer)
            DLSize += 20000
            formMYDMsg("Downloading (%d%%)...\n" % (100 * DLSize / ZSize))
    except Exception as e:
        # They may not exist.
        try:
            Fpr.close()
        except Exception:
            pass
        try:
            Fpw.close()
        except Exception:
            pass
        formMYDReturn("")
        formMYD(Parent, (("(OK)", TOP, "ok"), ), "ok", "MW", "Ooops.",
                "Error downloading new version.\n\n%s" % e, "", 3)
        return ""
    Fpr.close()
    Fpw.close()
    formMYDReturn("")
    if Which == "dlprod":
        Answer = formMYD(Parent, (("Quit %s" % PROG_NAME, TOP, "quit"),
                                  ("Don't Quit", TOP, "cont")),
                         "cont", "GB", "Finished?",
                         "The downloaded program file has been saved "
                         "as\n\n%s\n\nYou should quit %s using the Quit "
                         "button below, unzip the downloaded file, test the "
                         "new program file to make sure it is OK, then rename "
                         "it %s.py and move it to the proper location to "
                         "replace the old version.\n\nTAKE NOTE OF WHERE THE "
                         "FILE HAS BEEN DOWNLOADED TO!" %
                         (SetupsDir + GetFile, PROG_NAME, PROG_NAMELC))
    if Answer == "quit":
        return "quit"
    return ""
# END: checkForUpdates


##############################################
# BEGIN: checkLatiLong(Which, InValue, Format)
# LIB:checkLatiLong():2016.007
#   Accepts N00.000, 00.000N, or 00.000 (assumed N or E) values and returns
#   the requested Format.
def checkLatiLong(Which, InValue, Format):
    Value = InValue.upper().strip()
    if Which == "lati":
        try:
            if Value.startswith("N") or Value.startswith("+"):
                Lat = float(Value[1:])
                Sign = "N"
            elif Value.startswith("S") or Value.startswith("-"):
                Lat = abs(float(Value[1:]))
                Sign = "S"
            elif Value.endswith("N"):
                Lat = floatt(Value)
                Sign = "N"
            elif Value.endswith("S"):
                Lat = floatt(Value)
                Sign = "S"
            else:
                # raise ValueError
                Lat = floatt(Value)
                Sign = "N"
                if Lat < 0.0:
                    Sign = "S"
            if Lat > 90.0:
                return (1, "RW", "Latitude > 90.0.", 2, "")
            if Format == 0:
                if Sign == "S":
                    Lat = -Lat
                return (0, Lat)
            elif Format == 7:
                return (0, Sign + "%010.7f" % Lat)
        except ValueError:
            return (1, "RW", "Bad latitude: '%s'" % Value, 2, "")
    elif Which == "long":
        try:
            if Value.startswith("E") or Value.startswith("+"):
                Long = float(Value[1:])
                Sign = "E"
            elif Value.startswith("W") or Value.startswith("-"):
                Long = abs(float(Value[1:]))
                Sign = "W"
            elif Value.endswith("E"):
                Long = floatt(Value)
                Sign = "E"
            elif Value.endswith("W"):
                Long = floatt(Value)
                Sign = "W"
            else:
                # raise ValueError
                Long = floatt(Value)
                Sign = "E"
                if Long < 0.0:
                    Sign = "W"
            if Long > 180.0:
                return (1, "RW", "Longitude > 180.0.", 2, "")
            if Format == 0:
                if Sign == "W":
                    Long = -Long
                return (0, Long)
            elif Format == 7:
                return (0, Sign + "%011.7f" % Long)
        except ValueError:
            return (1, "RW", "Bad longitude: '%s'" % Value, 2, "")
# END: checkLatiLong


###########################
# BEGIN: cleanAFilename(In)
# LIB:cleanAFilename():2013.246
#   Goes through the passed string and turns any sep characters or colons into
#   -.
def cleanAFilename(In):
    Out = ""
    for C in In:
        if C == sep or C == ":":
            C = "-"
        Out += C
    return Out
# END: cleanAFilename


###########################
# BEGIN: findRecordSize(Fp)
# FUNC:findRecordSize():2019.060
#   If this finds the wrong size the caller(s) will have to deal with it (they
#   will read garbage at some point).
def findRecordSize(Fp):
    # Try to read 4K. If that works get what should be the station ID and go
    # looking for it. If 4K is read assume the size is 4K, but go through and
    # check the smaller possible sizes. Etc etc etc.
    Fp.seek(0)
    Record = Fp.read(4096)
    if len(Record) < 256:
        return (1, "MW", "File too small. Unknown record size.", 3)
    StaID = Record[8:13]
    # The Q330s always seem to write 4K blocks of some record size even if they
    # need to be padded with x00 to get them to 4K. The Nanometrics stuff does
    # not. They just stop writing at the last record of data. So this is a
    # little more complicated than it was in the beginning.
    if len(Record) == 4096:
        RecordSize = 4096
        if Record[8 + 2048:13 + 2048] == StaID:
            RecordSize = 2048
        if Record[8 + 2048:13 + 2048] == StaID:
            RecordSize = 2048
        if Record[8 + 1024:13 + 1024] == StaID:
            RecordSize = 1024
        if Record[8 + 512:13 + 512] == StaID:
            RecordSize = 512
        if Record[8 + 256:13 + 256] == StaID:
            RecordSize = 256
    # Must be 256 since it is 256 less than 4096.
    elif len(Record) == 3840:
        RecordSize = 256
    # I've seen this one.
    elif len(Record) == 3584:
        RecordSize = 512
        if Record[8 + 256:13 + 256] == StaID:
            Record = 256
    # There should probably be more like above, but they have not cropped up
    # yet in real life.
    elif len(Record) == 2048:
        RecordSize = 2048
        if Record[8 + 1024:13 + 1024] == StaID:
            RecordSize = 1024
        if Record[8 + 512:13 + 512] == StaID:
            RecordSize = 512
        if Record[8 + 256:13 + 256] == StaID:
            RecordSize = 256
    elif len(Record) == 1024:
        RecordSize = 1024
        if Record[8 + 512:13 + 512] == StaID:
            RecordSize = 512
        if Record[8 + 256:13 + 256] == StaID:
            RecordSize = 256
    elif len(Record) == 512:
        RecordSize = 512
        if Record[8 + 256:13 + 256] == StaID:
            RecordSize = 256
    # If it is less than this we won't be here.
    else:
        RecordSize = 256
    return (0, RecordSize)
# END: findRecordSize


#################################
# BEGIN: formClose(Who, e = None)
# LIB:formClose():2018.236
#   Handles closing a form. Who can be a PROGFrm[] item or a Tcl pointer.
def formClose(Who, e=None):
    # In case it is a "busy" form that takes a long time to close.
    updateMe(0)
    if isinstance(Who, astring):
        # The form may not exist or PROGFrm[Who] may be pointing to a "lost"
        # form, or who knows what, so try.
        try:
            if PROGFrm[Who] is None:
                return
            PROGFrm[Who].destroy()
        except Exception:
            pass
    else:
        Who.destroy()
    try:
        PROGFrm[Who] = None
    except Exception:
        pass
    return
###############################
# BEGIN: formCloseAll(e = None)
# FUNC:formCloseAll():2018.231
#   Goes through all of the forms and shuts them down.


def formCloseAll(e=None):
    for Frmm in list(PROGFrm.keys()):
        # Try to call a formXControl() function. Some may have them and some
        # may not.
        try:
            # Expecting from formXControl() functions:
            # Ret[0] 0 == Continue.
            # Ret[0] 1 == Problem solved, can continue.
            # Ret[0] 2 == Problem has or has not been resolved, should stop.
            # What actually gets done is handled by the caller this just passes
            # back anything that is not 0.
            Ret = eval("form%sControl" % Frmm)("close")
            if Ret[0] != 0:
                return Ret
        except Exception:
            formClose(Frmm)
    return (0, )
# END: formClose


######################
# BEGIN: class Command
# LIB:Command():2006.114
#   Pass arguments to functions from button presses and menu selections! Nice!
#   In your declaration:  ...command = Command(func, args,...)
#   Also use in bind() statements
#       x.bind("<****>", Command(func, args...))
class Command:
    def __init__(self, func, *args, **kw):
        self.func = func
        self.args = args
        self.kw = kw

    def __call__(self, *args, **kw):
        args = self.args + args
        kw.update(self.kw)
        self.func(*args, **kw)
# END: Command


#########################
# BEGIN: clearQPGlobals()
# FUNC:clearQPGlobals():2019.231
#   Clears out the QP global variables in one call.
def clearQPGlobals():
    global QPData
    global QPLogs
    global QPGaps
    global QPErrors
    global QPStaIDs
    global QPNetCodes
    global QPTagNos
    global QPSWVers
    global QPLocIDs
    global QPSampRates
    global QPInstruments
    global QPAntSpikesWhacked
    global QPUnknownChanIDs
    global QPUnknownBlocketteType
    global QPFilesProcessed
    global QPPlotRanges
    global QPListChannels
    QPData.clear()
    del QPLogs[:]
    QPGaps.clear()
    del QPErrors[:]
    del QPStaIDs[:]
    del QPNetCodes[:]
    del QPTagNos[:]
    del QPSWVers[:]
    del QPLocIDs[:]
    QPSampRates.clear()
    del QPInstruments[:]
    QPAntSpikesWhacked = 0
    del QPUnknownChanIDs[:]
    del QPUnknownBlocketteType[:]
    del QPFilesProcessed[:]
    del QPPlotRanges[:]
    QPListChannels.clear()
# END: clearQPGlobals


##################################################
# BEGIN: compFs(DirVar, FileVar, VarSet, e = None)
# LIB:compFs():2018.310
# Needs setMsg(), beep()
#     from fnmatch import fnmatch
#     from os.path import basename, dirname, isdir
#   Attempts to complete the directory or file name in an Entry field using
#   the Tab key.
#   - If DirVar is set to a field's StringVar and FileVar is None then the
#     routine only looks for directories.
#   - If DirVar and FileVar are set to StringVars then it works to find a
#     complete fliespec, but getting the path and filenames from different
#     fields.
#   - If DirVar is None and FileVar is set to a StringVar then it trys to
#     complete path and filenames.
#
#   Call compFsSetup() after the Entry field has been created.
def compFs(DirVar, FileVar, VarSet, e=None):
    if VarSet is not None:
        setMsg(VarSet, "", "")
    # ---- Caller only wants directories.
    if DirVar is not None and FileVar is None:
        Dir = dirname(DirVar.get())
        # This is a slight gotchya. If the field is empty that might mean that
        # the user means "/" should be the starting point. If it is they will
        # have to enter the / since if we are on Windows I'd have no idea what
        # the default should be.
        if len(Dir) == 0:
            beep(2)
            return
        if Dir.endswith(sep) is False:
            Dir += sep
        # Now get what must be a partial directory name, treat it as a file
        # name, but then only allow the result of everything to be a directory.
        PartialFile = basename(DirVar.get())
        if len(PartialFile) == 0:
            beep(2)
            return
        PartialFile += "*"
        Files = listdir(Dir)
        Matched = []
        for File in Files:
            if fnmatch(File, PartialFile):
                Matched.append(File)
        if len(Matched) == 0:
            beep(2)
            return
        elif len(Matched) == 1:
            Dir = Dir + Matched[0]
            # If whatever matched is not a directory then just beep and return,
            # otherwise make it look like a directory and put it into the
            # field.
            if isdir(Dir) is False:
                beep(2)
                return
            if Dir.endswith(sep) is False:
                Dir += sep
            DirVar.set(Dir)
            e.widget.icursor(END)
            return
        else:
            # Get the max number of characters that matched and put the partial
            # directory path into the Var. If Dir+PartialDir is really the
            # directory the user wants they will have to add the sep themselves
            # since with multiple matches I won't know what to do. Consider DIR
            # DIR2 DIR3 with a compFsMaxMatch() return of DIR. The directory
            # DIR would always be selected and set as the path which may not be
            # what the user wanted. Now this could cause trouble downstream
            # since I'm leaving a path in the field without a trailing sep
            # (everything tries to avoid doing that), so the caller will have
            # to worry about that.
            PartialDir = compFsMaxMatch(Matched)
            DirVar.set(Dir + PartialDir)
            e.widget.icursor(END)
            beep(1)
            return
    else:
        # ---- Find a file, but the filespec is in one field.
        if DirVar is None and FileVar is not None:
            Dir = dirname(FileVar.get())
            Which = 1
        # ---- Find a file, but path and file are in separate fields.
        elif DirVar is not None and FileVar is not None:
            Dir = dirname(DirVar.get())
            Which = 2
        if len(Dir) == 0:
            beep(2)
            return
        if Dir.endswith(sep) is False:
            Dir += sep
        PartialFile = basename(FileVar.get())
        if len(PartialFile) == 0:
            beep(2)
            return
        # Match anything to what the user has entered for the file name.
        PartialFile += "*"
        Files = listdir(Dir)
        Matched = []
        for File in Files:
            if fnmatch(File, PartialFile):
                Matched.append(File)
        if len(Matched) == 0:
            beep(2)
            return
        elif len(Matched) == 1:
            File = Matched[0]
            Filespec = Dir + File
            # We can stick a directory name in a single field, but the user
            # will have to change the directory field if they are separate (it
            # may be that the user is not allowed to change the directory field
            # value and I don't want to violate that, plus that could be really
            # confusing for everyone when we have multiple matches).
            if isdir(Filespec):
                if Which == 1:
                    if Filespec.endswith(sep) is False:
                        Filespec += sep
                    FileVar.set(Filespec)
                    e.widget.icursor(END)
                    return
                elif Which == 2:
                    beep(2)
                    return
            if Which == 1:
                FileVar.set(Filespec)
            elif Which == 2:
                FileVar.set(File)
            e.widget.icursor(END)
            return
        else:
            PartialFile = compFsMaxMatch(Matched)
            if Which == 1:
                FileVar.set(Dir + PartialFile)
            elif Which == 2:
                FileVar.set(PartialFile)
            e.widget.icursor(END)
            beep(1)
            return
################################
# BEGIN: compFsMaxMatch(TheList)
# FUNC:compFsMaxMatch():2018.310
#   Goes through the items in TheList (should be str's) and returns the string
#   that matches the start of all of the items.
#   This is the same as the library function maxMatch().


def compFsMaxMatch(TheList):
    # This should be the only special case. What is the sound of one thing
    # matching itself?
    if len(TheList) == 1:
        return TheList[0]
    Accum = ""
    CharIndex = 0
    # If anything goes wrong just return whatever we've accumulated. This will
    # end by no items being in TheList or one of the items running out of
    # characters (the try) or by the TargetChar not matching a character from
    # one of the items (the raise).
    try:
        while True:
            TargetChar = TheList[0][CharIndex]
            for ItemIndex in arange(1, len(TheList)):
                if TargetChar != TheList[ItemIndex][CharIndex]:
                    raise Exception
            Accum += TargetChar
            CharIndex += 1
    except Exception:
        pass
    return Accum
##############################################################
# BEGIN: compFsSetup(LFrm, LEnt, DirVar, FileVar, VarSet = "")
# FUNC:compFsSetup():2010.255
#   Just sets up the bind's for the passed Entry field. See compFs() for the
#   DirVar and FileVar explainations.


def compFsSetup(LFrm, LEnt, DirVar, FileVar, VarSet):
    LEnt.bind("<FocusIn>", Command(compFsTabOff, LFrm))
    LEnt.bind("<FocusOut>", Command(compFsTabOn, LFrm))
    LEnt.bind("<Key-Tab>", Command(compFs, DirVar, FileVar, VarSet))
    return
#####################################
# BEGIN: compFsTabOff(LFrm, e = None)
# FUNC:compFsTabOff():2010.225


def compFsTabOff(LFrm, e=None):
    LFrm.bind("<Key-Tab>", compFsNullCall)
    return
###################################
# BEGIN: compFsTabOn(LFrm, e = None)
# FUNC:compFsTabOn():2010.225


def compFsTabOn(LFrm, e=None):
    LFrm.unbind("<Key-Tab>")
    return
#################################
# BEGIN: compFsNullCall(e = None)
# FUNC:compFsNullCall():2012.294


def compFsNullCall(e=None):
    return "break"
# END: compFs


#####################################
# BEGIN: deComma(In, Special = False)
# LIB:deComma():2006.192
#   Removes trailng spaces and commas. Special makes it look for commas before
#   a "^^^" special printing idiom like   ^r^blah,blah,^^^   at the end of In.
def deComma(In, Special=False):
    if Special is False:
        Len = len(In) - 1
        while Len >= 0:
            if In[Len] != "," and In[Len] != " ":
                break
            Len -= 1
        return In[:Len + 1]
    else:
        if In.endswith("^^^") is False:
            # Same as above.
            Len = len(In) - 1
            while Len >= 0:
                if In[Len] != "," and In[Len] != " ":
                    break
                Len -= 1
            return In[:Len + 1]
        else:
            Len = len(In) - 4
            while Len >= 0:
                if In[Len] != "," and In[Len] != " ":
                    break
                Len -= 1
            return In[:Len + 1] + "^^^"
# END: deComma


####################################
# BEGIN: diskSizeFormat(Which, Size)
# LIB:diskSizeFormat():2013.036
#   Which = b or e. See below.
def diskSizeFormat(Which, Size):
    # The user must do whatever it takes to pass bytes. Which determines what
    # will be returned: b=millions/1024K or e=mega/1000K bytes.
    # Binary 2^20, 1024K.
    if Which == "b":
        if Size >= 1099511627776:
            return "%.2fTiB" % (Size / 1099511627776.0)
        elif Size >= 1073741824:
            return "%.2fGiB" % (Size / 1073741824.0)
        elif Size >= 1048576:
            return "%.2fMiB" % (Size / 1048576.0)
        elif Size >= 1024:
            return "%.2fKiB" % (Size / 1024.0)
        else:
            return "%dB" % Size
    # Engineering 10^6, 1000K.
    elif Which == "e":
        if Size >= 1000000000000:
            return "%.2fTB" % (Size / 1000000000000.0)
        elif Size >= 1000000000:
            return "%.2fGB" % (Size / 1000000000.0)
        elif Size >= 1000000:
            return "%.2fMB" % (Size / 1000000.0)
        elif Size >= 1000:
            return "%.2fKB" % (Size / 1000.0)
        else:
            return "%dB" % Size
    return "Error"
# END: diskSizeFormat


###############################################################
# BEGIN: dt2Time(InFormat, OutFormat, DateTime, Verify = False)
# LIB:dt2Time():2018.270
#   InFormat = -1 = An Epoch has been passed.
#               0 = Figure out what was passed.
#           other = Use if the caller knows exactly what they have.
#   OutFormat = -1 = Epoch
#                0 = Y M D D H M S.s
#                1 = Uses OPTDateFormatRVar value.
#            other = whatever supported format the caller wants
#   The format of the time will always be HH:MM:SS.sss.
#   Returns (0/1, <answer or error msg>) if Verify is True, or <answer/0/"">
#   if Verify is False. Confusing, but most of the calls are with Verify set
#   to False, and having to always put [1] at the end of each call was getting
#   old fast. This probably will cause havoc if there are other errors like
#   passing date/times that cannot be deciphered, or passing bad In/OutFormat
#   codes (just "" will be returned), but that's the price of progress. You'll
#   still have to chop off the milliseconds if they are not wanted ([:-4]).
#   Needs option_add(), intt(), floatt(), rtnPattern()
def dt2Time(InFormat, OutFormat, DateTime, Verify=False):
    global Y2EPOCH
    if InFormat == -1:
        YYYY = 1970
        while True:
            if YYYY % 4 != 0:
                if DateTime >= 31536000:
                    DateTime -= 31536000
                else:
                    break
            elif YYYY % 100 != 0 or YYYY % 400 == 0:
                if DateTime >= 31622400:
                    DateTime -= 31622400
                else:
                    break
            else:
                if DateTime >= 31536000:
                    DateTime -= 31536000
                else:
                    break
            YYYY += 1
        DOY = 1
        while DateTime >= 86400:
            DateTime -= 86400
            DOY += 1
        HH = 0
        while DateTime >= 3600:
            DateTime -= 3600
            HH += 1
        MM = 0
        while DateTime >= 60:
            DateTime -= 60
            MM += 1
        SS = DateTime
        MMM, DD = dt2Timeydoy2md(YYYY, DOY)
    else:
        DateTime = DateTime.strip().upper()
        # The caller will have to decide if these returns are OK or not.
        if len(DateTime) == 0:
            if OutFormat - 1:
                if Verify is False:
                    return 0.0
                else:
                    return (0, 0.0)
            elif OutFormat == 0:
                if Verify is False:
                    return 0, 0, 0, 0, 0, 0, 0.0
                else:
                    return (0, 0, 0, 0, 0, 0, 0, 0.0)
            elif InFormat == 5:
                if Verify is False:
                    return "00:00:00:00"
                else:
                    return (0, "00:00:00:00")
            else:
                if Verify is False:
                    return ""
                else:
                    return (0, "")
        # The overall goal of the decode will be to get the passed string time
        # into YYYY, MMM, DD, DOY, HH, MM integers and SS.sss float values
        # then proceed to the encoding section ready for anything.
        # These will try and figure out what the date is between the usual
        # formats.
        # Checks first to see if the date and time are together (like with a :)
        # or if there is a space between them.
        if InFormat == 0:
            Parts = DateTime.split()
            if len(Parts) == 1:
                # YYYY:DOY:... - 71:2 will pass.
                if DateTime.find(":") != -1:
                    Parts = DateTime.split(":")
                    # There has to be something that looks like YYYY:DOY.
                    if len(Parts) >= 2:
                        InFormat = 11
                # YYYY-MM-DD:HH:...
                # If there was only one thing and there are dashes then it
                # could be Y-M-D or Y-M-D:H:M:S. Either way there must be 3
                # parts.
                elif DateTime.find("-") != -1:
                    Parts = DateTime.split("-")
                    if len(Parts) == 3:
                        InFormat = 21
                # YYYYMMMDD:HH:... - 68APR3 will pass.
                elif (DateTime.find("A") != -1 or DateTime.find("E") != -1 or
                      DateTime.find("O") != -1 or DateTime.find("U") != -1):
                    InFormat = 31
                # YYYYDOYHHMMSS - Date/time must be exactly like this.
                elif len(DateTime) == 13:
                    if rtnPattern(DateTime) == "0000000000000":
                        InFormat = 41
                # YYYYMMDDHHMMSS - Date/time must be exactly like this.
                elif len(DateTime) == 14:
                    if rtnPattern(DateTime) == "00000000000000":
                        InFormat = 51
            # (There is no 1974JAN23235959 that I know of, but the elif for
            # it would be here. ->
            # There were two parts.
            else:
                Date = Parts[0]
                Time = Parts[1]
                # YYYY:DOY HH:MM...
                if Date.find(":") != -1:
                    # Must have at least YYYY:DOY.
                    Parts = Date.split(":")
                    if len(Parts) >= 2:
                        InFormat = 12
                # May be YYYY-MM-DD HH:MM...
                elif Date.find("-") != -1:
                    Parts = Date.split("-")
                    if len(Parts) == 3:
                        InFormat = 22
                # YYYYMMMDD - 68APR3 will pass.
                elif (Date.find("A") != -1 or Date.find("E") != -1 or
                      Date.find("O") != -1 or Date.find("U") != -1):
                    InFormat = 32
            # If it is still 0 then something is wrong.
            if InFormat == 0:
                if Verify is False:
                    return ""
                else:
                    return (1, "RW", "Bad date/time(%d): '%s'" %
                            (InFormat, DateTime), 2, "")
        # These can be fed from the Format 0 stuff above, or called directly
        # if the caller knows what DateTime is.
        if InFormat < 20:
            # YYYY:DOY:HH:MM:SS.sss
            # Sometimes this comes as  YYYY:DOY:HH:MM:SS:sss. We'll look for
            # that here. (It's a Reftek thing.)
            if InFormat == 11:
                DT = DateTime.split(":")
                DT += (5 - len(DT)) * ["0"]
                if len(DT) == 6:
                    DT[4] = "%06.3f" % (intt(DT[4]) + intt(DT[5]) / 1000.0)
                    DT = DT[:-1]
            # YYYY:DOY HH:MM:SS.sss
            elif InFormat == 12:
                Parts = DateTime.split()
                Date = Parts[0].split(":")
                Date += (2 - len(Date)) * ["0"]
                Time = Parts[1].split(":")
                Time += (3 - len(Time)) * ["0"]
                DT = Date + Time
            else:
                if Verify is False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)." %
                            InFormat, 3, "")
            # Two-digit years shouldn't happen a lot, so this is kinda
            # inefficient.
            if DT[0] < "100":
                YYYY = intt(Date[0])
                if YYYY < 70:
                    YYYY += 2000
                else:
                    YYYY += 1900
                DT[0] = str(YYYY)
            # After we have all of the parts then do the check if the caller
            # wants.
            if Verify is True:
                Ret = dt2TimeVerify("ydhms", DT)
                if Ret[0] != 0:
                    return Ret
            # I'm using intt() and floatt() throughout just because it's safer
            # than the built-in functions.
            # This trick makes it so the Epoch for a year only has to be
            # calculated once during a program's run.
            YYYY = intt(DT[0])
            DOY = intt(DT[1])
            MMM, DD = dt2Timeydoy2md(YYYY, DOY)
            HH = intt(DT[2])
            MM = intt(DT[3])
            SS = floatt(DT[4])
        elif InFormat < 30:
            # YYYY-MM-DD:HH:MM:SS.sss
            if InFormat == 21:
                Parts = DateTime.split(":", 1)
                Date = Parts[0].split("-")
                Date += (3 - len(Date)) * ["0"]
                # Just the date must have been supplied.
                if len(Parts) == 1:
                    Time = ["0", "0", "0"]
                else:
                    Time = Parts[1].split(":")
                    Time += (3 - len(Time)) * ["0"]
                DT = Date + Time
            # YYYY-MM-DD HH:MM:SS.sss
            elif InFormat == 22:
                Parts = DateTime.split()
                Date = Parts[0].split("-")
                Date += (3 - len(Date)) * ["0"]
                Time = Parts[1].split(":")
                Time += (3 - len(Time)) * ["0"]
                DT = Date + Time
            # If parts of 23 are missing we will fill them in with Jan, 1st,
            # or 00:00:00.
            # If parts of 24 are missing we will format to the missing item
            # then stop and return what we have.
            elif InFormat == 23 or InFormat == 24:
                # The /DOY may or may not be there.
                if DateTime.find("/") == -1:
                    Parts = DateTime.split()
                    Date = Parts[0].split("-")
                    if InFormat == 23:
                        Date += (3 - len(Date)) * ["1"]
                    else:
                        # The -1's will stand in from the missing items.
                        # OutFormat=24 will figure it out from there.
                        Date += (3 - len(Date)) * ["-1"]
                    if len(Parts) == 2:
                        Time = Parts[1].split(":")
                        if InFormat == 23:
                            Time += (3 - len(Time)) * ["0"]
                        else:
                            Time += (3 - len(Time)) * ["-1"]
                    else:
                        if InFormat == 23:
                            Time = ["0", "0", "0"]
                        else:
                            Time = ["-1", "-1", "-1"]
                # Has a /. We'll only use the YYYY-MM-DD part and assume
                # nothing about the DOY part.
                else:
                    Parts = DateTime.split()
                    Date = Parts[0].split("-")
                    if InFormat == 23:
                        Date += (3 - len(Date)) * ["0"]
                    elif InFormat == 24:
                        Date += (3 - len(Date)) * ["-1"]
                    Date[2] = Date[2].split("/")[0]
                    if len(Parts) == 2:
                        Time = Parts[1].split(":")
                        if InFormat == 23:
                            Time += (3 - len(Time)) * ["0"]
                        else:
                            Time += (3 - len(Time)) * ["-1"]
                    else:
                        if InFormat == 23:
                            Time = ["0", "0", "0"]
                        else:
                            Time = ["1-", "-1", "-1"]
                DT = Date + Time
            else:
                if Verify is False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)." %
                            InFormat, 3, "")
            if DT[0] < "100":
                YYYY = intt(DT[0])
                if YYYY < 70:
                    YYYY += 2000
                else:
                    YYYY += 1900
                DT[0] = str(YYYY)
            if Verify is True:
                if InFormat != 24:
                    Ret = dt2TimeVerify("ymdhms", DT)
                else:
                    Ret = dt2TimeVerify("xymdhms", DT)
                if Ret[0] != 0:
                    return Ret
            YYYY = intt(DT[0])
            MMM = intt(DT[1])
            DD = intt(DT[2])
            # This will get done in OutFormat=24.
            if InFormat != 24:
                DOY = dt2Timeymd2doy(YYYY, MMM, DD)
            else:
                DOY = -1
            HH = intt(DT[3])
            MM = intt(DT[4])
            SS = floatt(DT[5])
        elif InFormat < 40:
            # YYYYMMMDD:HH:MM:SS.sss
            if InFormat == 31:
                Parts = DateTime.split(":", 1)
                Date = Parts[0]
                if len(Parts) == 1:
                    Time = ["0", "0", "0"]
                else:
                    Time = Parts[1].split(":")
                    Time += (3 - len(Time)) * ["0"]
            # YYYYMMMDD HH:MM:SS.sss
            elif InFormat == 32:
                Parts = DateTime.split()
                Date = Parts[0]
                Time = Parts[1].split(":")
                Time += (3 - len(Time)) * ["0"]
            else:
                if Verify is False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)." %
                            InFormat, 3, "")
            # Date is still "YYYYMMMDD", so just make place holders.
            DT = ["0", "0", "0"] + Time
            YYYY = intt(Date)
            if YYYY < 100:
                if YYYY < 70:
                    YYYY += 2000
                else:
                    YYYY += 1900
            MMM = 0
            DD = 0
            M = 1
            for Month in PROG_CALMONS[1:]:
                try:
                    i = Date.index(Month)
                    MMM = M
                    DD = intt(Date[i + 3:])
                    break
                except Exception:
                    pass
                M += 1
            if Verify is True:
                # DT values need to be strings for the dt2TimeVerify() intt()
                # call. It's assumed the values would come from split()'ing
                # something, so they would normally be strings to begin with.
                DT[0] = str(YYYY)
                DT[1] = str(MMM)
                DT[2] = str(DD)
                Ret = dt2TimeVerify("ymdhms", DT)
                if Ret[0] != 0:
                    return Ret
            DOY = dt2Timeymd2doy(YYYY, MMM, DD)
            HH = intt(Time[0])
            MM = intt(Time[1])
            SS = intt(Time[2])
        elif InFormat < 50:
            # YYYYDOYHHMMSS
            if InFormat == 41:
                if DateTime.isdigit() is False:
                    if Verify is False:
                        return ""
                    else:
                        return (1, "RW", "Non-digits in value.", 2)
                YYYY = intt(DateTime[:4])
                DOY = intt(DateTime[4:7])
                HH = intt(DateTime[7:9])
                MM = intt(DateTime[9:11])
                SS = floatt(DateTime[11:])
                if Verify is True:
                    DT = []
                    DT.append(str(YYYY))
                    DT.append(str(DOY))
                    DT.append(str(HH))
                    DT.append(str(MM))
                    DT.append(str(SS))
                    Ret = dt2TimeVerify("ydhms", DT)
                    if Ret[0] != 0:
                        return Ret
                MMM, DD = dt2Timeydoy2md(YYYY, DOY)
            else:
                if Verify is False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)." %
                            InFormat, 3, "")
        elif InFormat < 60:
            # YYYYMMDDHHMMSS
            if InFormat == 51:
                if DateTime.isdigit() is False:
                    if Verify is False:
                        return ""
                    else:
                        return (1, "RW", "Non-digits in value.", 2)
                YYYY = intt(DateTime[:4])
                MMM = intt(DateTime[4:6])
                DD = intt(DateTime[6:8])
                HH = intt(DateTime[8:10])
                MM = intt(DateTime[10:12])
                SS = floatt(DateTime[12:])
                if Verify is True:
                    DT = []
                    DT.append(str(YYYY))
                    DT.append(str(MMM))
                    DT.append(str(DD))
                    DT.append(str(HH))
                    DT.append(str(MM))
                    DT.append(str(SS))
                    Ret = dt2TimeVerify("ymdhms", DT)
                    if Ret[0] != 0:
                        return Ret
                DOY = dt2Timeymd2doy(YYYY, MMM, DD)
            else:
                if Verify is False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)." %
                            InFormat, 3, "")
        # If the caller just wants to work with the seconds we'll split it up
        # and return the number of seconds into the day. In this case the
        # OutFormat value will not be used.
        elif InFormat == 100:
            Parts = DateTime.split(":")
            Parts += (3 - len(Parts)) * ["0"]
            if Verify is True:
                Ret = dt2TimeVerify("hms", Parts)
                if Ret[0] != 0:
                    return Ret
            if Verify is False:
                return (intt(Parts[0]) * 3600) + \
                       (intt(Parts[1]) * 60) + float(Parts[2])
            else:
                return (0, (intt(Parts[0]) * 3600) + (intt(Parts[1]) * 60) +
                        float(Parts[2]))
    # Now that we have all of the parts do what the caller wants and return the
    # result.
    # Return the Epoch.
    if OutFormat == -1:
        try:
            Epoch = Y2EPOCH[YYYY]
        except KeyError:
            Epoch = 0.0
            for YYY in arange(1970, YYYY):
                if YYY % 4 != 0:
                    Epoch += 31536000.0
                elif YYY % 100 != 0 or YYY % 400 == 0:
                    Epoch += 31622400.0
                else:
                    Epoch += 31536000.0
            Y2EPOCH[YYYY] = Epoch
        Epoch += ((DOY - 1) * 86400.0) + (HH * 3600.0) + (MM * 60.0) + SS
        if Verify is False:
            return Epoch
        else:
            return (0, Epoch)
    elif OutFormat == 0:
        if Verify is False:
            return YYYY, MMM, DD, DOY, HH, MM, SS
        else:
            return (0, YYYY, MMM, DD, DOY, HH, MM, SS)
    elif OutFormat == 1:
        Format = OPTDateFormatRVar.get()
        if Format == "YYYY:DOY":
            OutFormat = 11
        elif Format == "YYYY-MM-DD":
            OutFormat = 22
        elif Format == "YYYYMMMDD":
            OutFormat = 32
        # Usually used for troubleshooting.
        elif len(Format) == 0:
            try:
                Epoch = Y2EPOCH[YYYY]
            except KeyError:
                for YYY in arange(1970, YYYY):
                    if YYY % 4 != 0:
                        Epoch += 31536000.0
                    elif YYY % 100 != 0 or YYY % 400 == 0:
                        Epoch += 31622400.0
                    else:
                        Epoch += 31536000.0
                Y2EPOCH[YYYY] = Epoch
            Epoch += ((DOY - 1) * 86400.0) + (HH * 3600.0) + (MM * 60.0) + SS
            if Verify is False:
                return Epoch
            else:
                return (0, Epoch)
    # This is the easiest way I can think of to keep an SS of 59.9999 from
    # being rounded and formatted to 60.000. Some stuff at some point may slip
    # into the microsecond realm. Then this whole library of functions may have
    # to be changed.
    if SS % 1 > .999:
        SS = int(SS) + .999
    # These OutFormat values are the same as InFormat values, plus others.
    # YYYY:DOY:HH:MM:SS.sss
    if OutFormat == 11:
        if Verify is False:
            return "%d:%03d:%02d:%02d:%06.3f" % (YYYY, DOY, HH, MM, SS)
        else:
            return (0, "%d:%03d:%02d:%02d:%06.3f" % (YYYY, DOY, HH, MM, SS))
    # YYYY:DOY HH:MM:SS.sss
    elif OutFormat == 12:
        if Verify is False:
            return "%d:%03d %02d:%02d:%06.3f" % (YYYY, DOY, HH, MM, SS)
        else:
            return (0, "%d:%03d %02d:%02d:%06.3f" % (YYYY, DOY, HH, MM, SS))
    # YYYY:DOY - just because it's popular (for LOGPEEK) and it saves having
    # the caller always doing  .split()[0]
    elif OutFormat == 13:
        if Verify is False:
            return "%d:%03d" % (YYYY, DOY)
        else:
            return (0, "%d:%03d" % (YYYY, DOY))
    # YYYY:DOY:HH:MM:SS - just because it's really popular in POCUS and other
    # programs.
    elif OutFormat == 14:
        if Verify is False:
            return "%d:%03d:%02d:%02d:%02d" % (YYYY, DOY, HH, MM, int(SS))
        else:
            return (0, "%d:%03d:%02d:%02d:%02d" % (YYYY, DOY, HH, MM, int(SS)))
    # YYYY-MM-DD:HH:MM:SS.sss
    elif OutFormat == 21:
        if Verify is False:
            return "%d-%02d-%02d:%02d:%02d:%06.3f" % (YYYY, MMM, DD, HH, MM,
                                                      SS)
        else:
            return (0, "%d-%02d-%02d:%02d:%02d:%06.3f" % (YYYY, MMM, DD, HH,
                                                          MM, SS))
    # YYYY-MM-DD HH:MM:SS.sss
    elif OutFormat == 22:
        if Verify is False:
            return "%d-%02d-%02d %02d:%02d:%06.3f" % (YYYY, MMM, DD, HH, MM,
                                                      SS)
        else:
            return (0, "%d-%02d-%02d %02d:%02d:%06.3f" % (YYYY, MMM, DD, HH,
                                                          MM, SS))
    # YYYY-MM-DD/DOY HH:MM:SS.sss
    elif OutFormat == 23:
        if Verify is False:
            return "%d-%02d-%02d/%03d %02d:%02d:%06.3f" % (YYYY, MMM, DD, DOY,
                                                           HH, MM, SS)
        else:
            return (0, "%d-%02d-%02d/%03d %02d:%02d:%06.3f" % (YYYY, MMM, DD,
                                                               DOY, HH, MM,
                                                               SS))
    # Some portion of YYYY-MM-DD HH:MM:SS. Returns integer seconds.
    # In that this is a human-entered thing (programs don't store partial
    # date/times) we'll return whatever was entered without the /DOY, since the
    # next step would be to do something like look it up in a database.
    elif OutFormat == 24:
        DateTime = "%d" % YYYY
        if MMM != -1:
            DateTime += "-%02d" % MMM
        else:
            # Return what we have if this item was not provided, same on down.
            if Verify is False:
                return DateTime
            else:
                return (0, DateTime)
        if DD != -1:
            DateTime += "-%02d" % DD
        else:
            if Verify is False:
                return DateTime
            else:
                return (0, DateTime)
        if HH != -1:
            DateTime += " %02d" % HH
        else:
            if Verify is False:
                return DateTime
            else:
                return (0, DateTime)
        if MM != "-1":
            DateTime += ":%02d" % MM
        else:
            if Verify is False:
                return DateTime
            else:
                return (0, DateTime)
        # Returns integer second since the caller has no idea what is coming
        # back.
        if SS != "-1":
            DateTime += ":%02d" % SS
        if Verify is False:
            return DateTime
        else:
            return (0, DateTime)
    # YYYY-MM-DD
    elif OutFormat == 25:
        if Verify is False:
            return "%d-%02d-%02d" % (YYYY, MMM, DD)
        else:
            return (0, "%d-%02d-%02d" % (YYYY, MMM, DD))
    # YYYYMMMDD:HH:MM:SS.sss
    elif OutFormat == 31:
        if Verify is False:
            return "%d%s%02d:%02d:%02d:%06.3f" % (YYYY, PROG_CALMONS[MMM], DD,
                                                  HH, MM, SS)
        else:
            return (0, "%d%s%02d:%02d:%02d:%06.3f" % (YYYY, PROG_CALMONS[MMM],
                                                      DD, HH, MM, SS))
    # YYYYMMMDD HH:MM:SS.sss
    elif OutFormat == 32:
        if Verify is False:
            return "%d%s%02d %02d:%02d:%06.3f" % (YYYY, PROG_CALMONS[MMM], DD,
                                                  HH, MM, SS)
        else:
            return (0, "%d%s%02d %02d:%02d:%06.3f" % (YYYY, PROG_CALMONS[MMM],
                                                      DD, HH, MM, SS))
    # YYYYDOYHHMMSS.sss
    elif OutFormat == 41:
        if Verify is False:
            return "%d%03d%02d%02d%06.3f" % (YYYY, DOY, HH, MM, SS)
        else:
            return (0, "%d%03d%02d%02d%06.3f" % (YYYY, DOY, HH, MM, SS))
    # YYYYMMDDHHMMSS.sss
    elif OutFormat == 51:
        if Verify is False:
            return "%d%02d%02d%02d%02d%06.3f" % (YYYY, MMM, DD, HH, MM, SS)
        else:
            return (0, "%d%02d%02d%02d%02d%06.3f" % (YYYY, MMM, DD, HH, MM,
                                                     SS))
    # Returns what ever OPTDateFormatRVar is set to.
    # 80 is dt, 81 is d and 82 is t.
    elif OutFormat == 80 or OutFormat == 81 or OutFormat == 82:
        if OPTDateFormatRVar.get() == "YYYY:DOY":
            if OutFormat == 80:
                if Verify is False:
                    return "%d:%03d:%02d:%02d:%06.3f" % (YYYY, DOY, HH, MM, SS)
                else:
                    return (0, "%d:%03d:%02d:%02d:%06.3f" % (YYYY, DOY, HH, MM,
                                                             SS))
            elif OutFormat == 81:
                if Verify is False:
                    return "%d:%03d" % (YYYY, DOY)
                else:
                    return (0, "%d:%03d" % (YYYY, DOY))
            elif OutFormat == 82:
                if Verify is False:
                    return "%02d:%02d:%06.3f" % (HH, MM, SS)
                else:
                    return (0, "%02d:%02d:%06.3f" % (HH, MM, SS))
        elif OPTDateFormatRVar.get() == "YYYY-MM-DD":
            if OutFormat == 80:
                if Verify is False:
                    return "%d-%02d-%02d %02d:%02d:%06.3f" % (YYYY, MMM, DD,
                                                              HH, MM, SS)
                else:
                    return (0, "%d-%02d-%02d %02d:%02d:%06.3f" % (YYYY, MMM,
                                                                  DD, HH, MM,
                                                                  SS))
            elif OutFormat == 81:
                if Verify is False:
                    return "%d-%02d-%02d" % (YYYY, MMM, DD)
                else:
                    return (0, "%d-%02d-%02d" % (YYYY, MMM, DD))
            elif OutFormat == 82:
                if Verify is False:
                    return "%02d:%02d:%06.3f" % (HH, MM, SS)
                else:
                    return (0, "%02d:%02d:%06.3f" % (HH, MM, SS))
        elif OPTDateFormatRVar.get() == "YYYYMMMDD":
            if OutFormat == 80:
                if Verify is False:
                    return "%d%s%02d %02d:%02d:%06.3f" % (YYYY,
                                                          PROG_CALMONS[MMM],
                                                          DD, HH, MM, SS)
                else:
                    return (0, "%d%s%02d %02d:%02d:%06.3f" %
                            (YYYY, PROG_CALMONS[MMM], DD, HH, MM, SS))
            elif OutFormat == 81:
                if Verify is False:
                    return "%d%s%02d" % (YYYY, PROG_CALMONS[MMM], DD)
                else:
                    return (0, "%d%s%02d" % (YYYY, PROG_CALMONS[MMM], DD))
            elif OutFormat == 82:
                if Verify is False:
                    return "%02d:%02d:%06.3f" % (HH, MM, SS)
                else:
                    return (0, "%02d:%02d:%06.3f" % (HH, MM, SS))
        elif len(OPTDateFormatRVar.get()) == 0:
            if Verify is False:
                return str(DateTime)
            else:
                return (0, str(DateTime))
    else:
        if Verify is False:
            return ""
        else:
            return (1, "MWX", "dt2Time: Unknown OutFormat code (%d)." %
                    OutFormat, 3, "")
################################
# BEGIN: dt2Timedhms2Secs(InStr)
# FUNC:dt2Timedhms2Secs():2018.235
#   Returns the number of seconds in strings like 1h30m.


def dt2Timedhms2Secs(InStr):
    InStr = InStr.replace(" ", "").lower()
    if len(InStr) == 0:
        return 0
    Chars = list(InStr)
    Value = 0
    SubValue = ""
    for Char in Chars:
        if Char.isdigit():
            SubValue += Char
        elif Char == "s":
            Value += intt(SubValue)
            SubValue = ""
        elif Char == "m":
            Value += intt(SubValue) * 60
            SubValue = ""
        elif Char == "h":
            Value += intt(SubValue) * 3600
            SubValue = ""
        elif Char == "d":
            Value += intt(SubValue) * 86400
            SubValue = ""
    # Must have just been passed a number with no s m h or d or 1h30 which will
    # be treated as 1 hour, 30 seconds.
    if len(SubValue) != 0:
        Value += intt(SubValue)
    return Value
#############################################
# BEGIN: dt2TimeDT(Format, DateTime, Fix = 2)
# FUNC:dt2TimeDT():2018.270
#   This is for the one-off time conversion items that have been needed here
#   and there for specific items. Input is some string of date and/or time.
#   Some of these can be done with dt2Time(), but these are more for when the
#   code knows exactly what it has and exactly what it needs. It's program
#   dependent and maybe useful to others.
#     Format = 1 = [dd]hhmmss or even blank to [DD:]HH:MM:SS
#                  Replaces dhms2DHMS().
#              2 = YYYY:DOY:HH:MM:SS to ISO8601 time YYYY-MM-DDTHH:MM:SSZ.
#                  No time zone conversion is done (or any error checking) so
#                  you need to make sure the passed time is really Zulu or this
#                  will return a lie.
#                  Replaces YDHMS28601().
#              3 = The opposite of above.
#                  Replaces iso86012YDHMS().
#              4 = Adds /DOY to a passed date/time that can be just the date,
#                  but that must be a YYYY-MM-DD format. Date and time must be
#                  separated by a space. It's the only date format that I've
#                  ever wanted to add the DOY to. Returns the fixed up
#                  date/time. Does NO error checking, and, as you can see, if
#                  the date is not the right format it will choke causing an
#                  embarrassing crash. You have been warned.
#                  Replaces addDDD().
#              5 = Anti-InFormat 4. Is OK if /DOY is not there.
#                  Replaces remDDD().
#              6 = yyydoyhhmmss... to YYYY:DOY:HH:MM...
#                  Replaces ydhmst2YDHMST().
#              7 = YYYY:DOY:HH:MM:SS -> YYYY-MM-DD:HH:MM:SS.
#                  Replaces ydhms2ymdhmsDash().
#              8 = YYYYMMDDHHMMSS to YYYY-MM-DD HH:MM:SS
#              9 = YYYYMMDDHHMMSS to YYYY-MM-DD
#             10 = HH:MM:SS to int H, M, S
#             11 = Seconds to HH:MM:SS (Use Fix to pad with extra blanks if
#                  large hour numbers are expected)
#             12 = HH:MM:SS/HH:MM/HH/blank to seconds


def dt2TimeDT(Format, DateTime, Fix=2):
    # Everything wants a string, except 11. Python 3 will probably pass
    # DateTime as bytes.
    if Format != 11:
        if isinstance(DateTime, astring) is False:
            DateTime = DateTime.decode("latin-1")
    if Format == 1:
        # Check the length before the .strip(). DateTime may be just blanks,
        # but we'll use the length to determine what to return.
        Len = len(DateTime)
        DateTime = DateTime.strip()
        if len(DateTime) == 0:
            return "00:00:00:00"
        if Len == 6:
            return "%s:%s:%s" % (DateTime[:2], DateTime[2:4], DateTime[4:6])
        else:
            return "%s:%s:%s:%s" % (DateTime[:2], DateTime[2:4], DateTime[4:6],
                                    DateTime[6:8])
    elif Format == 2:
        Parts = DateTime.split(":")
        if len(Parts) != 5:
            return ""
        MM, DD, = dt2Timeydoy2md(intt(Parts[0]), intt(Parts[1]))
        return "%s-%02d-%02dT%s:%s:%sZ" % (Parts[0], MM, DD, Parts[2],
                                           Parts[3], Parts[4])
    elif Format == 3:
        Parts = DateTime.split("T")
        if len(Parts) != 2:
            return ""
        # Who knows what may be wrong with the passed time.
        try:
            Parts2 = Parts[0].split("-")
            YYYY = intt(Parts2[0])
            MMM = intt(Parts2[1])
            DD = intt(Parts2[2])
            DOY = dt2Timeymd2doy(YYYY, MMM, DD)
            Parts2 = Parts[1].split(":")
            HH = intt(Parts2[0])
            MM = intt(Parts2[1])
            SS = intt(Parts2[2])
        except Exception:
            return ""
        return "%d:%03d:%02d:%02d:%02d" % (YYYY, DOY, HH, MM, SS)
    elif Format == 4:
        Parts = DateTime.split()
        if len(Parts) == 0:
            return ""
        Parts2 = Parts[0].split("-")
        YYYY = intt(Parts2[0])
        MMM = intt(Parts2[1])
        DD = intt(Parts2[2])
        DOY = dt2Timeymd2doy(YYYY, MMM, DD)
        if len(Parts) == 1:
            return "%s/%03d" % (Parts[0], DOY)
        else:
            return "%s/%03d %s" % (Parts[0], DOY, Parts[1])
    elif Format == 5:
        Parts = DateTime.split()
        if len(Parts) == 0:
            return ""
        # A little bit of protection.
        try:
            i = Parts[0].index("/")
            Parts[0] = Parts[0][:i]
        except ValueError:
            pass
        if len(Parts) == 1:
            return Parts[0]
        else:
            return "%s %s" % (Parts[0], Parts[1])
    # yyyydoyhhmmssttt or yyyydoyhhmmss.
    elif Format == 6:
        Len = len(DateTime)
        DateTime = DateTime.strip()
        # yyyydoyhhmm
        if Len == 12:
            if len(DateTime) == 0:
                return "0000:000:00:00"
            return "%s:%s:%s:%s" % (DateTime[:4], DateTime[4:7], DateTime[7:9],
                                    DateTime[9:11])
        # yyyydoyhhmmss  All of the fields that may contain this are 14 bytes
        # long with a trailing space which got removed above (it was that way
        # in LOGPEEK anyways).
        elif Len == 14:
            if len(DateTime) == 0:
                return "0000:000:00:00:00"
            return "%s:%s:%s:%s:%s" % (DateTime[:4], DateTime[4:7],
                                       DateTime[7:9], DateTime[9:11],
                                       DateTime[11:13])
        # yyyydoyhhmmssttt
        elif Len == 16:
            if len(DateTime) == 0:
                return "0000:000:00:00:00:000"
            return "%s:%s:%s:%s:%s:%s" % (DateTime[:4], DateTime[4:7],
                                          DateTime[7:9], DateTime[9:11],
                                          DateTime[11:13], DateTime[13:])
        return ""
    elif Format == 7:
        Parts = DateTime.split(":")
        YYYY = intt(Parts[0])
        DOY = intt(Parts[1])
        MMM, DD = dt2Timeydoy2md(YYYY, DOY)
        return "%d-%02d-%02d:%s:%s:%s" % (YYYY, MMM, DD, Parts[2], Parts[3],
                                          Parts[4])
    # YYYYMMDDHHMMSS to YYYY-MM-DD HH:MM:SS
    elif Format == 8:
        if len(DateTime) == 14:
            return "%s-%s-%s %s:%s:%s" % (DateTime[0:4], DateTime[4:6],
                                          DateTime[6:8], DateTime[8:10],
                                          DateTime[10:12], DateTime[12:14])
        return ""
    # YYYYMMDDHHMMSS to YYYY-MM-DD
    elif Format == 9:
        if len(DateTime) == 14:
            return "%s-%s-%s" % (DateTime[0:4], DateTime[4:6], DateTime[6:8])
        return ""
    # HH:MM:SS to int H, M, S
    elif Format == 10:
        try:
            Parts2 = DateTime.split(":")
            Parts = []
            for Part in Parts2:
                Parts.append(intt(Part))
            Parts += (3 - len(Parts)) * [0]
        except Exception:
            return 0, 0, 0
        return Parts[0], Parts[1], Parts[2]
    # Seconds to HH:MM:SS (use Fix to pad with spaces)
    elif Format == 11:
        HH = DateTime // 3600
        Sub = HH * 3600
        MM = (DateTime - Sub) // 60
        Sub += MM * 60
        SS = (DateTime - Sub)
        H = "%02d" % HH
        if Fix > 2:
            H = "%*s" % (Fix, H)
        return "%s:%02d:%02d" % (H, MM, SS)
    # HH:MM:SS to seconds
    elif Format == 12:
        try:
            Parts2 = DateTime.split(":")
            Parts = []
            for Part in Parts2:
                Parts.append(intt(Part))
            Parts += (3 - len(Parts)) * [0]
        except Exception:
            return 0
        return Parts[0] * 3600 + Parts[1] * 60 + Parts[2]
######################################################################
# BEGIN: dt2TimeMath(DeltaDD, DeltaSS, YYYY, MMM, DD, DOY, HH, MM, SS)
# LIB:dt2TimeMath():2013.039
#   Adds or subtracts (depending on the sign of DeltaDD/DeltaSS) the requested
#   amount of time to/from the passed date.  Returns a tuple with the results:
#           (YYYY, MMM, DD, DOY, HH, MM, SS)
#   Pass -1 or 0 for MMM/DD, or DOY depending on which is to be used. If MMM/DD
#   are >=0 then MMM, DD, and DOY will be filled in on return. If MMM/DD is -1
#   then just DOY will be used/returned. If MMM/DD are 0 then DOY will be used
#   but MMM and DD will also be returned.
#   SS will be int or float depending on what was passed.


def dt2TimeMath(DeltaDD, DeltaSS, YYYY, MMM, DD, DOY, HH, MM, SS):
    DeltaDD = int(DeltaDD)
    DeltaSS = int(DeltaSS)
    if YYYY < 1000:
        if YYYY < 70:
            YYYY += 2000
        else:
            YYYY += 1900
    # Work in DOY.
    if MMM > 0:
        DOY = dt2Timeymd2doy(YYYY, MMM, DD)
    if DeltaDD != 0:
        if DeltaDD > 0:
            Forward = 1
        else:
            Forward = 0
        while True:
            # Speed limit the change to keep things simpler.
            if DeltaDD < -365 or DeltaDD > 365:
                if Forward == 1:
                    DOY += 365
                    DeltaDD -= 365
                else:
                    DOY -= 365
                    DeltaDD += 365
            else:
                DOY += DeltaDD
                DeltaDD = 0
            if YYYY % 4 != 0:
                Leap = 0
            elif YYYY % 100 != 0 or YYYY % 400 == 0:
                Leap = 1
            else:
                Leap = 0
            if DOY < 1 or DOY > 365 + Leap:
                if Forward == 1:
                    DOY -= 365 + Leap
                    YYYY += 1
                else:
                    YYYY -= 1
                    if YYYY % 4 != 0:
                        Leap = 0
                    elif YYYY % 100 != 0 or YYYY % 400 == 0:
                        Leap = 1
                    else:
                        Leap = 0
                    DOY += 365 + Leap
            if DeltaDD == 0:
                break
    if DeltaSS != 0:
        if DeltaSS > 0:
            Forward = 1
        else:
            Forward = 0
        while True:
            # Again, speed limit just to keep the code reasonable.
            if DeltaSS < -59 or DeltaSS > 59:
                if Forward == 1:
                    SS += 59
                    DeltaSS -= 59
                else:
                    SS -= 59
                    DeltaSS += 59
            else:
                SS += DeltaSS
                DeltaSS = 0
            if SS < 0 or SS > 59:
                if Forward == 1:
                    SS -= 60
                    MM += 1
                    if MM > 59:
                        MM = 0
                        HH += 1
                        if HH > 23:
                            HH = 0
                            DOY += 1
                            if DOY > 365:
                                if YYYY % 4 != 0:
                                    Leap = 0
                                elif YYYY % 100 != 0 or YYYY % 400 == 0:
                                    Leap = 1
                                else:
                                    Leap = 0
                                if DOY > 365 + Leap:
                                    YYYY += 1
                                    DOY = 1
                else:
                    SS += 60
                    MM -= 1
                    if MM < 0:
                        MM = 59
                        HH -= 1
                        if HH < 0:
                            HH = 23
                            DOY -= 1
                            if DOY < 1:
                                YYYY -= 1
                                if YYYY % 4 != 0:
                                    DOY = 365
                                elif YYYY % 100 != 0 or YYYY % 400 == 0:
                                    DOY = 366
                                else:
                                    DOY = 365
            if DeltaSS == 0:
                break
    if MMM != -1:
        MMM, DD = dt2Timeydoy2md(YYYY, DOY)
    return (YYYY, MMM, DD, DOY, HH, MM, SS)
####################################
# BEGIN: dt2TimeVerify(Which, Parts)
# FUNC:dt2TimeVerify():2015.048
#   This could figure out what to check just by passing [Y,M,D,D,H,M,S], but
#   that means the splitters() in dt2Time would need to work much harder to
#   make sure all of the Parts were there, thus it needs a Which value.


def dt2TimeVerify(Which, Parts):
    if Which.startswith("ydhms"):
        YYYY = intt(Parts[0])
        MMM = -1
        DD = -1
        DOY = intt(Parts[1])
        HH = intt(Parts[2])
        MM = intt(Parts[3])
        SS = floatt(Parts[4])
    elif Which.startswith("ymdhms") or Which.startswith("xymdhms"):
        YYYY = intt(Parts[0])
        MMM = intt(Parts[1])
        DD = intt(Parts[2])
        DOY = -1
        HH = intt(Parts[3])
        MM = intt(Parts[4])
        SS = floatt(Parts[5])
    elif Which.startswith("hms"):
        YYYY = -1
        MMM = -1
        DD = -1
        DOY = -1
        HH = intt(Parts[0])
        MM = intt(Parts[1])
        SS = floatt(Parts[2])
    if YYYY != -1 and (YYYY < 1000 or YYYY > 9999):
        return (1, "RW", "Bad year value: %d" % YYYY, 2, "")
    # Normally do the normal checking.
    if Which.startswith("x") is False:
        if MMM != -1 and (MMM < 1 or MMM > 12):
            return (1, "RW", "Bad month value: %d" % MMM, 2, "")
        if DD != -1:
            if DD < 1 or DD > 31:
                return (1, "RW", "Bad day value: %d" % DD, 2, "")
            if YYYY % 4 != 0:
                if DD > PROG_MAXDPMNLY[MMM]:
                    return (1, "RW", "Too many days for month %d: %d" %
                            (MMM, DD), 2, "")
            elif YYYY % 100 != 0 or YYYY % 400 == 0:
                if DD > PROG_MAXDPMLY[MMM]:
                    return (1, "RW", "Too many days for month %d: %d" %
                            (MMM, DD), 2, "")
            else:
                if DD > PROG_MAXDPMNLY[MMM]:
                    return (1, "RW", "Too many days for month %d: %d" %
                            (MMM, DD), 2, "")
        if DOY != -1:
            if DOY < 1 or DOY > 366:
                return (1, "RW", "Bad day of year value: %d" % DOY, 2, "")
            if YYYY % 4 != 0 and DOY > 365:
                return (1, "RW" "Too many days for non-leap year: %d" % DOY,
                        2, "")
        if HH < 0 or HH >= 24:
            return (1, "RW", "Bad hour value: %d" % HH, 2, "")
        if MM < 0 or MM >= 60:
            return (1, "RW", "Bad minute value: %d" % MM, 2, "")
        if SS < 0 or SS >= 60:
            return (1, "RW", "Bad seconds value: %06.3f" % SS, 2, "")
    # "x" checks.
    # Here if Which starts with "x" then it is OK if month and day values are
    # missing. This is for checking an entry like  2013-7  for example. If the
    # portion of the date(/time) that was passed looks OK then (0,) will be
    # returned. If it is something like  2013-13  then that will be bad.
    else:
        # If the user entered just the year, then we are done, etc.
        if MMM != -1:
            if MMM < 1 or MMM > 12:
                return (1, "RW", "Bad month value: %d" % MMM, 2, "")
            if DD != -1:
                if DD < 1 or DD > 31:
                    return (1, "RW", "Bad day value: %d" % DD, 2, "")
                if YYYY % 4 != 0:
                    if DD > PROG_MAXDPMNLY[MMM]:
                        return (1, "RW", "Too many days for month %d: %d" %
                                (MMM, DD), 2, "")
                elif YYYY % 100 != 0 or YYYY % 400 == 0:
                    if DD > PROG_MAXDPMLY[MMM]:
                        return (1, "RW", "Too many days for month %d: %d" %
                                (MMM, DD), 2, "")
                else:
                    if DD > PROG_MAXDPMNLY[MMM]:
                        return (1, "RW", "Too many days for month %d: %d" %
                                (MMM, DD), 2, "")
                if DOY != -1:
                    if DOY < 1 or DOY > 366:
                        return (1, "RW", "Bad day of year value: %d" % DOY, 2,
                                "")
                    if YYYY % 4 != 0 and DOY > 365:
                        return (1, "RW"
                                "Too many days for non-leap year: %d" % DOY,
                                2, "")
                if HH != -1:
                    if HH < 0 or HH >= 24:
                        return (1, "RW", "Bad hour value: %d" % HH, 2, "")
                    if MM != -1:
                        if MM < 0 or MM >= 60:
                            return (1, "RW", "Bad minute value: %d" % MM, 2,
                                    "")
                        # Checking these special values are usually from user
                        # input and are date/time values and not timing values,
                        # so the seconds part here will just be an int.
                        if SS != -1:
                            if SS < 0 or SS >= 60:
                                return (1, "RW", "Bad seconds value: %d" % SS,
                                        2, "")
    return (0,)
##########################################
# BEGIN: dt2TimeydoyMath(Delta, YYYY, DOY)
# LIB:dt2TimeydoyMath():2013.036
#   Adds or subtracts the passed number of days (Delta) to the passed year and
#   day of year values. Returns YYYY and DOY.
#   Replaces ydoyMath().


def dt2TimeydoyMath(Delta, YYYY, DOY):
    if Delta == 0:
        return YYYY, DOY
    if Delta > 0:
        Forward = 1
    elif Delta < 0:
        Forward = 0
    while True:
        # Speed limit the change to keep things simpler.
        if Delta < -365 or Delta > 365:
            if Forward == 1:
                DOY += 365
                Delta -= 365
            else:
                DOY -= 365
                Delta += 365
        else:
            DOY += Delta
            Delta = 0
        # This was isLeap(), but it's such a simple thing I'm not sure it was
        # worth the function call, so it became this in all library functions.
        if YYYY % 4 != 0:
            Leap = 0
        elif YYYY % 100 != 0 or YYYY % 400 == 0:
            Leap = 1
        else:
            Leap = 0
        if DOY < 1 or DOY > 365 + Leap:
            if Forward == 1:
                DOY -= 365 + Leap
                YYYY += 1
            else:
                YYYY -= 1
                if YYYY % 4 != 0:
                    Leap = 0
                elif YYYY % 100 != 0 or YYYY % 400 == 0:
                    Leap = 1
                else:
                    Leap = 0
                DOY += 365 + Leap
            break
        if Delta == 0:
            break
    return YYYY, DOY
##################################
# BEGIN: dt2Timeydoy2md(YYYY, DOY)
# FUNC:dt2Timeydoy2md():2013.030
#   Does no values checking, so make sure you stuff is in one sock before
#   coming here.


def dt2Timeydoy2md(YYYY, DOY):
    if DOY < 32:
        return 1, DOY
    elif DOY < 60:
        return 2, DOY - 31
    if YYYY % 4 != 0:
        Leap = 0
    elif YYYY % 100 != 0 or YYYY % 400 == 0:
        Leap = 1
    else:
        Leap = 0
    # Check for this special day.
    if Leap == 1 and DOY == 60:
        return 2, 29
    # The PROG_FDOM values for Mar-Dec are set up for non-leap years. If it is
    # a leap year and the date is going to be Mar-Dec (it is if we have made it
    # this far), subtract Leap from the day.
    DOY -= Leap
    # We start through PROG_FDOM looking for dates in March.
    Month = 3
    for FDOM in PROG_FDOM[4:]:
        # See if the DOY is less than the first day of next month.
        if DOY <= FDOM:
            # Subtract the DOY for the month that we are in.
            return Month, DOY - PROG_FDOM[Month]
        Month += 1
    # If anything goes wrong...
    return 0, 0
######################################
# BEGIN: dt2Timeymd2doy(YYYY, MMM, DD)
# FUNC:dt2Timeymd2doy():2013.030


def dt2Timeymd2doy(YYYY, MMM, DD):
    if YYYY % 4 != 0:
        return (PROG_FDOM[MMM] + DD)
    elif (YYYY % 100 != 0 or YYYY % 400 == 0) and MMM > 2:
        return (PROG_FDOM[MMM] + DD + 1)
    else:
        return (PROG_FDOM[MMM] + DD)
##################################################################
# BEGIN: dt2Timeymddhms(OutFormat, YYYY, MMM, DD, DOY, HH, MM, SS)
# FUNC:dt2Timeymddhms():2018.235
#   The general-purpose time handler for when the time is already split up into
#   it's parts.
#   Make MMM, DD -1 if passing DOY and DOY -1 if passing MMM, DD.


def dt2Timeymddhms(OutFormat, YYYY, MMM, DD, DOY, HH, MM, SS):
    global Y2EPOCH
    # Returns a float Epoch is SS is a float, otherwise an int value.
    if OutFormat == -1:
        Epoch = 0
        if YYYY < 70:
            YYYY += 2000
        if YYYY < 100:
            YYYY += 1900
        try:
            Epoch = Y2EPOCH[YYYY]
        except KeyError:
            for YYY in arange(1970, YYYY):
                if YYY % 4 != 0:
                    Epoch += 31536000
                elif YYY % 100 != 0 or YYY % 400 == 0:
                    Epoch += 31622400
                else:
                    Epoch += 31536000
            Y2EPOCH[YYYY] = Epoch
        if DOY == -1:
            DOY = dt2Timeymd2doy(YYYY, MMM, DD)
        return Epoch + ((DOY - 1) * 86400) + (HH * 3600) + (MM * 60) + SS
##########################################
# BEGIN: dt2Timeystr2Epoch(YYYY, DateTime)
# FUNC:dt2Timeystr2Epoch():2018.238
#   A slightly specialized time to Epoch converter where the input can be
#       YYYY, DOY:HH:MM:SS:TTT or
#       None,YYYY:DOY:HH:MM:SS:TTT or
#       None,YYYY-MM-DD:HH:MM:SS:TTT
#   DOY/DD and HH must be separated by a ':' -- no space.
#   The separator between SS and TTT may be : or .
#   Returns 0 if ANYTHING goes wrong.
#   Returns a float if the seconds were a float (SS:TTT or SS.ttt), or an int
#   if the time was incomplete or there was an error. The caller will just
#   have to do an int() if that is all they want.
#   Replaces str2Epoch(). Used a lot in RT130/72A data decoding.


def dt2Timeystr2Epoch(YYYY, DateTime):
    global Y2EPOCH
    Epoch = 0
    try:
        if DateTime.find("-") != -1:
            DT = DateTime.split(":", 1)
            Parts2 = DT[0].split("-")
            Parts = []
            for Parts in Parts2:
                Parts.append(float(Part))
            Parts2 = DT[1].split(":")
            for Parts in Parts2:
                Parts.append(float(Part))
            # There "must" have been a :TTT part.
            if len(Parts) == 6:
                Parts[5] += Parts[6] / 1000.0
        else:
            Parts2 = DateTime.split(":")
            Parts = []
            for Part in Parts2:
                Parts.append(float(Part))
            # There "must" have been a :TTT part.
            if len(Parts) == 7:
                Parts[5] += Parts[6] / 1000.0
        if YYYY is None:
            # Change this since it gets used in loops and dictionary keys
            # and stuff. If the year is passed (below) then we're already
            # covered.
            Parts[0] = int(Parts[0])
            # Just in case someone generates 2-digit years.
            if Parts[0] < 100:
                if Parts[0] < 70:
                    Parts[0] += 2000
                else:
                    Parts[0] += 1900
        else:
            # Check this just in case. The caller will just have to figure
            # this one out.
            if YYYY < 1970:
                YYYY = 1970
            Parts = [YYYY, ] + Parts
        Parts += (5 - len(Parts)) * [0]
        # This makes each year's Epoch get calculated only once.
        try:
            Epoch = Y2EPOCH[Parts[0]]
        except KeyError:
            for YYY in arange(1970, Parts[0]):
                if YYY % 4 != 0:
                    Epoch += 31536000
                elif YYY % 100 != 0 or YYY % 400 == 0:
                    Epoch += 31622400
                else:
                    Epoch += 31536000
            Y2EPOCH[Parts[0]] = Epoch
        # This goes along with forcing YYYY to 1970 if things are bad.
        if Parts[1] < 1:
            Parts[1] = 1
        Epoch += ((int(Parts[1]) - 1) * 86400) + (int(Parts[2]) * 3600) + \
            (int(Parts[3]) * 60) + Parts[4]
    except ValueError:
        Epoch = 0
    return Epoch
##########################################################
# BEGIN: dtHoursFromNow(ToFormat, OutFormat, DateTime, TZ)
# FUNC:dtHoursFromNow():2018.264
#   Takes in a time (presumably in the future) and returns the decimal hours
#   from now until then.
#   ToFormat is the format of the time we are going to.
#   OutFormat is how to format the return time, "H" for decimal hours, or "H:M"
#   for HH:MM (no seconds).
#   TZ is either "GMT" or "LT" pertaining to the input DateTime.
#   The current time it uses (in YYYY:DOY:HH:MM:SS) is also returned.
#       (decimal hours, current time used)


def dtHoursFromNow(ToFormat, OutFormat, DateTime, TZ):
    InEpoch = dt2Time(ToFormat, -1, DateTime)
    if TZ == "GMT":
        NowDateTime = getGMT(0)
    elif TZ == "LT":
        NowDateTime = getGMT(13)
    NowEpoch = dt2Time(11, -1, NowDateTime)
    Diff = InEpoch - NowEpoch
    if OutFormat == "H":
        return ("%.2f hours" % (Diff / 3600.0), NowDateTime)
    elif OutFormat == "H:M":
        H = Diff / 3600.0
        M = int((H - int(H)) * 60.0)
        H = int(H)
        # Rounding may cause this.
        if M > 59:
            H += 1
            M = M - 60
        return ("%02d:%02d" % (H, M), NowDateTime)
    return ("0.00", "0000:000:00:00:00")
############################
# BEGIN: dt2DateDiff(D1, D2)
# FUNC:dt2DateDiff():2018.238
#   Takes two dates in YYYY-MM-DD format and returns the number of whole days
#   between them. Dates must be proper format or there will be crashing.
#   Returned is D2-D1, so D2 should be later than D1, but doesn't have to be.


def dt2DateDiff(D1, D2):
    Parts = D1.split("-")
    D1Y = int(Parts[0])
    D1M = int(Parts[1])
    D1D = int(Parts[2])
    Parts = D2.split("-")
    D2Y = int(Parts[0])
    D2M = int(Parts[1])
    D2D = int(Parts[2])
    # Pick off the easy one.
    if D1Y == D2Y and D1M == D2M:
        return D2D - D1D
    # Almost easy.
    elif D1Y == D2Y:
        D1Doy = dt2Timeymd2doy(D1Y, D1M, D1D)
        D2Doy = dt2Timeymd2doy(D2Y, D2M, D2D)
        return D2Doy - D1Doy
    else:
        # The full monty.
        D1E = dt2Timeymddhms(-1, D1Y, D1M, D1D, -1, 0, 0, 0)
        D2E = dt2Timeymddhms(-1, D2Y, D2M, D2D, -1, 0, 0, 0)
        return int((D2E - D1E) / 86400)
# END: dt2Time


###############################
# BEGIN: fileSelected(e = None)
# FUNC:fileSelected():2019.232
#   Puts together the filespec(s) of the file(s) selected by the user and
#   handles the higher-level functions to get it/them processed and the data
#   into QPData and it's friends.
def fileSelected(e=None):
    global QPEarliestData
    global QPLatestData
    global QPUserChannels
    global QPFilesProcessed
    global QPCDSelect
    Iam = stack()[0][3]
    Root.focus_set()
    setMsg("MF", "", "")
    DataDir = PROGDataDirVar.get()
    # Just in case.
    if DataDir.endswith(sep) is False:
        DataDir += sep
        PROGDataDirVar.set(DataDir)
    # Also just in case.
    if exists(DataDir) is False:
        setMsg("MF", "RW", "%s: Directory %s does not exist." % (Iam, DataDir),
               2)
        return
    # Loop through the selected file(s) in the Listbox.
    Sel = MFFiles.curselection()
    if len(Sel) == 0:
        setMsg("MF", "RW",
               "No data source files/folders have been selected.", 2)
        return
    # Do this up here, because it can take a while for the TPS plot to clear.
    progControl("go")
    plotMFClear()
    setMsg("INFO", "", "")
    formLOGClear()
    formQPERRClear()
    formTPSClear(True)
    formGPSClear("read")
    ListChannels = OPTListChannelsCVar.get()
    # Check out the dates that the user may have entered as limits for reading.
    # Use changeDateFormat() in case the user put in dates not matching the
    # current format and since it will return detailed error messages if
    # anything is wrong.
    if changeDateFormat("MF") is False:
        progControl("stopped")
        return
    Date = FromDateVar.get().strip()
    if len(Date) != 0:
        # FINISHME - when do these dates get verified?
        FromEpoch = dt2Time(0, -1, Date + " 00:00:00")
        # This is for the Antelope RT mode. There will be a string comparison
        # in that section.
        Y, M, Dt, Dy, H, Mm, S = dt2Time(0, 0, Date + " 00:00:00")
        FromAntRTDate = "%04d%s%03d" % (Y, sep, Dy)
    else:
        FromEpoch = 0.0
        FromAntRTDate = ""
    Date = ToDateVar.get().strip()
    if len(Date) != 0:
        ToEpoch = dt2Time(0, -1, Date + " 24:00:00")
        Y, M, Dt, Dy, H, Mm, S = dt2Time(0, 0, Date + " 00:00:00")
        ToAntRTDate = "%04d%s%03d" % (Y, sep, Dy)
    else:
        ToEpoch = maxFloat
        ToAntRTDate = ""
    if FromEpoch > ToEpoch:
        setMsg("MF", "RW",
               "The From date field value is after the To value.", 2)
        progControl("stopped")
        return
    # If this is not "" there will be filtering.
    PROGStaIDFilterVar.set(PROGStaIDFilterVar.get().upper().strip())
    StaIDFilter = PROGStaIDFilterVar.get()
    # Check out The Gap.
    if plotMFMindTheGap() is False:
        progControl("stopped")
        return
    # Either they get it right, or they turn off the checkbutton.
    if OPTPlotTPSCVar.get() == 1:
        Ret = formTPSChkChans()
        if Ret[0] != 0:
            setMsg("MF", Ret)
            progControl("stopped")
            return
    # I suppose there could be a "msg" function in plotMF() to do these
    # messages, but since the main plot of the program is the main point of the
    # program we'll just do it directly.
    setMsg("MF", "CB", "Working...")
    formTPSMsg("CB", "Working...")
    formLOGMsg("CB", "Working...")
    formQPERRMsg("CB", "Working...")
    clearQPGlobals()
    GapsDetect = OPTGapsDetectCVar.get()
    # Clean up and get the list of selected channels to display.
    Ret = setQPUserChannels("plot")
    if Ret[0] != 0:
        setMsg("MF", Ret)
        fileSelectedStop("", "")
        return
    BMSDataDirBut = OPTBMSDataDirRVar.get()
    OpMode = OPTOpModeRVar.get()
    # NORMAL OP MODE
    if OpMode == "Normal":
        # Collect all of the files then start running through them (.bms
        # source only).
        Files = []
        for Index in Sel:
            Filename = MFFiles.get(Index)
            # Blank line in the list.
            if len(Filename) == 0:
                continue
            # Most items may have a (x bytes) or something like that following
            # the file.
            if Filename.find(" (") != -1:
                Filename = Filename[:Filename.index(" (")].strip()
            # The plotter will use this/these in the title and the files will
            # be listed in the INFO area when we're done.
            QPFilesProcessed.append(Filename)
            Filespec = DataDir + Filename
            # The directory may have gotten changed, but the list of files not.
            if exists(Filespec) is False:
                setMsg("MF", "RW", "File %s does not exist." % Filespec, 2)
                fileSelectedStop("", "")
                return
            FilespecLC = Filespec.lower()
            if FilespecLC.endswith(".bms"):
                QPCDSelect = "Other"
                # We only really want to check this once, but we don't know
                # until here that a .bms folder was selected.
                if BMSDataDirBut not in ("data", "sdata"):
                    setMsg("MF", "RW",
                           "Select the '.bms Dir' checkbutton again.", 2)
                    fileSelectedStop("", "")
                    return
                FilesOnStick = listdir(Filespec)
                BMSDataDirs = []
                # The 2000 file limit for data or sdata directories "thing"
                # throws a bit of a wrench into the works. Just .sort()ing on
                # the directories will put things in the wrong order, and we
                # can't just reverse the sort, either, so do the rightthing
                # after this if it is detected.
                Limit2000 = False
                for File in FilesOnStick:
                    if File.startswith(BMSDataDirBut):
                        BMSDataDirs.append(File)
                    if Limit2000 is False and \
                            File.startswith(BMSDataDirBut + "-"):
                        Limit2000 = True
                if len(BMSDataDirs) == 0:
                    setMsg("MF", "RW", "No %s directories found in %s." %
                           (BMSDataDirBut, Filespec), 2)
                    fileSelectedStop("", "")
                    return
                # This will put the 'data-' directories in the right order if
                # there are any.
                BMSDataDirs.sort()
                # I don't know if it is possible to stop recording at just the
                # right time toend up with no "data" or "sdata" directory, but
                # we'll make this as complicated as possible to make it
                # not matter.
                if Limit2000 is True:
                    NewDataDirs = []
                    FoundBare = False
                    for Dir in BMSDataDirs:
                        if Dir.find(BMSDataDirBut + "-") != -1:
                            NewDataDirs.append(Dir)
                            continue
                        else:
                            FoundBare = True
                            continue
                    # Now add this last. It is the most recent data.
                    if FoundBare is True:
                        NewDataDirs.append(BMSDataDirBut)
                    BMSDataDirs = NewDataDirs
                for BMSDataDir in BMSDataDirs:
                    if exists(Filespec + sep + BMSDataDir + sep) is False:
                        setMsg("MF", "RW", "%s does not exist." %
                               (Filespec + sep + BMSDataDir + sep), 2)
                        fileSelectedStop("", "")
                        return
                    # Get a list of all the files that we will be looking at.
                    Ret = walkDirs2(Filespec, False, sep + BMSDataDir + sep,
                                    "", 1)
                    if Ret[0] != 0:
                        setMsg("MF", Ret)
                        fileSelectedStop("", "")
                        return
                    # Sort the files we found in the data/sdata dir here, but
                    # then don't sort all of the files below or if there is
                    # data and data- directories things will beout of time
                    # order. If there is just a "data" dir then this will
                    # suffice.
                    Ret[1].sort()
                    Files += Ret[1]
                if len(Files) == 0:
                    setMsg("MF", "RW",
                           "No files were found in %s directories in %s" %
                           (BMSDataDirBut, Filespec + sep), 2)
                    fileSelectedStop("", "")
                    return
                FileCount = 0
                for File in Files:
                    FileCount += 1
                    if FileCount % 100 == 0:
                        setMsg("MF", "CB", "Working on file %d of %d..." %
                               (FileCount, len(Files)))
                    Ret = q330Process(File, FromEpoch, ToEpoch, StaIDFilter,
                                      GapsDetect, 0, False, None, None, None,
                                      0)
                    if Ret[0] != 0:
                        # Dump whatever may have been collected so far to keep
                        # from being a RAM pig.
                        clearQPGlobals()
                        setMsg("MF", Ret)
                        fileSelectedStop("", "")
                        return
                    # Check for stopping each file.
                    PROGStopBut.update()
                    if PROGRunning == 0:
                        clearQPGlobals()
                        setMsg("MF", "YB", "Stopped.")
                        fileSelectedStop("YB", "Stopped.")
                        return
            # .ALL files usually made by EzBaler.
            elif FilespecLC.endswith(".all"):
                QPCDSelect = "Other"
                # It's all-in-one so there is only the one file to process.
                # Tell q330process() to handle the update messages and checking
                # for the Stop button press.
                Ret = q330Process(Filespec, FromEpoch, ToEpoch, StaIDFilter,
                                  GapsDetect, 0, False, PROGStopBut,
                                  PROGRunning, "MF", 0)
                if Ret[0] != 0:
                    clearQPGlobals()
                    setMsg("MF", Ret)
                    fileSelectedStop("", "")
                    return
                PROGStopBut.update()
                if PROGRunning == 0:
                    clearQPGlobals()
                    setMsg("MF", "YB", "Stopped.")
                    fileSelectedStop("YB", "Stopped.")
                    return
            elif FilespecLC.endswith(".soh"):
                QPCDSelect = "Other"
                # Again, just one file to work with.
                Ret = q330Process(Filespec, FromEpoch, ToEpoch, StaIDFilter,
                                  GapsDetect, 0, False, PROGStopBut,
                                  PROGRunning, "MF", 0)
                if Ret[0] != 0:
                    clearQPGlobals()
                    setMsg("MF", Ret)
                    fileSelectedStop("", "")
                    return
                PROGStopBut.update()
                if PROGRunning == 0:
                    clearQPGlobals()
                    setMsg("MF", "YB", "Stopped.")
                    fileSelectedStop("YB", "Stopped.")
                    return
            elif FilespecLC.endswith(".sdr"):
                QPCDSelect = "Other"
                Ret = walkDirs2(Filespec, False, "", "", 0)
                if Ret[0] != 0:
                    setMsg("MF", Ret)
                    fileSelectedStop("", "")
                    return
                Files = Ret[1]
                if len(Files) == 0:
                    setMsg("MF", "RW",
                           "No channel files were found in\n   %s" % Filespec,
                           2)
                    fileSelectedStop("", "")
                    return
                Files.sort()
                FileCount = 0
                # There is no real good way to check these. We'll just have to
                # try reading all of them and hope for the best. They should
                # have the channel name in the file name, but we'll just go by
                # the stuff in the files themselves.
                for File in Files:
                    # BLINE/BGOFF started this one. This is so the list of
                    # files and the log file
                    # can be kept in the directory with the data files.
                    if File.endswith(".txt") or File.endswith(".TXT"):
                        continue
                    FileCount += 1
                    if FileCount % 50 == 0:
                        setMsg("MF", "CB", "Working on file %d of %d..." %
                               (FileCount, len(Files)))
                    Ret = q330Process(File, FromEpoch, ToEpoch, StaIDFilter,
                                      GapsDetect, 0, True, None, None, None, 0)
                    if Ret[0] != 0:
                        clearQPGlobals()
                        setMsg("MF", Ret)
                        fileSelectedStop("", "")
                        return
                    # Check for stopping each file.
                    PROGStopBut.update()
                    if PROGRunning == 0:
                        clearQPGlobals()
                        setMsg("MF", "YB", "Stopped.")
                        fileSelectedStop("YB", "Stopped.")
                        return
            # A folder that ends in .nan, has seed or ms files (one channel
            # per file), and may have .csv files with engineering data in them.
            # It's a Nanometrics thing. This works for data recorded on
            # TitanSMA SDCards, and stuff extracted from an Apollo server...at
            # least the stuff we have.
            elif FilespecLC.endswith(".nan"):
                QPCDSelect = "NM"
                Ret = walkDirs2(Filespec, False, "", "", 1)
                if Ret[0] != 0:
                    setMsg("MF", Ret)
                    fileSelectedStop("", "")
                    return
                Files = Ret[1]
                if len(Files) == 0:
                    setMsg("MF", "RW",
                           "No channel files were found in\n   %s" % Filespec,
                           2)
                    fileSelectedStop("", "")
                    return
                Files.sort()
                FileCount = 0
                for File in Files:
                    FileCount += 1
                    if FileCount % 50 == 0:
                        setMsg("MF", "CB", "Working on file %d of %d..." %
                               (FileCount, len(Files)))
                    if File.endswith(".csv"):
                        Ret = nanProcessCSV(File, FromEpoch, ToEpoch,
                                            StaIDFilter)
                        if Ret[0] != 0:
                            clearQPGlobals()
                            setMsg("MF", Ret, 3)
                            fileSelectedStop("", "")
                            return
                    # These are Nanometrics files that need to be converted
                    # with SohConvert.javafrom Nanometrics. I won't read them.
                    elif File.endswith(".np"):
                        continue
                    # It's not normal for the user to have to run the
                    # SohConvert program on the .np files, but if it is done
                    # the program creates a .log file. Just ignore it.
                    elif File.endswith(".log"):
                        continue
                    else:
                        # Nanometrics miniseed files can be split or
                        # multiplexed, so False here.
                        Ret = q330Process(File, FromEpoch, ToEpoch,
                                          StaIDFilter, GapsDetect, 0, False,
                                          None, None, None, 0)
                        if Ret[0] != 0:
                            clearQPGlobals()
                            setMsg("MF", Ret)
                            fileSelectedStop("", "")
                            return
                    # Check for stopping each file.
                    PROGStopBut.update()
                    if PROGRunning == 0:
                        clearQPGlobals()
                        setMsg("MF", "YB", "Stopped.")
                        fileSelectedStop("YB", "Stopped.")
                        return
            elif FilespecLC.endswith(".ant"):
                QPCDSelect = "Other"
                Ret = walkDirs2(Filespec, False, "", "", 0)
                if Ret[0] != 0:
                    setMsg("MF", Ret)
                    fileSelectedStop("", "")
                    return
                Files = Ret[1]
                if len(Files) == 0:
                    setMsg("MF", "RW",
                           "No channel files were found in\n   %s" % Filespec,
                           2)
                    fileSelectedStop("", "")
                    return
                Files.sort()
                FileCount = 0
                WhackAntSpikes = OPTWhackAntSpikesCVar.get()
                # There is no real good way to check these. We'll just have to
                # try reading allof them and hope for the best. They should
                # have the channel name in the file name, but we'll just go by
                # the stuff in the files themselves.
                for File in Files:
                    FileCount += 1
                    if FileCount % 50 == 0:
                        setMsg("MF", "CB", "Working on file %d of %d..." %
                               (FileCount, len(Files)))
                    # This will be called with SkipFile == False. .ant folders
                    # may be channel files or multiplexed files.
                    Ret = q330Process(File, FromEpoch, ToEpoch, StaIDFilter,
                                      GapsDetect, WhackAntSpikes, False, None,
                                      None, None, 0)
                    if Ret[0] != 0:
                        clearQPGlobals()
                        setMsg("MF", Ret)
                        fileSelectedStop("", "")
                        return
                    # Check for stopping each file.
                    PROGStopBut.update()
                    if PROGRunning == 0:
                        clearQPGlobals()
                        setMsg("MF", "YB", "Stopped.")
                        fileSelectedStop("Stopped.")
                        return
            elif len(FilespecLC) != 0:
                setMsg("MF", "RW",
                       "I don't know what to do with the file/folder\n   %s" %
                       Filespec, 2)
                fileSelectedStop("", "")
                return
        # Sometimes the data is so screwed up it can't even be viewed. This
        # might help especially if the file names for .bms, .sdr, .nan and .ant
        # sources were messed up. QPEEK gets a list of files in those
        # directories, SORT THEM, and then processes and plots them. This could
        # cause the data to be jumbled(though probably not in .sdr sources
        # since they are supposed to be one channel per file). This will, of
        # course, make overlaps go away, which might be a bad thing.
        if OPTSortAfterReadCVar.get() == 1:
            for Key in list(QPData.keys()):
                QPData[Key].sort()
    # B44 MEMORY STICK MODE
    # Just a abbreviated .bms chunk of the normal mode above.
    elif OpMode == "B44Stick":
        QPCDSelect = "Other"
        # Collect all of the files then start running through them
        # (.bms source only).
        Files = []
        # Since we are "in" the BMS we can only process this stick, so there's
        # no Sel loop here.
        # The DataDir is the name of the "folder" to go through.
        Basename = DataDir[:-1]
        QPFilesProcessed.append(Basename)
        Filespec = Basename
        if exists(Filespec) is False:
            setMsg("MF", "RW", "File %s does not exist." % Filespec, 2)
            fileSelectedStop("", "")
            return
        if BMSDataDirBut not in ("data", "sdata"):
            setMsg("MF", "RW",
                   "Select the '.bms Dir' checkbutton again.", 2)
            fileSelectedStop("", "")
            return
        FilesOnStick = listdir(Filespec)
        BMSDataDirs = []
        Limit2000 = False
        for File in FilesOnStick:
            if File.startswith(BMSDataDirBut):
                BMSDataDirs.append(File)
            if Limit2000 is False and File.startswith(BMSDataDirBut + "-"):
                Limit2000 = True
        if len(BMSDataDirs) == 0:
            setMsg("MF", "RW", "No %s directories found in %s." %
                   (BMSDataDirBut, Filespec), 2)
            fileSelectedStop("", "")
            return
        BMSDataDirs.sort()
        if Limit2000 is True:
            NewDataDirs = []
            FoundBare = False
            for Dir in BMSDataDirs:
                if Dir.find(BMSDataDirBut + "-") != -1:
                    NewDataDirs.append(Dir)
                    continue
                else:
                    FoundBare = True
                    continue
            if FoundBare is True:
                NewDataDirs.append(BMSDataDirBut)
            BMSDataDirs = NewDataDirs
        for BMSDataDir in BMSDataDirs:
            if exists(Filespec + sep + BMSDataDir + sep) is False:
                setMsg("MF", "RW", "%s does not exist." %
                       (Filespec + sep + BMSDataDir + sep), 2)
                fileSelectedStop("", "")
                return
            Ret = walkDirs2(Filespec, False, sep + BMSDataDir + sep, "", 1)
            if Ret[0] != 0:
                setMsg("MF", Ret)
                fileSelectedStop("", "")
                return
            Ret[1].sort()
            Files += Ret[1]
        if len(Files) == 0:
            setMsg("MF", "RW", "No files were found in %s directories in %s" %
                   (BMSDataDirBut, Filespec + sep), 2)
            fileSelectedStop("", "")
            return
        FileCount = 0
        for File in Files:
            FileCount += 1
            if FileCount % 100 == 0:
                setMsg("MF", "CB", "Working on file %d of %d..." %
                       (FileCount, len(Files)))
            Ret = q330Process(File, FromEpoch, ToEpoch, StaIDFilter,
                              GapsDetect, 0, False, None, None, None, 0)
            if Ret[0] != 0:
                clearQPGlobals()
                setMsg("MF", Ret)
                fileSelectedStop("", "")
                return
            PROGStopBut.update()
            if PROGRunning == 0:
                clearQPGlobals()
                setMsg("MF", "YB", "Stopped.")
                fileSelectedStop("YB", "Stopped.")
                return
        if OPTSortAfterReadCVar.get() == 1:
            for Key in list(QPData.keys()):
                QPData[Key].sort()
    # ANTELOPE DB MODE.
    elif OpMode == "AntRT":
        QPCDSelect = "Other"
        for Index in Sel:
            Station = MFFiles.get(Index)
            # Most items may have a (x bytes) or something like that following
            # the file.
            if Station.find(" (") != -1:
                Station = Station[:Station.index(" (")].strip()
            # Blank line in the list.
            if len(Station) == 0:
                continue
            # The plotter will use this/these in the title.
            QPFilesProcessed.append(Station)
            # Break this up, build a list of directories to step through, and
            # start finding files.
            Parts = Station.split("-")
            YearDir = Parts[0]
            TStation = "%s" % Parts[1]
            # This is the same idea as in loadSourceFiles(). We need to check
            # everything again in case the user did "something".
            Files = listdir(DataDir)
            Found = False
            for File in Files:
                if File == "db":
                    Found = True
                    break
            if Found is False:
                clearQPGlobals()
                setMsg("MF", Ret)
                fileSelectedStop("", "")
                setMsg("MF", "YB", "No 'db' directory found.", 2)
                return
            # Scan for the year directories and throw out anything that doesn't
            # look like one.
            Ret = listdir(DataDir + "db")
            Years = []
            for Year in Ret:
                if len(Year) == 4 and intt(Year) != 0 and Year == YearDir:
                    Years.append(Year)
            # Now build a list of what should be the DOY directories and
            # filter.
            Doys = []
            for Year in Years:
                Ret = listdir(DataDir + "db" + sep + Year)
                for Doy in Ret:
                    if len(Doy) == 3 and intt(Doy) != 0 and Doy not in Doys:
                        # Now we can start paying attention to any entered date
                        # range.
                        Date = "%s%s%s" % (Year, sep, Doy)
                        if len(FromAntRTDate) != 0:
                            if Date < FromAntRTDate:
                                continue
                        if len(ToAntRTDate) != 0:
                            if Date > ToAntRTDate:
                                continue
                        Doys.append(Doy)
            Years.sort()
            Doys.sort()
            Filespecs = []
            # Step through everything and process the files that start with
            # TStation.
            for Year in Years:
                if Year != YearDir:
                    continue
                for Doy in Doys:
                    TheDir = DataDir + "db" + sep + Year + sep + Doy + sep
                    # This rt mode may ask for directories that don't exists.
                    # walkDirs2() will complain. Instead of that do this check
                    # here. We'll save the complaining for important more
                    # things.
                    if exists(TheDir) is False:
                        continue
                    Ret = walkDirs2(TheDir, False, sep + TStation + ".", "", 0)
                    if Ret[0] != 0:
                        clearQPGlobals()
                        setMsg("MF", Ret)
                        fileSelectedStop("", "")
                        setMsg("MF", Ret)
                        return
                    Filespecs += Ret[1]
            Filespecs.sort()
            FileCount = 0
            # There is no real good way to check these. We'll just have to try
            # reading all of them and hope for the best. They should have the
            # channel name in the file name, but we'll just go by the stuff in
            # the files themselves.
            for Filespec in Filespecs:
                FileCount += 1
                if FileCount % 50 == 0:
                    setMsg("MF", "CB", "Working on file %d of %d..." %
                           (FileCount, len(Filespecs)))
                Ret = q330Process(Filespec, FromEpoch, ToEpoch, StaIDFilter,
                                  GapsDetect, 0, True, None, None, None, 0)
                if Ret[0] != 0:
                    clearQPGlobals()
                    setMsg("MF", Ret)
                    fileSelectedStop("", "")
                    return
                # Check for stopping each file.
                PROGStopBut.update()
                if PROGRunning == 0:
                    clearQPGlobals()
                    setMsg("MF", "YB", "Stopped.")
                    fileSelectedStop("YB", "Stopped.")
                    return
    # IN THE ROOT DIR OF A NANOMETRICS SD CARD.
    elif OpMode == "NanoCard":
        QPCDSelect = "NM"
        if len(Sel) != 1:
            setMsg("MF", "RW",
                   "You must select the Nano Card item for this operating "
                   "mode.", 2)
            fileSelectedStop("", "")
            return
        # Collect all of the files then start running through them
        # (.bms source only).
        Files = []
        # The plotter will use this/these in the title and the files will be
        # listed in the INFO area when we're done.
        QPFilesProcessed.append("Nano Card")
        # The directory may have gotten changed, but the list of files not.
        if exists(DataDir) is False:
            setMsg("MF", "RW", "Directory %s does not exist." % DataDir, 2)
            fileSelectedStop("", "")
            return
        Ret = walkDirs2(DataDir, False, "", "", 1)
        if Ret[0] != 0:
            setMsg("MF", Ret)
            fileSelectedStop("", "")
            return
        Files = Ret[1]
        if len(Files) == 0:
            setMsg("MF", "RW", "No channel files were found in\n   %s" %
                   DataDir, 2)
            fileSelectedStop("", "")
            return
        Files.sort()
        FileCount = 0
        for File in Files:
            FileCount += 1
            if FileCount % 50 == 0:
                setMsg("MF", "CB", "Working on file %d of %d..." %
                       (FileCount, len(Files)))
            # &&&&& This list may grow a bunch. &&&&&
            # System Volume = A folder put on the card by Saminda's bench
            # computer.
            # events = A folder of data chopped out of the main body.
            if File.find("System Volume") != -1 or File.find("events") != -1:
                continue
            if File.endswith(".csv"):
                Ret = nanProcessCSV(File, FromEpoch, ToEpoch, StaIDFilter)
                if Ret[0] != 0:
                    clearQPGlobals()
                    setMsg("MF", Ret, 3)
                    fileSelectedStop("", "")
                    return
            # These are Nanometrics files that need to be converted with
            # SohConvert.java from Nanometrics. I won't read them.
            elif File.endswith(".np"):
                continue
            # It's not normal for the user to have to run the SohConvert
            # program on the .np files, but if it is done the program creates
            # a .log file. Just ignore it.
            elif File.endswith(".log"):
                continue
            else:
                Ret = q330Process(File, FromEpoch, ToEpoch, StaIDFilter,
                                  GapsDetect, 0, True, None, None, None,
                                  ListChannels)
                if Ret[0] != 0:
                    clearQPGlobals()
                    setMsg("MF", Ret)
                    fileSelectedStop("", "")
                    return
            # Check for stopping each file.
            PROGStopBut.update()
            if PROGRunning == 0:
                clearQPGlobals()
                setMsg("MF", "YB", "Stopped.")
                fileSelectedStop("YB", "Stopped.")
                return
    # SEISCOMP3 ARCHIVE
    elif OpMode == "SC3":
        # %%%%%% not well tested
        QPCDSelect = "Other"
        for Index in Sel:
            YNS = MFFiles.get(Index)
            # Blank line in the list.
            if len(YNS) == 0:
                continue
            # The plotter will use this/these in the title.
            QPFilesProcessed.append(YNS)
            DataDir = PROGDataDirVar.get() + YNS + "/"
            ChanDirs = listdir(DataDir)
            # Go through the channel directories that the user has selected
            # and build a list of all of the data files.
            Filespecs = []
            for ChanDir in ChanDirs:
                Parts = ChanDir.split(".")
                if len(Parts) < 2:
                    continue
                Ret = walkDirs(DataDir + ChanDir, False, "", 1, False)
                Filespecs += Ret[1]
            Filespecs.sort()
            FileCount = 0
            # There is no real good way to check these. We'll just have to try
            # reading all of them and hope for the best. They should have the
            # channel name in the file name, but we'll just go by the stuff in
            # the files themselves.
            for Filespec in Filespecs:
                FileCount += 1
                if FileCount % 50 == 0:
                    setMsg("MF", "CB", "Working on file %d of %d..." %
                           (FileCount, len(Filespecs)))
                Ret = q330Process(Filespec, FromEpoch, ToEpoch, StaIDFilter,
                                  GapsDetect, 0, True, None, None, None, 0)
                if Ret[0] != 0:
                    clearQPGlobals()
                    setMsg("MF", Ret)
                    fileSelectedStop("", "")
                    return
                # Check for stopping each file.
                PROGStopBut.update()
                if PROGRunning == 0:
                    clearQPGlobals()
                    setMsg("MF", "YB", "Stopped.")
                    fileSelectedStop("YB", "Stopped.")
                    return
    # Collect the earliest and latest times in the data.
    QPEarliestData = maxFloat
    QPLatestData = -maxFloat
    for Chan in QPData:
        # It's possible that channels we didn't know how to decode will be in
        # here, but will be [], so try.
        try:
            Min = min(QPData[Chan])[0]
            if Min < QPEarliestData:
                QPEarliestData = Min
            Max = max(QPData[Chan])[0]
            if Max > QPLatestData:
                QPLatestData = Max
        except ValueError:
            pass
    # Must be something worth looking at.
    if QPEarliestData != maxFloat:
        plotMFPlot()
        formTPSPlot()
    # If data from the GPS position channels was collected then append the
    # GPSPOS entries to QPLogs.
    QPDataChans = list(QPData.keys())
    # I'm not sure of a better way to do this. It has to be a partial match
    # because there may or may not be a location code. All four channels need
    # to be in there to trigger the messages.
    GNSChan = ""
    for QPDataChan in QPDataChans:
        if QPDataChan.startswith("GNS"):
            GNSChan = QPDataChan
            break
    GLAChan = ""
    for QPDataChan in QPDataChans:
        if QPDataChan.startswith("GLA"):
            GLAChan = QPDataChan
            break
    GLOChan = ""
    for QPDataChan in QPDataChans:
        if QPDataChan.startswith("GLO"):
            GLOChan = QPDataChan
            break
    GELChan = ""
    for QPDataChan in QPDataChans:
        if QPDataChan.startswith("GEL"):
            GELChan = QPDataChan
            break
    if len(GNSChan) != 0 and len(GLAChan) != 0 and len(GLOChan) != 0 and \
            len(GELChan) != 0:
        # FINISHME - This is completely wrong. I'm going to combine the first
        # entry from each channel, then the second entry, etc., until I run out
        # of entries for one of the channels. If the compression is different
        # this will be totally bad. I'd use the timestamp, but they may not
        # match and then I'd have to get fuzzy. For a system on some glacier
        # this may make a difference, but for anything else it's good enough.
        # What a silly way to record GPS positions.
        # Later: It may be one block per sample, so this might not be so bad.
        # That's a good way to waste disk space.
        Index = 0
        try:
            while True:
                Sats = QPData[GNSChan][Index][1]
                DT = dt2Time(-1, 22, QPData[GNSChan][Index][0])[:-4]
                Pos = "%f %f %f" % (QPData[GLAChan][Index][1],
                                    QPData[GLOChan][Index][1],
                                    QPData[GELChan][Index][1])
                QPLogs.append("%s GPSPOS: %s SATS: %s" % (DT, Pos, Sats))
                Index += 1
        except IndexError:
            pass
    else:
        if len(GNSChan) == 0:
            QPLogs.append("--- GNS channel data missing for full GPSPOS.")
        if len(GLAChan) == 0:
            QPLogs.append("--- GLA channel data missing for full GPSPOS.")
        if len(GLOChan) == 0:
            QPLogs.append("--- GLO channel data missing for full GPSPOS.")
        if len(GELChan) == 0:
            QPLogs.append("--- GEL channel data missing for full GPSPOS.")
    if len(QPLogs) == 0:
        formClose("LOG")
    else:
        formLOG()
    if len(QPErrors) == 0:
        formClose("QPERR")
    else:
        formQPERR()
    INFOMsg = "Source(s) read:\n"
    for File in QPFilesProcessed:
        INFOMsg += "   " + File + "\n"
    # Don't say anything if nothing was plotted.
    if QPEarliestData != maxFloat:
        INFOMsg += ("Data times:\n   %s to\n   %s\n" %
                    (dt2Time(-1, 80, QPEarliestData),
                     dt2Time(-1, 80, QPLatestData)))
    INFOMsg += "Station ID:\n"
    for Value in QPStaIDs:
        INFOMsg += "   %s\n" % Value
    INFOMsg += "Net Code:\n"
    for Value in QPNetCodes:
        INFOMsg += "   %s\n" % Value
    INFOMsg += "Tag Number:\n"
    for Value in QPTagNos:
        INFOMsg += "   %s\n" % Value
    INFOMsg += "Software Version:\n"
    for Value in QPSWVers:
        INFOMsg += "   %s\n" % Value
    # Comes from Nanometrics CSV files, so this may not exist, or have been
    # filled in.
    if len(QPInstruments) != 0:
        INFOMsg += "Instrument(s):\n"
        for Value in QPInstruments:
            INFOMsg += "   %s\n" % Value
    setMsg("INFO", "", INFOMsg)
    # Now finish by displaying something in the MF message area.
    if QPEarliestData == maxFloat:
        if len(PROGStaIDFilterVar.get()) == 0:
            if len(FromDateVar.get()) == 0 and len(ToDateVar.get()) == 0:
                setMsg("MF", "YB",
                       "There doesn't appear to be anything I know how to "
                       "plot.", 2)
            else:
                setMsg("MF", "YB",
                       "There doesn't appear to be anything I know how to "
                       "plot in the entered time range.", 2)
        else:
            # In case this is why there is nothing to plot (like maybe there
            # was something in the field, but the wrong file was read).
            if len(FromDateVar.get()) == 0 and len(ToDateVar.get()) == 0:
                setMsg("MF", "YB",
                       "There doesn't appear to be anything I know how to "
                       "plot for station %s." %
                       PROGStaIDFilterVar.get(), 2)
            else:
                setMsg("MF", "YB",
                       "There doesn't appear to be anything I know how to "
                       "plot for station %s in the entered time range." %
                       PROGStaIDFilterVar.get(), 2)
    else:
        Message = "Done."
        C = ""
        if len(QPStaIDs) > 1:
            Message += " %d station IDs: %s" % (len(QPStaIDs), QPStaIDs)
            C = "YB"
        if len(QPNetCodes) > 1:
            Message += " %d network codes: %s." % (len(QPNetCodes), QPNetCodes)
            C = "YB"
        if len(QPTagNos) > 1:
            Message += " %d tag numbers." % len(QPTagNos)
            C = "YB"
        if len(QPSWVers) > 1:
            Message += " %d different firmware versions." % len(QPSWVers)
            C = "YB"
        if QPAntSpikesWhacked > 0:
            Message += " %d Antelope %s whacked." % (QPAntSpikesWhacked,
                                                     sP(QPAntSpikesWhacked,
                                                        ("spike", "spikes")))
            C = "YB"
        if OPTSortAfterReadCVar.get() == 1:
            Message += " Data is being sorted after reading! Be careful."
            C = "YB"
        setMsg("MF", C, Message)
    if ListChannels != 0:
        print("Channels list:")
        Keys = list(QPListChannels.keys())
        Keys.sort()
        for Key in Keys:
            print(Key)
            print("   %s" % QPListChannels[Key])
    progControl("stopped")
    return
####################################
# BEGIN: fileSelectedStop(MClr, Msg)
# FUNC:fileSelectedStop():2013.047


def fileSelectedStop(MClr, Msg):
    formTPSMsg(MClr, Msg)
    formLOGMsg(MClr, Msg)
    formQPERRMsg(MClr, Msg)
    progControl("stopped")
    return
##################################
# BEGIN: setQPUserChannels(Action)
# FUNC:setQPUserChannels():2019.232


def setQPUserChannels(Action):
    global QPUserChannels
    global QPUnknownChanIDs
    # Clean up and get the list of selected channels to display.
    formCPREFClean()
    UserChannels = eval("CPREFIDs%sVar" % CPREFCurrChanRVar.get()).get()
    if len(UserChannels) == 0:
        return (1, "RW",
                "No channels have been entered in the selected Channel "
                "Preferences list.", 2)
    # Break the list up into a List and then check to see if there is
    # a 3-letter entry for each item in the list. The user will see the missing
    # ones later. We'll remove them from the list for now.
    QPUserChannels = UserChannels.split(",")
    QPUCCopy = deepcopy(QPUserChannels)
    for Chan in QPUCCopy:
        # The user list might be LHZ01 where the QPChans entries will only
        # ever be LHZ.
        # Throw out the channels we don't know how to deal with.
        if Chan[:3] not in QPChans:
            # If we are replotting the main display we'll assume the user saw
            # these already.
            if Action == "plot":
                QPErrors.append((1, "RW",
                                 "Don't know how to plot channel '%s' in "
                                 "list. Removed from list." % Chan, 2))
            # This is why we need the deepcopy.
            QPUserChannels.remove(Chan)
            # Make a note of this for the Q330Process() part so it doesn't
            # crash trying to find this channel in QPChans.  There may be
            # XYZ01, XYZ02 and we want the whole family XYZ to be in here.
            if Chan[:3] not in QPUnknownChanIDs:
                QPUnknownChanIDs.append(Chan[:3])
    # Oops. We removed everything.
    if len(QPUserChannels) == 0:
        return (1, "RW",
                "No channels have been entered in the selected Channel "
                "Preferences list that the program knows how to deal with.", 2)
    return (0, )
# END: fileSelected


###################
# BEGIN: floatt(In)
# LIB:floatt():2018.256
#    Handles all of the annoying shortfalls of the float() function (vs C).
#    Does not handle scientific notation numbers.
def floatt(In):
    In = str(In).strip()
    if len(In) == 0:
        return 0.0
    # At least let the system give it the ol' college try.
    try:
        return float(In)
    except Exception:
        Number = ""
        for c in In:
            if c.isdigit() or c == ".":
                Number += c
            elif (c == "-" or c == "+") and len(Number) == 0:
                Number += c
            elif c == ",":
                continue
            else:
                break
        try:
            return float(Number)
        except ValueError:
            return 0.0
# END: floatt


####################
# BEGIN: fmti(Value)
# LIB:fmti():2018.235
#   Just couldn't rely on the system to do this, although this won't handle
#   European-style numbers.
def fmti(Value):
    Value = int(Value)
    if Value > -1000 and Value < 1000:
        return str(Value)
    Value = str(Value)
    NewValue = ""
    # There'll never be a + sign.
    if Value[0] == "-":
        Offset = 1
    else:
        Offset = 0
    CountDigits = 0
    for i in arange(len(Value) - 1, -1 + Offset, -1):
        NewValue = Value[i] + NewValue
        CountDigits += 1
        if CountDigits == 3 and i != 0:
            NewValue = "," + NewValue
            CountDigits = 0
    if Offset != 0:
        if NewValue.startswith(","):
            NewValue = NewValue[1:]
        NewValue = Value[0] + NewValue
    return NewValue
# END: fmti


#################################
# BEGIN: formABOUT(Parent = Root)
# LIB:formABOUT():2018.324
def formABOUT(Parent=Root):
    # For the versions info and calling __X functions.
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    Message = "%s (%s)\n   " % (PROG_LONGNAME, PROG_NAME)
    Message += "Version %s\n" % PROG_VERSION
    Message += "%s\n" % "PASSCAL Instrument Center"
    Message += "%s\n\n" % "Socorro, New Mexico USA"
    Message += "%s\n" % "Email: passcal@passcal.nmt.edu"
    Message += "%s\n" % "Phone: 575-835-5070"
    Message += "\n"
    Message += "--- Configuration ---\n"
    Message += \
        "%s %dx%d\nProportional font: %s (%d)\nFixed font: %s (%d)\n" % \
        (PROGSystemName, PROGScreenWidthNow, PROGScreenHeightNow,
         PROGPropFont["family"], PROGPropFont["size"],
         PROGMonoFont["family"], PROGMonoFont["size"])
    # Some programs don't care about the geometry of the main display.
    try:
        # If the variable has something in it get the current geometry.
        # The variable value may still be the initial value from when the
        # program was started.
        if len(PROGGeometryVar.get()) != 0:
            Message += "\n"
            Message += "Geometry: %s\n" % Root.geometry()
    except Exception:
        pass
    # Some don't have setup files.
    try:
        if len(PROGSetupsFilespec) != 0:
            Message += "\n"
            Message += "--- Setups File ---\n"
            Message += "%s\n" % PROGSetupsFilespec
    except Exception:
        pass
    # This is only for QPEEK.
    try:
        if len(ChanPrefsFilespec) != 0:
            Message += "\n"
            Message += "--- Channel Preferences File ---\n"
            Message += "%s\n" % ChanPrefsFilespec
    except Exception:
        pass
    Message += "\n"
    Message += "--- Versions ---\n"
    Message += "Python: %s\n" % PROG_PYVERSION
    Message += "Tcl/Tk: %s/%s\n" % (tkntr.TclVersion, tkntr.TkVersion)
    try:
        Message += "pySerial: %s" % SerialVERSION
    except Exception:
        pass
    # Some do, some don't have this.
    try:
        if len(PROG_LASTWORK) != 0:
            Message += "\n"
            Message += "--- Last Worked On ---\n"
            Message += "%s\n" % PROG_LASTWORK
    except Exception:
        pass
    formMYD(Parent, (("(OK)", LEFT, "ok"), ), "ok", "WB", "About",
            Message.strip())
    return
# END: formABOUT


################################################################
# BEGIN: formCAL(Parent, Months = 3, Show = True, AllowX = True,
#                OrigFont = False, CloseQuit = "close")
# LIB:formCAL():2019.058
#   Displays a 1 or 3-month calendar with dates and day-of-year numbers.
#   Pass in 1 or 3 as desired.
#   Show = Should the form show itself or no? (True/False)
#   AllowX = Should the [X] button be allowed for closing? (True/False)
#   OrigFont = False allows the font to change if the program has Font BIGGER,
#              and Font smaller functionality, otherwise the font is stuck
#              with the original font in use when the program started.
#   CloseQuit = "close" shows a Close button for the form, "quit" shows a
#               Quit button and calls progQuitter(True) when pressed.
PROGFrm["CAL"] = None
CALTmModeRVar = StringVar()
CALTmModeRVar.set("lt")
CALDtModeRVar = StringVar()
CALDtModeRVar.set("dates")
PROGSetups += ["CALTmModeRVar", "CALDtModeRVar"]
CALYear = 0
CALMonth = 0
CALText1 = None
CALText2 = None
CALText3 = None
CALMonths = 3
CALTipLastValue = ""


def formCAL(Parent, Months=3, Show=True, AllowX=True, OrigFont=False,
            CloseQuit="close"):
    global CALText1
    global CALText2
    global CALText3
    global CALMonths
    global CALTipLastValue
    CALTipLastValue = ""
    if showUp("CAL"):
        return
    CALMonths = Months
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    LFrm = PROGFrm["CAL"] = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    if AllowX is True:
        if CloseQuit == "close":
            LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "CAL"))
        elif CloseQuit == "quit":
            LFrm.protocol("WM_DELETE_WINDOW", Command(progQuitter, True))
    if CALTmModeRVar.get() == "gmt":
        LFrm.title("Calendar (GMT)")
    elif CALTmModeRVar.get() == "lt":
        GMTDiff = getGMT(20)
        LFrm.title("Calendar (LT: GMT%+.2f hours)" % (float(GMTDiff) / 3600.0))
    LFrm.iconname("Cal")
    Sub = Frame(LFrm)
    if CALMonths == 3:
        if OrigFont is False:
            CALText1 = Text(Sub, bg=Clr["D"], fg=Clr["B"], height=11,
                            width=29, relief=SUNKEN,
                            cursor=Button().cget("cursor"), state=DISABLED)
        else:
            CALText1 = Text(Sub, bg=Clr["D"], fg=Clr["B"], height=11,
                            width=29, relief=SUNKEN, font=PROGOrigMonoFont,
                            cursor=Button().cget("cursor"), state=DISABLED)
        CALText1.pack(side=LEFT, padx=7)
    if OrigFont is False:
        CALText2 = Text(Sub, bg=Clr["W"], fg=Clr["B"], height=11,
                        width=29, relief=SUNKEN,
                        cursor=Button().cget("cursor"), state=DISABLED)
    else:
        CALText2 = Text(Sub, bg=Clr["W"], fg=Clr["B"], height=11,
                        width=29, relief=SUNKEN, font=PROGOrigMonoFont,
                        cursor=Button().cget("cursor"), state=DISABLED)
    CALText2.pack(side=LEFT, padx=7)
    if CALMonths == 3:
        if OrigFont is False:
            CALText3 = Text(Sub, bg=Clr["D"], fg=Clr["B"], height=11,
                            width=29, relief=SUNKEN,
                            cursor=Button().cget("cursor"), state=DISABLED)
        else:
            CALText3 = Text(Sub, bg=Clr["D"], fg=Clr["B"], height=11,
                            width=29, relief=SUNKEN, font=PROGOrigMonoFont,
                            cursor=Button().cget("cursor"), state=DISABLED)
        CALText3.pack(side=LEFT, padx=7)
    Sub.pack(side=TOP, padx=3, pady=3)
    if CALYear != 0:
        formCALMove("c")
    else:
        formCALMove("n")
    Sub = Frame(LFrm)
    BButton(Sub, text="<<", command=Command(formCALMove,
                                            "-y")).pack(side=LEFT)
    BButton(Sub, text="<", command=Command(formCALMove,
                                           "-m")).pack(side=LEFT)
    BButton(Sub, text="Today", command=Command(formCALMove,
                                               "n")).pack(side=LEFT)
    BButton(Sub, text=">", command=Command(formCALMove,
                                           "+m")).pack(side=LEFT)
    BButton(Sub, text=">>", command=Command(formCALMove,
                                            "+y")).pack(side=LEFT)
    Sub.pack(side=TOP, padx=3, pady=3)
    Sub = Frame(LFrm)
    SSub = Frame(Sub)
    SSSub = Frame(SSub)
    LRb = Radiobutton(SSSub, text="GMT", value="gmt",
                      variable=CALTmModeRVar,
                      command=Command(formCALMove, "c"))
    LRb.pack(side=LEFT)
    ToolTip(LRb, 35, "Use what appears to be this computer's GMT time.")
    LRb = Radiobutton(SSSub, text="LT", value="lt",
                      variable=CALTmModeRVar,
                      command=Command(formCALMove, "c"))
    LRb.pack(side=LEFT)
    ToolTip(LRb, 35,
            "Use what appears to be this computer's time and time zone "
            "setting.")
    SSSub.pack(side=TOP, anchor="w")
    SSub.pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    SSub = Frame(Sub)
    LRb = Radiobutton(SSub, text="Dates", value="dates",
                      variable=CALDtModeRVar,
                      command=Command(formCALMove, "c"))
    LRb.pack(side=TOP, anchor="w")
    ToolTip(LRb, 35, "Show the calendar dates.")
    LRb = Radiobutton(SSub, text="DOY", value="doy",
                      variable=CALDtModeRVar,
                      command=Command(formCALMove, "c"))
    LRb.pack(side=TOP, anchor="w")
    ToolTip(LRb, 35, "Show the day-of-year numbers.")
    SSub.pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    if CloseQuit == "close":
        BButton(Sub, text="Close", fg=Clr["R"],
                command=Command(formClose, "CAL")).pack(side=LEFT)
    elif CloseQuit == "quit":
        BButton(Sub, text="Quit", fg=Clr["R"],
                command=Command(progQuitter, True)).pack(side=LEFT)
    Sub.pack(side=TOP, padx=3, pady=3)
    if Show is True:
        center(Parent, LFrm, "CX", "I", True)
    return
####################################
# BEGIN: formCALMove(What, e = None)
# FUNC:formCALMove():2018.235
#   Handles changing the calendar form's display.


def formCALMove(What, e=None):
    global CALYear
    global CALMonth
    global CALText1
    global CALText2
    global CALText3
    PROGFrm["CAL"].focus_set()
    DtMode = CALDtModeRVar.get()
    Year = CALYear
    Month = CALMonth
    if What == "-y":
        Year -= 1
    elif What == "-m":
        Month -= 1
    elif What == "n":
        if CALTmModeRVar.get() == "gmt":
            Year, Month, Day = getGMT(4)
        elif CALTmModeRVar.get() == "lt":
            Year, DOY, HH, MM, SS = getGMT(11)
            GMTDiff = getGMT(20)
            if GMTDiff != 0:
                Year, Month, Day, DOY, HH, MM, SS = dt2TimeMath(0, GMTDiff,
                                                                Year, 0, 0,
                                                                DOY, HH, MM,
                                                                SS)
    elif What == "+m":
        Month += 1
    elif What == "+y":
        Year += 1
    elif What == "c":
        if CALTmModeRVar.get() == "gmt":
            PROGFrm["CAL"].title("Calendar (GMT)")
        elif CALTmModeRVar.get() == "lt":
            GMTDiff = getGMT(20)
            PROGFrm["CAL"].title("Calendar (LT: GMT%+.2f hours)" %
                                 (float(GMTDiff) / 3600.0))
    if Year < 1971:
        beep(1)
        return
    elif Year > 2050:
        beep(1)
        return
    if Month > 12:
        Year += 1
        Month = 1
    elif Month < 1:
        Year -= 1
        Month = 12
    CALYear = Year
    CALMonth = Month
    # Only adjust this back one month if we are showing all three months.
    if CALMonths == 3:
        Month -= 1
    if Month < 1:
        Year -= 1
        Month = 12
    for i in arange(0, 0 + 3):
        # Skip the first and last months if we are only showing one month.
        if CALMonths == 1 and (i == 0 or i == 2):
            continue
        LTxt = eval("CALText%d" % (i + 1))
        LTxt.configure(state=NORMAL)
        LTxt.delete("0.0", END)
        LTxt.tag_delete(*LTxt.tag_names())
        DOM1date = 0
        DOM1doy = PROG_FDOM[Month]
        if (Year % 4 == 0 and Year % 100 != 0) or Year % 400 == 0:
            if Month > 2:
                DOM1doy += 1
        if i == 1:
            LTxt.insert(END, "\n")
            LTxt.insert(END,
                        "%s" %
                        (PROG_CALMON[Month] + " " + str(Year)).center(29))
            LTxt.insert(END, "\n\n")
            IdxS = LTxt.index(CURRENT)
            LTxt.tag_config(IdxS, background=Clr["W"], foreground=Clr["R"])
            LTxt.insert(END, " Sun ", IdxS)
            IdxS = LTxt.index(CURRENT)
            LTxt.tag_config(IdxS, background=Clr["W"], foreground=Clr["U"])
            LTxt.insert(END, "Mon Tue Wed Thu Fri", IdxS)
            IdxS = LTxt.index(CURRENT)
            LTxt.tag_config(IdxS, background=Clr["W"], foreground=Clr["R"])
            LTxt.insert(END, " Sat", IdxS)
            LTxt.insert(END, "\n")
        else:
            LTxt.insert(END, "\n")
            LTxt.insert(END,
                        "%s" %
                        (PROG_CALMON[Month] + " " + str(Year)).center(29))
            LTxt.insert(END, "\n\n")
            LTxt.insert(END, " Sun Mon Tue Wed Thu Fri Sat")
            LTxt.insert(END, "\n")
        All = monthcalendar(Year, Month)
        if CALTmModeRVar.get() == "gmt":
            NowYear, NowMonth, NowDay = getGMT(4)
        elif CALTmModeRVar.get() == "lt":
            # Do this so NowDay gets set. It may not get altered below.
            NowYear, NowMonth, NowDay = getGMT(18)
            NowYear, DOY, HH, MM, SS = getGMT(11)
            GMTDiff = getGMT(20)
            NowDay = 0
            if GMTDiff != 0:
                NowYear, NowMonth, NowDay, DOY, HH, MM, SS = dt2TimeMath(0,
                                                                         GMTDiff, NowYear, 0, 0, DOY, HH, MM, SS)  # noqa: E501
        if DtMode == "dates":
            TargetDay = DOM1date + NowDay
        elif DtMode == "doy":
            TargetDay = DOM1doy + NowDay
        for Week in All:
            LTxt.insert(END, " ")
            for DD in Week:
                if DD != 0:
                    if DtMode == "dates":
                        ThisDay = DOM1date + DD
                        ThisDay1 = DOM1date + DD
                        ThisDay2 = DOM1doy + DD
                    elif DtMode == "doy":
                        ThisDay = DOM1doy + DD
                        ThisDay1 = DOM1doy + DD
                        ThisDay2 = DOM1date + DD
                    IdxS = LTxt.index(CURRENT)
                    if ThisDay == TargetDay and Month == NowMonth and \
                            Year == NowYear:
                        LTxt.tag_config(IdxS, background=Clr["C"],
                                        foreground=Clr["B"])
                    if DtMode == "dates":
                        LTxt.tag_bind(IdxS, "<Enter>",
                                      Command(formCALStartTip, LTxt,
                                              "%s/%03d" % (ThisDay1,
                                                           ThisDay2)))
                    elif DtMode == "doy":
                        LTxt.tag_bind(IdxS, "<Enter>",
                                      Command(formCALStartTip, LTxt,
                                              "%03d/%s" % (ThisDay1,
                                                           ThisDay2)))
                    LTxt.tag_bind(IdxS, "<Leave>", formCALHideTip)
                    LTxt.tag_bind(IdxS, "<ButtonPress>", formCALHideTip)
                    if DtMode == "dates":
                        if ThisDay < 10:
                            LTxt.insert(END, "  ")
                        else:
                            LTxt.insert(END, " ")
                        LTxt.insert(END, "%d" % ThisDay, IdxS)
                    elif DtMode == "doy":
                        LTxt.insert(END, "%03d" % ThisDay, IdxS)
                    LTxt.insert(END, " ")
                else:
                    LTxt.insert(END, "    ")
            LTxt.insert(END, "\n")
        LTxt.configure(state=DISABLED)
        Month += 1
        if Month > 12:
            Year += 1
            Month = 1
    return


#################################################
# BEGIN: formCALStartTip(Parent, Value, e = None)
# FUNC:formCALStartTip():2011.110
#   Pops up a "tool tip" when mousing over the dates of Date/DOY.
CALTip = None


def formCALStartTip(Parent, Value, e=None):
    # Multiple <Entry> events can be generated just by moving the cursor
    # around.
    # If we are still in the same date number just return.
    if Value == CALTipLastValue:
        return
    formCALHideTip()
    formCALShowTip(Parent, Value)
    return
######################################
# BEGIN: formCALShowTip(Parent, Value)
# FUNC:formCALShowTip():2019.058


def formCALShowTip(Parent, Value):
    global CALTip
    global CALTipLastValue
    # The tooltips were not working under Python 3 and lesser versions of
    # Tkinter until I added the .update(). Then AttributeErrors started
    # showing up. So just try everything and hid the tip on any error.
    try:
        CALTip = Toplevel(Parent)
        CALTip.withdraw()
        CALTip.wm_overrideredirect(1)
        LLb = Label(CALTip, text=Value, bg=Clr["Y"], bd=1,
                    fg=Clr["B"], relief=SOLID, padx=3, pady=3)
        LLb.pack()
        x = Parent.winfo_pointerx() + 5
        y = Parent.winfo_pointery() + 5
        CALTip.wm_geometry("+%d+%d" % (x, y))
        CALTip.update()
        CALTip.deiconify()
        CALTip.lift()
    except Exception:
        formCALHideTip()
    CALTipLastValue = Value
    return
#################################
# BEGIN: formCALHideTip(e = None)
# FUNC:formCALHideTip():2011.110


def formCALHideTip(e=None):
    global CALTip
    global CALTipLastValue
    try:
        CALTip.destroy()
    except Exception:
        pass
    CALTip = None
    CALTipLastValue = ""
    return
# END: formCAL


####################
# BEGIN: formCPREF()
# FUNC:formCPREF():2018.352
PROGFrm["CPREF"] = None
CPREFCurrChanRVar = StringVar()
CPREFCurrChanRVar.set("1")
CPREFCurrNameVar = StringVar()
# This is the easiest way to do this and not have to do something special to
# the ChanPrefsSetups saver and loader.
CPREFName1Var = StringVar()
CPREFIDs1Var = StringVar()
CPREFName2Var = StringVar()
CPREFIDs2Var = StringVar()
CPREFName3Var = StringVar()
CPREFIDs3Var = StringVar()
CPREFName4Var = StringVar()
CPREFIDs4Var = StringVar()
CPREFName5Var = StringVar()
CPREFIDs5Var = StringVar()
CPREFName6Var = StringVar()
CPREFIDs6Var = StringVar()
CPREFName7Var = StringVar()
CPREFIDs7Var = StringVar()
CPREFName8Var = StringVar()
CPREFIDs8Var = StringVar()
CPREFName9Var = StringVar()
CPREFIDs9Var = StringVar()
CPREFName10Var = StringVar()
CPREFIDs10Var = StringVar()
CPREFName11Var = StringVar()
CPREFIDs11Var = StringVar()
CPREFName12Var = StringVar()
CPREFIDs12Var = StringVar()
CPREFName13Var = StringVar()
CPREFIDs13Var = StringVar()
CPREFName14Var = StringVar()
CPREFIDs14Var = StringVar()
CPREFName15Var = StringVar()
CPREFIDs15Var = StringVar()
CPREFName16Var = StringVar()
CPREFIDs16Var = StringVar()
CPREFName17Var = StringVar()
CPREFIDs17Var = StringVar()
CPREFName18Var = StringVar()
CPREFIDs18Var = StringVar()
CPREFName19Var = StringVar()
CPREFIDs19Var = StringVar()
CPREFName20Var = StringVar()
CPREFIDs20Var = StringVar()
# FINISHME - Leave these in here for a while. This will make a bridge until
# everyone has a channel prefs setups file. They get saved and loaded after
# anything happens with the make setups file, so they should always be good
# to go.
PROGSetups += ["CPREFCurrChanRVar", "CPREFCurrNameVar",
               "CPREFName1Var", "CPREFIDs1Var",
               "CPREFName2Var", "CPREFIDs2Var",
               "CPREFName3Var", "CPREFIDs3Var",
               "CPREFName4Var", "CPREFIDs4Var",
               "CPREFName5Var", "CPREFIDs5Var",
               "CPREFName6Var", "CPREFIDs6Var",
               "CPREFName7Var", "CPREFIDs7Var",
               "CPREFName8Var", "CPREFIDs8Var",
               "CPREFName9Var", "CPREFIDs9Var",
               "CPREFName10Var", "CPREFIDs10Var",
               "CPREFName11Var", "CPREFIDs11Var",
               "CPREFName12Var", "CPREFIDs12Var",
               "CPREFName13Var", "CPREFIDs13Var",
               "CPREFName14Var", "CPREFIDs14Var",
               "CPREFName15Var", "CPREFIDs15Var",
               "CPREFName16Var", "CPREFIDs16Var",
               "CPREFName17Var", "CPREFIDs17Var",
               "CPREFName18Var", "CPREFIDs18Var",
               "CPREFName19Var", "CPREFIDs19Var",
               "CPREFName20Var", "CPREFIDs20Var"]
ChanPrefsSetups = ["CPREFCurrChanRVar", "CPREFCurrNameVar",
                   "CPREFName1Var", "CPREFIDs1Var",
                   "CPREFName2Var", "CPREFIDs2Var",
                   "CPREFName3Var", "CPREFIDs3Var",
                   "CPREFName4Var", "CPREFIDs4Var",
                   "CPREFName5Var", "CPREFIDs5Var",
                   "CPREFName6Var", "CPREFIDs6Var",
                   "CPREFName7Var", "CPREFIDs7Var",
                   "CPREFName8Var", "CPREFIDs8Var",
                   "CPREFName9Var", "CPREFIDs9Var",
                   "CPREFName10Var", "CPREFIDs10Var",
                   "CPREFName11Var", "CPREFIDs11Var",
                   "CPREFName12Var", "CPREFIDs12Var",
                   "CPREFName13Var", "CPREFIDs13Var",
                   "CPREFName14Var", "CPREFIDs14Var",
                   "CPREFName15Var", "CPREFIDs15Var",
                   "CPREFName16Var", "CPREFIDs16Var",
                   "CPREFName17Var", "CPREFIDs17Var",
                   "CPREFName18Var", "CPREFIDs18Var",
                   "CPREFName19Var", "CPREFIDs19Var",
                   "CPREFName20Var", "CPREFIDs20Var"]
CPREF_SLOTS = 20


def formCPREF(e=None):
    if showUp("CPREF"):
        return
    LFrm = PROGFrm["CPREF"] = Toplevel(Root)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formCPREFControl, "close"))
    LFrm.title("Channel Preferences")
    LFrm.iconname("CPref")
    Label(LFrm,
          text="Place a list of the channels you want records decoded for, "
               "and in the order you want them plotted,\nseparated by commas "
               "in the IDs field. A name can be given to the set to make it "
               "easier to remember.\nSelect the radiobutton for the list of "
               "channels to be used and read the source data file back "
               "on\nthe main display. If you want LOG records decoded place "
               "\"LOG\" in the list.").pack(side=TOP)
    for i in arange(1, 1 + CPREF_SLOTS):
        Sub = Frame(LFrm)
        Radiobutton(Sub, text="Name:", variable=CPREFCurrChanRVar,
                    value=str(i), command=formCPREFCurrName).pack(side=LEFT)
        LEnt = Entry(Sub, textvariable=eval("CPREFName%dVar" % i), width=14)
        LEnt.pack(side=LEFT)
        LEnt.bind("<KeyRelease>", formCPREFCurrName)
        Label(Sub, text=" IDs:").pack(side=LEFT)
        Entry(Sub, textvariable=eval("CPREFIDs%dVar" % i),
              width=105).pack(side=LEFT)
        Sub.pack(side=TOP, anchor="w", padx=3)
    Sub = Frame(LFrm)
    BButton(Sub, text="Copy",
            command=formCPREFCopy).pack(side=LEFT)
    BButton(Sub, text="Paste",
            command=formCPREFPaste).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Scan For Channel IDs",
            command=formSFCI).pack(side=LEFT)
    BButton(Sub, text="Sort", command=formCPREFSort).pack(side=LEFT)
    BButton(Sub, text="Save", command=Command(formCPREFControl,
                                              "save")).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Close", fg=Clr["R"],
            command=Command(formCPREFControl, "close")).pack(side=LEFT)
    Sub.pack(side=TOP, padx=3, pady=3)
    center(Root, LFrm, "CX", "I", True)
    return
####################################
# BEGIN: formCPREFCurrName(e = None)
# FUNC:formCPREFCurrName():2012.319
#   Just sets CPREFCurrNameVar to the name of the currently selected field
#   every time the radiobutton selection is changed or the form is closed for
#   others (like the main display to show).


def formCPREFCurrName(e=None):
    CPREFCurrNameVar.set(eval("CPREFName%sVar" %
                              CPREFCurrChanRVar.get()).get())
    return
########################
# BEGIN: formCPREFCopy()
# FUNC:formCPREFCopy():2018.310
#   Grabs the list of channel IDs from the selected slot.


def formCPREFCopy():
    formCPREFClean()
    Chans = eval("CPREFIDs%sVar" % CPREFCurrChanRVar.get()).get()
    if len(Chans) == 0:
        Answer = formMYD("CPREF", (("(OK)", TOP, "ok"), ), "ok", "",
                         "She's Empty.", "There is nothing to copy.")
        return Answer
    PROGClipboardVar.set(Chans)
    setMsg("CPREF", "", "Copied.")
    return
#########################
# BEGIN: formCPREFPaste()
# FUNC:formCPREFPaste():2018.310
#   Sticks what should be a list of channel IDs into the selected slot. These
#   will normally be from the list built in formSFCI().


def formCPREFPaste():
    if len(PROGClipboardVar.get()) == 0:
        Answer = formMYD("CPREF", (("(OK)", TOP, "ok"), ), "ok", "",
                         "She's Empty.", "There is nothing to paste.")
        return
    if len(eval("CPREFIDs%sVar" % CPREFCurrChanRVar.get()).get()) != 0:
        Answer = formMYD("CPREF",
                         (("Yes", LEFT, "yes"), ("No!", LEFT, "no")),
                         "no", "YB", "I See Old Channels.",
                         "The selected channel slot already contains "
                         "something. It will be overwritten. Is that OK?")
        if Answer == "no":
            setMsg("CPREF", "", "Nothing done.")
            return
    eval("CPREFName%sVar" % CPREFCurrChanRVar.get()).set("PASTED")
    eval("CPREFIDs%sVar" % CPREFCurrChanRVar.get()).set(PROGClipboardVar.get())
    setMsg("CPREF", "", "Pasted.")
    formCPREFCurrName()
    return
########################
# BEGIN: formCPREFSort()
# FUNC:formCPREFSort():2018.310


def formCPREFSort():
    PROGFrm["CPREF"].focus_set()
    DSorter = {}
    formCPREFClean()
    for i in arange(1, 1 + CPREF_SLOTS):
        # Combining the items to make the key will help with duplicate names
        # while...
        Key = eval("CPREFName%dVar" % i).get() + "," + \
            eval("CPREFIDs%dVar" % i).get()
        if Key == ",":
            continue
        # ...doing this will get rid of duplicate entries.
        DSorter[Key] = [eval("CPREFName%dVar" % i).get(),
                        eval("CPREFIDs%dVar" % i).get()]
    # Clear.
    for i in arange(1, 1 + CPREF_SLOTS):
        eval("CPREFName%dVar" % i).set("")
        eval("CPREFIDs%dVar" % i).set("")
    CPREFCurrChanRVar.set("1")
    Keys = sorted(DSorter.keys())
    Slot = 1
    for Key in Keys:
        eval("CPREFName%dVar" % Slot).set(DSorter[Key][0])
        eval("CPREFIDs%dVar" % Slot).set(DSorter[Key][1])
        Slot += 1
    formCPREFCurrName()
    return
#########################
# BEGIN: formCPREFClean()
# FUNC:formCPREFClean():2018.310


def formCPREFClean():
    for i in arange(1, 1 + CPREF_SLOTS):
        eval("CPREFName%dVar" % i).set(eval("CPREFName%dVar" %
                                            i).get().strip().upper())
        Str = eval("CPREFIDs%dVar" % i).get()
        Str = Str.replace(" ", "")
        Str = Str.upper()
        Str = deComma(Str)
        # FINISHME - should probably look for anything other than letters,
        # numbers and commas
        eval("CPREFIDs%dVar" % i).set(Str)
    formCPREFCurrName()
    return
#################################
# BEGIN: formCPREFControl(Action)
# FUNC:formCPREFControl():2018.352


def formCPREFControl(Action):
    if Action == "close":
        formCPREFCurrName()
        saveChanPrefsSetups()
        formClose("CPREF")
    # For the Save button and others to use.
    elif Action == "save":
        saveChanPrefsSetups()
    return
# END: formCPREF


##########################################
# BEGIN: formFind(Who, WhereMsg, e = None)
# LIB:formFind():2018.236
#   Implements a "find" function in a form's Text() field.
#   The caller must set up the global Vars:
#      <Who>FindLookForVar = StringVar()
#      <Who>FindLastLookForVar = StringVar()
#      <Who>FindLinesVar = StringVar()
#      <Who>FindIndexVar = IntVar()
#      <Who>FindUseCaseCVar = IntVar()
#      <Who>FindReplaceWithVar = StringVar()
#   <Who> must be the string "INE", "CKTRD", etc.
#   Then on the form set up an Entry field and two Buttons like:
#
#   LEnt = Entry(Sub, width = 20, textvariable = <Who>FindLookForVar)
#   LEnt.pack(side = LEFT)
#   LEnt.bind("<Return>", Command(formFind, "<Who>", "<Who>"))
#   LEnt.bind("<KP_Enter>", Command(formFind, "<Who>", "<Who>"))
#   BButton(Sub, text = "Find", command = Command(formFind, "<Who>", \
#           "<Who>")).pack(side = LEFT)
#   BButton(Sub, text = "Next", command = Command(formFindNext, "<Who>", \
#           "<Who>")).pack(side = LEFT)
#   Checkbutton(Sub, text = "Use Case", \
#           variable = <Who>FindUseCaseCVar).pack(side = LEFT)
#   Entry(Sub, width = 20, \
#           textvariable = <Who>FindReplaceWithVar).pack(side = LEFT)
#   BButton(SSSub, text = "<Replace With", command = Command(formFindReplace, \
#           <Who>, <Who>)).pack(side = LEFT)
#
#   Must be defined even if it not used.
#      <Who>FindUseCaseCVar = IntVar()
#   Does not need to be defined if there is no replacing going on.
#      <Who>FindReplaceWithVar = StringVar()
#
def formFind(Who, WhereMsg, e=None):
    if WhereMsg is not None:
        setMsg(WhereMsg, "CB", "Finding...")
    LTxt = PROGTxt[Who]
    Case = eval("%sFindUseCaseCVar" % Who).get()
    if Case == 0:
        LookFor = eval("%sFindLookForVar" % Who).get().lower()
    else:
        LookFor = eval("%sFindLookForVar" % Who).get()
    LTxt.tag_delete("Find%s" % Who)
    LTxt.tag_delete("FindN%s" % Who)
    if len(LookFor) == 0:
        eval("%sFindLinesVar" % Who).set("")
        eval("%sFindIndexVar" % Who).set(-1)
        if WhereMsg is not None:
            setMsg(WhereMsg)
        return 0
    Found = 0
    # Do this in case there is a lot of text from any previous find, otherwise
    # the display just sits there.
    updateMe(0)
    eval("%sFindLastLookForVar" % Who).set(LookFor)
    eval("%sFindLinesVar" % Who).set("")
    eval("%sFindIndexVar" % Who).set(-1)
    FindLines = ""
    N = 1
    while True:
        if len(LTxt.get("%d.0" % N)) == 0:
            break
        if Case == 0:
            Line = LTxt.get("%d.0" % N, "%d.0" % (N + 1)).lower()
        else:
            Line = LTxt.get("%d.0" % N, "%d.0" % (N + 1))
        C = 0
        try:
            # Keep going through here until we run out of Line to search.
            while True:
                Index = Line.index(LookFor, C)
                TagStart = "%d.%d" % (N, Index)
                C = Index + len(LookFor)
                TagEnd = "%d.%d" % (N, C)
                LTxt.tag_add("Find%s" % Who, TagStart, TagEnd)
                LTxt.tag_config("Find%s" % Who, background=Clr["U"],
                                foreground=Clr["W"])
                FindLines += " %s,%s" % (TagStart, TagEnd)
                Found += 1
        except Exception:
            pass
        N += 1
    if Found == 0:
        if WhereMsg is not None:
            setMsg(WhereMsg, "", "No matches found.")
    else:
        eval("%sFindLinesVar" % Who).set(FindLines)
        formFindNext(Who, WhereMsg, True, True)
        if WhereMsg is not None:
            setMsg(WhereMsg, "", "Matches found: %d" % Found)
    return Found
#################################################
# BEGIN: formFindReplace(Who, WhereMsg, e = None)
# FUNC:formFindReplace():2018.236
#   A Simple-minded find and replace function.
#   If WhereMsg is None then the caller is responsible for displying all
#   messages.


def formFindReplace(Who, WhereMsg, e=None):
    if WhereMsg is not None:
        setMsg(WhereMsg, "CB", "Replacing...")
    LTxt = PROGTxt[Who]
    LookFor = eval("%sFindLookForVar" % Who).get()
    ReplaceWith = eval("%sFindReplaceWithVar" % Who).get()
    # These come in as straight chars ("\" + "n"). Convert them to real \n's.
    if ReplaceWith.endswith("\\n"):
        ReplaceWith = "%s\n" % ReplaceWith[:-2]
    LTxt.tag_delete("Find%s" % Who)
    LTxt.tag_delete("FindN%s" % Who)
    updateMe(0)
    # FindLines = ""
    N = 1
    Found = 0
    while True:
        if len(LTxt.get("%d.0" % N)) == 0:
            break
        Line = LTxt.get("%d.0" % N, "%d.0" % (N + 1))
        if Line.find(LookFor) != -1:
            Line = Line.replace(LookFor, ReplaceWith)
            LTxt.delete("%d.0" % N, "%d.0" % (N + 1))
            LTxt.insert("%d.0" % N, Line)
            Line = LTxt.get("%d.0" % N, "%d.0" % (N + 1))
            TagStart = "%d.%d" % (N, Line.find(ReplaceWith))
            TagEnd = "%d.%d" % (N, Line.find(ReplaceWith) + len(ReplaceWith))
            LTxt.tag_add("Find%s" % Who, TagStart, TagEnd)
            LTxt.tag_config("Find%s" % Who, background=Clr["U"],
                            foreground=Clr["W"])
            Found += 1
        N += 1
    if Found > 0:
        chgPROGChgBar(WhereMsg, 1)
    if WhereMsg is not None:
        setMsg(WhereMsg, "GB", "Done. Replaced: %d" % Found)
    return Found
######################################################
# BEGIN: formFindReplaceTrunc(Who, WhereMsg, e = None)
# FUNC:formFindReplaceTrunc():2018.236
#   A Simple-minded find and replace function that finds LookFor, truncates
#   that line, then appends ReplaceWith in its place.


def formFindReplaceTrunc(Who, WhereMsg, e=None):
    if WhereMsg is not None:
        setMsg(WhereMsg, "CB", "Replacing...")
    LTxt = PROGTxt[Who]
    LookFor = eval("%sFindLookForVar" % Who).get()
    ReplaceWith = eval("%sFindReplaceWithVar" % Who).get()
    # These come in as straight chars ("\" + "n"). Convert them to real \n's.
    if ReplaceWith.endswith("\\n"):
        ReplaceWith = "%s\n" % ReplaceWith[:-2]
    LTxt.tag_delete("Find%s" % Who)
    LTxt.tag_delete("FindN%s" % Who)
    updateMe(0)
    # FindLines = ""
    N = 1
    Found = 0
    while True:
        if len(LTxt.get("%d.0" % N)) == 0:
            break
        Line = LTxt.get("%d.0" % N, "%d.0" % (N + 1))
        if Line.find(LookFor) != -1:
            Index = Line.index(LookFor)
            Line = Line[:Index] + ReplaceWith
            LTxt.delete("%d.0" % N, "%d.0" % (N + 1))
            LTxt.insert("%d.0" % N, Line)
            Line = LTxt.get("%d.0" % N, "%d.0" % (N + 1))
            TagStart = "%d.%d" % (N, Line.find(ReplaceWith))
            TagEnd = "%d.%d" % (N, Line.find(ReplaceWith) + len(ReplaceWith))
            LTxt.tag_add("Find%s" % Who, TagStart, TagEnd)
            LTxt.tag_config("Find%s" % Who, background=Clr["U"],
                            foreground=Clr["W"])
            Found += 1
        N += 1
    if Found > 0:
        chgPROGChgBar(WhereMsg, 1)
    if WhereMsg is not None:
        setMsg(WhereMsg, "GB", "Done. Replaced: %d" % Found)
    return Found
###########################################################################
# BEGIN: formFindNext(Who, WhereMsg, Find = False, First = False, e = None)
# FUNC:formFindNext():2014.069


def formFindNext(Who, WhereMsg, Find=False, First=False, e=None):
    LTxt = PROGTxt[Who]
    LTxt.tag_delete("FindN%s" % Who)
    FindLines = eval("%sFindLinesVar" % Who).get().split()
    if len(FindLines) == 0:
        beep(1)
        return
    # Figure out which line we are at (at the top of the Text()) and then go
    # through the found lines and find the closest one. Only do this on the
    # first go of a search and not on "next" finds.
    if First is True:
        Y0, Dummy = LTxt.yview()
        AtLine = LTxt.index("@0,%d" % Y0)
        # If we are at the top of the scroll then just go on normally.
        if AtLine > "1.0":
            AtLine = intt(AtLine)
            Index = -1
            Found = False
            for Line in FindLines:
                Index += 1
                Line = intt(Line)
                if Line >= AtLine:
                    Found = True
                    break
            # If the current position is past the last found item just let
            # things happen normally (i.e. jump to the first item found).
            if Found is True:
                eval("%sFindIndexVar" % Who).set(Index - 1)
    Index = eval("%sFindIndexVar" % Who).get()
    Index += 1
    try:
        Line = FindLines[Index]
        eval("%sFindIndexVar" % Who).set(Index)
    except IndexError:
        Index = 0
        Line = FindLines[Index]
        eval("%sFindIndexVar" % Who).set(0)
    # Make the "current find" red.
    TagStart, TagEnd = Line.split(",")
    LTxt.tag_add("FindN%s" % Who, TagStart, TagEnd)
    LTxt.tag_config("FindN%s" % Who, background=Clr["R"],
                    foreground=Clr["W"])
    LTxt.see(TagStart)
    # If this is the first find just let the caller set a message.
    if Find is False:
        setMsg(WhereMsg, "", "Match %d of %d." % (Index + 1, len(FindLines)))
    return
# END: formFind


###################################
# BEGIN: formFONTSZ(Parent, Format)
# LIB:formFONTSZ():2019.051
#   Displays the current font sizes and allows the user to enter new ones.
#   Includes the BIGGER and smaller font stepper functions.
PROGFrm["FONTSZ"] = None
FONTSZPropVar = StringVar()
FONTSZMonoVar = StringVar()
PROGSetups += ["PROGPropFontSize", "PROGMonoFontSize"]


def formFONTSZ(Parent, Format):
    if showUp("FONTSZ"):
        return
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    LFrm = PROGFrm["FONTSZ"] = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "CAL"))
    LFrm.title("Set Font Sizes")
    Label(LFrm, text="The current font sizes are shown.\nEnter new values and "
                     "click the Set button.\nSome systems may use positive "
                     "integers, and\nsome may use negative integers. "
                     "Experiment.\n(Some systems may not have some font "
                     "sizes.)").pack(side=TOP)
    FONTSZPropVar.set("%s" % PROGPropFont["size"])
    FONTSZMonoVar.set("%s" % PROGMonoFont["size"])
    # In case different programs have more or fewer fonts to control.
    if Format == 0:
        Sub = Frame(LFrm)
        labelEntry2(Sub, 11, "Proportional Font: ", 35,
                    "Generally for buttons and labels and dialog box "
                    "messages.", FONTSZPropVar, 6)
        Sub.pack(side=TOP)
        Sub = Frame(LFrm)
        labelEntry2(Sub, 11, "Mono-Spaced Font: ", 35,
                    "Generally for text display fields.", FONTSZMonoVar, 6)
        Sub.pack(side=TOP)
    Sub = Frame(LFrm)
    BButton(Sub, text="Set", command=formFONTSZGo).pack(side=LEFT)
    BButton(Sub, text="Close", fg=Clr["R"],
            command=Command(formClose, "FONTSZ")).pack(side=LEFT)
    Sub.pack(side=TOP, padx=3, pady=3)
    center(Parent, LFrm, "C", "I", True)
    return
#######################
# BEGIN: formFONTSZGo()
# FUNC:formFONTSZGo():2019.049


def formFONTSZGo():
    NewProp = intt(FONTSZPropVar.get())
    NewMono = intt(FONTSZMonoVar.get())
    if NewProp != PROGPropFont["size"]:
        PROGPropFontSize.set(NewProp)
        fontSetSize()
    if NewMono != PROGMonoFont["size"]:
        PROGMonoFontSize.set(NewMono)
        fontSetSize()
    return
#####################
# BEGIN: fontBigger()
# FUNC:fontBigger():2012.069


def fontBigger():
    PSize = PROGPropFont["size"]
    if PSize < 0:
        PSize -= 1
    else:
        PSize += 1
    MSize = PROGMonoFont["size"]
    if MSize < 0:
        MSize -= 1
    else:
        MSize += 1
    # If either font is at the limit don't change the other one or they will
    # get 'out of synch' with each other (like if the original prop font size
    # was 12 and the mono font size was 10).
    if abs(PSize) > 16 or abs(MSize) > 16:
        beep(1)
        return
    PROGPropFontSize.set(PSize)
    PROGMonoFontSize.set(MSize)
    fontSetSize()
    return
######################
# BEGIN: fontSmaller()
# FUNC:fontSmaller():2012.067


def fontSmaller():
    PSize = PROGPropFont["size"]
    if PSize < 0:
        PSize += 1
    else:
        PSize -= 1
    MSize = PROGMonoFont["size"]
    if MSize < 0:
        MSize += 1
    else:
        MSize -= 1
    if abs(PSize) < 6 or abs(MSize) < 6:
        beep(1)
        return
    PROGPropFontSize.set(PSize)
    PROGMonoFontSize.set(MSize)
    fontSetSize()
    return
######################
# BEGIN: fontSetSize()
# FUNC:fontSetSize():2019.049


def fontSetSize():
    global PROGPropFontHeight
    PROGPropFont["size"] = PROGPropFontSize.get()
    PROGMonoFont["size"] = PROGMonoFontSize.get()
    PROGPropFontHeight = PROGPropFont.metrics("ascent") + \
        PROGPropFont.metrics("descent")
    # For any additional stuff a program may need to do.
    if "formFONTSZGoLocal" in globals():
        formFONTSZGoLocal()
    return
# END: formFONTSZ


##################
# BEGIN: formGPS()
# FUNC:formGPS():2016.195
PROGFrm["GPS"] = None
GPSPoints = []
GPSPOINTS_DT = 0
GPSPOINTS_FIX = 1
GPSPOINTS_SATS = 2
GPSPOINTS_LATI = 3
GPSPOINTS_LONG = 4
GPSPOINTS_ELEV = 5
GPSRunning = False


def formGPS():
    global GPSRunning
    if showUp("GPS") is True:
        return
    LFrm = PROGFrm["GPS"] = Toplevel(Root)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formGPSControl, "close"))
    LFrm.title("GPS Data Plotter")
    LFrm.iconname("GPS")
    Sub = Frame(LFrm)
    LCan = PROGCan["GPS"] = Canvas(LFrm, height=400, width=400,
                                   bg=DClr["GS"])
    LCan.pack(side=TOP)
    Sub = Frame(LFrm)
    BButton(Sub, text="Read/Plot", command=Command(formGPSRead,
                                                   "read")).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Export", command=formGPSExport).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Close", fg=Clr["R"],
            command=Command(formClose, "GPS")).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    LLb = Label(Sub, text="Hints ")
    LLb.pack(side=LEFT)
    ToolTip(LLb, 30, "--Normal/Left-click on dot: Clicking on a dot shows the "
            "information for that dot.\n\n--Other/Right-click on dot: Right-"
            "clicking on a dot removes that dot from the plot and replots all "
            "of the remaining points. Use this to remove outliers.")
    Sub.pack(side=TOP, pady=3)
    PROGMsg["GPS"] = Text(LFrm, font=PROGPropFont, height=2, wrap=WORD)
    PROGMsg["GPS"].pack(side=TOP, fill=X)
    center(Root, LFrm, "CX", "I", True)
    GPSRunning = False
    return
#############################
# BEGIN: formGPSClear(Action)
# FUNC:formGPSClear():2017.205


def formGPSClear(Action):
    global GPSPoints
# The form may not be up.
    try:
        PROGFrm["GPS"].title("GPS Data Plotter")
        LCan = PROGCan["GPS"]
        LCan.delete(ALL)
        LCan.configure(bg=DClr["GS"])
    except AttributeError:
        pass
    if Action == "read":
        del GPSPoints[:]
    setMsg("GPS", "", "")
    return
########################
# BEGIN: formGPSExport()
# FUNC:formGPSExport():2014.239


def formGPSExport():
    if GPSRunning is True:
        beep(1)
        return
    if len(GPSPoints) == 0:
        setMsg("GPS", "RW", "There are no points to export.", 2)
        return
    setMsg("GPS", "CB", "Exporting...")
    # The names may have naughty characters in them.
    File = cleanAFilename(QPFilesProcessed[0] + "gps.dat")
    try:
        Fp = open(PROGDataDirVar.get() + File, "w")
    except Exception:
        setMsg("GPS", "MW", "Error opening file\n   %s" % File, 3)
        return
    try:
        Fp.write("# GPS data points for %s\n" % list2Str(QPFilesProcessed))
        for Point in GPSPoints:
            Fp.write("%s\t%s\t%d\t%+f\t%+f\t%f\n" %
                     (Point[GPSPOINTS_DT], Point[GPSPOINTS_FIX],
                      Point[GPSPOINTS_SATS], Point[GPSPOINTS_LATI] - 90.0,
                      Point[GPSPOINTS_LONG] - 180.0, Point[GPSPOINTS_ELEV]))
        Fp.close()
    except Exception:
        setMsg("GPS", "MW", "Error writing to file\n   %s" % File, 3)
        return
    setMsg("GPS", "GB", "GPS data exported to Main Data Dir\n   %s" % File, 1)
    return
############################
# BEGIN: formGPSRead(Action)
# FUNC:formGPSRead():2019.024


def formGPSRead(Action):
    global GPSPoints
    global GPSRunning
    if GPSRunning is True:
        beep(1)
        return
    setMsg("GPS", "CB", "Plotting...")
    LCan = PROGCan["GPS"]
    formGPSClear(Action)
    GPSRunning = True
    Found = 0
    MinLati = maxFloat
    MaxLati = -maxFloat
    MinLong = maxFloat
    MaxLong = -maxFloat
    if Action == "read":
        if len(QPLogs) == 0:
            setMsg("GPS", "RW", "There are no LOG messages to read.", 2)
            GPSRunning = False
            return
        # Don't list all of the files.
        Filename = QPFilesProcessed[0]
        if len(QPFilesProcessed) > 1:
            Filename += "+"
        PROGFrm["GPS"].title(Filename)
        GPSPoints = []
        Lines = len(QPLogs)
        Index = 0
        while Index < Lines:
            Line = QPLogs[Index]
            if Line.find("GPS Status") != -1:
                Index += 1
                DateTime = "?"
                Fix = "?"
                Sats = 0
                Height = 0.0
                Lati = 0.0
                Long = 0.0
                while Index < Lines:
                    Line = QPLogs[Index].lower()
                    C = Line.find("fix type:")
                    if C != -1:
                        Fix = Line[C + 10:].strip().upper()
                        Index += 1
                        continue
                    C = Line.find("height:")
                    if C != -1:
                        Height = floatt(Line[C + 7:])
                        Index += 1
                        continue
                    C = Line.find("latitude:")
                    if C != -1:
                        Pos = Line[C + 10:]
                        Lati = floatt(Pos[0:2]) + floatt(Pos[2:]) / 60.0
                        if Pos.find("s") != -1:
                            Lati = -Lati
                        # The +90 and +180 below gets rid of the negative
                        # numbers so latitudes will always be 0 at south pole
                        # to 180 at north pole and longitudes will always be 0
                        # at Greenwich to 360 at Greenwich.
                        # Much easier to plot.
                        Lati += 90.0
                        Index += 1
                        continue
                    C = Line.find("longitude:")
                    if C != -1:
                        Pos = Line[C + 11:]
                        Long = floatt(Pos[0:3]) + floatt(Pos[3:]) / 60.0
                        if Pos.find("w") != -1:
                            Long = -Long
                        Long += 180.0
                        Index += 1
                        continue
                    C = Line.find("sat. used:")
                    if C != -1:
                        Sats = intt(Line[C + 11:])
                        Index += 1
                        continue
                    C = Line.find("last gps timemark:")
                    if C != -1:
                        DateTime = Line[C + 19:].strip()
                        # Hopefully the timemark will always be the last item.
                        if Height != 0.0 and Lati != 0.0 and Long != 0.0:
                            # FINISHME - something may need to go here to
                            # filter bad positions.
                            # Might as well determine these as we go.
                            if Lati < MinLati:
                                MinLati = Lati
                            if Lati > MaxLati:
                                MaxLati = Lati
                            if Long < MinLong:
                                MinLong = Long
                            if Long > MaxLong:
                                MaxLong = Long
                            GPSPoints.append([DateTime, Fix, Sats, Lati,
                                              Long, Height])
                            Found += 1
                        # Break out even if it is not.
                        break
                    Index += 1
            # Made up GPS messages from .nan processing.
            elif Line.find("GPSPOS:") != -1:
                # 0 1    2      3    4    5   6     7
                # d t GPSPOS: lati long elev SATS: sats
                Parts = Line.split()
                if len(Parts) < 8:
                    Index += 1
                    continue
                Fix = "?"
                Height = floatt(Parts[5])
                Ret = checkLatiLong("lati", Parts[3], 0)
                if Ret[0] != 0:
                    Index += 1
                    continue
                Lati = Ret[1]
                # The +90 and +180 below gets rid of the negative numbers so
                # latitudes will always be 0 at South Pole to 180 at North
                # Pole and longitudes will always be 0 at Greenwich to 360 at
                # Greenwich. Much easier to plot.
                Lati += 90.0
                Ret = checkLatiLong("long", Parts[4], 0)
                if Ret[0] != 0:
                    Index += 1
                    continue
                Long = Ret[1]
                Long += 180.0
                Sats = intt(Parts[7])
                DateTime = "%s %s" % (Parts[0], Parts[1])
                if Height != 0.0 and Lati != 0.0 and Long != 0.0:
                    # FINISHME - something may need to go here to filter bad
                    # positions. Might as well determine these as we go.
                    if Lati < MinLati:
                        MinLati = Lati
                    if Lati > MaxLati:
                        MaxLati = Lati
                    if Long < MinLong:
                        MinLong = Long
                    if Long > MaxLong:
                        MaxLong = Long
                    GPSPoints.append([DateTime, Fix, Sats, Lati, Long, Height])
                    Found += 1
            Index += 1
    elif Action == "replot":
        for Point in GPSPoints:
            #              0      1    2     3      4      5
            # Point = [DateTime, Fix, Sats, Lati, Long, Height]
            if Point[GPSPOINTS_LATI] < MinLati:
                MinLati = Point[GPSPOINTS_LATI]
            if Point[GPSPOINTS_LATI] > MaxLati:
                MaxLati = Point[GPSPOINTS_LATI]
            if Point[GPSPOINTS_LONG] < MinLong:
                MinLong = Point[GPSPOINTS_LONG]
            if Point[GPSPOINTS_LONG] > MaxLong:
                MaxLong = Point[GPSPOINTS_LONG]
            Found += 1
    if Found == 0:
        setMsg("GPS", "YB", "No GPS information was found.", 1)
        GPSRunning = False
        return
    # The MaxRange is going to be plotted in a square 75% of the height of the
    # canvas (which should be a square).
    PY = LCan.winfo_height()
    Buff = PY * .25 / 2.0
    PlotW = PY * .75
    Index = 0
    Plotted = 0
    # Special case, otherwise the point ends up in the lower left-hand corner.
    if Found == 1:
        XY = Buff + PlotW / 2.0
        ID = LCan.create_rectangle(
            XY - 2, XY - 2, XY + 2, XY + 2, fill=DClr["GD"])
        LCan.tag_bind(ID, "<Button-1>", Command(formGPSShowPoint, Index))
    else:
        # The plot area (no matter what shape the canvas is) is just going to
        # be the greater of maxlat x maxlat, or maxlong x maxlong for
        # simplicity, so figure out the max size in degrees.
        LatiRange = getRange(MinLati, MaxLati)
        # Just in case.
        if LatiRange == 0.0:
            LatiRange = 1.0
        # Get this back into -90 0 +90 range for the cos().
        LatiAve = (MinLati + MaxLati) / 2.0 - 90.0
        LongRange = getRange(MinLong, MaxLong)
        if LongRange == 0.0:
            LongRange = 1.0
        if LatiRange == 1.0 and LongRange == 1.0:
            canText(LCan, 10, PROGPropFontHeight, DClr["TX"], "No range.")
        else:
            canText(LCan, 10, PROGPropFontHeight, DClr["TX"],
                    "LatRange: %.2fm    LongRange: %.2fm" %
                    (LatiRange * 110574.27,
                     LongRange * 111319.458 * cos(LatiAve / 360.0 *
                                                  3.14159 * 2.0)))
        # The Nanometrics receivers that we were testing showed the same
        # position for days. Highly suspicious. Skip points that don't change.
        LastLati = -0.0
        LastLong = -0.0
        for Point in GPSPoints:
            Lati = Point[GPSPOINTS_LATI]
            Long = Point[GPSPOINTS_LONG]
            if Lati == LastLati and Long == LastLong:
                Index += 1
                continue
            LastLati = Lati
            LastLong = Long
            # We have to switch the Y zero point to the bottom of the canvas
            # (the plot area, actually) and then come back up the amount of the
            # latitude.
            YY = (Buff + PlotW) - ((Lati - MinLati) / LatiRange * PlotW)
            # These just plot in from the edge.
            XX = Buff + (Long - MinLong) / LongRange * PlotW
            # Don't make an outline so the boxes have a black border which
            # will separate them a little since they will mostly be on top of
            # each other (on black bg).
            ID = LCan.create_rectangle(XX - 2, YY - 2, XX + 2, YY + 2,
                                       fill=DClr["GD"])
            LCan.tag_bind(ID, "<Button-1>", Command(formGPSShowPoint, Index))
            if B2Glitch is True:
                LCan.tag_bind(ID, "<Button-2>", Command(formGPSZapPoint,
                                                        Index))
            LCan.tag_bind(ID, "<Button-3>", Command(formGPSZapPoint, Index))
            updateMe(0)
            Index += 1
            Plotted += 1
    setMsg("GPS", "", "Done. Plotted %d of %d." % (Plotted, Found))
    GPSRunning = False
    return
##########################################
# BEGIN: formGPSShowPoint(Index, e = None)
# FUNC:formGPSShowPoint():2014.239


def formGPSShowPoint(Index, e=None):
    if GPSRunning is True:
        beep(1)
        return
    setMsg("GPS", "",
           "Mark: %s   Fix: %s   Sats: %d\nLat: %s   Long: %s   Elev: %gm" %
           (GPSPoints[Index][GPSPOINTS_DT], GPSPoints[Index][GPSPOINTS_FIX],
            GPSPoints[Index][GPSPOINTS_SATS],
            pm2nsew("lat", GPSPoints[Index][GPSPOINTS_LATI] - 90.0),
            pm2nsew("lon", GPSPoints[Index][GPSPOINTS_LONG] - 180.0),
            GPSPoints[Index][GPSPOINTS_ELEV]))
    return
#########################################
# BEGIN: formGPSZapPoint(Index, e = None)
# FUNC:formGPSZapPoint():2014.239
#   Remove points from GPSPoints and calls for a replot. Searches for all
#   points that have the same lat/long as the one that was clicked on and
#   throws them out too.


def formGPSZapPoint(Index, e=None):
    global GPSPoints
    global GPSRunning
    if GPSRunning is True:
        beep(1)
        return
    GPSRunning = True
    # This might take a while.
    setMsg("GPS", "CB", "Plotting...")
    Lati = GPSPoints[Index][GPSPOINTS_LATI]
    Long = GPSPoints[Index][GPSPOINTS_LONG]
    NewGPSPoints = []
    for Point in GPSPoints:
        if Point[GPSPOINTS_LATI] != Lati and Point[GPSPOINTS_LONG] != Long:
            NewGPSPoints.append(Point)
    GPSPoints = deepcopy(NewGPSPoints)
    # Otherwise formGPSRead() won't go.
    GPSRunning = False
    formGPSRead("replot")
    return
###############################
# BEGIN: formGPSControl(Action)
# FUNC:formGPSControl():2016.005
#   Just for freeing up the GPSPoints.


def formGPSControl(Action):
    global GPSPoints
    global GPSRunning
    if Action == "close":
        del GPSPoints[:]
        formClose("GPS")
        GPSRunning = False
        return
# END: formGPS


HELPText = "QUICK START\n\
===========\n\
1. Start QPEEK by entering qpeek or qpeek.py or ./qpeek.py or by clicking \
on QPEEK's icon depending on the operating system of the computer and \
how the program was installed.\n\
\n\
2a. Normal mode: If this is the first time QPEEK has been run on the \
computer or in the user account a file navigation dialog will show up. \
Navigate to a directory with some data source files and click OK.\n\
\n\
2b. Antelope mode: If your intention is to run in Antelope mode and the \
file navigation box described above shows up just OK out of it. QPEEK \
defaults to the Normal mode, so select Options | AntRT, then click the \
Main Data Directory button and navigate to the directory with the \"db\" \
sub-direcotry.\n\
\n\
3. A list of data source files or stations should show up in the file \
list area.\n\
\n\
4. If this is the first time QPEEK has been run select one of the data \
source files/stations of interest in the file list area (just one click) \
and then select the menu item Commands|Scan For Channel IDs (or click the \
\"S\" button). If applicable, select data/ or sdata/ on the scan form \
if a .bms folder is to be scanned.\n\
\n\
5. Click the Scan button. The function will read through the selected \
data source directory/files and produce a button for each channel (like \
\"LHZ\") that it finds. You can either select the buttons one at a time \
and add them to the long empty field on the Scan For Channel IDs form \
in the order you want them plotted, or you can just click the Add All \
button to copy them all to the field in alphabetical order. Yellow \
buttons are channels that QPEEK does not know how to plot.\n\
\n\
6. Once the list of channels has been made click the Copy button. This \
puts the list of channels on a 'clipboard' in QPEEK.\n\
\n\
7. Click the Channel Preferences button on the scan form. On the form \
that comes up select one of the radiobuttons next to the Name labels \
(an empty line, or a previously used one) and click the Paste button at \
the bottom of the form. This will place the list of channels into the \
selected slot.\n\
\n\
8. Now go back to the main display and double-click the same data source \
file that was scanned, or click the Read button, to read and plot the \
data.\n\
\n\
Once you have \"standard\" sets of channel IDs in the Channel \
Preferences form you don't need to scan files anymore. If there are \
different sets of channels for different sets of stations (like POLENET or \
GLISN stations, as an example) or sets of equipment (like Guralp \
or Streckeisen stations) you just put together different lists of \
channels for those, select the right set for the data you are going to \
read, and then Read the data. If there are channels in the list that \
are not in the data, or vice versa, it won't matter. Messages about \
that will show up in the Errors/Messages display. You can then just alter \
the list of channels in use as needed and Read again.\n\
\n\
\n\
DESCRIPTION\n\
===========\n\
QPEEK is a graphical data visualizer program for examining the data files \
produced by Quanterra Q330 recorders and programs used to retrieve data \
from Quanterra Balers. QPEEK will read data from B44 baler memory sticks, \
.ALL and .SOH files produced by programs like EzBaler, and files that \
were created by splitting the data using a program like sdrsplit. QPEEK \
will also read data offloaded from the SD cards recorded by Nanometrics \
TitanSMA units. Reading these data sources is done when QPEEK is in the \
\"Normal\" mode. When it is switched to the \"AntRT\" mode (Antelope \
Realtime) QPEEK will read Antelope database data.\n\
\n\
Starting QPEEK is dependent on the operating system of the computer it is \
running on, and how it was installed. It may be by entering qpeek or \
qpeek.py on the command line, or by double-clicking on an icon.\n\
\n\
The first time QPEEK is started in an account it will request an initial \
directory to look in for data source files and sub-directories. This, and \
all of the program's settings, will be saved when QPEEK is stopped, so this \
should not need to be done on subsequent starts.\n\
\n\
Since any combination of channels may be in a data source file or \
directory QPEEK must be told which channels to look for and plot. This \
can be done by first scanning the data source that is going to be plotted \
using the Commands | Scan For Channel IDs function. The list of channels \
it produces can then be copied and pasted into a slot on the Commands \
| Channel Preferences form. This list will tell QPEEK which channels to \
look for. This list can be use when reading data files from any similarly \
programmed station/recorder. The scanning does not need to be done for each \
individual recorder.\n\
\n\
\n\
DATA SOURCE FILE FORMATS\n\
========================\n\
There are two different operating modes; Normal and AntRT (Antelope \
Realtime). The Normal mode reads files and directories from Quanterra \
Q330 and Nanometrics recorders. The AntRT mode reads the seed/mseed \
files created by Antelope. The operating mode is selected in the Options \
menu.\n\
\n\
Baler Memory Sticks (Normal mode)\n\
---------------------------------\n\
The data on USB memory sticks writtin by Quanterra B44s can be read by \
QPEEK if the files and directories on a memory stick are dragged to a \
folder/directory that ends in .bms. A folder named WHIT1.bms, for \
example, would be created and then the contents of the first memory \
stick from the station WHIT could be placed in there. The contents of a \
second stick could be placed in WHIT2.bms. WHIT1.bms and WHIT2.bms would \
then show up in the file list when QPEEK is pointed at the parent directory \
where the two folders are located.\n\
\n\
The B44's usually record a data/ and an sdata/ directory. Which directory \
gets read when a .bms folder is selected can be controlled using the '.bms \
Dir' radiobuttons on the main display.\n\
\n\
.ALL And .SOH Files (Normal mode)\n\
---------------------------------\n\
These types, usually produced by EzBaler or some such program, can be read \
directly by QPEEK without renaming them or placing them in a specially named \
folder or sub-directory. There names must just end with .all or .soh.\n\
\n\
Files From sdrsplit (Normal mode)\n\
---------------------------------\n\
If, for example, a .ALL or .SOH file is split up using a program like \
sdrsplit into a file for each channel these can be read by QPEEK if they \
are all placed into a folder/directory whose name ends with .sdr.\n\
\n\
This method can also be used when reading real time data files created \
by Antelope. If each file is a separate channel in something like a \
directory for each day, then the day directory need only be renamed \
so it ends with .sdr and QPEEK will be able to read the data for the \
day. If there is data from more than one station in the day directory \
the Station field on the main display can be used to filter out and \
plot data from only one of them.\n\
\n\
Files From An Antelope Archive (Normal mode)\n\
--------------------------------------------\n\
Data files created by Antelope can be read using the .sdr extension, but \
only if the individual files are one channel per file. If the files are \
multiplexed, then the .ant extension should be used for the day directories \
in the example above.\n\
\n\
Files From Nanometrics Sources (Normal mode)\n\
--------------------------------------------\n\
I only have access to data from a few of these units, so things may still \n\
need to be changed.\n\
\n\
Generally, Nanometrics recorders write seismic data to seed/mini-seed \
files, and State Of Health (SOH) information to ASCII Comma Separated \
Values (CSV) files. QPEEK will read through directories whose names \
end with .nan and plot the information from those two file types.\n\
\n\
GPS postion information, if found in the CSV files, will be converted to \
LOG file lines and displayed in the LOG Messages window so their values \
can be plotted using the Commands | GPS Data Plotter function. The \
positions are recorded every minute, which for seismic work is a bit \
excessive. QPEEK only generates a LOG line once every 30 minutes.\n\
\n\
Newer Nanometrics units can write the SOH information to miniseed files, \
instead of to the CSV files. QPEEK can decode that information as well. \
Most of the channels that are made up from the CSV files and the channels \
recorded to miniseed have different channel names in QPEEK, eventhough \
they may be the same data point. Which ones get plotted can be controlled \
by changing the channel preferences list. In some cases duplicate points \
may be plotted if both miniseed and CSV recording is enabled in the \
recorder.\n\
\n\
GPS position information recorded to the miniseed SOH files will be \
converted to LOG lines, instead of being plotted, just like the position \
information in the CSV files.\n\
\n\
Antelope Realtime Databases (AntRT mode)\n\
----------------------------------------\n\
Reading Antelope databases requires using the AntRT operating mode. This \
gives QPEEK a chance at figuring out what it is reading when there are \
no file or directory extentions, like .ant. When operating in this mode \
QPEEK must be pointed at the directory that contains the \"db\" (NOT \
inside the db directory), and the directory structure must be\n\
\n\
   [db dir]/YEAR/DOY/(data files)\n\
\n\
The data files must be one channel per file. If there are multiple \
stations the first part of the data file names must be\n\
\n\
   [station].(usually net code).?.?.year?.doy?\n\
\n\
The only part of the file name QPEEK looks at will be the station name \
before the first period. The YEAR directories must be 4-digit numbers, \
and the DOY directories must be 3-digit numbers.\n\
\n\
The program will display YYYY-Station in the file list when in this \
mode.\n\
\n\
Data Collected From Q330s by SeisComp3 (SC3 mode)\n\
-------------------------------------------------\n\
THIS IS CURRENTLY OFFLINE FOR TESTING.\n\
\n\
Data in a SeisComp archive basically match the miniseed files that can \
be offloaded from a Q330 baler. The only [test] system at PASSCAL has \
archive arranged like\n\
\n\
    ...archive/Year/NetworkCode/StationID/Channels/data files\n\
\n\
The Option menu item for SeisComp3 should be selected and then the \
Main Data Directory should be set to the Year directory. The \
Year/NetCode/StaID will be shown in the file selection list.\n\
\n\
\n\
TO READ OR NOT TO READ\n\
======================\n\
To see some settings take effect a source data file will need to be read \
or re-read, while other settings will only need the display to be replotted. \
Some of the tooltips indicate (Read), (Read or Replot), or (Replot) to \
indicate what needs to be done. The Detect Gaps checkbutton tooltip \
indicates (Read or Replot). This means that if a file is read with \
gap detection turned off you cannot just turn it on and Replot to display \
the gaps. The file must be re-Read. If it is selected when the file is read \
then it can be turned off and it will not show up in a Replot. It can then \
subsequently be turned back on and show up again.\n\
\n\
Items that will change with a Replot:\n\
   Options | MP Coloring...\n\
   Options | Show YYYY... (any of the date formats)\n\
   Magnify radiobuttons\n\
   Background color radiobuttons\n\
   Gap Len value\n\
   Removing channels from a channel list\n\
\n\
Items that need source data to be (re)Read:\n\
   Options | Whack Antelope Spikes\n\
   Station name filtering\n\
   Changing to a different channel list, or adding channels to the\n\
       current list\n\
   Changing .bms Dir radiobuttons\n\
   Detect Gaps checkbutton (maybe - see above)\n\
   Turning on TPS plot\n\
\n\
\n\
DATE FORMATS\n\
============\n\
QPEEK understands the date as\n\
   YYYY-MM-DD like 2012-11-10\n\
   YYYY:DOY like 2012:315\n\
   YYYYMMMDD like 2012NOV10\n\
\n\
Text messages from the Q330 seem to use the YYYY-MM-DD format so it is the \
default. Changing how QPEEK displays the dates it displays can be changed \
with the Options menu items.\n\
\n\
\n\
COLORS\n\
======\n\
Categories of items that are plotted using one color (i.e. not items \
like mass positions) use the same color for the lines and points when \
on a black background. Most of the items are plotted with black or \
dark blue on a white background since those are about the only colors \
that show up well.\n\
\n\
- Raw data is green.\n\
- Electrical current readings are red.\n\
- Voltages are green.\n\
- Temperatures are cyan (blue).\n\
- Counts of things (bytes, reboots, etc.), control signal values,\n\
  amount of buffers used, etc. are white.\n\
- Realtime communications-related items are orange.\n\
- Time or timing-related values (data and communications) are\n\
  yellow.\n\
\n\
\n\
MAIN DISPLAY BUTTONS AND FIELDS\n\
===============================\n\
Main Data Directory Button\n\
--------------------------\n\
Click this item to open a directory selection dialog and use it to \
navigate to a folder/directory where .bms/.ALL/.SOH/.sdr/.nan data source \
files/folders/directories are located. When the OK button on the \
directory selector is clicked the file list area will be filled in \
with the data source files and folders that QPEEK understands.\n\
\n\
A directory path may also be entered directly into the file path field \
and the Return key pressed to change directories, or to reload the list \
of files.\n\
\n\
From And To Date Fields\n\
-----------------------\n\
Dates can be entered in these fields that will restrict the reading of \
data source files to just those two dates from 00:00 on the From date \
to 24:00 on the To date. The date can be entered in any of the formats \
described in the DATE FORMATS section.\n\
\n\
The C buttons open a calendar whose dates can be clicked on to select \
a data for each field.\n\
\n\
The File Selection List\n\
-----------------------\n\
The file selection list displays all of the data format types that QPEEK \
knows how to deal with based on the file extensions (.bms, .all, .SOH, \
.srd, .ant and .nan) of the names of the files and directories in the \
Main Data Directory in the \"Normal\" operating mode. In the \"AntRT\" \
mode YYYY-Station will be displayed. In the SeisComp3 mode the \
Year/NetCode/StaID will be shown.\n\
\n\
The Options | Calculate File Sizes menu item will toggle showing the \
sizes of the sources or the number of files for each station when in \
the AntRT mode. This can sometimes take a while to calculate, so it \
can be turned off.\n\
\n\
----- Find:= Field and Clear Button\n\
Text can be placed in the Find field, the Return key pressed, and the \
list of files will be rearranged such that the items containing the \
entered text will be listed in a group at the top of the list. You \
could use this to pull out all of the \"WHIT\" station files from a \
long list of files, for example. The Clear button clears the field and \
updates the list of items.\n\
\n\
----- Reload Button\n\
Just refreshes the list of items in case items have been added to the data \
directory or renamed.\n\
\n\
Station Field\n\
-------------\n\
Sometimes there may be more than one station's information in a source \
file or directory/folder. If that happens you can enter the name of the \
station of interest in this field and records from any other station will \
be ignored. Using the Scan For Channel IDs function will list the stations \
found in a data source when it is finished scanning.\n\
\n\
This only applies to seed/mini-seed data sources. Nanometrics CSV files \
do not contain any information about which station they are information \
for, so QPEEK will read and plot all of the CSV files in its path without \
knowing which station the data belongs to even if it is correctly filtering \
the seed/mini-seed information.\n\
\n\
Replot Button\n\
-------------\n\
This can be used to redo the plots after changing the background color \
radiobuttons, changing the channel list (with the exception of adding \
channels to the current list -- that will require a re-Read), changing \
the magnification level, changing the gap detection length, changing the \
date format, or changing the mass position coloring ranges.\n\
\n\
The Replot button cannot be used when adding channels to the current \
channel list to get those channel to plot, or when changing the station \
name in the Station field, or when turning on Detect Gaps when it was \
off during the initial reading of the data source.\n\
\n\
Background: B W Radiobuttons\n\
----------------------------\n\
The background color of the main and the TPS display plot areas are \
controlled with this. The choices are \"B\"lack or \"W\"hite. Black is \
good for viewing on a computer and White is good for saving toner when \
printing. The Replot button can be used to replot if the background \
color selection is changed.\n\
\n\
Magnifiy: 1xL 2xL 4xL Radiobuttons\n\
          1xH 2xH Radiobuttons\n\
----------------------------------\n\
In 1xL mode all information is plotted in the horizontal space of the \
plotting area. 2xL and 4xL simply extend the plotting area so that \
the plots are stretched out two and four times the width of the visible \
plotting area. The horizontal scrollbar at the bottom of the plotting \
area would then be used to see all of the data.\n\
\n\
The 1xH and 2xH radiobuttons are for doubling the height of the plots.\n\
\n\
S,C Buttons, Curr: Field\n\
------------------------\n\
The S button simply opens the Scan For Channel IDs form.\n\
The C button simply opens the Channel Preferences form.\n\
\n\
The Curr: field displays the currently selected slot on the Channel \
Preferences form. The slot cannot be changed from here. It is just for \
informational purposes.\n\
\n\
.bms Dir: data/ sdata/ Radiobuttons\n\
-----------------------------------\n\
The B44 memory sticks usually have a sub-directory on them named \"data\" \
which contains all channels recorded by the Q330, and a sub-directory \
named \"sdata\" which contains only the State Of Health (SOH) channels \
of data and the low sample rate seismic data. Use these radiobuttons to \
select which directory to read data from and plot.\n\
\n\
Detect Gaps Checkbutton, Len Field\n\
----------------------------------\n\
QPEEK can be directed to detect gaps when no data from any channel was \
recorded. The gaps will appear as red bars above all other plots. The \
program splits up the range of data into 5-minute blocks (similar to \
the TPS plot) and keeps track of if there was any data point recorded \
during each 1-minute block (starting from 00:00:00 UT each day).\n\
\n\
Because of the sparse number of data points currently extracted the \
minimum gap length that can reliably be detected depends on what is \
being read. For low sample rate SOH-type data sources the minimum gap \
length may be upto 120 minutes. For any source containing 20sps data \
and up it can be between 5 and 30 minutes. 40sps and up should be able \
to always handle 1-minute gap detection.\n\
\n\
The gap time length may be entered in the Len field. It must be some \
multiple of 1 minute, and can be entered like 5m, 300s, 120m, 1h, 2h30m, \
etc. If just a number is entered it will be treated as seconds.\n\
\n\
When the Detect Gaps checkbutton is on it will, of course, slow down \
the reading of a data source a little bit since the timestamp of every \
data point will be examined. QPEEK will skip channels that are not in \
the channel list, and even skip whole files when reading a .sdr data \
source if that file's channels are not being plotted, there may be a \
lot of false alarms when just reading a few channels.\n\
\n\
----- TPS Checkbutton, Chans Field -----\n\
The TPS checkbutton controls whether or not the Time-Power-Squared \
plotting form will appear when a data source is read.\n\
\n\
The TPS plot shows the square root of the average of the sum of the \
squares of the data amplitudes over each 5-minute period of each day \
covering the time range of the data on the main display. Each \
horizontal line is one UT day. All of the requested data is plotted \
after the data source has been read, but because it can take a long \
time to plot everything replotting the information must be done \
manually using the Plot button on the TPS form. This allows you to \
quickly zoom in on an area and then redo the TPS plot without having \
to wait for it every time you zoom in or out.\n\
\n\
The Chans field on the main display is used to tell the TPS plotter which \
channels to plot. The desired seismic data channels to plot should be \
entered into this field. Multiple channels may be entered separated by a \
comma. An asterisk can be added to the end of a channel name to cover \
things like location codes. For example \"LHZ*\" will cover plotting \
channels LHZ01 and LHZ02. LHZ02 may also be entered if you just want \
that channel plotted.  If the channels have location codes and you just \
enter \"LHZ\" in the list LHZ01 and LHZ02 will not be plotted. The asterisk \
may only be added at the end of the channel name, and not, for example, \
in the middle like L*Z.\n\
\n\
Each 5-minute block is color-coded according to the average amplitude \
during the period. There are four ranges, Antarctica, Low, Medium and \
High.\n\
Antarctica:\n\
    Dark Grey/Black - there was no data for that period or the\n\
                      average really was 0.0 (not likely)\n\
          Dark blue - the average was +/-10 counts\n\
               Cyan - +/-100 counts\n\
              Green - +/-1,000 counts\n\
             Yellow - +/-10,000 counts\n\
                Red - +/-100,000 counts\n\
            Magenta - +/-1,000,000 counts\n\
   Light Grey/White - > +/-1,000,000 counts\n\
Low:\n\
    Dark Grey/Black - there was no data for that period or the\n\
                      average really was 0.0 (not likely)\n\
          Dark blue - the average was +/-20 counts\n\
               Cyan - +/-200 counts\n\
              Green - +/-2,000 counts\n\
             Yellow - +/-20,000 counts\n\
                Red - +/-200,000 counts\n\
            Magenta - +/-2,000,000 counts\n\
   Light Grey/White - > +/-2,000,000 counts\n\
Medium:\n\
    Dark Grey/Black - there was no data for that period or the\n\
                      average really was 0.0 (not likely)\n\
          Dark blue - the average was +/-50 counts\n\
               Cyan - +/-500 counts\n\
              Green - +/-5,000 counts\n\
             Yellow - +/-50,000 counts\n\
                Red - +/-500,000 counts\n\
            Magenta - +/-5,000,000 counts\n\
   Light Grey/White - > +/-5,000,000 counts\n\
High:\n\
    Dark Grey/Black - there was no data for that period or the\n\
                      average really was 0.0 (not likely)\n\
          Dark blue - the average was +/-80 counts\n\
               Cyan - +/-800 counts\n\
              Green - +/-8,000 counts\n\
             Yellow - +/-80,000 counts\n\
                Red - +/-800,000 counts\n\
            Magenta - +/-8,000,000 counts\n\
   Light Grey/White - > +/-8,000,000 counts\n\
\n\
The ranges are selected by using the \"A\", \"L\", \"M\" and \"H\" \
radiobuttons on the plot window. The Chans field is the same as the \
Chans field on the main display.\n\
\n\
Clicking along a days line will draw a box around the point clicked \
on and display the counts value during the five-minute period for that \
point on the line. It also draws a line on the main display at the same \
time as the 5-minute block of time that was clicked on.\n\
\n\
A broken dark line of boxes (a dotted line) indicates that there was no \
data for 1 or more days. This is done instead of plotting, possibly 100's, \
of lines with nothing in them. The dots in the line can be clicked on \
to see how many days were skipped.\n\
\n\
----- Read/Stop Buttons -----\n\
The Read button starts the reading of the selected source file, and \
the Stop button halts the reading of a source file. This may be \
used if it is decided that the file selected is not the correct one, \
or is too long to wait to be read.\n\
\n\
----- Write .ps Button -----\n\
This creates a PostScript file of the the contents of the main plot area. \
When selected a dialog box will allow the name of the output file to be \
selected or entered.\n\
\n\
----- Replot Button (lower right-hand corner) -----\n\
Use this button after changing the size of the window to command QPEEK \
to redraw the plots. There is too much inconsistancy between installations \
and versions of operating systems for this to be done automatically. Some \
systems command QPEEK to redraw the plots after the resizing of the \
window is finished, and some send the command many times WHILE resizing \
the window. On some systems it is easy to change this behavior, and on \
some systems it isn't. This button makes it the responsibility of the \
user to click the Redraw button when they are finished resizing the \
window.\n\
\n\
This button is the same as the other Replot button below the files list.\n\
\n\
\n\
MENU COMMANDS\n\
=============\n\
FILE MENU\n\
---------\n\
----- Delete Setups File -----\n\
Sometimes a bad setups file can cause the program to misbehave. This can \
be caused by upgrading the program, but reading the setups file created \
by an earlier version of the program, or reading the setups file copied \
from a computer with a different operating system. This function will \
delete the current setups file (whose location can be found in the Help | \
About command box) and then quit the program without saving the current \
parameters. This will reset all parameters to their default values. When \
the program is restarted a dialog box should appear saying that the setups \
file could not be found.\n\
\n\
----- Quit -----\n\
Quits QPEEK.\n\
\n\
\n\
COMMANDS MENU\n\
-------------\n\
----- Log Search -----\n\
Displays a window where a string of text may be entered. The LOG messages \
currently in the LOG Messages form will be searched for lines containing the \
entered string. The search is not case-sensitive. Just the lines matching \
the search string will be displayed. This is different from the search \
field at the bottom of the LOG Messages window.\n\
\n\
GPS Data Plotter\n\
----------------\n\
The LOG messages may contain GPS information. This function brings up a \
form that can be used to plot that information. Clicking the Read/Plot \
button will tell the function to read through all of the LOG messages \
that were read from the data source and plot the results. The plotted \
points can be clicked on to display the time, fix type, number of satellites \
used for the reading, latitude, longitude, and elevation values. The \
range of the latitude and longitude readings in meters are displayed at \
the top of the plot. These are just a simple max/min subtraction with \
the longitude corrected for latitude.\n\
\n\
Some recorders will record the same GPS position repeatedly even when the \
GPS is off. The function filters out positions that are the same more than \
once in a row. This is why the display may show something like 'Plotted \
4 of 1235.'\n\
\n\
Right-clicking on a point will delete that point from the plot to allow \
throwing out \"bad\" points. If multiple data points are plotted on top \
of each other multiple right-clicks will be required to eliminate them, \
except points will the same lat/long as the point clicked on will also \
be removed. Re-reading the data source is required to recover the deleted \
points.\n\
\n\
The Export button writes the currently read data points (all points \
read minus any deleted with the right-click) to a tab-delimited file. The \
lines have the format\n\
\n\
    # Comment about the source information file(s)\n\
    Time  Latitude  Longitude  Elevation\n\
    Time  Latitude  Longitude  Elevation\n\
    ...\n\
The file will be in the current data directory and will be named the \
same as the source information file with \"gps.dat\" appended to the \
end.\n\
\n\
TPS Form\n\
--------\n\
This opens the TPS Plot form. If a data source is read, and the TPS \
checkbutton was not checked, this can be used to open the TPS Plot form. \
After setting the channels to plot (Chans field) the Plot button on \
the TPS Plot form can then be used to generate the plots without rereading \
the data source.\n\
\n\
----- Scan For Channel IDs -----\n\
----- Channel Preferences -----\n\
Figuring out which channels to look for and/or plot is not entirely \
straightforward in QPEEK. Mostly this is because the channel naming \
convention used to record the different kinds of data, while being \
\"standardized\", is not set in stone, and in reality the channels \
can be named whatever the user wants.\n\
\n\
The Scan For Channel IDs form is a helper that will read through a data \
source file selected in the main file selection list and list all of the \
channels found as buttons on the form. A button may be clicked to add \
that channel to the entry field on the form. Once the list of channels \
is created it can be copied to the Channel Preferences form using the \
Copy button on the Scan For Channel IDs form, and pasted into a slot \
using the Paste button on the Channel Preferences form. The Add All \
button creates a list of channels using all of the channels/buttons found \
during a scan.\n\
\n\
Internally the program has a set list of three-character codes for \
channels that it knows how to deal with. This list can be seen using \
the Help | Known Channels menu item. If a channel is found in the data \
that is not in that list the button for that channel will be yellow. \
The program code will need to be altered to support channels that are \
shown in yellow.\n\
\n\
The Channel Preferences form tells QPEEK which set of channels to look \
for when reading a data source. Up to 20 different lists/slots of \
channels may be created. The list that is selected with the radiobutton \
at the left side of each slot is the one that will be applied to the \
next data source file that is read. Each list/slot can be given a name \
to make it easier to remember what the list of channels applies to. \
The name of the selected list will appear in the 'Curr:' field on the \
main display.\n\
\n\
The channels in the slots are listed like\n\
\n\
    LCQ,VCO,VEA,LHZ,LHN,LHE...\n\
\n\
The order that the channels are listed will be the order that they are \
plotted. If the order of the channels is changed the data only need be \
replotted to show the change with the exception of adding channels to the \
list. If channels are added the data source file will need to be re-Read \
so the data for the added channel(s) can be extracted.\n\
\n\
A copy of a list can be made by using the Copy button to copy the selected \
entry, then by selecting another slot's radiobutton and then using the \
Paste button.\n\
\n\
The Save button saves all of the current entries. Closing the for also \
saves all of the current channel list entries.\n\
\n\
All channel names that QPEEK \"makes up\" from information in Nanometrics \
CSV files will begin with the letter N.\n\
\n\
\n\
OPTIONS MENU\n\
------------\n\
----- Op Mode: Normal -----\n\
----- Op Mode: AntRT, db/YEAR/DOY/data -----\n\
----- Op Mode: Inside B44 Memory Stick -----\n\
----- Op Mode: Inside Nanometrics SD Card -----\n\
----- Op Mode: SeisComp3, YEAR/NetCode/StaID/Chan/data -----\n\
These are the operating modes of the program. The Normal mode is for \
reading data sources from Quanterra Q330 and Nanometrics recorders that \
have been copied to the computer's disk, and that have the appropriate \
file or folder extension.\n\
\n\
The AntRT mode is for reading Antelope realtime databases with the \
structure\n\
\n\
   db/YEAR/DOY/dataFiles\n\
\n\
The Inside B44 Memory Stick mode is just that. You may navigate to the root \
directory of the memory stick and not copy the stick to a .bms folder. \
Reading will be slower.\n\
\n\
The Inside Nanometrics SD Card setting should be selecting when trying \
to directly read an SC Card without copying the contents to a .nan folder. \
QPEEK should be set to the root directory of the card where the YYYY year \
directories are located and not the directory \"above\" it as in most \
other cases. \"Nano Card\" will be displayed in the data source list. \
Select it, and/or double-click on it for scanning and reading just like \
a normal filename or station name in the other modes.\n\
\n\
The SeisComp3 mode is for reading Q330 data collected by SeisComp3. The \
format of the directrories must be\n\
\n\
    ...YEAR/NetCode/StaID/Chan/data\n\
\n\
----- Show Seismic Data In Counts -----\n\
Shows the max and min values for seismic data plot in counts, instead \
of converting them to volts.\n\
\n\
This option is still under development.\n\
\n\
----- MP Coloring: x.x, x.x, x.x, x.x -----\n\
These items are used to set the voltage ranges and values for coloring \
mass position plots. The x.x values are in volts. The first range of \
settings are for \"regular\" broadband sensors (Guralp, Streckheisen) \
and the second set is for Trillium sensors.\n\
\n\
----- Show YYYY-MM-DD Dates -----\n\
----- Show YYYYMMMDD Dates -----\n\
----- Show YYYY:DOY Dates ----\n\
Selecting one of these menu items will tell QPEEK to display all of \
the dates in the selected format, except for the actual SOH messages. \
They will always be in the YYYY-DD-MM format used by the Q330 and \
Nanometrics recorders.\n\
\n\
----- Calculate File Sizes -----\n\
The sizes of the files and folders/directories listed in the file \
selection list can have their sizes displayed. This is off by default, \
because calculating the sizes of all of the files listed can take a \
significant amount of time. Each \"file\" can contain 1000's of files \
each depending on the type.\n\
\n\
When in the AntRT operaing mode the number after the station name in the \
files list area will be the number of files found for each year-station.\n\
\n\
This defaults to 'off' every time QPEEK is started.\n\
\n\
----- Sort Data By Time After Read -----\n\
Sometimes the data is so screwed up it can't even be viewed. This might \
help, especially if the file names for .bms, .sdr, .nan or .ant sources are \
not named such that they sort correctly. QPEEK gets a list of files in \
those directories, SORTS THEM, and then processes and plots them. This \
could cause the data to be jumbled (though probably not in .sdr sources \
since they are supposed to be one channel per file and one file per \
channel). Using this setting will, of course, make overlaps go away, \
which might be a bad thing.\n\
\n\
This defaults to 'off' every time QPEEK is started.\n\
\n\
----- Whack Antelope Spikes -----\n\
Antelope can be configured to insert large values into a data stream \
when data is missing. It's very annoying, because the spkies will show \
up when plotting, but everything else will be flatlined. Selecting this \
option will detect data points that have this large value (2^28) \
and simply ignore them which will create a gap in the data which is \
what you would really want to see.\n\
\n\
This defaults to 'off' every time QPEEK is started.\n\
\n\
----- Supress Unhandled Blockette Messages -----\n\
The SEED Standard comes in many forms. Some recorders may include \
blockettes that QPEEK doesn't do anything with or know how to do anything \
with. Selecting this keeps messages about those blockettes out of the \
error messages section if the number of messages is excessive.\n\
\n\
----- Set Font Sizes -----\n\
Brings up a form so the proportional and mono-spaced font sizes can be \
changed. Some systems use positive integers and some use negative numbers. \
The user will need to experiment. A larger number value is always a \
larger font.\n\
\n\
Most systems do not support all numerical values, so a value may be \
changed with no resulting change in size of the displayed fonts.\n\
\n\
----- List Each File's Channels -----\n\
Sometimes the scanning for channel IDs routine doesn't find channels \
that show up as being found, but not in the channels list when reading \
the data files. Turning this on will list each file that was read and \
which channel IDs that were in the file when scanning an/or when reading \
and plotting the data. The listing will come out after all of the files \
have been read. This is only for troubleshooting and only works for \
certain data source files.\n\
\n\
----- Debug -----\n\
This option just prints out extra information whilt the program is \
running and is just for troubleshooting. The program may or may not \
crash while this is selected, since what gets printed varies based on \
what in the program is being debugged.\n\
\n\
\n\
FORMS MENU\n\
----------\n\
The currently open forms of the program will show up here. Selecting one \
will bring that form to the foreground.\n\
\n\
\n\
HELP MENU\n\
---------\n\
----- Known Channels -----\n\
This lists the 3-letter SEED codes that the program knows how to extract \
and plot. Changing this list is not difficult (for me), but requires \
changes to the program's code.\n\
\n\
----- Calendar -----\n\
Just a built-in calendar that shows dates or DOY values for three months \
in a row. Of course the clock of the computer that is running QPEEK must \
be correct for this to show the right current date.\n\
\n\
----- Check For Updates -----\n\
If the computer is connected to the Internet selecting this function will \
contact PASSCAL's website and check the version of the program against \
a copy of the program that is on the website. Appropriate dialog boxes \
will be shown depending on the result of the check.\n\
\n\
If the current version is old the Download button in the dialog box that \
will appear may be clicked to obtain the new version. The new version will \
be delivered in a zipped file/folder and the name of the program will be \
preceeded by \"new\". A dialog box indicating the location of the \
downloaded file will be shown after the downloading finishes. Once it has \
been confirmed by the user that the \"new\" program file is OK, it should \
be renamed (remove \"new\") and placed in the proper location which \
depends on the operating system of the computer.\n"


##############################################
# BEGIN: formHELP(Parent, AllowWriting = True)
# LIB:formHELP():2019.042
#   Put the help contents in global variable HELPText somewhere in the
#   program.
PROGFrm["HELP"] = None
HELPFindLookForVar = StringVar()
HELPFilespecVar = StringVar()
PROGSetups += ["HELPFindLookForVar", "HELPFilespecVar"]
HELPFindIndexVar = IntVar()
HELPFindLastLookForVar = StringVar()
HELPFindLinesVar = StringVar()
HELPFindUseCaseCVar = IntVar()
HELPHeight = 25
HELPWidth = 80
HELPFont = PROGOrigMonoFont


def formHELP(Parent, AllowWriting=True):
    if PROGFrm["HELP"] is not None:
        PROGFrm["HELP"].deiconify()
        PROGFrm["HELP"].lift()
        return
    LFrm = PROGFrm["HELP"] = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "HELP"))
    LFrm.title("Help - %s" % PROG_NAME)
    LFrm.iconname("Help")
    Sub = Frame(LFrm)
    LTxt = PROGTxt["HELP"] = Text(Sub, font=HELPFont, height=HELPHeight,
                                  width=HELPWidth, wrap=WORD, relief=SUNKEN)
    LTxt.pack(side=LEFT, expand=YES, fill=BOTH)
    LSb = Scrollbar(Sub, orient=VERTICAL, command=LTxt.yview)
    LSb.pack(side=RIGHT, fill=Y)
    LTxt.configure(yscrollcommand=LSb.set)
    Sub.pack(side=TOP, expand=YES, fill=BOTH)
    Sub = Frame(LFrm)
    labelTip(Sub, "Find:=", LEFT, 30, "[Find]")
    LEnt = Entry(Sub, width=20, textvariable=HELPFindLookForVar)
    LEnt.pack(side=LEFT)
    LEnt.bind("<Return>", Command(formFind, "HELP", "HELP"))
    LEnt.bind("<KP_Enter>", Command(formFind, "HELP", "HELP"))
    BButton(Sub, text="Find", command=Command(formFind,
                                              "HELP", "HELP")).pack(side=LEFT)
    BButton(Sub, text="Next", command=Command(formFindNext,
                                              "HELP", "HELP")).pack(side=LEFT)
    if AllowWriting:
        Label(Sub, text=" ").pack(side=LEFT)
        BButton(Sub, text="Write To File",
                command=Command(formHELPWrite, LTxt)).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Close", fg=Clr["R"],
            command=Command(formClose, "HELP")).pack(side=LEFT)
    Sub.pack(side=TOP, padx=3, pady=3)
    PROGMsg["HELP"] = Text(LFrm, font=PROGPropFont, height=3, wrap=WORD)
    PROGMsg["HELP"].pack(side=TOP, fill=X)
    LTxt.insert(END, HELPText)
    # Clear this so the formFind() routine does the right thing if this form
    # was brought up previously.
    HELPFindLinesVar.set("")
    center(Parent, LFrm, "CX", "I", True)
    if len(HELPFilespecVar.get()) == 0 or \
            exists(dirname(HELPFilespecVar.get())) is False:
        # Not all programs will have all directory vars. Look for them in this
        # order.
        try:
            HELPFilespecVar.set(PROGWorkDirVar.get() +
                                PROG_NAMELC + "help.txt")
        except NameError:
            try:
                HELPFilespecVar.set(PROGMsgsDirVar.get() + PROG_NAMELC +
                                    "help.txt")
            except NameError:
                # The program HAS to have one of these, so don't try here. Let
                # it crash.
                HELPFilespecVar.set(PROGDataDirVar.get() + PROG_NAMELC +
                                    "help.txt")
    return
#####################################
# BEGIN: formHELPWrite(Who, e = None)
# FUNC:formHELPWrite():2019.042


def formHELPWrite(Who, e=None):
    Dir = dirname(HELPFilespecVar.get())
    if Dir.endswith(sep) is False:
        Dir += sep
    File = basename(HELPFilespecVar.get())
    Filespec = formMYDF("HELP", 3, "Save Help To...", Dir, File)
    if len(Filespec) == 0:
        setMsg("HELP", "", "Nothing done.")
    try:
        Fp = open(Filespec, "w")
    except Exception:
        setMsg("HELP", "MW", "Error opening help file\n   %s\n   %s" %
               (Filespec, e), 3)
        return
    Fp.write("Help for %s version %s\n\n" % (PROG_NAME, PROG_VERSION))
    N = 1
    while True:
        if len(Who.get("%d.0" % N)) == 0:
            break
        # The lines from the Text field do not come with a \n after each screen
        # line, so we'll have to split the lines up ourselves.
        Line = Who.get("%d.0" % N, "%d.0" % (N + 1))
        N += 1
        if len(Line) < 65:
            Fp.write(Line)
            continue
        Out = ""
        for c in Line:
            if c == " " and len(Out) > 60:
                Fp.write(Out + "\n")
                Out = ""
            elif c == "\n":
                Fp.write(Out + "\n")
                Out = ""
            else:
                Out += c
    Fp.close()
    HELPFilespecVar.set(Filespec)
    setMsg("HELP", "", "Help written to\n   %s" % Filespec)
    return
# END: formHELP


############################################################################
# BEGIN: formMYD(Parent, Clicks, Close, C, Title, Msg1, Msg2 = "", Bell = 0,
#                CenterX = 0, CenterY = 0, Width = 0)
# LIB:formMYD():2019.030
#   The built-in dialog boxes cannot be associated with a particular frame, so
#   the Root/Main window kept popping to the front and covering up everything
#   on some systems when they were used, so I wrote this. I'm sure there is a
#   "classy" way of doing this, but I couldn't figure out how to return a
#   value after the window was destroyed from a classy-way.
#
#   The dialog box can contain an input field by using something like:
#
#   Answer = formMYD(Root, (("Input60", TOP, "input"), ("Write", LEFT, \
#           "input"), ("(Cancel)", LEFT, "cancel")), "cancel", "YB"\
#           "Let it be written...", \
#   "Enter a message to write to the Messages section (60 characters max.):")
#   if Answer == "cancel":
#       return
#   What = Answer.strip()
#   if len(What) == 0 or len(What) > 60:
#       Root.bell()
#   stdout.write("%s\n"%What)
#
#   The "Input60" tells the function to make an entry field 60 characters
#   long.  The "input" return value for the "Write" button tells the routine
#   to return whatever was entered into the input field. The "Cancel" button
#   will return "cancel", which, of course, could be confusing if the user
#   entered "cancel" in the input field and hit the Write button. In the case
#   above the Cancel button should probably return something like "!@#$%^&",
#   or something else that the user would never normally enter.
#
#   A "default" button may be designated by enclosing the text in ()'s.
#   Pressing the Return or keypad Enter keys will return that button's value.
#   The Cancel button is the default button in this example.
#
#   A caller must clear, or may set MYDAnswerVar to clear a previous
#   call's entry or to provide a default value for the input field before
#   calling formMYD().
#
#   To bring up a "splash dialog" for messages like "Working..." use
#
#       formMYD(Root, (), "", "", "", "Working...")
#
#   Call formMYDReturn to get rid of the dialog:
#
#       formMYDReturn("")
#
#   CenterX and CenterY can be used to position the dialog box when there is
#   no Parent if they are set to something other than 0 and 0.
#
#   Width may be set to something other than 0 to not use the default message
#   width.
#
#   Adding a length and text to the end of a button definition like below will
#   cause a ToolTip item to be created for that button.
#       ("Stop", LEFT, "stop", 35, "Click this to stop the program.")
#
#   This is REALLY kinda kludgy, but for a good cause, since there's already a
#          lot of arguments...
#      The normal font will be PROGPropFont.
#      If the Msg1 value begins with "|" then PROGMonoFont.
#      Starts with "}" will be PROGOrigPropFont.
#      Starts with "]" will be PROGOrigMonoFont.
#
#   A secret Shift-Control-Button-1 click on the main message Label (the text
#   of Msg1) will return the string "woohoo" which can be used to exit a
#   while-loop of some kind to get the dialog box to keep the program held in
#   place until someone knows the secret to get the program to continue.
#   Passing
#       (("NB", TOP, "NB"),)
#   for the buttons will make no button show up. It's really only useful when
#   using this secret code. The user won't normally know how to break out of
#   a caller's loop.
#   Shift-Control-1 can also be used to indicate some kind of 'override'
#   answer to the caller.
MYDFrame = None
MYDLabel = None
MYDAnswerVar = StringVar()


def formMYD(Parent, Clicks, Close, C, Title, Msg1, Msg2="", Bell=0,
            CenterX=0, CenterY=0, Width=0):
    global MYDFrame
    global MYDLabel
    # This Should Never Happen, but I think I saw it once where MYDFrame seemed
    # to no longer point to a dialog that was visible. It may have just been a
    # XQuartz glitch. The other alternative here is to destroy MYDFrame. We'll
    # see how this works.
    if MYDFrame is not None:
        MYDFrame.deiconify()
        MYDFrame.lift()
        beep(2)
        return
    if Parent is not None:
        # Allow either way of passing the parent.
        if isinstance(Parent, astring) is True:
            Parent = PROGFrm[Parent]
        # Without this update() sometimes when running a program through ssh
        # everyone loses track of where the parent is and the dialog box ends
        # up at 0,0. It's possible for this to fail if the program is being
        # stopped in a funny way (like with a ^C over an ssh connection, etc.).
        try:
            Parent.update()
        except Exception:
            pass
    TheFont = PROGPropFont
    MonoFont = False
    if Msg1.startswith("|"):
        Msg1 = Msg1[1:]
        TheFont = PROGMonoFont
        MonoFont = True
    if Msg1.startswith("}"):
        Msg1 = Msg1[1:]
        TheFont = PROGOrigPropFont
        MonoFont = False
    if Msg1.startswith("]"):
        Msg1 = Msg1[1:]
        TheFont = PROGOrigMonoFont
        MonoFont = True
    LFrm = MYDFrame = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formMYDReturn, Close))
    # A number shows up in the title bar if you do .title(""), and if you don't
    # set the title it ends up with the program name in it.
    if len(Title) == 0:
        Title = " "
    LFrm.title(Title)
    LFrm.iconname(Title)
    # Gets rid of some of the extra title bar buttons.
    LFrm.transient(Parent)
    # If C is X or it ends with X then set this so we get the modality right.
    # Clean up C so the rest of the function doesn't need to worry about it.
    GSG = False
    if len(C) != 0:
        if C == "X":
            GSG = True
            C = ""
        else:
            if C.endswith("X"):
                GSG = True
                C = C.replace("X", "")
            LFrm.configure(bg=Clr[C[0]])
    # Break up the incoming message about every 50 characters or whatever Width
    # is set to.
    if Width == 0:
        Width = 50
    if len(Msg1) > Width:
        Count = 0
        Mssg = ""
        for c in Msg1:
            if Count == 0 and c == " ":
                continue
            if Count > Width and c == " ":
                Mssg += "\n"
                Count = 0
                continue
            if c == "\n":
                Mssg += c
                Count = 0
                continue
            Count += 1
            Mssg += c
        Msg1 = Mssg
    # This is an extra line that gets added to the message after a blank line.
    if len(Msg2) != 0:
        if len(Msg2) > Width:
            Count = 0
            Mssg = ""
            for c in Msg2:
                if Count == 0 and c == " ":
                    continue
                if Count > Width and c == " ":
                    Mssg += "\n"
                    Count = 0
                    continue
                if c == "\n":
                    Mssg += c
                    Count = 0
                    continue
                Count += 1
                Mssg += c
            Msg1 += "\n\n" + Mssg
        else:
            Msg1 += "\n\n" + Msg2
    Sub = Frame(LFrm)
    if len(C) == 0:
        if MonoFont is False:
            MYDLabel = Label(Sub, text=Msg1, bd=15, font=TheFont)
        else:
            MYDLabel = Label(Sub, text=Msg1, bd=15, font=TheFont,
                             justify=LEFT)
        MYDLabel.pack(side=LEFT)
        Sub.pack(side=TOP)
        Sub = Frame(LFrm)
    else:
        if MonoFont is False:
            MYDLabel = Label(Sub, text=Msg1, bd=15, bg=Clr[C[0]],
                             fg=Clr[C[1]], font=TheFont)
        else:
            MYDLabel = Label(Sub, text=Msg1, bd=15, bg=Clr[C[0]],
                             fg=Clr[C[1]], font=TheFont, justify=LEFT)
        MYDLabel.pack(side=LEFT)
        Sub.pack(side=TOP)
        Sub = Frame(LFrm, bg=Clr[C[0]])
    MYDLabel.bind("<Shift-Control-Button-1>", Command(formMYDReturn, "woohoo"))
    One = False
    InputField = False
    for Click in Clicks:
        if Click[0] == "NB" and Click[2] == "NB":
            continue
        if Click[0].startswith("Input"):
            InputEnt = Entry(LFrm, textvariable=MYDAnswerVar,
                             width=intt(Click[0][5:]))
            InputEnt.pack(side=TOP, padx=10, pady=10)
            # So we know to do the focus_set at the end.
            InputField = True
            continue
        if One is True:
            if len(C) == 0:
                Label(Sub, text=" ").pack(side=LEFT)
            else:
                Label(Sub, text=" ", bg=Clr[C[0]]).pack(side=LEFT)
        But = BButton(Sub, text=Click[0], command=Command(formMYDReturn,
                                                          Click[2]))
        if Click[0].startswith("Clear") or Click[0].startswith("(Clear"):
            But.configure(fg=Clr["U"], activeforeground=Clr["U"])
        elif Click[0].startswith("Close") or Click[0].startswith("(Close"):
            But.configure(fg=Clr["R"], activeforeground=Clr["R"])
        But.pack(side=Click[1])
        # Check to see if there is ToolTip text.
        try:
            ToolTip(But, Click[3], Click[4])
        except Exception:
            pass
        if Click[0].startswith("(") and Click[0].endswith(")"):
            LFrm.bind("<Return>", Command(formMYDReturn, Click[2]))
            LFrm.bind("<KP_Enter>", Command(formMYDReturn, Click[2]))
        if Click[1] != TOP:
            One = True
    Sub.pack(side=TOP, padx=3, pady=3)
    # CX. Always keep these fully on the display.
    center(Parent, LFrm, "CX", "I", True, CenterX, CenterY)
    # If the user clicks to dismiss the window before any one of these things
    # get taken care of then there will be touble. This may only happen when
    # the user is running the program over a network and not on the local
    # machine. It has something to do with changes made to the beep() routine
    # which fixed a problem of occasional missing beeps.
    try:
        LFrm.focus_set()
        if GSG is False:
            # This is not working well on macOS with the "stock" Pythons/
            # Tkinters. We'll try this for a while and see how it goes.
            # What a pain. This is why PASSCAL created our own Python/Tkinter
            # package.
            if PROGSystem == "dar" and PROG_PYVERS != 2:
                pass
            else:
                LFrm.grab_set()
        else:
            if PROGSystem == "dar" and PROG_PYVERS != 2:
                pass
            else:
                LFrm.grab_set_global()
        if InputField is True:
            InputEnt.focus_set()
            InputEnt.icursor(END)
        if Bell != 0:
            beep(Bell)
        # Everything will pause here until one of the buttons are pressed if
        # there are any, then the box will be destroyed, but the value will
        # still be returned since it was saved in a "global".
        if len(Clicks) != 0:
            LFrm.wait_window()
    except Exception:
        pass
    # At least do this much cleaning for the caller.
    MYDAnswerVar.set(MYDAnswerVar.get().strip())
    return MYDAnswerVar.get()
######################################
# BEGIN: formMYDReturn(What, e = None)
# FUNC:formMYDReturn():2018.256


def formMYDReturn(What, e=None):
    # If What is "input" just leave whatever is in the var in there.
    global MYDFrame
    if What != "input":
        MYDAnswerVar.set(What)
    MYDAnswerVar.set(MYDAnswerVar.get().strip())
    # This function may get called more than once if there are errors and
    # stuff, especially when the dialog box is just being used for splash
    # messages.
    try:
        MYDFrame.destroy()
    except Exception:
        pass
    MYDFrame = None
    updateMe(0)
    return
############################
# BEGIN: formMYDMsg(Message)
# FUNC:formMYDMsg():2008.012


def formMYDMsg(Message):
    global MYDLabel
    MYDLabel.config(text=Message)
    updateMe(0)
    return
# END: formMYD


######################################################################
# BEGIN: formMYDF(Parent, Mode, Title, StartDir, StartFile, Mssg = "",
#                EndsWith = "", SameCase = True, CompFs = True, \
#                DePrefix = "", ReturnEmpty = False)
# LIB:formMYDF():2019.030
# NEEDS: PROGMsgsDirVar, PROGDataDirVar, PROGWorkDirVar if anyone tries to
#        use the Default codes in Mode.
#   Mode = (These must be the first character in Mode)
#          0 = allow picking a file
#          1 = just allow picking directories
#          2 = allow picking/making directories only
#          3 = pick/save directories and files (the works)
#          4 = pick multiple files and change dirs
#          5 = enter/pick a file, but not change directory
#          6 = enter/pick a file, but not change directory, and don't show the
#              directory
#   Mode = A second character that specifies one of the "main" directories
#          that can be selected using a "Default" button.
#          D = Main Data Directory (PROGDataDirVar.get())
#          W = Main Work Directory (PROGWorkDirVar.get())
#          M = Main Messages Directory (PROGMsgsDirVar.get())
#   Mode = Optional last character, X, that indicates that the form should use
#          grab_set_global(). This gets detected and stripped off right away so
#          the rest of the function doesn't have to worry about it.
#   Mssg = A message that can be displayed at the top of the form. It's a
#          Label, so \n's should be put in the passed Mssg string.
#   EndsWith = The module will only display files ending with EndsWith, the
#              entered filename's case will be matched with the case of
#              EndsWith (IF it is all one case or another). EndsWith may be
#              a passed string of file extensions seperated by commas. EndsWith
#              will be added to the entered filename if it is not there before
#              returning, but only if there is one extension passed.
#   SameCase = If True then the case of the filename must match the case of
#              the EndsWith items.
#   CompFs = If True then directory and file completion with the Tab key are
#            set up.
#   DePrefix = If not "" then only the files starting with DePrefix will be
#              loaded and the DePrefix will be removed.
#   ReturnEmpty = If True it allows clearing the file name field and clicking
#                 the OK button, instead of complaining that no file name was
#                 entered.
#   Tabbing in the directory field is supported.
MYDFFrame = None
MYDFModeVar = StringVar()
MYDFDirVar = StringVar()
MYDFFiles = None
MYDFDirField = None
MYDFFileVar = StringVar()
MYDFEndsWithVar = StringVar()
MYDFAnswerVar = StringVar()
MYDFHiddenCVar = IntVar()
MYDFSameCase = True
MYDFDePrefix = ""
MYDFReturnEmpty = False


def formMYDF(Parent, Mode, Title, StartDir, StartFile, Mssg="",
             EndsWith="", SameCase=True, CompFs=True, DePrefix="",
             ReturnEmpty=False):
    global MYDFFrame
    global MYDFFiles
    global MYDFDirField
    global MYDFSameCase
    global MYDFDePrefix
    global MYDFReturnEmpty
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    # Mode may be passed as an integer 1 or as a string "1X".
    Mode = str(Mode)
    GSG = False
    if Mode.endswith("X"):
        GSG = True
        Mode = Mode.replace("X", "")
    # Everyone else looks for the str() version.
    MYDFModeVar.set(Mode)
    MYDFEndsWithVar.set(EndsWith)
    MYDFSameCase = SameCase
    MYDFDePrefix = DePrefix
    MYDFReturnEmpty = ReturnEmpty
    # Without this update() sometimes when running a program through ssh
    # everyone loses track of where the parent is and the dialog box ends up
    # at 0,0.
    # It's possible for this to fail if the program is being stopped in a funny
    # way (like with a ^C over an ssh connection, etc.).
    try:
        Parent.update()
    except Exception:
        pass
    LFrm = MYDFFrame = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.protocol("WM_DELETE_WINDOW", Command(formMYDFReturn, None))
    LFrm.bind("<Button-1>", Command(setMsg, "MYDF", "", ""))
    LFrm.title(Title)
    LFrm.iconname("PickDF")
    Sub = Frame(LFrm)
    # The incoming Mode may be something like "1W". There are special versions
    # of intt() out there that don't convert the passed item to a string first,
    # so now we want the beginning number part.
    ModeI = intt(Mode)
    if len(Mssg) != 0:
        Label(Sub, text=Mssg).pack(side=TOP)
    if ModeI != 5 and ModeI != 6:
        BButton(Sub, text="Up", command=formMYDFUp).pack(side=LEFT)
        LLb = Label(Sub, text=":=")
        LLb.pack(side=LEFT)
        ToolTip(LLb, 35,
                "[List Files] Press the Return key to list the files in the "
                "entered directory after editing the directory name.")
        LEnt = MYDFDirField = Entry(Sub, textvariable=MYDFDirVar, width=65)
        LEnt.pack(side=LEFT, fill=X, expand=YES)
        if CompFs is True:
            LEnt.bind("<FocusIn>", Command(formMYDFCompFsTabOff, LFrm))
            LEnt.bind("<FocusOut>", Command(formMYDFCompFsTabOn, LFrm))
            LEnt.bind("<Key-Tab>", Command(formMYDFCompFs, MYDFDirVar, None))
        LEnt.bind("<Return>", formMYDFFillFiles)
        LEnt.bind("<KP_Enter>", formMYDFFillFiles)
        # KeyPress clears the field when typing. KeyRelease clears the field
        # after it has been filled in.
        LEnt.bind("<KeyPress>", formMYDFClearFiles)
        if ModeI == 2 or ModeI == 3:
            But = BButton(Sub, text="Mkdir", command=formMYDFNew)
            But.pack(side=LEFT)
            ToolTip(But, 25,
                    "Add the name of the new directory to the end of the "
                    "current contents of the entry field and click this "
                    "button to create the new directory.")
    # Just show the current directory.
    elif ModeI == 5:
        MYDFDirField = Entry(Sub, textvariable=MYDFDirVar,
                             state=DISABLED)
        MYDFDirField.pack(side=LEFT, fill=X, expand=YES)
    # Just don't show the current directory. Create, but don't pack.
    elif ModeI == 6:
        MYDFDirField = Entry(Sub, textvariable=MYDFDirVar)
    Sub.pack(side=TOP, fill=X)
    Sub = Frame(LFrm)
    if ModeI == 4:
        LLb = MYDFFiles = Listbox(Sub, relief=SUNKEN, bd=2, height=15,
                                  selectmode=EXTENDED, width=65)
    else:
        LLb = MYDFFiles = Listbox(Sub, relief=SUNKEN, bd=2, height=15,
                                  selectmode=SINGLE, width=65)
    LLb.pack(side=LEFT, expand=YES, fill=BOTH)
    LLb.bind("<ButtonRelease-1>", formMYDFPicked)
    Scroll = Scrollbar(Sub, command=LLb.yview)
    Scroll.pack(side=RIGHT, fill=Y)
    LLb.configure(yscrollcommand=Scroll.set)
    Sub.pack(side=TOP, expand=YES, fill=BOTH)
    # The user can type in the filename.
    if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
        Sub = Frame(LFrm)
        Label(Sub, text="Filename:=").pack(side=LEFT)
        LEnt = Entry(Sub, textvariable=MYDFFileVar)
        LEnt.pack(side=LEFT, fill=X, expand=YES)
        if CompFs is True:
            LEnt.bind("<FocusIn>", Command(formMYDFCompFsTabOff, LFrm))
            LEnt.bind("<FocusOut>", Command(formMYDFCompFsTabOn, LFrm))
            LEnt.bind("<Key-Tab>", Command(formMYDFCompFs, None, MYDFFileVar))
        LEnt.bind("<Return>", Command(formMYDFReturn, ""))
        LEnt.bind("<KP_Enter>", Command(formMYDFReturn, ""))
        Sub.pack(side=TOP, fill=X, padx=3)
    Sub = Frame(LFrm)
    # Create a Default button and tooltip if the Mode says so.
    if isinstance(Mode, astring):
        if Mode.find("D") != -1:
            LBu = BButton(Sub, text="Default",
                          command=Command(formMYDFDefault, "D"))
            LBu.pack(side=LEFT)
            ToolTip(LBu, 35, "Set to Main Data Directory")
            Label(Sub, text=" ").pack(side=LEFT)
        elif Mode.find("W") != -1:
            LBu = BButton(Sub, text="Default",
                          command=Command(formMYDFDefault, "W"))
            LBu.pack(side=LEFT)
            ToolTip(LBu, 35, "Set to Main Work Directory")
            Label(Sub, text=" ").pack(side=LEFT)
        elif Mode.find("M") != -1:
            LBu = BButton(Sub, text="Default",
                          command=Command(formMYDFDefault, "M"))
            LBu.pack(side=LEFT)
            ToolTip(LBu, 35, "Set to Main Messages Directory")
            Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="OK", command=Command(formMYDFReturn,
                                            "")).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Cancel",
            command=Command(formMYDFReturn, None)).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    LCb = Checkbutton(Sub, text="Show hidden\nfiles",
                      variable=MYDFHiddenCVar, command=formMYDFFillFiles)
    LCb.pack(side=LEFT)
    ToolTip(LCb, 35, "Select this to show hidden/system files in the list.")
    Sub.pack(side=TOP, pady=3)
    PROGMsg["MYDF"] = Text(LFrm, font=PROGPropFont, height=1,
                           width=1, highlightthickness=0, insertwidth=0,
                           takefocus=0)
    PROGMsg["MYDF"].pack(side=TOP, expand=YES, fill=X)
    PROGMsg["MYDF"].bind("<Button-1>", formMYDFNullCall)
    if len(StartDir) == 0:
        StartDir = sep
    if StartDir.endswith(sep) is False:
        StartDir += sep
    MYDFDirVar.set(StartDir)
    Ret = formMYDFFillFiles()
    if Ret is True and (ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6):
        MYDFFileVar.set(StartFile)
    center(Parent, LFrm, "CX", "I", True)
    # Set the cursor for the user.
    if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
        LEnt.focus_set()
        LEnt.icursor(END)
    if GSG is False:
        # See formMYD().
        if PROGSystem == "dar" and PROG_PYVERS != 2:
            pass
        else:
            MYDFFrame.grab_set()
    else:
        if PROGSystem == "dar" and PROG_PYVERS != 2:
            pass
        else:
            MYDFFrame.grab_set_global()
    # Everything will pause here until one of the buttons are pressed, then the
    # box will be destroyed, but the value will still be returned since it was
    # saved in a "global".
    MYDFFrame.wait_window()
    return MYDFAnswerVar.get()
#####################################
# BEGIN: formMYDFClearFiles(e = None)
# FUNC:formMYDFClearFiles():2013.207


def formMYDFClearFiles(e=None):
    if MYDFFiles.size() > 0:
        MYDFFiles.delete(0, END)
    return
#####################################################
# BEGIN: formMYDFFillFiles(FocusSet = True, e = None)
# FUNC:formMYDFFillFiles():2018.330
#   Fills the Listbox with a list of directories and files, or with drive
#   letters if in Windows (and the directory field is just sep).


def formMYDFFillFiles(FocusSet=True, e=None):
    # Some directoryies may have a lot of files in them which could make
    # listdir() take a long time, so do this.
    setMsg("MYDF", "CB", "Reading...")
    ModeI = intt(MYDFModeVar.get())
    # Make sure whatever is in the directory field ends, or is at least a
    # separator.
    if len(MYDFDirVar.get()) == 0:
        MYDFDirVar.set(sep)
    if MYDFDirVar.get().endswith(sep) is False:
        MYDFDirVar.set(MYDFDirVar.get() + sep)
    Dir = MYDFDirVar.get()
    if PROGSystem == "dar" or PROGSystem == "lin" or PROGSystem == "sun" or \
            (PROGSystem == "win" and Dir != sep):
        try:
            Files = listdir(Dir)
        except Exception:
            MYDFFileVar.set("")
            setMsg("MYDF", "YB", " There is no such directory.", 2)
            return False
        MYDFDirField.icursor(END)
        MYDFFiles.delete(0, END)
        # This is sortedLow(), but coded in here to reduce dependencies.
        USList = {}
        for Item in Files:
            USList[Item.lower()] = Item
        SKeys = sorted(USList.keys())
        Files = []
        for Key in SKeys:
            Files.append(USList[Key])
        # Always add this to the top of the list unless we are not supposed to.
        if ModeI != 5 and ModeI != 6:
            MYDFFiles.insert(END, " .." + sep)
        # To show or not to show.
        ShowHidden = MYDFHiddenCVar.get()
        # Do the directories first.
        for File in Files:
            # The DePrefix stuff is only applied to the files (below).
            if ShowHidden == 0:
                # This may need/want to be system dependent at some point
                # (i.e. more than just files that start with a . or _ in
                # different OSs).
                if File.startswith(".") or File.startswith("_"):
                    continue
            if isdir(Dir + sep + File):
                MYDFFiles.insert(END, " " + File + sep)
        # Check to see if we are going to be filtering.
        EndsWith = ""
        if len(MYDFEndsWithVar.get()) != 0:
            EndsWith = MYDFEndsWithVar.get()
        EndsWithParts = EndsWith.split(",")
        Found = False
        for File in Files:
            # DeFile will be used for what the user ends up seeing, and File
            # will be used internally.
            DeFile = File
            if len(MYDFDePrefix) != 0:
                if File.startswith(MYDFDePrefix) is False:
                    continue
                DeFile = File[len(MYDFDePrefix):]
            if ShowHidden == 0:
                if DeFile.startswith(".") or DeFile.startswith("_"):
                    continue
            if isdir(Dir + sep + File) is False:
                if len(EndsWith) == 0:
                    # We only want to see the file sizes when we are looking
                    # for files.
                    if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
                        # Trying to follow links will trip this so just show
                        # them.
                        try:
                            MYDFFiles.insert(END, " %s  (bytes: %s)" %
                                             (DeFile,
                                              fmti(getsize(Dir + sep + File))))
                        except OSError:
                            MYDFFiles.insert(END, " %s  (a link?)" % DeFile)
                    else:
                        MYDFFiles.insert(END, " %s" % DeFile)
                    Found += 1
                else:
                    for EndsWithPart in EndsWithParts:
                        if File.endswith(EndsWithPart):
                            if ModeI == 0 or ModeI == 3 or ModeI == 5 or \
                                    ModeI == 6:
                                try:
                                    MYDFFiles.insert(END, " %s  (bytes: %s)" %
                                                     (DeFile,
                                                      fmti(getsize(Dir + sep +
                                                                   File))))
                                except OSError:
                                    MYDFFiles.insert(END, " %s  (a link?)" %
                                                     DeFile)
                            else:
                                MYDFFiles.insert(END, " %s" % DeFile)
                            Found += 1
                            break
        if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
            setMsg("MYDF", "", "%d %s found." % (Found, sP(Found, ("file",
                                                                   "files"))))
        else:
            setMsg("MYDF")
    elif PROGSystem == "win" and Dir == sep:
        MYDFFiles.delete(0, END)
        # This loop takes a while to run.
        updateMe(0)
        Found = 0
        for Drive in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
            Drivespec = "%s:%s" % (Drive, sep)
            if exists(Drivespec):
                MYDFFiles.insert(END, Drivespec)
                Found += 1
        if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
            setMsg("MYDF", "", "%d %s found." % (Found, sP(Found, ("drive",
                                                                   "drives"))))
        else:
            setMsg("MYDF")
    if FocusSet is True:
        PROGMsg["MYDF"].focus_set()
    return True
###############################
# BEGIN: formMYDFDefault(Which)
# FUNC:formMYDFDefault():2012.343


def formMYDFDefault(Which):
    if Which == "D":
        MYDFDirVar.set(PROGDataDirVar.get())
    elif Which == "W":
        MYDFDirVar.set(PROGWorkDirVar.get())
    elif Which == "M":
        MYDFDirVar.set(PROGMsgsDirVar.get())
    formMYDFFillFiles()
    return
######################
# BEGIN: formMYDFNew()
# FUNC:formMYDFNew():2018.233


def formMYDFNew():
    setMsg("MYDF")
    # Make sure whatever is in the directory field ends in a separator.
    if len(MYDFDirVar.get()) == 0:
        MYDFDirVar.set(sep)
    if MYDFDirVar.get().endswith(sep) is False:
        MYDFDirVar.set(MYDFDirVar.get() + sep)
    Dir = MYDFDirVar.get()
    if exists(Dir):
        setMsg("MYDF", "YB", " Directory already exists.", 2)
        return
    try:
        makedirs(Dir)
        formMYDFFillFiles()
        setMsg("MYDF", "GB", " New directory made.", 1)
    except Exception as e:
        # The system messages can be a bit cryptic and/or long, so simplifiy
        # them here.
        if str(e).find("ermission") != -1:
            setMsg("MYDF", "RW", " Permission denied.", 2)
        else:
            setMsg("MYDF", "RW", " " + str(e), 2)
        return
    return
#################################
# BEGIN: formMYDFPicked(e = None)
# FUNC:formMYDFPicked():2018.235


def formMYDFPicked(e=None):
    # This is just to get the focus off of the entry field if it was there
    # since the user is now clicking on things.
    PROGMsg["MYDF"].focus_set()
    setMsg("MYDF")
    ModeI = intt(MYDFModeVar.get())
    # Make sure whatever is in the directory field ends with a separator.
    if len(MYDFDirVar.get()) == 0:
        MYDFDirVar.set(sep)
    if MYDFDirVar.get().endswith(sep) is False:
        MYDFDirVar.set(MYDFDirVar.get() + sep)
    Dir = MYDFDirVar.get()
    # Otherwise the selected stuff will just keep piling up.
    if ModeI == 4:
        MYDFFileVar.set("")
    # There could be multiple files selected. Go through all of them and see
    # if any of them will make us change directories.
    Sel = MYDFFiles.curselection()
    SelectedFiles = ""
    for Index in Sel:
        # If the user is not right on the money this will sometimes trip.
        try:
            Selected = MYDFFiles.get(Index).strip()
        except TclError:
            beep(1)
            return
        if Selected == ".." + sep:
            Parts = Dir.split(sep)
            # If there are only two Parts then we have hit the top of the
            # directory tree.
            NewDir = ""
            if len(Parts) == 2 and len(Parts[1]) == 0:
                if PROGSystem == "dar" or PROGSystem == "lin" or \
                        PROGSystem == "sun":
                    NewDir = Parts[0] + sep
                elif PROGSystem == "win":
                    NewDir = sep
            else:
                for i in arange(0, len(Parts) - 2):
                    NewDir += Parts[i] + sep
            # Insurance.
            if len(NewDir) == 0:
                NewDir = sep
            MYDFDirVar.set(NewDir)
            formMYDFFillFiles()
            return
        # We have to do a Texas Two-Step here to get everyone in the right
        # frame of mind. If Dir is just sep then we have just clicked on a
        # directory and not on a file, so make it look like the former.
        if PROGSystem == "win" and Dir == sep:
            Dir = Selected
            Selected = ""
        if isdir(Dir + Selected):
            MYDFDirVar.set(Dir + Selected)
            formMYDFFillFiles()
            return
        if ModeI == 1 or ModeI == 2:
            setMsg("MYDF", "YB", "That is not a directory.", 2)
            MYDFFiles.selection_clear(0, END)
            return
        # Must have clicked on a file with byte size and that must be allowed.
        if Selected.find("  (") != -1:
            Selected = Selected[:Selected.index("  (")]
        # Build the whole path for the multiple file mode.
        if ModeI == 4:
            if len(SelectedFiles) == 0:
                SelectedFiles = Dir + Selected
            else:
                # An * should be a safe separator, right?
                SelectedFiles += "*" + Dir + Selected
        else:
            # This should end up the only file.
            SelectedFiles = Selected
    MYDFFileVar.set(SelectedFiles)
    return
#############################
# BEGIN: formMYDFUp(e = None)
# FUNC:formMYDFUp():2018.235


def formMYDFUp(e=None):
    # This is just to get the focus off of the entry field if it was there.
    PROGMsg["MYDF"].focus_set()
    setMsg("MYDF")
    Dir = MYDFDirVar.get()
    Parts = Dir.split(sep)
    # If there are only two Parts then we have hit the top of the directory
    # tree.
    NewDir = ""
    if len(Parts) == 2 and len(Parts[1]) == 0:
        if PROGSystem == "dar" or PROGSystem == "lin" or PROGSystem == "sun":
            NewDir = Parts[0] + sep
        elif PROGSystem == "win":
            NewDir = sep
    else:
        for i in arange(0, len(Parts) - 2):
            NewDir += Parts[i] + sep
    # Insurance.
    if len(NewDir) == 0:
        NewDir = sep
    MYDFDirVar.set(NewDir)
    formMYDFFillFiles()
    return
#########################################
# BEGIN: formMYDFReturn(Return, e = None)
# FUNC:formMYDFReturn():2018.236


def formMYDFReturn(Return, e=None):
    # This update keeps the "OK" button from staying pushed in when there is
    # a lot to do before returning.
    MYDFFrame.update()
    setMsg("MYDF")
    ModeI = intt(MYDFModeVar.get())
    if Return is None:
        MYDFAnswerVar.set("")
    elif len(Return) != 0:
        MYDFAnswerVar.set(Return)
    elif len(Return) == 0:
        # The programmer is responsible for making sure this is used correctly.
        if len(MYDFDirVar.get()) == 0 and MYDFReturnEmpty is True:
            MYDFAnswerVar.set("")
        else:
            if ModeI == 1 or ModeI == 2:
                if len(MYDFDirVar.get()) == 0:
                    setMsg("MYDF", "YB", "There is no directory name.", 2)
                    return
                if MYDFDirVar.get().endswith(sep) is False:
                    MYDFDirVar.set(MYDFDirVar.get() + sep)
                MYDFAnswerVar.set(MYDFDirVar.get())
            elif ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
                # Just in case the user tries to pull a fast one. Mode 5
                # above is just for completness since the directory must be
                # right (it can't be changed by the user).
                if len(MYDFDirVar.get()) == 0:
                    setMsg("MYDF", "YB", "There is no directory name.", 2)
                    return
                # What was the point of coming here??
                if len(MYDFFileVar.get()) == 0:
                    setMsg("MYDF", "YB", "No filename has been entered.", 2)
                    return
                # File names only at this point.
                if MYDFFileVar.get().find(sep) != -1:
                    setMsg("MYDF", "RW",
                           "Character  %s  cannot be in the Filename." % sep,
                           2)
                    return
                if len(MYDFEndsWithVar.get()) != 0:
                    # I'm sure this may come back to haunt US someday, but
                    # make sure that the case of the entered filename matches
                    # the passed 'extension'.  My programs, as a general rule,
                    # are written to handle all upper or lowercase file names,
                    # but not mixed. Of course, on some operating systems it
                    # won't make any difference.
                    if MYDFSameCase is True:
                        if MYDFEndsWithVar.get().isupper():
                            if MYDFFileVar.get().isupper() is False:
                                setMsg("MYDF", "YB",
                                       "Filename needs to be all uppercase "
                                       "letters.", 2)
                                return
                        elif MYDFEndsWithVar.get().islower():
                            if MYDFFileVar.get().islower() is False:
                                setMsg("MYDF", "YB",
                                       "Filename needs to be all lowercase "
                                       "letters.", 2)
                                return
                    # If the user didn't put the 'extension' on the file add it
                    # so the caller won't have to do it, unless the caller
                    # passed multiple extensions, then don't do anything.
                    if MYDFEndsWithVar.get().find(",") == -1:
                        if MYDFFileVar.get().endswith(
                                MYDFEndsWithVar.get()) is False:
                            MYDFFileVar.set(MYDFFileVar.get() +
                                            MYDFEndsWithVar.get())
                MYDFAnswerVar.set(MYDFDirVar.get() + MYDFFileVar.get())
            elif ModeI == 4:
                MYDFAnswerVar.set(MYDFFileVar.get())
    MYDFFrame.destroy()
    updateMe(0)
    return
##################################################
# BEGIN: formMYDFCompFs(DirVar, FileVar, e = None)
# FUNC:formMYDFCompFs():2018.236
#   Attempts to complete the directory or file name in an Entry field using
#   the Tab key.
#   - If DirVar is set to a field's StringVar and FileVar is None then the
#     routine only looks for directories and leaves completion results in
#     DirVar.
#   - If FileVar is set to a field's StringVar and DirVar is None then the
#     routine tries to complete a file name using the files loaded into the
#     file listbox and leaves the results in FileVar.


def formMYDFCompFs(DirVar, FileVar, e=None):
    setMsg("MYDF")
# ---- Directories...looking to find you.
    if DirVar is not None:
        Dir = dirname(DirVar.get())
        # This is a slight gotchya. If the field is empty that might mean that
        # the user means "/" should be the starting point. If it is they will
        # have to enter the / since if we are on Windows I'd have no idea what
        # the default should be.
        if len(Dir) == 0:
            beep(2)
            return
        if Dir.endswith(sep) is False:
            Dir += sep
        # Now get what must be a partial directory name, treat it as a file
        # name, but then only allow the result of everything to be a directory.
        PartialFile = basename(DirVar.get())
        if len(PartialFile) == 0:
            beep(2)
            return
        PartialFile += "*"
        Files = listdir(Dir)
        Matched = []
        for File in Files:
            if fnmatch(File, PartialFile):
                Matched.append(File)
        if len(Matched) == 0:
            beep(2)
            return
        elif len(Matched) == 1:
            Dir = Dir + Matched[0]
            # If whatever matched is not a directory then just beep and
            # return, otherwise make it look like a directory and put it into
            # the field.
            if isdir(Dir) is False:
                beep(2)
                return
            if Dir.endswith(sep) is False:
                Dir += sep
            DirVar.set(Dir)
            e.widget.icursor(END)
            formMYDFFillFiles(False)
            return
        else:
            # Get the max number of characters that matched and put the
            # partial directory path into the Var. If Dir+PartialDir is really
            # the directory the user wants they will have to add the sep
            # themselves since with multiple matches I won't know what to do.
            # Consider DIR DIR2 DIR3 with a formMYDFMaxMatch() return of DIR.
            # The directory DIR would always be selected and set as the path
            # which  may not be what the user wanted. Now this could cause
            # trouble downstream since I'm leaving a path in the field without
            # a trailing sep (everything tries to avoid doing that), so the
            # caller will have to worry about that.
            PartialDir = formMYDFMaxMatch(Matched)
            DirVar.set(Dir + PartialDir)
            e.widget.icursor(END)
            beep(1)
    elif FileVar is not None:
        PartialFile = FileVar.get().strip()
        if len(PartialFile) == 0:
            beep(1)
            return
        Files = MYDFFiles.get(0, END)
        Index = 0
        Found = 0
        for Sel in arange(0, len(Files)):
            # .strip() off the leading space.
            File = Files[Sel].strip()
            if File.startswith(PartialFile):
                Index = Sel
                Found += 1
        if Found == 0:
            beep(1)
            return
        elif Found > 1:
            MYDFFiles.see(Index)
            beep(1)
            return
        MYDFFiles.see(Index)
        File = Files[Index].strip()
        # File contains the matching line from the Listbox, but it should have
        # an (x bytes) message at the end. Get rid of that.
        if File.find("  (") != -1:
            File = File[:File.index("  (")]
        FileVar.set(File)
        e.widget.icursor(END)
    return
#############################################
# BEGIN: formMYDFCompFsTabOff(LFrm, e = None)
# FUNC:formMYDFCompFsTabOff():2010.225


def formMYDFCompFsTabOff(LFrm, e=None):
    LFrm.bind("<Key-Tab>", formMYDFNullCall)
    return
############################################
# BEGIN: formMYDFCompFsTabOn(LFrm, e = None)
# FUNC:formMYDFCompFsTabOn():2010.225


def formMYDFCompFsTabOn(LFrm, e=None):
    LFrm.unbind("<Key-Tab>")
    return
##################################
# BEGIN: formMYDFMaxMatch(TheList)
# FUNC:formMYDFMaxMatch():2018.310
#   Goes through the items in TheList (should be str's) and returns the string
#   that matches the start of all of the items.
#   This is the same as the library function maxMatch().


def formMYDFMaxMatch(TheList):
    # This should be the only special case. What is the sound of one thing
    # matching itself?
    if len(TheList) == 1:
        return TheList[0]
    Accum = ""
    CharIndex = 0
    # If anything goes wrong just return whatever we've accumulated. This will
    # end by no items being in TheList or one of the items running out of
    # characters (the try) or by the TargetChar not matching a character from
    # one of the items (the raise).
    try:
        while True:
            TargetChar = TheList[0][CharIndex]
            for ItemIndex in arange(1, len(TheList)):
                if TargetChar != TheList[ItemIndex][CharIndex]:
                    raise Exception
            Accum += TargetChar
            CharIndex += 1
    except Exception:
        pass
    return Accum
###################################
# BEGIN: formMYDFNullCall(e = None)
# FUNC:formMYDFNullCall():2013.037


def formMYDFNullCall(e=None):
    return "break"
# END: formMYDF


##########################################################################
# BEGIN: formMYDT(Parent, Clicks, Close, BgFg, Find, Title, Wrap, TheText)
# LIB:formMYDT():2018.310
#   This is a general purpose dialog box that just has one big Text() field to
#   dump TheText to.
#   Add "X" to the passed BgFg colors to make the function use
#   grab_set_global().
#   Find = True will set up the regular searching stuff below the Text().
MYDTFrame = None
MYDTText = None
MYDTAnswerVar = StringVar()
MYDTFindLookForVar = StringVar()
MYDTFindIndexVar = IntVar()
MYDTFindLastLookForVar = StringVar()
MYDTFindLinesVar = StringVar()
MYDTFindUseCaseCVar = IntVar()
MYDTFilespecVar = StringVar()
PROGSetups += ["MYDTFindLookForVar", "MYDTFilespecVar"]
# FINISHME - maybe add autosizing and things like that.


def formMYDT(Parent, Clicks, Close, BgFg, Find, Title, Wrap, TheText):
    global MYDTFrame
    global MYDTText
    if Parent is not None:
        # Allow either way of passing the parent.
        if isinstance(Parent, astring) is True:
            Parent = PROGFrm[Parent]
        # Without this update() sometimes when running a program through ssh
        # everyone loses track of where the parent is and the dialog box ends
        # up at 0,0.
        # It's possible for this to fail if the program is being stopped in a
        # funny way (like with a ^C over an ssh connection, etc.).
        try:
            Parent.update()
        except Exception:
            pass
    TheFont = PROGPropFont
    if TheText[0].startswith("|"):
        TheText = TheText[1:]
        TheFont = PROGMonoFont
    if TheText[0].startswith("}"):
        TheText = TheText[1:]
        TheFont = PROGOrigPropFont
    if TheText[0].startswith("]"):
        TheText = TheText[1:]
        TheFont = PROGOrigMonoFont
    # Do this so PROGFrm{} does not need to exist.
    if Find is False:
        LFrm = MYDTFrame = Toplevel(Parent)
    else:
        PROGFrm["MYDT"] = LFrm = MYDTFrame = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.protocol("WM_DELETE_WINDOW", Command(formMYDTReturn, "ok"))
    # A number shows up in the title bar if you do .title(""), and if you don't
    # set the title it ends up with the program name in it.
    if len(Title) == 0:
        Title = " "
    LFrm.title(Title)
    LFrm.iconname(Title)
    # Gets rid of some of the extra title bar buttons.
    LFrm.transient(Parent)
    Sub = Frame(LFrm)
    GSG = False
    if BgFg.find("X") != -1:
        GSG = True
        BgFg = BgFg.replace("X", "")
    if len(BgFg) == 0:
        BgFg = "WB"
    if Find is False:
        LTxt = MYDTText = Text(Sub, bg=Clr[BgFg[0]], fg=Clr[BgFg[1]],
                               width=90, height=25, wrap=Wrap, font=TheFont,
                               relief=SUNKEN)
    else:
        PROGTxt["MYDT"] = LTxt = MYDTText = Text(Sub, bg=Clr[BgFg[0]],
                                                 fg=Clr[BgFg[1]], width=90,
                                                 height=25, wrap=Wrap,
                                                 font=TheFont, relief=SUNKEN)
    LTxt.pack(side=LEFT, expand=YES, fill=BOTH)
    LSb = Scrollbar(Sub, orient=VERTICAL, command=LTxt.yview)
    LSb.pack(side=RIGHT, fill=Y)
    LTxt.configure(yscrollcommand=LSb.set)
    Sub.pack(side=TOP, expand=YES, fill=BOTH)
    LSb = Scrollbar(LFrm, orient=HORIZONTAL, command=LTxt.xview)
    LSb.pack(side=TOP, fill=X)
    LTxt.configure(xscrollcommand=LSb.set)
    for Line in TheText:
        if Line.startswith("CCBB"):
            Cb = Checkbutton(LTxt, bg=Clr[BgFg[0]], bd=0,
                             activebackground=Clr[BgFg[0]
                                                  ], highlightthickness=0,
                             cursor="left_ptr")
            LTxt.window_create(END, window=Cb)
            Line = Line[4:]
        MYDTText.insert(END, Line + "\n")
    if Find is True:
        Sub = Frame(LFrm)
        labelTip(Sub, "Find:=", LEFT, 30, "[Find]")
        LEnt = Entry(Sub, width=20, textvariable=MYDTFindLookForVar)
        LEnt.pack(side=LEFT)
        LEnt.bind("<Return>", Command(formFind, "MYDT", "MYDT"))
        LEnt.bind("<KP_Enter>", Command(formFind, "MYDT", "MYDT"))
        BButton(Sub, text="Find", command=Command(formFind, "MYDT",
                                                  "MYDT")).pack(side=LEFT)
        BButton(Sub, text="Next", command=Command(formFindNext, "MYDT",
                                                  "MYDT")).pack(side=LEFT)
        Label(Sub, text=" ").pack(side=LEFT)
        Sub.pack(side=TOP, padx=3, pady=3)
    # Clicks like: (("OK", LEFT, "ok"), ("Close", LEFT, "close"))
    One = False
    Sub = Frame(LFrm)
    for Click in Clicks:
        if One is True:
            Label(Sub, text=" ").pack(side=LEFT)
        But = BButton(Sub, text=Click[0], command=Command(formMYDTReturn,
                                                          Click[2]))
        if Click[0].startswith("Clear") or Click[0].startswith("(Clear"):
            But.configure(fg=Clr["U"], activeforeground=Clr["U"])
        elif Click[0].startswith("Close") or Click[0].startswith("(Close"):
            But.configure(fg=Clr["R"], activeforeground=Clr["R"])
        But.pack(side=Click[1])
        # Check to see if there is ToolTip text.
        try:
            ToolTip(But, Click[3], Click[4])
        except Exception:
            pass
        if Click[0].startswith("(") and Click[0].endswith(")"):
            LFrm.bind("<Return>", Command(formMYDTReturn, Click[2]))
            LFrm.bind("<KP_Enter>", Command(formMYDTReturn, Click[2]))
        if Click[1] != TOP:
            One = True
    Sub.pack(side=TOP, padx=3, pady=3)
    # The only time it will be needed.
    if Find is True:
        PROGMsg["MYDT"] = Text(LFrm, font=PROGPropFont, height=3,
                               wrap=WORD)
        PROGMsg["MYDT"].pack(side=TOP, fill=X)
    center(Parent, LFrm, "CX", "I", True)
    # If the user clicks to dismiss the window before any one of these things
    # get taken care of then there will be trouble. This may only happen when
    # the user is running the program over a network and not on the local
    # machine. It has something to do with changes made to the beep() routine
    # which fixed a problem of occasional missing beeps.
    try:
        LFrm.focus_set()
        if GSG is False:
            LFrm.grab_set()
        else:
            LFrm.grab_set_global()
        # Everything will pause here until one of the buttons are pressed if
        # there are any, then the box will be destroyed, but the value will
        # still be returned. since it was saved in a global.
        if len(Clicks) != 0:
            LFrm.wait_window()
    except Exception:
        pass
    return MYDTAnswerVar.get()
#######################################
# BEGIN: formMYDTReturn(What, e = None)
# FUNC:formMYDReturn():2012.263


def formMYDTReturn(What, e=None):
    MYDTAnswerVar.set(What)
    MYDTFrame.destroy()
    updateMe(0)
    return
# END: formMYDT


##################
# BEGIN: formLOG()
# FUNC:formLOG():2019.002
# Handles the window that displays the LOG messages in QPLogs.
PROGFrm["LOG"] = None
PROGTxt["LOG"] = None
LOGFindLookForVar = StringVar()
LOGFilespecVar = StringVar()
PROGSetups += ["LOGFindLookForVar", "LOGFilespecVar"]
LOGFindLastLookForVar = StringVar()
LOGFindLinesVar = StringVar()
LOGFindIndexVar = IntVar()
LOGFindUseCaseCVar = IntVar()
LOGStopBut = None


# @profile(sort_by='cumulative', lines_to_print=10, strip_dirs=True)
def formLOG():
    global QPLogs
    global LOGStopBut
    if PROGFrm["LOG"] is not None:
        PROGFrm["LOG"].deiconify()
        PROGFrm["LOG"].lift()
        LTxt = PROGTxt["LOG"]
    else:
        LFrm = PROGFrm["LOG"] = Toplevel(Root)
        LFrm.withdraw()
        LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "LOG"))
        LFrm.title("LOG Messages")
        LFrm.iconname("LOG")
        Sub = Frame(LFrm)
        SSub = Frame(Sub)
        LTxt = PROGTxt["LOG"] = Text(SSub, font=PROGMonoFont, wrap=NONE,
                                     width=95, relief=SUNKEN)
        LTxt.pack(side=LEFT, expand=YES, fill=BOTH)
        LSb = Scrollbar(SSub, orient=VERTICAL, command=LTxt.yview)
        LSb.pack(side=RIGHT, fill=Y)
        LTxt.configure(yscrollcommand=LSb.set)
        SSub.pack(side=TOP, expand=YES, fill=BOTH)
        LSb = Scrollbar(Sub, command=LTxt.xview, orient=HORIZONTAL)
        LSb.pack(side=BOTTOM, fill=X)
        LTxt.configure(xscrollcommand=LSb.set)
        Sub.pack(side=TOP, expand=YES, fill=BOTH, padx=1, pady=1)
        Sub = Frame(LFrm)
        labelTip(Sub, "Find:=", LEFT, 30, "[Find]")
        LEnt = Entry(Sub, width=20, textvariable=LOGFindLookForVar)
        LEnt.pack(side=LEFT)
        LEnt.bind("<Return>", Command(formFind, "LOG", "LOG"))
        LEnt.bind("<KP_Enter>", Command(formFind, "LOG", "LOG"))
        BButton(Sub, text="Find",
                command=Command(formFind, "LOG", "LOG")).pack(side=LEFT)
        BButton(Sub, text="Next",
                command=Command(formFindNext, "LOG", "LOG")).pack(side=LEFT)
        Label(Sub, text=" ").pack(side=LEFT)
        LOGStopBut = BButton(Sub, text="Stop",
                             command=Command(formLOGControl, "stop"))
        LOGStopBut.pack(side=LEFT)
        Label(Sub, text=" ").pack(side=LEFT)
        BButton(Sub, text="Write To File",
                command=Command(formWrite, "LOG", "LOG", "LOG",
                                "Write LOG Messages", LOGFilespecVar, True,
                                "", True, False)).pack(side=LEFT)
        Label(Sub, text=" ").pack(side=LEFT)
        BButton(Sub, text="Close", fg=Clr["R"],
                command=Command(formClose, "LOG")).pack(side=LEFT)
        Sub.pack(side=TOP, padx=3, pady=3)
        PROGMsg["LOG"] = Text(LFrm, font=PROGPropFont, height=3,
                              wrap=WORD)
        PROGMsg["LOG"].pack(side=TOP, fill=X)
        center(Root, LFrm, "NW", "O", True)
    # FINISHME - this is where we need to start changing the library
    # (formHELP in this case) functions to handle things like no Work dir.
    # It should fall back to the Data dir if PROGWorkDirVar does not exist.
    # (this was PROGWorkDirVar)
    if len(LOGFilespecVar.get()) == 0 or \
            exists(dirname(LOGFilespecVar.get())) is False:
        LOGFilespecVar.set(PROGDataDirVar.get() + PROG_NAMELC + "log.txt")
    # Clear this so the formFind() routine does the right thing if this form
    # was brought up previously.
    LOGFindLinesVar.set("")
    if len(QPLogs) > 2000000:
        # Make sure it can be seen.
        showUp("LOG")
        formMYD("LOG", (("(OK)", TOP, "ok"), ), "ok", "YB", "Wow!",
                "There are more than 2,000,000 LOG messages. This may take "
                "quite a while to display. You can use the Stop button if you "
                "want to stop early.")
    setMsg("LOG", "CB", "Working...")
    formLOGControl("go")
    LTxt.delete(0.0, END)
    LineCount = 0
    for Line in QPLogs:
        LTxt.insert(END, Line + "\n")
        LineCount += 1
        if LineCount % 500000 == 0:
            LOGStopBut.update()
            if LOGRunning == 0:
                formLOGControl("stop")
                break
            setMsg("LOG", "CB", "Working on line %d..." % LineCount)
    setMsg("LOG", "", "%d %s listed." % (LineCount, sP(LineCount, ("line",
                                                                   "lines"))))
    formLOGControl("stopped")
    return
#######################
# BEGIN: formLOGClear()
# FUNC:formLOGClear():2018.310


def formLOGClear():
    if PROGFrm["LOG"] is not None:
        PROGTxt["LOG"].delete(0.0, END)
        PROGTxt["LOG"].tag_delete("hilite")
        setMsg("LOG", "", "")
    return
#########################################
# BEGIN: formLOGHilite(Line, HClr = "YB")
# BEGIN:formLOGHilite():2019.002


def formLOGHilite(Line, HClr="YB"):
    LFrm = PROGFrm["LOG"]
    if LFrm is not None:
        LTxt = PROGTxt["LOG"]
        LTxt.tag_delete("hilite")
        LTxt.yview(Line - 5)
        LTxt.tag_add("hilite", "%d.0" % Line, "%d.end" % Line)
        LTxt.tag_configure("hilite", background=Clr[HClr[0]],
                           foreground=Clr[HClr[1]])
        LFrm.deiconify()
        LFrm.lift()
    return
##############################
# BEGIN: formLOGMsg(MClr, Msg)
# FUNC:formLOGMsg():2018.310


def formLOGMsg(MClr, Msg):
    if PROGFrm["LOG"] is not None:
        setMsg("LOG", MClr, Msg)
    return
###############################
# BEGIN: formLOGControl(Action)
# FUNC:formLOGControl():2012.318


def formLOGControl(Action):
    global LOGRunning
    if Action == "go":
        buttonBG(LOGStopBut, "R", NORMAL)
        LOGRunning = 1
        return
    elif Action == "stop":
        if LOGRunning == 0:
            beep(1)
        else:
            buttonBG(LOGStopBut, "Y", NORMAL)
    elif Action == "stopped":
        buttonBG(LOGStopBut, "D", DISABLED)
    LOGRunning = 0
    return
# END: formLOG


######################
# BEGIN: formLOGSRCH()
# FUNC:formLOGSRCH():2018.310
PROGFrm["LOGSRCH"] = None
LOGSRCHSearchVar = StringVar()
PROGSetups += ["LOGSRCHSearchVar"]


def formLOGSRCH():
    if PROGFrm["LOGSRCH"] is not None:
        PROGFrm["LOGSRCH"].deiconify()
        PROGFrm["LOGSRCH"].lift()
        return
    # Create the window and put it in the middle of the screen.
    LFrm = PROGFrm["LOGSRCH"] = Toplevel(Root)
    LFrm.withdraw()
    LFrm.title("LOG Search")
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "LOGSRCH"))
    # Where the user will enter the search string.
    LEnt = Entry(LFrm, textvariable=LOGSRCHSearchVar)
    LEnt.pack(side=TOP, fill=X)
    LEnt.bind('<Return>', formLOGSRCHGo)
    LEnt.focus_set()
    LEnt.icursor(END)
    # Where the search results will be.
    Sub = Frame(LFrm)
    PROGTxt["LOGSRCH"] = Text(Sub, width=80, height=20, wrap=NONE,
                              relief=SUNKEN)
    PROGTxt["LOGSRCH"].pack(side=LEFT, expand=YES, fill=BOTH)
    Scroll = Scrollbar(Sub, orient=VERTICAL,
                       command=PROGTxt["LOGSRCH"].yview)
    Scroll.pack(side=RIGHT, fill=Y)
    PROGTxt["LOGSRCH"].configure(yscrollcommand=Scroll.set)
    Sub.pack(side=TOP, fill=BOTH, expand=YES)
    Sub = Frame(LFrm)
    BButton(Sub, text="Search", command=formLOGSRCHGo).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Close", fg=Clr["R"],
            command=Command(formClose, "LOGSRCH")).pack(side=LEFT)
    Sub.pack(side=TOP, padx=3, pady=3)
    # Status messages.
    PROGMsg["LOGSRCH"] = Text(LFrm, font=PROGPropFont, height=1)
    PROGMsg["LOGSRCH"].pack(side=TOP, fill=X)
    setMsg("LOGSRCH", "WB",
           "Enter what to search for in the field at the top of the form.")
    center(Root, "LOGSRCH", "CX", "I", True)
    return
################################
# BEGIN: formLOGSRCHGo(e = None)
# FUNC:formLOGSRCHGo():2012.325


def formLOGSRCHGo(e=None):
    LTxt = PROGTxt["LOGSRCH"]
    LTxt.delete(0.0, END)
    updateMe(0)
    LookFor = LOGSRCHSearchVar.get().lower()
    if len(QPLogs) == 0:
        setMsg("LOGSRCH", "RW", "There are no LOG messages to search.", 2)
        return
    setMsg("LOGSRCH", "CB", "Searching...")
    busyCursor(1)
    Found = 0
    for Line in QPLogs:
        LLine = Line.lower()
        if LLine.find(LookFor) != -1:
            LTxt.insert(END, Line + "\n")
            Found += 1
    setMsg("LOGSRCH", "", "%d matching %s found." %
           (Found, sP(Found, ("line", "lines"))))
    busyCursor(0)
    return
# END: formLOGSRCH


#############################################################################
# BEGIN: formPCAL(Parent, Where, ChgBar, EntryVar, EntryWidget, Wait, Format,
#                OrigFont = False, Title = "")
# LIB:formPCAL():2019.030
#    Fills in EntryVar with a date selected by the user in YYYY:DOY,
#    YYYY-MM-DD, or YYYYMMMDD format. If a time like:
#        YYYY:DOY:HH:MM:SS
#        YYYY-MM-DD HH:MM:SS
#        YYYYMMMDD HH:MM:SS
#    is in the EntryVar value then that time will be preserved and returned
#    appended to the selected date. No error checking of the time is done.
#    Parent = String or PROGFrm[]
#    Where = center() Where value ("C", "N", "W"...)
#    ChgBar = If "" then the calling form's ChgBar will not be messed with.
#    EntryVar = The StringVar() to leave the selection in.
#    EntryWidget = The Entry() field that goes with the EntryVar.
#    Wait = True/False - should the form capture input until it is dissmissed?
#    Format = The format of the date to return. Can also be a StringVar that
#             contains the desired format string (as shown above).
#    OrigFont = True forces the dialog to use the system font that was going
#               to be used by the program before any user font settings were
#               set.
#    Title = An alternate title to use.
PROGFrm["PCAL"] = None
PCALYear = 0
PCALMonth = 0
PCALTime = ""
PCALText = None
PCALDtModeRVar = StringVar()
PCALDtModeRVar.set("dates")
PROGSetups += ["PCALDtModeRVar"]


def formPCAL(Parent, Where, ChgBar, EntryVar, EntryWidget, Wait, Format,
             OrigFont=False, Title=""):
    global PCALYear
    global PCALMonth
    global PCALTime
    global PCALText
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    if isinstance(Format, StringVar):
        Format = Format.get()
    # Set the calendar to the month of whatever may already be in the EntryVar
    # and seal with any time that was passed.
    PCALTime = ""
    if rtnPattern(EntryVar.get()).startswith("0000-0"):
        PCALYear = intt(EntryVar.get())
        PCALMonth = intt(EntryVar.get()[5:])
        Parts = EntryVar.get().split()
        if len(Parts) == 2:
            PCALTime = Parts[1]
    elif rtnPattern(EntryVar.get()).startswith("0000:0"):
        PCALYear = intt(EntryVar.get())
        DOY = intt(EntryVar.get()[5:])
        PCALMonth, DD = dt2Timeydoy2md(PCALYear, DOY)
        Parts = EntryVar.get().split(":")
        if len(Parts) > 2:
            Parts += (5 - len(Parts)) * ["0"]
            PCALTime = "%02d:%02d:%02d" % (intt(Parts[2]), intt(Parts[3]),
                                           intt(Parts[4]))
    elif rtnPattern(EntryVar.get()).startswith("0000AAA"):
        PCALYear = intt(EntryVar.get())
        MMM = EntryVar.get()[4:4 + 3].upper()
        PCALMonth = PROG_MONNUM[MMM]
        Parts = EntryVar.get().split()
        if len(Parts) == 2:
            PCALTime = Parts[1]
    if PROGFrm["PCAL"] is not None:
        formPCALMove("c", ChgBar, EntryVar, EntryWidget, Format)
        PROGFrm["PCAL"].deiconify()
        PROGFrm["PCAL"].lift()
        PROGFrm["PCAL"].focus_set()
        return
    LFrm = PROGFrm["PCAL"] = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "PCAL"))
    if len(Title) == 0:
        LFrm.title("Pick A Date")
    else:
        LFrm.title(Title)
    LFrm.iconname("PickCal")
    if Wait is True:
        # Gets rid of some of the extra title bar buttons.
        LFrm.transient(Parent)
    if OrigFont is False:
        PCALText = Text(LFrm, bg=Clr["W"], fg=Clr["B"], height=11,
                        width=29, relief=SUNKEN, state=DISABLED)
    else:
        PCALText = Text(LFrm, bg=Clr["W"], fg=Clr["B"],
                        font=PROGOrigMonoFont, height=11, width=29,
                        relief=SUNKEN, state=DISABLED)
    PCALText.pack(side=TOP, padx=3, pady=3)
    if PCALYear != 0:
        formPCALMove("c", ChgBar, EntryVar, EntryWidget, Format)
    else:
        formPCALMove("n", ChgBar, EntryVar, EntryWidget, Format)
    Sub = Frame(LFrm)
    BButton(Sub, text="<<",
            command=Command(formPCALMove, "-y", ChgBar, EntryVar, EntryWidget,
                            Format)).pack(side=LEFT)
    BButton(Sub, text="<",
            command=Command(formPCALMove, "-m", ChgBar, EntryVar, EntryWidget,
                            Format)).pack(side=LEFT)
    BButton(Sub, text="Today",
            command=Command(formPCALMove, "n", ChgBar, EntryVar, EntryWidget,
                            Format)).pack(side=LEFT)
    BButton(Sub, text=">",
            command=Command(formPCALMove, "+m", ChgBar, EntryVar, EntryWidget,
                            Format)).pack(side=LEFT)
    BButton(Sub, text=">>",
            command=Command(formPCALMove, "+y", ChgBar, EntryVar, EntryWidget,
                            Format)).pack(side=LEFT)
    Sub.pack(side=TOP, padx=3, pady=3)
    Sub = Frame(LFrm)
    Radiobutton(Sub, text="Dates", value="dates", variable=PCALDtModeRVar,
                command=Command(formPCALMove, "c", ChgBar, EntryVar,
                                EntryWidget, Format)).pack(side=LEFT)
    Radiobutton(Sub, text="DOY", value="doy", variable=PCALDtModeRVar,
                command=Command(formPCALMove, "c", ChgBar, EntryVar,
                                EntryWidget, Format)).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Close", fg=Clr["R"],
            command=Command(formClose, "PCAL")).pack(side=TOP)
    Sub.pack(side=TOP, padx=3, pady=3)
    center(Parent, LFrm, Where, "I", True)
    if Wait is True:
        LFrm.focus_set()
        # See formMYD().
        if PROGSystem == "dar" and PROG_PYVERS != 2:
            pass
        else:
            LFrm.grab_set()
        LFrm.wait_window()
    return
#######################################################
# BEGIN: formPCALCursorControl(Widget, Which, e = None)
# FUNC:formPCALCursorControl():2018.236


def formPCALCursorControl(Widget, Which, e=None):
    if e is None:
        Widget.config(cursor="")
    elif e.type == "7":
        Widget.config(cursor="%s" % Which)
    else:
        Widget.config(cursor="")
    return
##################################################################
# BEGIN: formPCALMove(What, ChgBar, EntryVar, EntryWidget, Format)
# FUNC:formPCALMove():2018.235
#   Handles changing the calendar form's display.


def formPCALMove(What, ChgBar, EntryVar, EntryWidget, Format):
    global PCALYear
    global PCALMonth
    global PCALText
    Year = PCALYear
    Month = PCALMonth
    if What == "-y":
        Year -= 1
    elif What == "-m":
        Month -= 1
    elif What == "n":
        (Year, Month, Day) = getGMT(4)
    elif What == "+m":
        Month += 1
    elif What == "+y":
        Year += 1
    elif What == "c":
        pass
    if Year < 1971:
        beep(1)
        return
    elif Year > 2050:
        beep(1)
        return
    if Month > 12:
        Year += 1
        Month = 1
    elif Month < 1:
        Year -= 1
        Month = 12
    PCALYear = Year
    PCALMonth = Month
    LTxt = PCALText
    LTxt.configure(state=NORMAL)
    LTxt.delete("0.0", END)
    # Otherwise "today" is cyan on every month.
    LTxt.tag_delete(*LTxt.tag_names())
    DtMode = PCALDtModeRVar.get()
    if DtMode == "dates":
        DOM1 = 0
    elif DtMode == "doy":
        DOM1 = PROG_FDOM[Month]
        if (Year % 4 == 0 and Year % 100 != 0) or Year % 400 == 0:
            if Month > 2:
                DOM1 += 1
    LTxt.insert(END, "\n%s\n\n " %
                (PROG_CALMON[Month] + " " + str(Year)).center(29))
    IdxS = LTxt.index(CURRENT)
    LTxt.tag_config(IdxS, background=Clr["W"], foreground=Clr["R"])
    LTxt.insert(END, "Sun", IdxS)
    IdxS = LTxt.index(CURRENT)
    LTxt.tag_config(IdxS, background=Clr["W"], foreground=Clr["U"])
    LTxt.insert(END, " Mon Tue Wed Thu Fri ", IdxS)
    IdxS = LTxt.index(CURRENT)
    LTxt.tag_config(IdxS, background=Clr["W"], foreground=Clr["R"])
    LTxt.insert(END, "Sat ", IdxS)
    LTxt.insert(END, "\n")
    All = monthcalendar(Year, Month)
    NowYear, NowMonth, NowDay = getGMT(4)
    TargetDay = DOM1 + NowDay
    for Week in All:
        LTxt.insert(END, " ")
        for DD in Week:
            if DD != 0:
                ThisDay = DOM1 + DD
                IdxS = LTxt.index(CURRENT)
                if ThisDay == TargetDay and Month == NowMonth and \
                        Year == NowYear:
                    LTxt.tag_config(IdxS, background=Clr["C"],
                                    foreground=Clr["B"])
                    LTxt.tag_bind(IdxS, "<Button-1>",
                                  Command(formPCALPicked, ChgBar, EntryVar,
                                          EntryWidget, (Year, Month, DD),
                                          Format))
                    LTxt.tag_bind(IdxS, "<Enter>",
                                  Command(formPCALCursorControl, LTxt,
                                          "right_ptr black white"))
                    LTxt.tag_bind(IdxS, "<Leave>",
                                  Command(formPCALCursorControl, LTxt, ""))
                else:
                    LTxt.tag_bind(IdxS, "<Button-1>",
                                  Command(formPCALPicked, ChgBar, EntryVar,
                                          EntryWidget, (Year, Month, DD),
                                          Format))
                    LTxt.tag_bind(IdxS, "<Enter>",
                                  Command(formPCALCursorControl, LTxt,
                                          "right_ptr black white"))
                    LTxt.tag_bind(IdxS, "<Leave>",
                                  Command(formPCALCursorControl, LTxt, ""))
                if DtMode == "dates":
                    if ThisDay < 10:
                        LTxt.insert(END, "  ")
                        LTxt.insert(END, "%d" % ThisDay, IdxS)
                        LTxt.insert(END, " ")
                    else:
                        LTxt.insert(END, " ")
                        LTxt.insert(END, "%d" % ThisDay, IdxS)
                        LTxt.insert(END, " ")
                elif DtMode == "doy":
                    LTxt.insert(END, "%03d" % ThisDay, IdxS)
                    LTxt.insert(END, " ")
            else:
                LTxt.insert(END, "    ")
        LTxt.insert(END, "\n")
    LTxt.configure(state=DISABLED)
    return
##############################################################################
# BEGIN: formPCALPicked(ChgBar, EntryVar, EntryWidget, Date, Format, e = None)
# FUNC:formPCALPicked():2018.236


def formPCALPicked(ChgBar, EntryVar, EntryWidget, Date, Format, e=None):
    if Format.startswith("YYYY-MM-DD"):
        if len(PCALTime) == 0:
            EntryVar.set("%d-%02d-%02d" % Date)
        else:
            EntryVar.set("%d-%02d-%02d %s" % (Date, PCALTime))  # noqa: F507
    elif Format == "YYYY:DOY":
        DOY = dt2Timeymd2doy(Date[0], Date[1], Date[2])
        if len(PCALTime) == 0:
            EntryVar.set("%d:%03d" % (Date[0], DOY))
        else:
            EntryVar.set("%d:%03d:%s" % (Date[0], DOY, PCALTime))
    elif Format == "YYYYMMMDD":
        if Date[1] > 0 and Date[1] < 13:
            MMM = PROG_CALMONS[Date[1]]
        else:
            MMM = "ERR"
        if len(PCALTime) == 0:
            EntryVar.set("%d%s%02d" % (Date[0], MMM, Date[2]))
        else:
            EntryVar.set("%d%s%02d %s" % (Date[0], MMM, Date[2], PCALTime))
    if EntryWidget is not None:
        EntryWidget.icursor(END)
    # The program or the ChgBar form may not have any changebars.
    try:
        if len(ChgBar) != 0:
            # This is relevent portion of chgPROGChgBar() just to get rid of
            # the reference.
            try:
                PROGBar[ChgBar].configure(bg=Clr["R"])
                eval("%sBarSVar" % ChgBar).set(1)
            except Exception:
                pass
    except NameError:
        pass
    formClose("PCAL")
    return
# END: formPCAL


#######################################
# BEGIN: formQPERR(MClr = "", Msg = "")
# FUNC:formQPERR():2019.002
#   Handles the window that displays the error messages that were collected
#   in QPErrors.
PROGFrm["QPERR"] = None
PROGTxt["QPERR"] = None
QPERRFindLookForVar = StringVar()
QPERRFilespecVar = StringVar()
PROGSetups += ["QPERRFindLookForVar", "QPERRFilespecVar"]
QPERRFindLastLookForVar = StringVar()
QPERRFindLinesVar = StringVar()
QPERRFindIndexVar = IntVar()
QPERRFindUseCaseCVar = IntVar()


def formQPERR(MClr="", Msg=""):
    global QPErrors
    if PROGFrm["QPERR"] is not None:
        PROGFrm["QPERR"].deiconify()
        PROGFrm["QPERR"].lift()
        LTxt = PROGTxt["QPERR"]
    else:
        LFrm = PROGFrm["QPERR"] = Toplevel(Root)
        LFrm.withdraw()
        LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "QPERR"))
        LFrm.title("Errors/Messages")
        LFrm.iconname("QPERR")
        Sub = Frame(LFrm)
        SSub = Frame(Sub)
        LTxt = PROGTxt["QPERR"] = Text(SSub, font=PROGMonoFont,
                                       wrap=NONE, height=12, width=95,
                                       relief=SUNKEN)
        LTxt.pack(side=LEFT, expand=YES, fill=BOTH)
        LSb = Scrollbar(SSub, orient=VERTICAL, command=LTxt.yview)
        LSb.pack(side=RIGHT, fill=Y)
        LTxt.configure(yscrollcommand=LSb.set)
        SSub.pack(side=TOP, expand=YES, fill=BOTH)
        LSb = Scrollbar(Sub, command=LTxt.xview, orient=HORIZONTAL)
        LSb.pack(side=BOTTOM, fill=X)
        LTxt.configure(xscrollcommand=LSb.set)
        Sub.pack(side=TOP, expand=YES, fill=BOTH, padx=1, pady=1)
        Sub = Frame(LFrm)
        labelTip(Sub, "Find:=", LEFT, 30, "[Find]")
        LEnt = Entry(Sub, width=20, textvariable=QPERRFindLookForVar)
        LEnt.pack(side=LEFT)
        LEnt.bind("<Return>", Command(formFind, "QPERR", "QPERR"))
        LEnt.bind("<KP_Enter>", Command(formFind, "QPERR", "QPERR"))
        BButton(Sub, text="Find",
                command=Command(formFind, "QPERR", "QPERR")).pack(side=LEFT)
        BButton(Sub, text="Next",
                command=Command(formFindNext, "QPERR",
                                "QPERR")).pack(side=LEFT)
        Label(Sub, text=" ").pack(side=LEFT)
        BButton(Sub, text="Write To File",
                command=Command(formWrite, "QPERR", "QPERR", "QPERR",
                                "Write QPErrors Lines", QPERRFilespecVar,
                                True, "", True, False)).pack(side=LEFT)
        Label(Sub, text=" ").pack(side=LEFT)
        BButton(Sub, text="Close", fg=Clr["R"],
                command=Command(formClose, "QPERR")).pack(side=LEFT)
        Sub.pack(side=TOP, padx=3, pady=3)
        PROGMsg["QPERR"] = Text(LFrm, font=PROGPropFont, height=3,
                                wrap=WORD)
        PROGMsg["QPERR"].pack(side=TOP, fill=X)
        center(Root, LFrm, "N", "O", True)
    # FINISHME - this is where we need to start changing the library (formHELP
    # in this case) functions to handle things like no Work dir. It should fall
    # back to the Data dir if PROGWorkDirVar does not exist.
    # (this was PROGWorkDirVar)
    if len(QPERRFilespecVar.get()) == 0 or \
            exists(dirname(QPERRFilespecVar.get())) is False:
        QPERRFilespecVar.set(PROGDataDirVar.get() + PROG_NAMELC + "log.txt")
    # Clear this so the formFind() routine does the right thing if this form
    # was brought up previously.
    QPERRFindLinesVar.set("")
    setMsg("QPERR", "CB", "Working...")
    LTxt.delete(0.0, END)
    for Line in QPErrors:
        # These are standard error messages which I will figure out how to
        # display in all their glory someday. FINISHME
        # Sometimes the Filespec where the error occurs is in Line[4] and
        # sometimes not. Use the value to figure out the best way to print the
        # messages.
        if Line[0] == 1:
            LTxt.insert(END, "%s\n" % Line[2])
        elif Line[0] == 4:
            LTxt.insert(END, "%s  File: %s\n" % (Line[2], basename(Line[4])))
    setMsg("QPERR", "", "%d %s encountered." %
           (len(QPErrors), sP(len(QPErrors), ("error", "errors"))))
    return
#########################
# BEGIN: formQPERRClear()
# FUNC:formQPERRClear():2018.310


def formQPERRClear():
    if PROGFrm["QPERR"] is not None:
        PROGTxt["QPERR"].delete(0.0, END)
        setMsg("QPERR", "", "")
    return
################################
# BEGIN: formQPERRMsg(MClr, Msg)
# FUNC:formQPERRMsg(MClr, Msg):2018.310


def formQPERRMsg(MClr, Msg):
    if PROGFrm["QPERR"] is not None:
        setMsg("QPERR", MClr, Msg)
    return
# END: formQPERR


###################
# BEGIN: formSFCI()
# FUNC:formSFCI():2013.049
#   Scans through the selected source on the main display and makes a list of
#   all of the channels it finds in the file. The user can then rearrange them
#   in the order they want, copy the list to a "clipboard" (!!) and then
#   paste it into a slot in the Channel Preferences form. Fancy schmancy.
PROGFrm["SFCI"] = None
SFCISubButtons = None
SFCIButtons = []
SFCIIDsVar = StringVar()
SFCIChans = []
SFCIStaIDs = []
SFCIScanBut = None
SFCIStopBut = None
SFCIRunning = 0


def formSFCI():
    global SFCISubButtons
    global SFCIScanBut
    global SFCIStopBut
    if showUp("SFCI"):
        return
    LFrm = PROGFrm["SFCI"] = Toplevel(Root)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "SFCI"))
    LFrm.title("Scan For Channel IDs")
    LFrm.iconname("SFCIds")
    Label(LFrm,
          text="Select the source to read on the main display (as well as the "
               "desired '.bms Dir'\nradiobutton, if appropriate) and click "
               "the Scan button below.").pack(side=TOP)
    # Where the buttons will be created.
    SFCISubButtons = Frame(LFrm)
    SFCISubButtons.pack(side=TOP, padx=10, pady=10)
    Entry(LFrm, width=120, textvariable=SFCIIDsVar).pack(side=TOP,
                                                         padx=3)
    Sub = Frame(LFrm)
    labelTip(Sub, ".bms Dir:", LEFT, 35,
             "Select which directory on a baler memory stick to read if "
             "applicable.")
    Radiobutton(Sub, text="data" + sep, variable=OPTBMSDataDirRVar,
                value="data").pack(side=LEFT)
    Radiobutton(Sub, text="sdata" + sep, variable=OPTBMSDataDirRVar,
                value="sdata").pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    SFCIScanBut = BButton(Sub, text="Scan", command=formSFCIGo)
    SFCIScanBut.pack(side=LEFT)
    SFCIStopBut = BButton(Sub, text="Stop", state=DISABLED,
                          command=Command(formSFCIControl, "stop"))
    SFCIStopBut.pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Add All", command=formSFCIAll).pack(side=LEFT)
    BButton(Sub, text="Copy", command=formSFCICopy).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Channel Preferences",
            command=formCPREF).pack(side=LEFT)
    BButton(Sub, text="Clear", fg=Clr["U"],
            command=Command(formSFCIClear, "ids")).pack(side=LEFT)
    BButton(Sub, text="Close", fg=Clr["R"],
            command=Command(formSFCIControl, "close")).pack(side=LEFT)
    Sub.pack(side=TOP, padx=3, pady=3)
    PROGMsg["SFCI"] = Text(LFrm, font=PROGPropFont, height=2)
    PROGMsg["SFCI"].pack(side=LEFT, fill=X, expand=YES)
    # Bring up any old set of buttons.
    formSFCIMakeButtons()
    center(Root, LFrm, "W", "O", True)
    return
#####################
# BEGIN: formSFCIGo()
# FUNC:formSFCIGo():2019.232
#   This will basically be a simplified combination of fileSelected() and
#   then q330Process().


def formSFCIGo():
    global SFCIButtons
    setMsg("MF")
    formTPSMsg("", "")
    formLOGMsg("", "")
    formQPERRMsg("", "")
    PROGFrm["SFCI"].focus_set()
    formSFCIControl("go")
    DataDir = PROGDataDirVar.get()
    # Just in case.
    if DataDir.endswith(sep) is False:
        DataDir += sep
        PROGDataDirVar.set(DataDir)
    Sel = MFFiles.curselection()
    if len(Sel) == 0 or len(MFFiles.get(Sel[0])) == 0:
        setMsg("SFCI", "RW",
               "No data source files/folders have been selected.", 2)
        formSFCIControl("stopped")
        return
    # Only one-at-a-time for this routine.
    if len(Sel) > 1:
        setMsg("SFCI", "RW",
               "Only one data source file/folder may be selected for "
               "scanning.", 2)
        formSFCIControl("stopped")
        return
    OpMode = OPTOpModeRVar.get()
    if OpMode == "Normal":
        Filename = MFFiles.get(Sel[0])
        # Most items may have a (x bytes) or something like that.
        if Filename.find(" (") != -1:
            Filename = Filename[:Filename.index(" (")].strip()
        Filespec = DataDir + Filename
        Message = "Scanning %s..." % Filespec
        setMsg("SFCI", "CB", Message)
        BMSDataDirBut = OPTBMSDataDirRVar.get()
        # The directory may have gotten changed, but the list of files not.
        if exists(Filespec) is False:
            setMsg("SFCI", "RW", "File %s does not exist." % Filespec, 2)
            formSFCIControl("stopped")
            return
        formSFCIClear("all")
        FilespecLC = Filespec.lower()
        if FilespecLC.endswith(".bms"):
            if BMSDataDirBut not in ("data", "sdata"):
                setMsg("SFCI", "RW",
                       "Select the '.bms Dir' checkbutton again.", 2)
                formSFCIControl("stopped")
                return
            # Could be multiple data/sdata dirs on a stick: "data" and
            # "data-<dt>".
            try:
                FilesOnStick = listdir(Filespec)
            except OSError as e:
                setMsg("MF", "MW", "Error listing files:\n   %s" % e, 3)
                formSFCIControl("stopped")
                return
            BMSDataDirs = []
            for File in FilesOnStick:
                if File.startswith(BMSDataDirBut):
                    BMSDataDirs.append(File)
            if len(BMSDataDirs) == 0:
                setMsg("MF", "RW", "No %s directories found in %s." %
                       (BMSDataDirBut, Filespec), 2)
                fileSelectedStop("", "")
                return
            BMSDataDirs.sort()
            # Collect all of the files from the directories in here then start
            # reading through them.
            Files = []
            for BMSDataDir in BMSDataDirs:
                if exists(Filespec + sep + BMSDataDir + sep) is False:
                    setMsg("SFCI", "RW", "%s does not exist." %
                           (Filespec + sep + BMSDataDir + sep), 2)
                    formSFCIControl("stopped")
                    return
                Ret = walkDirs2(Filespec, False, sep + BMSDataDir + sep, "", 1)
                if Ret[0] != 0:
                    setMsg("SFCI", Ret)
                    formSFCIControl("stopped")
                    return
                Files += Ret[1]
            if len(Files) == 0:
                setMsg("SFCI", "RW",
                       "No files were found in %s directories in %s" %
                       (BMSDataDirBut, Filespec + sep), 2)
                formSFCIControl("stopped")
                return
            Files.sort()
            FileCount = 0
            for File in Files:
                FileCount += 1
                if FileCount % 100 == 0:
                    setMsg("SFCI", "CB", "%s\nWorking on file %d of %d..." %
                           (Message, FileCount, len(Files)))
                formSFCIFileProcess(File, "", False)
                # Check for stopping each file.
                SFCIStopBut.update()
                # Just break out and show what we've collected so far.
                if SFCIRunning == 0:
                    break
        elif FilespecLC.endswith(".all"):
            formSFCIFileProcess(Filespec, Message, False)
        elif FilespecLC.endswith(".soh"):
            formSFCIFileProcess(Filespec, Message, False)
        elif FilespecLC.endswith(".sdr"):
            Ret = walkDirs2(Filespec, False, "", "", 0)
            if Ret[0] != 0:
                setMsg("SFCI", Ret)
                formSFCIControl("stopped")
                return
            Files = Ret[1]
            if len(Files) == 0:
                setMsg("SFCI", "RW", "No channel files were found in\n   %s" %
                       Filespec, 2)
                formSFCIControl("stopped")
                return
            Files.sort()
            FileCount = 0
            for File in Files:
                FileCount += 1
                if FileCount % 50 == 0:
                    setMsg("SFCI", "CB", "%s\nWorking on file %d of %d..." %
                           (Message, FileCount, len(Files)))
                formSFCIFileProcess(File, "", True)
                SFCIStopBut.update()
                if SFCIRunning == 0:
                    break
        elif FilespecLC.endswith(".nan"):
            Ret = walkDirs2(Filespec, False, "", "", 1)
            if Ret[0] != 0:
                setMsg("SFCI", Ret)
                formSFCIControl("stopped")
                return
            Files = Ret[1]
            if len(Files) == 0:
                setMsg("SFCI", "RW", "No channel files were found in\n   %s" %
                       (Filespec + sep), 2)
                formSFCIControl("stopped")
                return
            Files.sort()
            FileCount = 0
            for File in Files:
                FileCount += 1
                if FileCount % 50 == 0:
                    setMsg("SFCI", "CB", "%s\nWorking on file %d of %d..." %
                           (Message, FileCount, len(Files)))
                # Nanometrics miniseed files can be split or multiplexed,
                # so False here.
                formSFCIFileProcess(File, "", False)
                SFCIStopBut.update()
                if SFCIRunning == 0:
                    break
        elif FilespecLC.endswith(".ant"):
            Ret = walkDirs2(Filespec, False, "", "", 0)
            if Ret[0] != 0:
                setMsg("SFCI", Ret)
                formSFCIControl("stopped")
                return
            Files = Ret[1]
            if len(Files) == 0:
                setMsg("SFCI", "RW", "No channel files were found in\n   %s" %
                       (Filespec + sep), 2)
                formSFCIControl("stopped")
                return
            Files.sort()
            FileCount = 0
            for File in Files:
                FileCount += 1
                if FileCount % 50 == 0:
                    setMsg("SFCI", "CB", "%s\nWorking on file %d of %d..." %
                           (Message, FileCount, len(Files)))
                formSFCIFileProcess(File, "", False)
                SFCIStopBut.update()
                if SFCIRunning == 0:
                    break
        elif len(FilespecLC) != 0:
            setMsg("SFCI", "RW",
                   "I don't know what to do with the file/folder\n   %s" %
                   Filespec, 2)
            formSFCIControl("stopped")
            return
    elif OpMode == "B44Stick":
        formSFCIClear("all")
        # The data directory should be pointing in the memory stick.
        Filespec = DataDir
        Message = "Scanning %s..." % Filespec
        setMsg("SFCI", "CB", Message)
        BMSDataDirBut = OPTBMSDataDirRVar.get()
        # The directory may have gotten changed, but the list of files not.
        if exists(Filespec) is False:
            setMsg("SFCI", "RW", "File %s does not exist." % Filespec, 2)
            formSFCIControl("stopped")
            return
        if BMSDataDirBut not in ("data", "sdata"):
            setMsg("SFCI", "RW",
                   "Select the '.bms Dir' checkbutton again.", 2)
            formSFCIControl("stopped")
            return
        # Could be multiple data/sdata dirs on a stick: "data" and "data-<dt>".
        try:
            FilesOnStick = listdir(Filespec)
        except OSError as e:
            setMsg("MF", "MW", "Error listing files:\n   %s" % e, 3)
            formSFCIControl("stopped")
            return
        BMSDataDirs = []
        for File in FilesOnStick:
            if File.startswith(BMSDataDirBut):
                BMSDataDirs.append(File)
        if len(BMSDataDirs) == 0:
            setMsg("MF", "RW", "No %s directories found in %s." %
                   (BMSDataDirBut, Filespec), 2)
            fileSelectedStop("", "")
            return
        BMSDataDirs.sort()
        # Collect all of the files from the directories in here then start
        # reading through them.
        Files = []
        for BMSDataDir in BMSDataDirs:
            if exists(Filespec + sep + BMSDataDir + sep) is False:
                setMsg("SFCI", "RW", "%s does not exist." %
                       (Filespec + sep + BMSDataDir + sep), 2)
                formSFCIControl("stopped")
                return
            Ret = walkDirs2(Filespec, False, sep + BMSDataDir + sep, "", 1)
            if Ret[0] != 0:
                setMsg("SFCI", Ret)
                formSFCIControl("stopped")
                return
            Files += Ret[1]
        if len(Files) == 0:
            setMsg("SFCI", "RW",
                   "No files were found in %s directories in %s" %
                   (BMSDataDirBut, Filespec + sep), 2)
            formSFCIControl("stopped")
            return
        Files.sort()
        FileCount = 0
        for File in Files:
            FileCount += 1
            if FileCount % 100 == 0:
                setMsg("SFCI", "CB", "%s\nWorking on file %d of %d..." %
                       (Message, FileCount, len(Files)))
            formSFCIFileProcess(File, "", False)
            # Check for stopping each file.
            SFCIStopBut.update()
            # Just break out and show what we've collected so far.
            if SFCIRunning == 0:
                break
    elif OpMode == "AntRT":
        Station = MFFiles.get(Sel[0])
        # Most items may have a (x bytes) or something like that.
        if Station.find(" (") != -1:
            Station = Station[:Station.index(" (")].strip()
        Parts = Station.split("-")
        YearDir = Parts[0]
        TStation = "%s" % Parts[1]
        Message = "Scanning %s..." % Station
        setMsg("SFCI", "CB", Message)
        # This is the same idea as in loadSourceFiles(). We need to check
        # everything again in case the user did "something".
        Files = listdir(DataDir)
        Found = False
        for File in Files:
            if File == "db":
                Found = True
                break
        if Found is False:
            setMsg("SFCI", "YB", "No 'db' directory found.", 2)
            formSFCIControl("stopped")
            return
        # Scan for the year directories and throw out anything that doesn't
        # look like one.
        Ret = listdir(DataDir + "db")
        Years = []
        for Year in Ret:
            if len(Year) == 4 and intt(Year) != 0:
                Years.append(Year)
        # Now build a list of what should be the DOY directories and filter.
        Doys = []
        for Year in Years:
            Ret = listdir(DataDir + "db" + sep + Year)
            for Doy in Ret:
                if len(Doy) == 3 and intt(Doy) != 0:
                    Doys.append(Doy)
        Years.sort()
        Doys.sort()
        Filespecs = []
        # Step through everything and process the files that start with
        # TStation.
        for Year in Years:
            if Year != YearDir:
                continue
            for Doy in Doys:
                TheDir = DataDir + "db" + sep + Year + sep + Doy + sep
                # This rt mode may ask for directories that don't exists.
                # walkDirs2() will complain. Instead of that do this check
                # here. We'll save the complaining for important more things.
                if exists(TheDir) is False:
                    continue
                Ret = walkDirs2(TheDir, False, sep + TStation + ".", "", 0)
                if Ret[0] != 0:
                    setMsg("SFCI", Ret)
                    return
                Filespecs += Ret[1]
        Filespecs.sort()
        FileCount = 0
        for Filespec in Filespecs:
            FileCount += 1
            if FileCount % 100 == 0:
                setMsg("SFCI", "CB", "%s\nWorking on file %d of %d..." %
                       (Message, FileCount, len(Filespecs)))
            formSFCIFileProcess(Filespec, "", False)
            # Check for stopping each file.
            SFCIStopBut.update()
            # Just break out and show what we've collected so far.
            if SFCIRunning == 0:
                break
    elif OpMode == "NanoCard":
        Message = "Scanning %s..." % DataDir
        Filename = MFFiles.get(Sel[0])
        if Filename != "Nano Card":
            setMsg("SFCI", "RW",
                   "The item Nano Card must be selected for this operating "
                   "mode.", 2)
            formSFCIControl("stopped")
            return
        if exists(DataDir) is False:
            setMsg("SFCI", "RW", "Directory %s does not exist." % DataDir, 2)
            formSFCIControl("stopped")
            return
        setMsg("SFCI", "CB", Message)
        formSFCIClear("all")
        Ret = walkDirs2(DataDir, False, "", "", 1)
        if Ret[0] != 0:
            setMsg("SFCI", Ret)
            formSFCIControl("stopped")
            return
        Files = Ret[1]
        if len(Files) == 0:
            setMsg("SFCI", "RW", "No channel files were found in\n   %s" %
                   DataDir, 2)
            formSFCIControl("stopped")
            return
        Files.sort()
        FileCount = 0
        ListChans = OPTListChannelsCVar.get()
        for File in Files:
            FileCount += 1
            if FileCount % 30 == 0:
                setMsg("SFCI", "CB", "%s\nWorking on file %d of %d..." %
                       (Message, FileCount, len(Files)))
            Break = True
            # The seismic data files seem to be a single channel, but the SOH
            # files are multiplexed on a Centaur. Titan, sorta.
            if File.find("SOH") != -1:
                Break = False
            formSFCIFileProcess(File, "", Break, ListChans)
            SFCIStopBut.update()
            if SFCIRunning == 0:
                break
        if ListChans != 0:
            print("Channels list:")
            Keys = list(QPListChannels.keys())
            Keys.sort()
            for Key in Keys:
                print(Key)
                print("   %s" % QPListChannels[Key])
    elif OpMode == "SC3":
        # %%%%% not well tested
        YNS = MFFiles.get(Sel[0])
        Station = YNS.split(sep)[-1]
        if Station not in SFCIStaIDs:
            SFCIStaIDs.append(Station)
        setMsg("SFCI", "CB", "Scanning %s..." % YNS)
        ChanDirs = listdir(DataDir + YNS)
        for ChanDir in ChanDirs:
            # Get all of the files for each channel.
            Files = listdir(DataDir + YNS + sep + ChanDir)
            for File in Files:
                formSFCIFileProcess(DataDir + YNS + sep +
                                    ChanDir + sep + File, "", True)
                SFCIStopBut.update()
                if SFCIRunning == 0:
                    break
    if len(SFCIChans) == 0:
        setMsg("SFCI", "YB", "No channels were found!?", 2)
        formSFCIControl("stopped")
        return
    SFCIChans.sort()
    AllGood = formSFCIMakeButtons()
    Stas = list2Str(SFCIStaIDs)
    if AllGood is True:
        setMsg("SFCI", "", "Done. Channels found: %s  Stations found: %s" %
               (len(SFCIChans), Stas))
    else:
        setMsg("SFCI", "YB",
               "Done. Channels found: %s  Stations found: %s\nSome channels "
               "unrecognized. Requires changing the program to read them." %
               (len(SFCIChans), Stas))
    formSFCIControl("stopped")
    return
##############################
# BEGIN: formSFCIMakeButtons()
# FUNC:formSFCIMakeButtons():2013.191


def formSFCIMakeButtons():
    ChanCount = 0
    AllGood = True
    for Chan in SFCIChans:
        if ChanCount % 14 == 0:
            Sub = Frame(SFCISubButtons)
        # If this is a recognized channel then make a ToolTip description using
        # the plot label info, otherwise point it out by making the button
        # yellow.
        if Chan[:3] in QPChans:
            LBut = BButton(Sub, text=Chan, font=PROGMonoFont,
                           command=Command(formSFCIAdd, Chan))
            LBut.pack(side=LEFT)
            ToolTip(LBut, 30, QPChans[Chan[:3]][QPCHANS_LABEL])
        else:
            LBut = BButton(Sub, text=Chan, font=PROGMonoFont,
                           bg=Clr["Y"], command=Command(formSFCIAdd, Chan))
            LBut.pack(side=LEFT)
            ToolTip(LBut, 30, "Unrecognized channel.")
            AllGood = False
        SFCIButtons.append(LBut)
        ChanCount += 1
        if ChanCount % 14 == 0:
            Sub.pack(side=TOP)
            SFCIButtons.append(Sub)
    if ChanCount % 14 != 0:
        Sub.pack(side=TOP)
        SFCIButtons.append(Sub)
    return AllGood
#####################################################################
# BEGIN: formSFCIFileProcess(Filespec, Message, Break, ListChans = 0)
# FUNC:formSFCIFileProcess():2019.232
#   This reads enough of the files to get the channel names/codes, but just
#   returns a True or False as to whether things went OK. Who knows what the
#   caller might send it. The caller can keep track of how many errors there
#   were and let the user know at the end of scanning.
#   If Break is True then the routine can break out of reading a file after
#   the first block, because the file should only have one channel of data in
#   it.


def formSFCIFileProcess(Filespec, Message, Break, ListChans=0):
    global SFCIChans
    global SFCIStaIDs
    global QPListChannels
    # These come from Nanometrics systems. Read the first line of each file
    # type and look at the column header titles. Certain titles will correspond
    # to certain "channels".
    if Filespec.endswith(".csv"):
        try:
            Fp = openFile(Filespec, "r")
        except Exception:
            return False
        # Nanometrics is so bad at these headers I'll just make everything
        # lowercase.
        # They are also really bad about calling the same thing in different
        # models a different title.
        # could maybe make use of "Instrument"
        # FINISHME-find out about "Timing status"
        Line = Fp.readline().lower()
        Parts = Line.split(",")
        for Index in arange(0, len(Parts)):
            Parts[Index] = Parts[Index].strip()
        if "gps receiver status" in Parts:
            if "NGS" not in SFCIChans:
                SFCIChans.append("NGS")
        if "timing phase lock" in Parts:
            if "NPL" not in SFCIChans:
                SFCIChans.append("NPL")
        if "phase lock loop status" in Parts:
            if "NPL" not in SFCIChans:
                SFCIChans.append("NPL")
        if "timing uncertainty(ns)" in Parts:
            if "NTU" not in SFCIChans:
                SFCIChans.append("NTU")
        if "time uncertainty(ns)" in Parts:
            if "NTU" not in SFCIChans:
                SFCIChans.append("NTU")
        if "supply voltage(v)" in Parts:
            if "NSV" not in SFCIChans:
                SFCIChans.append("NSV")
        if "supply voltage(mv)" in Parts:
            if "NSV" not in SFCIChans:
                SFCIChans.append("NSV")
        if "total current(a)" in Parts:
            if "NTC" not in SFCIChans:
                SFCIChans.append("NTC")
        if "temperature(&deg;c)" in Parts:
            if "NTM" not in SFCIChans:
                SFCIChans.append("NTM")
        if "sensor soh voltage 1(v)" in Parts:
            if "NM1" not in SFCIChans:
                SFCIChans.append("NM1")
        if "sensor soh voltage 2(v)" in Parts:
            if "NM2" not in SFCIChans:
                SFCIChans.append("NM2")
        if "sensor soh voltage 3(v)" in Parts:
            if "NM3" not in SFCIChans:
                SFCIChans.append("NM3")
        if "timing error(ns)" in Parts:
            if "NTE" not in SFCIChans:
                SFCIChans.append("NTE")
        if "time error(ns)" in Parts:
            if "NTE" not in SFCIChans:
                SFCIChans.append("NTE")
        if "time quality(&#37;)" in Parts:
            if "NTQ" not in SFCIChans:
                SFCIChans.append("NTQ")
        if "timing dac count" in Parts:
            if "NTD" not in SFCIChans:
                SFCIChans.append("NTD")
        if "controller current(ma)" in Parts:
            if "NCC" not in SFCIChans:
                SFCIChans.append("NCC")
        if "digitizer current(ma)" in Parts:
            if "NDC" not in SFCIChans:
                SFCIChans.append("NDC")
        if "nmx bus current(ma)" in Parts:
            if "NNC" not in SFCIChans:
                SFCIChans.append("NNC")
        if "sensor current(ma)" in Parts:
            if "NSC" not in SFCIChans:
                SFCIChans.append("NSC")
        if "serial port current(ma)" in Parts:
            if "NSP" not in SFCIChans:
                SFCIChans.append("NSP")
        Fp.close()
        return True
    try:
        Fp = open(Filespec, "rb")
    except Exception:
        return False
    if ListChans != 0:
        if Filespec not in QPListChannels:
            QPListChannels[Filespec] = []
    Ret = findRecordSize(Fp)
    if Ret[0] != 0:
        Fp.close()
        return False
    RecordSize = Ret[1]
    if ListChans != 0:
        QPListChannels[Filespec].append(RecordSize)
    Fp.seek(0)
    # Read the file in 10 record chunks to make it faster.
    RecordsSize = RecordSize * 10
    RecordsReads = 0
    NoOfRecords = int(getsize(Filespec) / RecordSize)
    while True:
        Records = Fp.read(RecordsSize)
        # We're done with this file.
        if len(Records) == 0:
            Fp.close()
            return True
        RecordsReads += 10
        if RecordsReads % 10000 == 0:
            SFCIStopBut.update()
            if SFCIRunning == 0:
                # Nothing wrong here. The caller will handle the stopping.
                Fp.close()
                return True
            # Sometimes the caller will provide a file name for big files to
            # show and report the status of reading.
            if len(Message) != 0:
                setMsg("SFCI", "CB", "%s\nReading record %d of %d..." %
                       (Message, RecordsReads, NoOfRecords))
        for i in arange(0, 10):
            Ptr = RecordSize * i
            Record = Records[Ptr:Ptr + RecordSize]
            # Need another set of Records.
            if len(Record) < RecordSize:
                break
            if PROG_PYVERS == 3:
                ChanID = Record[15:18].decode("latin-1")
            # The Q330 may create an "empty" file (all 00H) and then not
            # finish filling it in. The read()s keep reading, but there's
            # nothing to process. This detects that and goes on to the next
            # file. This may only happen in .bms-type data.
            if ChanID == "\x00\x00\x00":
                Fp.close()
                return True
            if PROG_PYVERS == 3:
                LocID = Record[13:15].decode("latin-1").strip()
            ChanID = ChanID + LocID
            if ChanID not in SFCIChans:
                SFCIChans.append(ChanID)
            if ListChans != 0:
                if ChanID not in QPListChannels[Filespec]:
                    QPListChannels[Filespec].append(ChanID)
            # Check this on the first Record of a file.
            if RecordsReads == 10:
                if PROG_PYVERS == 3:
                    StaID = Record[8:13].decode("latin-1").strip()
                if StaID not in SFCIStaIDs:
                    SFCIStaIDs.append(StaID)
            if Break is True:
                Fp.close()
                return True
    return True
####################################
# BEGIN: formSFCIAdd(Chan, e = None)
# FUNC:formSFCIAdd(Chan, e = None):2012.305


def formSFCIAdd(Chan, e=None):
    setMsg("SFCI", "", "")
    Str = SFCIIDsVar.get()
    Str = Str.replace(" ", "")
    Str = Str.upper()
    Str = deComma(Str)
    # Warn the user. It gets hard to keep track of these things.
    if Str.find(Chan) != -1:
        setMsg("SFCI", "YB", "%s may already be in the list." % Chan)
    if len(Str) > 0:
        Str += ","
    Str += Chan
    SFCIIDsVar.set(Str)
    return
##############################
# BEGIN: formSFCIAll(e = None)
# FUNC:formSFCIAll(e = None):2018.310


def formSFCIAll(e=None):
    setMsg("SFCI", "", "")
    SFCIIDsVar.set("")
    Str = ""
    for Item in SFCIButtons:
        # Sub won't have a "text".
        try:
            Chan = Item.cget("text")
            if len(Chan) == 0:
                continue
            if len(Str) > 0:
                Str += ","
            Str += Chan
        except TclError:
            pass
    SFCIIDsVar.set(Str)
    return
##############################
# BEGIN: formSFCIClear(Action)
# FUNC:formSFCIClear():2013.049


def formSFCIClear(Action):
    global SFCIButtons
    global SFCIChans
    global SFCIStaIDs
    if Action == "all":
        for Item in SFCIButtons:
            Item.destroy()
        del SFCIChans[:]
        del SFCIStaIDs[:]
    SFCIIDsVar.set("")
    updateMe(0)
    return
#######################
# BEGIN: formSFCICopy()
# FUNC:formSFCICopy():2012.304


def formSFCICopy():
    Str = SFCIIDsVar.get()
    Str = Str.replace(" ", "")
    Str = Str.upper()
    Str = deComma(Str)
    # FINISHME - should probably look for anything other than letters, numbers
    # and commas
    SFCIIDsVar.set(Str)
    PROGClipboardVar.set(Str)
    setMsg("SFCI", "", "Copied.")
    return
##############################
# BEGIN: formSFCIControl(What)
# FUNC:formSFCIControl():2016.005


def formSFCIControl(What):
    global SFCIRunning
    if What == "go":
        buttonBG(SFCIScanBut, "G", NORMAL)
        buttonBG(SFCIStopBut, "R", NORMAL)
        SFCIRunning = 1
        return
    elif What == "stop":
        if SFCIRunning == 0:
            beep(1)
        else:
            buttonBG(SFCIStopBut, "Y", NORMAL)
    elif What == "stopped":
        buttonBG(SFCIScanBut, "D", NORMAL)
        buttonBG(SFCIStopBut, "D", DISABLED)
    elif What == "close":
        if SFCIRunning == 1:
            beep(1)
            return
        formClose("SFCI")
    SFCIRunning = 0
    return
# END: formSFCI


##################
# BEGIN: formTPS()
# FUNC:formTPS():2018.310
PROGFrm["TPS"] = None
TPSColorRangeRVar = StringVar()
TPSColorRangeRVar.set("med")
TPSPSFilespecVar = StringVar()
PROGSetups += ["TPSColorRangeRVar", "TPSPSFilespecVar"]
TPSBufLeft = 0
TPSPWidth = 0
TPSStart = 0
TPSRunning = 0
TPSPlotted = False
TPSPlotBut = None
TPSStopBut = None


def formTPS():
    global TPSPlotBut
    global TPSStopBut
    global TPSRunning
    global TPSPlotted
    if showUp("TPS") is False:
        LFrm = PROGFrm["TPS"] = Toplevel(Root)
        LFrm.withdraw()
        LFrm.protocol("WM_DELETE_WINDOW", Command(formTPSControl, "close"))
        LFrm.title("TPS Plot")
        Sub = Frame(LFrm)
        SSub = Frame(Sub)
        # 915 = 25+(3*288)+25, plus 1
        LCan = PROGCan["TPS"] = Canvas(SSub, bg=DClr["TC"],
                                       relief=SUNKEN, height=500, width=915,
                                       scrollregion=(0, 0, 915, 500))
        LCan.bind("<Button-1>", Command(formTPSTimeClick, -1, "", 0, 0))
        LCan.bind("<Control-Button-1>", Command(formTPSTimeClick, -1, "", 0,
                                                0))
        LCan.bind("<MouseWheel>", Command(mouseWheel, LCan))
        LCan.bind("<Button-4>", Command(mouseWheel, LCan))
        LCan.bind("<Button-5>", Command(mouseWheel, LCan))
        LCan.pack(side=LEFT, expand=YES, fill=BOTH)
        Sb = Scrollbar(SSub, orient=VERTICAL, command=LCan.yview)
        Sb.pack(side=RIGHT, expand=NO, fill=Y)
        LCan.configure(yscrollcommand=Sb.set)
        SSub.pack(side=TOP, expand=YES, fill=BOTH)
        Sb = Scrollbar(Sub, orient=HORIZONTAL, command=LCan.xview)
        Sb.pack(side=BOTTOM, expand=NO, fill=X)
        LCan.configure(xscrollcommand=Sb.set)
        Sub.pack(side=TOP, expand=YES, fill=BOTH)
        Sub = Frame(LFrm)
        SSub = Frame(Sub)
        SSSub = Frame(SSub)
        LRb = Radiobutton(SSSub, text="A", variable=TPSColorRangeRVar,
                          value="antarctica")
        LRb.pack(side=LEFT)
        ToolTip(LRb, 30, "\"Antarctica\" color range.")
        LRb = Radiobutton(SSSub, text="L", variable=TPSColorRangeRVar,
                          value="low")
        LRb.pack(side=LEFT)
        ToolTip(LRb, 30, "\"Low\" color range.")
        LRb = Radiobutton(SSSub, text="M", variable=TPSColorRangeRVar,
                          value="med")
        LRb.pack(side=LEFT)
        ToolTip(LRb, 30, "\"Medium\" color range.")
        LRb = Radiobutton(SSSub, text="H", variable=TPSColorRangeRVar,
                          value="high")
        LRb.pack(side=LEFT)
        ToolTip(LRb, 30, "\"High\" color range.")
        SSSub.pack(side=TOP)
        SSSub = Frame(SSub)
        labelTip(SSSub, "Chans:", LEFT, 35,
                 "Enter the channels to plot separated by a comma.")
        Entry(SSSub, textvariable=OPTPlotTPSChansVar,
              width=25).pack(side=LEFT)
        SSSub.pack(side=TOP)
        SSub.pack(side=LEFT, padx=3, pady=3)
        Label(Sub, text=" ").pack(side=LEFT)
        TPSPlotBut = BButton(Sub, text="Plot",
                             command=Command(formTPSPlot, "button"))
        TPSPlotBut.pack(side=LEFT)
        TPSStopBut = BButton(Sub, text="Stop", state=DISABLED,
                             command=Command(formTPSControl, "stop"))
        TPSStopBut.pack(side=LEFT)
        BButton(Sub, text="Write .ps",
                command=Command(formWritePS, "TPS", "TPS",
                                TPSPSFilespecVar)).pack(side=LEFT)
        Label(Sub, text=" ").pack(side=LEFT)
        PROGMsg["TPS"] = Text(Sub, font=PROGPropFont, height=3,
                              wrap=WORD, cursor="")
        PROGMsg["TPS"].pack(side=LEFT, fill=X, expand=YES)
        LLb = Label(Sub, text=" Hints ")
        LLb.pack(side=LEFT)
        ToolTip(LLb, 35, "PLOT AREA HINT:\n--Click on plot: Clicking on a "
                "point on the plot shows the time and average signal value at "
                "that position in the message area, and shows a rule at that "
                "position in time on the main plot display. (Control-click "
                "also does this.)\n\n--Click on left/right: Click on the far "
                "left or right to clear the time and signal level value.\n\n-"
                "-Click on dotted line: Clicking on a dotted day line will "
                "display the number of days that were skipped to produce that "
                "dotted day line.")
        Sub.pack(side=TOP, fill=X, expand=NO)
        if len(TPSPSFilespecVar.get()) == 0:
            TPSPSFilespecVar.set(PROGDataDirVar.get())
        center(Root, LFrm, "E", "O", True)
        TPSPlotted = False
        TPSRunning = 0
    return
################################
# BEGIN: formTPSPlot(Who = None)
# FUNC:formTPSPlot():2018.310
#   Outsiders can call this to bring up the form and start the plotting.


def formTPSPlot(Who=None):
    global TPSPlotted
    LFrm = PROGFrm["TPS"]
    # If this is being called by the Plot button then the user probably doesn't
    # want to close the form, and the form probably doesn't need to be brought
    # up. Set the Var in case it is not since the form is up and being used.
    if Who == "button":
        OPTPlotTPSCVar.set(1)
    elif Who is None:
        if OPTPlotTPSCVar.get() == 0:
            if LFrm is not None:
                formTPSControl("close")
            return
        else:
            if LFrm is None:
                formTPS()
    # Do this here, because clearing can take a long time.
    setMsg("TPS", "CB", "Working...")
    formTPSControl("go")
    formTPSClear(False)
    if MFPlotted is False:
        setMsg("TPS", "RW", "It doesn't look like anything has been read.", 2)
        formTPSControl("stopped")
        return
    Ret = setQPUserChannels("tpsplot")
    if Ret[0] != 0:
        setMsg("TPS", Ret)
        formTPSControl("stopped")
        return
    Ret = formTPSChkChans()
    if Ret[0] != 0:
        setMsg("TPS", Ret)
        formTPSControl("stopped")
        return
    formTPSGo()
    L, T, R, B = PROGCan["TPS"].bbox(ALL)
    PROGCan["TPS"].configure(scrollregion=(0, 0, R + 25, (B + 15)))
    if TPSRunning == 0:
        setMsg("TPS", "YB", "Stopped.")
    else:
        setMsg("TPS", "", "Done.")
    # Do this after the check above.
    formTPSControl("stopped")
    TPSPlotted = True
    return


####################
# BEGIN: formTPSGo()
# FUNC:formTPSGo():2018.310
TPSPlotData = []


def formTPSGo():
    global TPSBufLeft
    global TPSPWidth
    global TPSStart
    global TPSRunning
    global TPSPlotData
    LFrm = PROGFrm["TPS"]
    LFrm.focus_set()
    # Don't list all of the files. Do this here, instead of formTPSPlot() so we
    # can put the Filename at the top of the plots too.
    Filename = QPFilesProcessed[0]
    if len(QPFilesProcessed) > 1:
        Filename += "+"
    LFrm.title("TPS Plot - %s" % Filename)
    LCan = PROGCan["TPS"]
    LCan.configure(bg=DClr["TC"])
    ColorRange = TPSColorRangeRVar.get()
    # Set the ranges for the colors based on the A H M L radiobuttons.
    if ColorRange == "antarctica":
        Bin0 = 0.0
        Bin1 = 100.0   # 10
        Bin2 = 10000.0   # 100
        Bin3 = 1000000.0   # 1000
        Bin4 = 100000000.0   # 10000
        Bin5 = 10000000000.0   # 100000
        Bin6 = 1000000000000.0   # 1000000
    elif ColorRange == "low":
        Bin0 = 0.0
        Bin1 = 400.0   # 20
        Bin2 = 40000.0   # 200
        Bin3 = 4000000.0   # 2000
        Bin4 = 400000000.0   # 20000
        Bin5 = 40000000000.0   # 200000
        Bin6 = 4000000000000.0   # 2000000
    elif ColorRange == "med":
        Bin0 = 0.0
        Bin1 = 2500.0   # 50
        Bin2 = 250000.0   # 500
        Bin3 = 25000000.0   # 5000
        Bin4 = 2500000000.0   # 50000
        Bin5 = 250000000000.0   # 500000
        Bin6 = 25000000000000.0   # 5000000
    elif ColorRange == "high":
        Bin0 = 0.0
        Bin1 = 6400.0   # 80
        Bin2 = 640000.0   # 800
        Bin3 = 64000000.0   # 8000
        Bin4 = 6400000000.0   # 80000
        Bin5 = 640000000000.0   # 800000
        Bin6 = 64000000000000.0   # 8000000
    # The channels to plot (and the order to plot them).
    PlotChans = OPTPlotTPSChansVar.get().split(",")
    # Will be a list of lists with a list for each 5-minute bin as
    #    [[sum of squared counts, number of data points, AveValue, X, Y], ...]
    del TPSPlotData[:]
    # Make sure GY never becomes a float, otherwise it could make the day plot
    # lines run together and generally look really bad on some operating/X11
    # systems.
    GY = PROGPropFontHeight
    TPSBufLeft = 25
    # 3 pixels per 5 mins.
    TPSPWidth = 3 * 288
    # The display will always show whole days, but only plot the data
    # corresponding to the main form's time range.
    # This is going to bite me in the ass wrt leap seconds, but there you go.
    # It's only a second.
    TPSStart = int(QPEarliestCurr / 86400.0) * 86400
    TPSEnd = int(QPLatestCurr / 86400.0) * 86400 + 86400
    # Get local copies.
    EarliestCurr = QPEarliestCurr
    LatestCurr = QPLatestCurr
    Message = ("File: %s   Station: %s   %s  to  %s  (%s)" %
               (Filename, list2Str(QPStaIDs), dt2Time(-1, 80, EarliestCurr),
                dt2Time(-1, 80, LatestCurr),
                timeRangeFormat(LatestCurr - EarliestCurr)))
    canText(LCan, 10, GY, DClr["TX"], Message)
    GY += PROGPropFontHeight
    # Channel loop.
    for Chan in QPUserChannels:
        if Chan not in QPSampRates:
            continue
        # Since there can be wildcards (*) just a 'not in' won't work.
        Found = False
        for PlotChan in PlotChans:
            if PlotChan.endswith("*"):
                Aster = PlotChan.index("*")
                if Chan[:Aster] == PlotChan[:Aster]:
                    Found = True
                    break
            elif Chan == PlotChan:
                Found = True
                break
        if Found is False:
            continue
        TheQPData = QPData[Chan]
        # This shouldn't happen.
        if len(TheQPData) == 0:
            ID = canText(LCan, 10, GY, DClr["TX"],
                         "No data to plot for '%s'." % Chan)
            L, T, R, B = LCan.bbox(ID)
            ID2 = LCan.create_rectangle(L, T - 1, R, B, fill=DClr["TC"],
                                        outline=DClr["TC"])
            LCan.tag_raise(ID, ID2)
            GY += PROGPropFontHeight
            continue
        # The labels "in" the plot need to be printed, then a box the same
        # color as the background be made, then the text lifted above the box,
        # so the hour lines that will be generated at the end will not be
        # visible through the text.
        # That goes for the message above too.
        NewLabel = QPChans[Chan[:3]][QPCHANS_LABEL]
        NewLabel = Chan + NewLabel[3:]
        # This will probably always be turned on for seismic data.
        if QPChans[Chan[:3]][QPCHANS_SHOWSR] == 1:
            Rate = QPSampRates[Chan]
            if Rate >= 1.0:
                Str = "%dsps" % Rate
            else:
                Str = "%gsps" % Rate
        else:
            Rate = ""
            Str = ""
        ID = canText(LCan, 10, GY, DClr["TX"], "%s %s" % (NewLabel, Str))
        L, T, R, B = LCan.bbox(ID)
        ID2 = LCan.create_rectangle(L, T - 1, R, B, fill=DClr["TC"],
                                    outline=DClr["TC"])
        LCan.tag_raise(ID, ID2)
        GY += PROGPropFontHeight
        formTPSTimeNumbers(LCan, GY)
        GY += PROGPropFontHeight
        # Create a list with as many 5-minute bins as we are going to need to
        # cover TPSStart to TPSEnd days.
        # TPSPlotData[0] = Counts value
        # TPSPlotData[1] = Number of data points
        TPSPlotData = [[0, 0] for i in
                       arange(int((TPSEnd - TPSStart) / 86400.0) * 288)]
        # Now go through all of the raw data points, calculate which bin
        # created above each data point value should be added to based on the
        # epoch time of each point and add it in.
        for Data in TheQPData:
            if Data[0] < TPSStart:
                continue
            if Data[0] > TPSEnd:
                break
            # Filter once more so we only plot what the user has selected and
            # not whole days (that's what the above lets pass).
            if Data[0] < EarliestCurr:
                continue
            if Data[0] > LatestCurr:
                break
            Index = int((Data[0] - TPSStart) // 300)
            TPSPlotData[Index][0] += Data[1]**2
            TPSPlotData[Index][1] += 1
        # Now go through the bins by day and each bin within each day.
        # Was there data the day(s) before?
        WasData = True
        Missing = 0
        for DayIndex in arange(0, len(TPSPlotData), 288):
            setMsg("TPS", "CB", "Plotting channel '%s'..." % Chan)
            GX = TPSBufLeft
            # Take a quick glance at this day's data. If there is none for a
            # day or more in-a-row, then we will just draw a special line
            # covering that time, instead of drawing a black line for each
            # empty day. Was there data for this day?
            HasData = False
            for BinIndex in arange(DayIndex, DayIndex + 288):
                if TPSPlotData[BinIndex][1] != 0:
                    HasData = True
                    break
            # We've just run out of data or there still is no data, so keep
            # looking.
            if HasData is False:
                Missing += 1
                WasData = False
                continue
            # We're back, so draw the special line.
            if WasData is False:
                for i in arange(0, 288):
                    # Draw a dotted line.
                    if i % 2 == 0:
                        ID = LCan.create_rectangle(GX - 1, GY - 1, GX + 1,
                                                   GY + 1, fill=Clr["K"],
                                                   outline=Clr["K"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(formTPSDaysMissing, Missing))
                    # For next 5-minute block.
                    GX += 3
                # For the next line, today's line, that has data.
                GX = TPSBufLeft
                GY += 5
                WasData = True
                Missing = 0
                if TPSRunning == 0:
                    # These have to be done here, instead of formTPSPlot(),
                    # because the lines need to be done before the color key,
                    # and formTPSColorKey() needs the Bin values, which are
                    # defined in here to make them local for speed.
                    formTPSTimeNumbers(LCan, GY)
                    formTPSTimeLines(LCan)
                    formTPSColorKey(LCan, Bin0, Bin1, Bin2, Bin3, Bin4,
                                    Bin5, Bin6)
                    return
            # Draw a normal day's line.
            for BinIndex in arange(DayIndex, DayIndex + 288):
                # If there are no points in this bin check the bins on either
                # side. If they both have values then calculate an average and
                # use that.
                if TPSPlotData[BinIndex][1] == 0:
                    try:
                        if TPSPlotData[BinIndex - 1][1] != 0 and \
                                TPSPlotData[BinIndex + 1][1] != 0:
                            Value = (TPSPlotData[BinIndex - 1][0] +
                                     TPSPlotData[BinIndex + 1][0])
                            Points = (TPSPlotData[BinIndex - 1][1] +
                                      TPSPlotData[BinIndex + 1][1])
                            # Save the results so we don't have to do this
                            # again.
                            TPSPlotData[BinIndex][0] = Value
                            TPSPlotData[BinIndex][1] = Points
                            AveValue = Value / Points
                        else:
                            AveValue = 0.0
                    except Exception:
                        AveValue = 0.0
                else:
                    AveValue = float(TPSPlotData[BinIndex][0]) / \
                        TPSPlotData[BinIndex][1]
                if AveValue == Bin0:
                    C = "K"
                elif AveValue < Bin1:
                    C = "U"
                elif AveValue < Bin2:
                    C = "C"
                elif AveValue < Bin3:
                    C = "G"
                elif AveValue < Bin4:
                    C = "Y"
                elif AveValue < Bin5:
                    C = "R"
                elif AveValue < Bin6:
                    C = "M"
                else:
                    C = "E"
                ID = LCan.create_rectangle(GX - 1, GY - 1, GX + 1, GY + 2,
                                           fill=Clr[C], outline=Clr[C])
                LCan.tag_bind(ID, "<Button-1>",
                              Command(formTPSTimeClick, BinIndex,
                                      fmti(sqrt(AveValue)), GX, GY))
                LCan.tag_bind(ID, "<Control-Button-1>",
                              Command(formTPSTimeClick, BinIndex,
                                      fmti(sqrt(AveValue)), GX, GY))
                # For next 5-minute block.
                GX += 3
            # For next day.
            GY += 5
            updateMe(0)
            if TPSRunning == 0:
                formTPSTimeLines(LCan)
                formTPSTimeNumbers(LCan, 0)
                formTPSColorKey(LCan, Bin0, Bin1, Bin2, Bin3, Bin4, Bin5, Bin6)
                return
        # For next channel.
        GY += PROGPropFontHeight
        L, T, R, B = LCan.bbox(ALL)
        LCan.configure(scrollregion=(0, 0, R + 25, (B + 15)))
        updateMe(0)
        if TPSRunning == 0:
            formTPSTimeLines(LCan)
            formTPSTimeNumbers(LCan, 0)
            formTPSColorKey(LCan, Bin0, Bin1, Bin2, Bin3, Bin4, Bin5, Bin6)
            return
    formTPSTimeLines(LCan)
    formTPSTimeNumbers(LCan, 0)
    formTPSColorKey(LCan, Bin0, Bin1, Bin2, Bin3, Bin4, Bin5, Bin6)
    return
###############################
# BEGIN: formTPSTimeLines(LCan)
# FUNC:formTPSTimeLines():2018.310


def formTPSTimeLines(LCan):
    L, T, R, B = LCan.bbox(ALL)
    for i in arange(1, 24):
        XX = TPSBufLeft + (i * (3.0 * 288.0 / 24.0))
        ID = LCan.create_line(XX, T + PROGPropFontHeight * 2, XX, B + 5,
                              fill=Clr["A"])
        LCan.tag_lower(ID, ALL)
    return
#####################################
# BEGIN: formTPSTimeNumbers(LCan, GY)
# FUNC:formTPSTimeNumbers():2018.310


def formTPSTimeNumbers(LCan, GY):
    if GY == 0:
        L, T, R, B = LCan.bbox(ALL)
        GY = B + PROGPropFontHeight / 2
    for i in arange(4, 24, 4):
        XX = TPSBufLeft + (i * (3.0 * 288.0 / 24.0))
        # Text with a black background.
        ID = canText(LCan, XX, GY, DClr["TX"], "%02d" % i, "c")
        L, T, R, B = LCan.bbox(ID)
        ID2 = LCan.create_rectangle(L, T - 1, R, B, fill=DClr["TC"],
                                    outline=DClr["TC"])
        LCan.tag_raise(ID, ID2)
    return
########################################################################
# BEGIN: formTPSColorKey(LCan, Bin0, Bin1, Bin2, Bin3, Bin4, Bin5, Bin6)
# FUNC:formTPSColorKey():2012.322


def formTPSColorKey(LCan, Bin0, Bin1, Bin2, Bin3, Bin4, Bin5, Bin6):
    L, T, R, B = LCan.bbox(ALL)
    GY = B + 25
    Index = 0
    for C in ("K", "U", "C", "G", "Y", "R", "M", "E"):
        GYY = GY + PROGPropFontHeight * Index
        LCan.create_rectangle(25, GYY - 5, 40, GYY + 5, fill=Clr[C],
                              outline=Clr["B"])
        if Index == 0:
            canText(LCan, 45, GYY, DClr["TX"], "%s counts" %
                    fmti(int(sqrt(eval("Bin%d" % Index)))))
        elif Index != 7:
            canText(LCan, 45, GYY, DClr["TX"], "+/- %s counts" %
                    fmti(int(sqrt(eval("Bin%d" % Index)))))
        else:
            canText(LCan, 45, GYY, DClr["TX"], "> %s counts" %
                    fmti(int(sqrt(Bin6))))
        Index += 1
    return
##########################
# BEGIN: formTPSChkChans()
# FUNC:formTPSChkChans():2018.310
#    Do this as a separate function, instead of as just part of the regular
#    plotting so any error message can be displayed in the right place by
#    external callers. Just make sure it gets called before plotting, because
#    formTPS does not check for any errors.


def formTPSChkChans():
    # So callers don't have to check.
    if OPTPlotTPSCVar.get() == 0:
        return (0, )
    Str = OPTPlotTPSChansVar.get().strip().upper()
    if Str.endswith(","):
        Str = Str[:-1]
    Str = Str.replace(" ", "")
    if len(Str) == 0:
        return (1, "RW",
                "TPS Chans: No channels have been entered. Either enter some "
                "or uncheck the TPS checkbutton.", 2)
    OPTPlotTPSChansVar.set(Str)
    # Check to see that only "S" type channels are in the list.
    PlotChans = Str.split(",")
    try:
        # Get each entered channel and look for matches. If there is a wildcard
        # we'll go through QPChans and find at least one match. It's the best
        # we can do.
        for Chan in PlotChans:
            if Chan.find("*") == -1:
                if QPChans[Chan[:3]][QPCHANS_TYPE] != "S":
                    return (1, "RW",
                            "TPS Chans: '%s' is not a seismic data channel." %
                            Chan, 2)
            else:
                ChanPart = Chan[:Chan.index("*")]
                Found = False
                for Key in list(QPChans.keys()):
                    if Key.startswith(ChanPart):
                        if QPChans[Key][QPCHANS_TYPE] != "S":
                            return (1, "RW",
                                    "TPS Chans: '%s' matches %s which is not "
                                    "a seismic data channel." % (Chan, Key), 2)
                        # Set this, but go through all of them in case
                        # ChanPart also matches a channel that is not a seismic
                        # channel.
                        Found = True
                if Found is False:
                    return (1, "RW",
                            "TPS Chans: %s: Does not match any channel." %
                            Chan, 2)
    except KeyError:
        return (1, "RW", "TPS Chans: %s: Unknown channel." % Chan, 2)
        pass
    return (0, )
##############################
# BEGIN: formTPSMsg(Colr, Msg)
# FUNC:formTPSMsg():2012.322


def formTPSMsg(Colr, Msg):
    setMsg("TPS", Colr, Msg, 0)
    return
######################
# BEGIN: formTPSOpen()
# FUNC:formTPSOpen()
#   For others to call to bring up the TPS form (like from a menu command).


def formTPSOpen():
    OPTPlotTPSCVar.set(1)
    formTPS()
    return
#############################
# BEGIN: formTPSClear(ClrMsg)
# FUNC:formTPSClear():2018.310
#   This can be called by outsiders.


def formTPSClear(ClrMsg):
    global TPSPlotted
    # Clear the TPS plot if it is up.
    if PROGFrm["TPS"] is not None:
        PROGFrm["TPS"].title("TPS Plot")
        LCan = PROGCan["TPS"]
        LCan.delete("all")
        LCan.configure(scrollregion=(0, 0, 1, 1))
        LCan.configure(bg=DClr["TC"])
        if ClrMsg is True:
            setMsg("TPS", "", "")
    if OPTPlotTPSCVar.get() == 0:
        formTPSControl("close")
    TPSPlotted = False
#####################################
# BEGIN: formTPSDaysMissing(Value, e)
# FUNC:formTPSDaysMissing():2013.199
#   Just displays the number of days missing from clicks on the special
#   'days missing' lines.


def formTPSDaysMissing(Value, e):
    LCan = PROGCan["TPS"]
    if TPSPlotted is False:
        beep(1)
        return
    # We don't get the position of these, so this is the best we can do.
    Cx = LCan.canvasx(e.x)
    Cy = LCan.canvasy(e.y)
    LCan.delete("TClick")
    plotMFShowTimeRule("TPS", -1)
    LCan.create_rectangle(Cx - 6, Cy - 6, Cx + 6, Cy + 6, width=4,
                          outline=Clr["O"], tags="TClick")
    # Clear out any time square from before.
    setMsg("TPS", "", "Days missing: %d" % Value)
    return
#########################################################
# BEGIN: formTPSTimeClick(Index, Value, GX, GY, e = None)
# FUNC:formTPSTimeClick():2013.199
#   Handles Canvas clicks.


def formTPSTimeClick(Index, Value, GX, GY, e=None):
    if TPSPlotted is False:
        beep(1)
        return
    LCan = PROGCan["TPS"]
    # This is a click from the Canvas().
    if Index == -1:
        Cx = LCan.canvasx(e.x)
        # Cy = LCan.canvasy(e.y)
        if Cx < TPSBufLeft or Cx > (TPSBufLeft + TPSPWidth):
            setMsg("TPS", "", "")
            plotMFShowTimeRule("TPS", -1)
            # Clear out any time square from before.
            LCan.delete("TClick")
    # This is a click from one of the rectangles.
    else:
        LCan.delete("TClick")
        LCan.create_rectangle(GX - 6, GY - 6, GX + 6, GY + 6, width=4,
                              outline=Clr["O"], tags="TClick")
        Epoch = float(TPSStart + (Index * 300) + 150)
        Time = dt2Time(-1, 80, Epoch)
        setMsg("TPS", "", "%s: %s counts" % (Time, Value))
        plotMFShowTimeRule("TPS", Epoch)
    return
#########################
# BEGIN: formTPSControl()
# FUNC:formTPSControl():2016.005


def formTPSControl(What):
    global TPSRunning
    global TPSPlotted
    global TPSPlotData
    if What == "go":
        buttonBG(TPSPlotBut, "G", NORMAL)
        buttonBG(TPSStopBut, "R", NORMAL)
        TPSRunning = 1
        busyCursor(1)
        return
    elif What == "stop":
        if TPSRunning == 0:
            beep(1)
        else:
            buttonBG(TPSStopBut, "Y", NORMAL)
    elif What == "stopped":
        buttonBG(TPSPlotBut, "D", NORMAL)
        buttonBG(TPSStopBut, "D", DISABLED)
    elif What == "close":
        formTPSControl("stopped")
        TPSPlotted = False
        del TPSPlotData[:]
        formClose("TPS")
        return
    TPSRunning = 0
    busyCursor(0)
    return
# END: formTPS


############################################################################
# BEGIN: formWrite(Parent, WhereMsg, TextWho, Title, FilespecVar, AllowPick,
#                Backup, Confirm, Clean)
# LIB:formWrite():2019.037
#   Writes the contents of the passed TextWho Text() widget to a file.
#   FilespecVar can be a StringVar or an astring.
#   If Backup is "" no backup will be made, but if it is not then the passed
#   string, like "-" or "~", will be appended to the original file name, the
#   original file will be renamed to that, and then the new stuff written to
#   the original file name.
#   If Confirm is True then the popup dialog question will be asked. If it is
#   False then no question will be asked, and the file will be overwritten to
#   the passed FilespecVar. A backup will be made depending on the value of
#   Backup.
#   Clean=True will go through each line and make sure it has ASCII characters.
#   Non-ASCII characters will be "?".
#   A List of text lines may be passed as TextWho and those will be written to
#   the file, insted of the contents of a Text().
FW_BACKUPCHAR = "-"


def formWrite(Parent, WhereMsg, TextWho, Title, FilespecVar, AllowPick,
              Backup, Confirm, Clean):
    # Sometimes it can take a while if there is a lot of text.
    updateMe(0)
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    # Can be StringVar or a string.
    if isinstance(FilespecVar, StringVar):
        InFile = FilespecVar.get()
    if isinstance(FilespecVar, astring):
        InFile = FilespecVar
    # Set this in case we are skipping the confirmation stuff.
    Filespec = InFile
    if Confirm is True:
        if AllowPick is True:
            setMsg(WhereMsg, "", "")
            Filespec = formMYDF(Parent, 0, Title, dirname(InFile),
                                basename(InFile))
            if len(Filespec) == 0:
                return ""
    setMsg(WhereMsg, "CB", "Working...")
    # Default to backing up and overwriting.
    Answer = "over"
    if Confirm is True and exists(Filespec):
        Answer = formMYD(Parent, (("Append", LEFT, "append"),
                                  ("Overwrite", LEFT, "over"),
                                  ("Cancel", LEFT, "cancel")),
                         "cancel", "YB", "Keep Everything.",
                         "The file\n\n%s\n\nalready exists. Would you like to "
                         "append to that file, overwrite it, or cancel?" %
                         Filespec)
        if Answer == "cancel":
            setMsg(WhereMsg, "", "Nothing done.")
            return ""
    try:
        if Answer == "over":
            if len(Backup) != 0:
                # rename() on some systems, like Windows, get upset if the
                # backup file already exists. No one else seems to care.
                if exists(Filespec + Backup):
                    remove(Filespec + Backup)
                rename(Filespec, Filespec + Backup)
            Fp = open(Filespec, "w")
        elif Answer == "append":
            Fp = open(Filespec, "a")
    except Exception as e:
        setMsg(WhereMsg, "MW", "Error opening file (2)\n   %s\n   %s" %
               (Filespec, e), 3)
        return ""
    # Now that the file is OK...
    if isinstance(FilespecVar, StringVar):
        FilespecVar.set(Filespec)
    if isinstance(TextWho, astring):
        LTxt = PROGTxt[TextWho]
        N = 1
        # In case the text field is empty.
        Line = "\n"
        while True:
            # This little convolution keeps empty lines from piling up at the
            # end of the file.
            if len(LTxt.get("%d.0" % N)) == 0:
                if Line != "\n":
                    Fp.write(Line)
                break
            if N > 1:
                if Clean is True:
                    Line = line2ASCII(Line)
                # The caller may not want the line cleaned, but that's doesn't
                # 'make it so'.
                try:
                    Fp.write(Line)
                except UnicodeEncodeError:
                    setMsg(WhereMsg, "RW", "Unicode in line:\n%s" % Line, 2)
                    return
            Line = LTxt.get("%d.0" % N, "%d.0" % (N + 1))
            N += 1
    elif isinstance(TextWho, list):
        for Line in Lines:
            if Line.endswith("\n"):
                if Clean is True:
                    Line = line2ASCII(Line)
                try:
                    Fp.write(Line)
                except UnicodeEncodeError:
                    setMsg(WhereMsg, "RW", "Unicode in line:\n%s" % Line, 2)
                    return
            else:
                if Clean is True:
                    Line = line2ASCII(Line)
                try:
                    Fp.write(Line + "\n")
                except UnicodeEncodeError:
                    setMsg(WhereMsg, "RW", "Unicode in line:\n%s" % Line, 2)
                    return
    Fp.close()
    if Answer == "append":
        setMsg(WhereMsg, "GB", "Appended text to file\n   %s" % Filespec)
    elif Answer == "over":
        setMsg(WhereMsg, "GB", "Wrote text to file\n   %s" % Filespec)
    # Return this in case the caller is interested.
    return Filespec
#################################################################
# BEGIN: formRead(Parent, TextWho, Title, FilespecVar, AllowPick)
# FUNC:formRead():2019.003
#   Same idea as formWrite(), but handles reading a file into a Text().
#   Only appends the text to the END of the passed Text() field.


def formRead(Parent, TextWho, Title, FilespecVar, AllowPick):
    # Sometimes it can take a while if there is a lot of text.
    updateMe(0)
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    Filespec = FilespecVar.get()
    if AllowPick is True:
        Filespec = formMYDF(Parent, 0, Title, dirname(Filespec),
                            basename(Filespec))
        if len(Filespec) == 0:
            return (2, "", "Nothing done.", 0, "")
    setMsg(WhereMsg, "CB", "Working...")
    if exists(Filespec) is False:
        return (2, "RW", "File to read does not exist:\n   %s" % Filespec, 2,
                Filespec)
    if isdir(Filespec):
        return (2, "RW", "Selected file is not a normal file:\n   %s" %
                Filespec, 2, Filespec)
    # We don't know who saved the file, what the encoding is, what the line
    # delimiters are, so use this to get it split up. formRead() is generally
    # just for ASCII text files, but you never know.
    Ret = readFileLinesRB(Filespec)
    if Ret[0] != 0:
        return Ret
    Lines = Ret[1]
    # Now that the file is OK...
    LTxt = PROGTxt[TextWho]
    if int(LTxt.index('end-1c').split('.')[0]) > 1:
        LTxt.insert(END, "\n")
    for Line in Lines:
        LTxt.insert(END, "%s\n" % Line)
    FilespecVar.set(Filespec)
    if len(Lines) == 1:
        return (0, "", "Read %d line from file\n   %s" %
                (len(Lines), Filespec), 0, Filespec)
    else:
        return (0, "", "Read %d lines from file\n   %s" %
                (len(Lines), Filespec), 0, Filespec)
################################
# BEGIN: formWriteNoUni(TextWho)
# FUNC:formWriteNoUni():2019.037
#   A quick check before going through everything in formWrite().


def formWriteNoUni(TextWho):
    # Sometimes it can take a while if there is a lot of text.
    updateMe(0)
    if isinstance(TextWho, astring):
        LTxt = PROGTxt[TextWho]
    Lines = LTxt.get("0.0", END).split("\n")
    for Line in Lines:
        try:
            Line.encode("ascii")
        except UnicodeEncodeError:
            return (1, "RW", "Unicode in line:\n   %s" % Line, 2)
    return (0,)
# END: formWrite


#################################################
# BEGIN: formWritePS(Parent, VarSet, FilespecVar)
# LIB:formWritePS():2019.003
def formWritePS(Parent, VarSet, FilespecVar):
    setMsg(VarSet, "", "")
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    LCan = PROGCan[VarSet]
    Running = 0
    # The form may not have an associated Running var.
    try:
        Running = eval("%sRunning.get()" % VarSet)
    except Exception:
        pass
    if Running != 0:
        setMsg(VarSet, "YB", "I'm busy...", 2)
        sleep(.5)
        return
    # This may not make any difference depending on the form.
    try:
        if LCan.cget("bg") != "#FFFFFF":
            Answer = formMYD(Parent, (("Stop And Let Me Redo It", TOP, "stop"),
                                      ("Continue anyway", TOP, "cont")),
                             "stop", "YB", "Charcoal Footprint?",
                             "For best results the background color of the "
                             "plot area should be white. In fact, for most "
                             "plots the .ps file's background will be white, "
                             "and so will the text, so you won't be able to "
                             "see anything.  Do you want to stop and replot, "
                             "or continue anyway?")
        if Answer == "stop":
            return
    except Exception:
        pass
    # This will cryptically crash if the canvas is empty.
    try:
        L, T, R, B = LCan.bbox(ALL)
        # Postscript is so litteral.
        R += 15
        B += 15
    except Exception:
        setMsg(VarSet, "RW", "Is the area to write blank?", 2)
        return
    Dir = dirname(FilespecVar.get())
    File = basename(FilespecVar.get())
    Filespec = formMYDF(Parent, 3, ".ps File To Save To...", Dir, File, "",
                        ".ps", False)
    if len(Filespec) == 0:
        return False
    Answer = overwriteFile(Parent, Filespec)
    if Answer == "stop":
        return
    setMsg(VarSet, "CB", "Working on a %d x %d pixel area..." % (R, B))
    # This might crash if the canvas is huge?
    try:
        Ps = LCan.postscript(height=B, width=R, pageheight=B,
                             pagewidth=R, pageanchor="nw", pagex=0, pagey=B,
                             x=0, y=0, colormode="color")
    except Exception as e:
        setMsg(VarSet, "MW", "Error converting canvas to postscript.\n   %s" %
               e, 3)
        return
    try:
        Fp = open(Filespec, "w")
        Fp.write(Ps)
        Fp.close()
    except Exception as e:
        setMsg(VarSet, "MW", "Error saving postscript commands.\n   %s" % e, 3)
        return
    setMsg(VarSet, "", "Wrote %d x %d area (%s bytes) to file\n   %s" %
           (R, B, fmti(len(Ps)), Filespec))
    FilespecVar.set(Filespec)
    return
# END: formWritePS


##############################
# BEGIN: getFolderSize(Folder)
# LIB:getFolderSize():2011.301
#   Stolen right from the Web from Samual Lampa, Dec 6, 2010.
def getFolderSize(Folder):
    # If a file is passed just return the size of it so the caller doesn't have
    # to keep checking.
    if isfile(Folder):
        return getsize(Folder)
    TotalSize = getsize(Folder)
    for Item in listdir(Folder):
        Itempath = Folder + sep + Item
        if isfile(Itempath):
            TotalSize += getsize(Itempath)
        elif isdir(Itempath):
            TotalSize += getFolderSize(Itempath)
    return TotalSize
# END: getFolderSize


#######################
# BEGIN: getGMT(Format)
# LIB:getGMT():2015.007
#   Gets the time in various forms from the system.
def getGMT(Format):
    # YYYY:DOY:HH:MM:SS (GMT)
    if Format == 0:
        return strftime("%Y:%j:%H:%M:%S", gmtime(time()))
    # YYYYDOYHHMMSS (GMT)
    elif Format == 1:
        return strftime("%Y%j%H%M%S", gmtime(time()))
    # YYYY-MM-DD (GMT)
    elif Format == 2:
        return strftime("%Y-%m-%d", gmtime(time()))
    # YYYY-MM-DD HH:MM:SS (GMT)
    elif Format == 3:
        return strftime("%Y-%m-%d %H:%M:%S", gmtime(time()))
    # YYYY, MM and DD (GMT) returned as ints
    elif Format == 4:
        GMT = gmtime(time())
        return (GMT[0], GMT[1], GMT[2])
    # YYYY-Jan-01 (GMT)
    elif Format == 5:
        return strftime("%Y-%b-%d", gmtime(time()))
    # YYYYMMDDHHMMSS (GMT)
    elif Format == 6:
        return strftime("%Y%m%d%H%M%S", gmtime(time()))
    # Reftek Texan (year-1984) time stamp in BBBBBB format (GMT)
    elif Format == 7:
        GMT = gmtime(time())
        return pack(">BBBBBB", (GMT[0] - 1984), 0, 1, GMT[3], GMT[4], GMT[5])
    # Number of seconds since Jan 1, 1970 from the system.
    elif Format == 8:
        return time()
    # YYYY-MM-DD/DOY HH:MM:SS (GMT)
    elif Format == 9:
        return strftime("%Y-%m-%d/%j %H:%M:%S", gmtime(time()))
    # YYYY-MM-DD/DOY (GMT)
    elif Format == 10:
        return strftime("%Y-%m-%d/%j", gmtime(time()))
    # YYYY, DOY, HH, MM, SS (GMT) returned as ints
    elif Format == 11:
        GMT = gmtime(time())
        return (GMT[0], GMT[7], GMT[3], GMT[4], GMT[5])
    # HH:MM:SS (GMT)
    elif Format == 12:
        return strftime("%H:%M:%S", gmtime(time()))
    # YYYY:DOY:HH:MM:SS (LT)
    elif Format == 13:
        return strftime("%Y:%j:%H:%M:%S", localtime(time()))
    # HHMMSS (GMT)
    elif Format == 14:
        return strftime("%H%M%S", gmtime(time()))
    # YYYY-MM-DD (LT)
    elif Format == 15:
        return strftime("%Y-%m-%d", localtime(time()))
    # YYYY-MM-DD/DOY Day (LT)
    elif Format == 16:
        return strftime("%Y-%m-%d/%j %A", localtime(time()))
    # MM-DD (LT)
    elif Format == 17:
        return strftime("%m-%d", localtime(time()))
    # YYYY, MM and DD (LT) returned as ints
    elif Format == 18:
        LT = localtime(time())
        return (LT[0], LT[1], LT[2])
    # YYYY-MM-DD/DOY HH:MM:SS Day (LT)
    elif Format == 19:
        return strftime("%Y-%m-%d/%j %H:%M:%S %A", localtime(time()))
    # Return GMT-LT difference.
    elif Format == 20:
        Secs = time()
        LT = localtime(Secs)
        GMT = gmtime(Secs)
        return dt2Timeymddhms(-1, LT[0], -1, -1, LT[7], LT[3], LT[4],
                              LT[5]) - dt2Timeymddhms(-1, GMT[0], -1, -1,
                                                      GMT[7], GMT[3], GMT[4],
                                                      GMT[5])
    # YYYY-MM-DD/DOY HH:MM:SS (LT)
    elif Format == 21:
        return strftime("%Y-%m-%d/%j %H:%M:%S", localtime(time()))
    # YYYY-MM-DD HH:MM:SS (LT)
    elif Format == 22:
        return strftime("%Y-%m-%d %H:%M:%S", localtime(time()))
    return ""
# END: getGMT


###########################
# BEGIN: getRange(Min, Max)
# LIB:getRange():2008.360
#   Returns the absolute value of the difference between Min and Max.
def getRange(Min, Max):
    if Min <= 0 and Max >= 0:
        return Max + abs(Min)
    elif Min <= 0 and Max <= 0:
        return abs(Min - Max)
    elif Max >= 0 and Min >= 0:
        return Max - Min
# END: getRange


########################
# BEGIN: startupThings()
# LIB:startupThings():2019.262
#   A set of functions that programs call to get things going. Some programs
#   may or may not use these.
#   If START_GETGMT is defined it can be used to change the time format in the
#   .msg start/stop messages.
# This can be used if needed. Copy the lines up to the command line argument
# loop area. PROG_SETUPSUSECWD will need to be there even if not used.
#    PROG_SETUPSUSECWD = False
#        elif Arg == "-cwd":
#            PROG_SETUPSUSECWD = True
#########################
# BEGIN: loadPROGSetups()
# FUNC:loadPROGSetups():2019.262
#   A standard setups file loader for programs that don't require anything
#   special loaded from the .set file just the PROGSetups items.
#   PROGSetupsFilespec is just for formABOUT() to use.
#   Note the return codes are a bit specific.
PROGSetupsFilespec = ""


def loadPROGSetups():
    global PROGSetupsFilespec
    # If there is a local function we'll call it as we go through the lines.
    CallLocal = "loadPROGSetupsLocal" in globals()
    SetupsDir = PROGSetupsDirVar.get()
    if len(SetupsDir) == 0:
        SetupsDir = "%s%s" % (abspath("."), sep)
    PROGSetupsFilespec = "%sset%s%s.set" % (SetupsDir, PROG_NAMELC,
                                            PROGUserIDLC)
    # The .set file may not be there. This may be the first time the program
    # has been run. Try to create a blank file. If that fails just go on. The
    # user will hear about it later.
    if exists(PROGSetupsFilespec) is False:
        try:
            Fp = open(PROGSetupsFilespec, "a")
            Fp.close()
        except Exception:
            pass
        return (1, "", "Setups file not found. Was looking for\n   %s" %
                PROGSetupsFilespec, 0, "")
    if PROGIgnoreSetups is True:
        return (0, "WB", "Setups ignored from\n   %s" % PROGSetupsFilespec,
                0, "")
    # The file should not be corrupted, but we don't know where it has been, so
    # use readFileLinesRB().
    Ret = readFileLinesRB(PROGSetupsFilespec, True)
    if Ret[0] != 0:
        return (5, Ret[1], Ret[2], Ret[3], Ret[4], Ret[5])
    Lines = Ret[1]
    if len(Lines) == 0:
        return (0, "WB", "No setup lines found in\n   %s" % PROGSetupsFilespec,
                0, "")
    Found = False
    VersGood = False
    # Just catch anything that might go wrong and blame it on the .set file.
    try:
        for Line in Lines:
            if len(Line) == 0 or Line.startswith("#"):
                continue
            if Line.startswith(PROG_NAME + ":"):
                Parts = Line.split()
                for Index in arange(0, len(Parts)):
                    Parts[Index] = Parts[Index].strip()
                # In case an old setups file is read that does not have the
                # version item.
                Parts += [""] * (3 - len(Parts))
                # If the version doesn't match what the program wants then
                # don't set VersGood and everything will be left with it's
                # default value.
                if Parts[2] == PROG_SETUPSVERS:
                    VersGood = True
                Found = True
                continue
            if Found is True and VersGood is True:
                if CallLocal is True:
                    # For handling a little more complicated return value.
                    Ret = loadPROGSetupsLocal(Line)
                    # The passed Line didn't have anything to do with the local
                    # stuff.
                    if Ret[0] == 0:
                        pass
                    # The local function processed the Line. Go on to the next.
                    elif Ret[0] == 1:
                        continue
                    # There was an exception. Do the same as the exception
                    # clause in here. The local function should return the
                    # same message as below.
                    elif Ret[0] == 2:
                        return Ret
                Parts = Line.split(";", 1)
                for Index in arange(0, len(Parts)):
                    Parts[Index] = Parts[Index].strip()
                for Item in PROGSetups:
                    if Parts[0] == Item:
                        if isinstance(eval(Item), StringVar):
                            eval(Item).set(Parts[1])
                            break
                        elif isinstance(eval(Item), IntVar):
                            eval(Item).set(intt(Parts[1]))
                            break
        if Found is False:
            return (3, "YB", "No %s setups found in\n   %s" %
                    (PROG_NAME, PROGSetupsFilespec), 2, "")
        else:
            if VersGood is False:
                return (4, "YB", "Setups version mismatch. Using defaults.",
                        0, "")
            return (0, "WB", "Setups loaded from\n   %s" % PROGSetupsFilespec,
                    0, "")
    except Exception as e:
        return (5, "MW",
                "Error loading setups from\n   %s\n   %s\n   Was loading "
                "line %s" % (PROGSetupsFilespec, e, Line), 3, "")
#########################
# BEGIN: savePROGSetups()
# FUNC:savePROGSetups():2018.236


def savePROGSetups():
    # If there is a local function we'll call it as we go through.
    CallLocal = "savePROGSetupsLocal" in globals()
    # Same as above.
    try:
        Fp = open(PROGSetupsFilespec, "w")
    except Exception as e:
        stdout.write("savePROGSetups(): %s\n" % e)
        return (2, "MW", "Error opening setups file\n   %s\n   %s" %
                (PROGSetupsFilespec, e), 3, "")
    try:
        Fp.write("%s: %s %s\n" % (PROG_NAME, PROG_VERSION, PROG_SETUPSVERS))
        for Item in PROGSetups:
            if isinstance(eval(Item), StringVar):
                Fp.write("%s; %s\n" % (Item, eval(Item).get()))
            elif isinstance(eval(Item), IntVar):
                Fp.write("%s; %d\n" % (Item, eval(Item).get()))
        # Keep the local stuff last, since that's where stuff from individual
        # programs usually needs to be.
        if CallLocal is True:
            Ret = savePROGSetupsLocal(Fp)
            if Ret[0] != 0:
                Fp.close()
                return Ret
        Fp.close()
        return (0, "", "Setups saved to\n   %s" % PROGSetupsFilespec, 0, "")
    except Exception as e:
        Fp.close()
        return (2, "MW", "Error saving setups to\n   %s\n   %s" %
                (PROGSetupsFilespec, e), 3, "")
###########################
# BEGIN: deletePROGSetups()
# FUNC:deletePROGSetups():2018.235


def deletePROGSetups():
    Answer = formMYD(Root,
                     (("Delete And Quit", TOP, "doit"),
                      ("Cancel", TOP, "cancel")),
                     "cancel", "YB", "Be careful.",
                     "This will delete the current setups file and quit the "
                     "program. This should only need to be done if the "
                     "contents of the current setups file is known to be "
                     "causing the program problems.")
    if Answer == "cancel":
        return
    SetupsDir = PROGSetupsDirVar.get()
    if len(SetupsDir) == 0:
        SetupsDir = "%s%s" % (abspath("."), sep)
    Filespec = SetupsDir + "set" + PROG_NAMELC + PROGUserIDLC + ".set"
    try:
        remove(Filespec)
    except Exception as e:
        formMYD(Root, (("(OK)", TOP, "ok"), ), "ok", "MW", "Oh No.",
                "The setups file could not be deleted.\n\n%s" % e)
        return
    progQuitter(False)
    return


#######################
# BEGIN: setMachineID()
# FUNC:setMachineID():2018.235
#   Asks the user what the name of the computer is for log file names, etc.
PROGMachineIDUCVar = StringVar()
PROGMachineIDLCVar = StringVar()
PROGSetups += ["PROGMachineIDUCVar", "PROGMachineIDLCVar"]


def setMachineID():
    MYDAnswerVar.set(PROGMachineIDUCVar.get())
    if PROGKiosk is False:
        Answer = formMYD(Root, (("Input12", TOP, "input"),
                                ("(OK)", LEFT, "input"),
                                ("Quit", LEFT, "quit")),
                         "cancel", "", "Who Am I?",
                         "Enter the ID of this computer. The ID may be left "
                         "blank if appropriate.")
    else:
        Answer = formMYD(Root, (("Input12", TOP, "input"),
                                ("(OK)", LEFT, "input")),
                         "ok", "", "Who Am I?",
                         "Enter the ID of this computer. The ID may be left "
                         "blank if appropriate.")
    # Keep going in here until the user gives a valid, or no answer.
    while True:
        if Answer == "quit":
            return (2, "", "Quit.", 0, "")
        elif Answer == "cancel" or len(Answer) == 0:
            Answer = ""
        else:
            if Answer.isalnum() is False:
                if PROGKiosk is False:
                    Answer = formMYD(Root, (("Input12", TOP, "input"),
                                            ("(OK)", LEFT, "input"),
                                            ("Quit", LEFT, "quit")),
                                     "cancel", "YB", "Who Am I?",
                                     "The ID may only be letters and numbers."
                                     "\n\nEnter the ID of this computer. The "
                                     "ID may be left blank if appropriate.")
                else:
                    Answer = formMYD(Root, (("Input12", TOP, "input"),
                                            ("(OK)", LEFT, "input")),
                                     "ok", "YB", "Who Am I?",
                                     "The ID may only be letters and numbers."
                                     "\n\nEnter the ID of this computer. The "
                                     "ID may be left blank if appropriate.")
                continue
        break
    PROGMachineIDUCVar.set(Answer.strip().upper())
    PROGMachineIDLCVar.set(Answer.strip().lower())
    return (0, )


##########################
# BEGIN: setPROGMsgsFile()
# FUNC:setPROGMsgsFile():2018.334
PROGMsgsFile = ""


def setPROGMsgsFile():
    global PROGMsgsFile
    PROGMsgsFile = ""
    StartsWith = PROGMachineIDLCVar.get()
    EndsWith = PROG_NAMELC + ".msg"
    if len(StartsWith) != 0:
        # Special case. Check to see if an "old style" (ccIDblah.msg) messages
        # file is laying around before looking for the ccID-YYYYDOY-blah.msg
        # style. If it is then use that one.
        if exists(PROGMsgsDirVar.get() + StartsWith + EndsWith):
            PROGMsgsFile = StartsWith + EndsWith
            msgLn(9, "", "Working with messages file\n   %s" %
                  (PROGMsgsDirVar.get() + PROGMsgsFile))
            return
        # If there isn't one just add the dash and look for the new style.
        StartsWith += "-"
    # In case the setups file came from another machine (usually the reason).
    if exists(PROGMsgsDirVar.get()) is False:
        # Hopefully PROGSetupsDirVar points to somewhere sensible.
        PROGMsgsDirVar.set(PROGSetupsDirVar.get())
    Files = sorted(listdir(PROGMsgsDirVar.get()))
    # Go through all of the files so we get the "latest" one (since they have
    # been sorted).
    Temp = ""
    for File in Files:
        if File.endswith(EndsWith):
            if len(StartsWith) != 0:
                if File.startswith(StartsWith):
                    Temp = File
            # If the user didn't enter anything then these are the only two
            # acceptable possibilities.
            elif rtnPattern(File).startswith("0000000-"):
                Temp = File
            elif File == EndsWith:
                Temp = File
    if len(Temp) == 0:
        if len(StartsWith) != 0:
            PROGMsgsFile = StartsWith
        PROGMsgsFile += (getGMT(1)[:7] + "-" + EndsWith)
    else:
        PROGMsgsFile = Temp
    # If this file doesn't exist just create an empty one so there will be
    # something for the program to find next time it is started on the off
    # chance that nothing gets written to it this time around.
    if exists(PROGMsgsDirVar.get() + PROGMsgsFile) is False:
        try:
            Fp = open(PROGMsgsDirVar.get() + PROGMsgsFile, "w")
            Fp.close()
        except Exception as e:
            msgLn(9, "MW", "Error creating messages file\n   %s\n   %s" %
                  ((PROGMsgsDirVar.get() + PROGMsgsFile), e), True, 3)
    msgLn(9, "", "Working with messages file\n   %s" % (PROGMsgsDirVar.get() +
                                                        PROGMsgsFile))
    return
###############################################
# BEGIN: loadPROGMsgs(Speak = True, Ask = True)
# FUNC:loadPROGMsgs():2018.340


def loadPROGMsgs(Speak=True, Ask=True):
    try:
        if exists(PROGMsgsDirVar.get() + PROGMsgsFile):
            # These files should not be corrupted, but you never know, so use
            # RB.
            Ret = readFileLinesRB(PROGMsgsDirVar.get() + PROGMsgsFile)
            if Ret[0] != 0:
                # Don't write this to the messages file since we can't get it
                # open.
                msgLn(9, "M", Ret[2], True, Ret[3])
                msgLn(9, "M", "This should be fixed before continuing.")
                return
            Lines = Ret[1]
            # An empty messages file may get created before coming here.
            if len(Lines) != 0:
                if Ask is True:
                    Answer = formMYD(Root, (("(Yes)", LEFT, "yes"),
                                            ("No", LEFT, "no")),
                                     "no", "", "Load It?",
                                     "There already is a messages file named"
                                     "\n\n%s\n\nNumber of lines: %d\n\nThis "
                                     "file will be used for the messages. Do "
                                     "you want to load its current contents "
                                     "into the messages section?" %
                                     (PROGMsgsFile, len(Lines)))
                else:
                    Answer = "yes"
                if Answer == "yes":
                    # Write directly to the messages section rather than use
                    # msgLn. It's much faster.
                    for Line in Lines:
                        MMsg.insert(END, Line + "\n")
                    MMsg.see(END)
        if Speak is True:
            GetGMT = 0
            if "START_GETGMT" in globals():
                GetGMT = START_GETGMT
            writeFile(0, "MSG", "==== %s %s started %s ====\n" %
                      (PROG_NAME, PROG_VERSION, getGMT(GetGMT)))
    except Exception as e:
        # Same here.
        msgLn(9, "M", "Error reading %s" % PROGMsgsFile, True, 3)
        msgLn(9, "M", "   %s" % str(e))
        msgLn(9, "M", "This should be fixed before continuing.")
    return


###########################
# BEGIN: setPROGSetupsDir()
# FUNC:setPROGSetupsDir():2018.334
#   Figures out what the "home" directory is.
PROGSetupsDirVar = StringVar()
# Default to the current directory if this is never called.
PROGSetupsDirVar.set("%s%s" % (abspath("."), sep))
if PROG_SETUPSUSECWD is False:
    PROGSetupsDirVar.set("%s%s" % (abspath("."), sep))
else:
    PROGSetupsDirVar.set(getcwd())


def setPROGSetupsDir():
    if PROG_SETUPSUSECWD is True:
        return (0,)
    try:
        if PROGSystem == 'dar' or PROGSystem == 'lin' or PROGSystem == 'sun':
            Dir = environ["HOME"]
            LookFor = "HOME"
        # Of course Windows had to be different.
        elif PROGSystem == 'win':
            Dir = environ["HOMEDRIVE"] + environ["HOMEPATH"]
            LookFor = "HOMEDRIVE+HOMEPATH"
        else:
            return (2, "RW",
                    "I don't know how to get the HOME directory on this "
                    "system. This system is not supported: %s" %
                    PROGSystem, 2, "")
        if Dir.endswith(sep) is False:
            Dir += sep
    except Exception as e:
        return (2, "MW",
                "There is an error building the directory to the setups file. "
                "The error is:\n\n%s\n\nThis will need to be corrected before "
                "this program can be used." % e, 3, "")
    # I guess it's possible for Dir to just be ":" on Windows, so we better
    # check.
    if exists(Dir) is False:
        return (2, "MW",
                "The %s directory\n\n%s\n\ndoes not exist. This will need to "
                "be corrected before this program may be used." %
                (LookFor, Dir), 3, "")
    if access(Dir, W_OK) is False:
        return (2, "MW",
                "The %s directory\n\n%s\n\nis not accessible for writing. "
                "This will need to be corrected before this program may be "
                "used." % (LookFor, Dir), 3, "")
    PROGSetupsDirVar.set(Dir)
    return (0,)


####################
# BEGIN: setUserID()
# FUNC:setUserID():2018.334
#   For programs that need to know the name of the user.
#   Not set up for kiosk-type programs (it has a Quit button).
PROGUserIDUC = ""
PROGUserIDLC = ""


def setUserID():
    global PROGUserIDUC
    global PROGUserIDLC
    # This (usually) comes up before the main form of a program, so we have to
    # figure out where it should go. We'll just use the center of displays less
    # than the 2560 pixels of a modern iMac, or the center of a 1024x768
    # display.
    if PROGScreenWidthNow > 2560:
        CX = 512
        CY = 384
    else:
        CX = PROGScreenWidthNow / 2
        CY = PROGScreenHeightNow / 2
    MYDAnswerVar.set("")
    # None Parent, otherwise it doesn't show up in Windows.
    Answer = formMYD(None, (("Input12", TOP, "input"),
                            ("(OK)", LEFT, "input"),
                            ("Quit", LEFT, "quit")),
                     "cancel", "", "Who Are You?",
                     "Enter your name or an ID that will be used for finding/"
                     "saving program related files. The field may be left "
                     "blank if appropriate.", "", 0, CX, CY)
    # Keep going in here until the user gives a valid, or no, answer.
    while True:
        if Answer == "quit":
            return (2, "", "Quit.", 0, "")
        elif Answer == "cancel" or len(Answer) == 0:
            Answer = ""
            break
        else:
            if Answer.isalnum() is False:
                Answer = formMYD(None, (("Input12", TOP, "input"),
                                        ("(OK)", LEFT, "input"),
                                        ("Quit", LEFT, "quit")),
                                 "cancel", "YB", "Who Are You?",
                                 "The ID may only be letters and numbers.\n\n"
                                 "Enter your name or an ID that will be used "
                                 "for finding/saving program related files. "
                                 "The field may be left blank if appropriate.",
                                 "", 0, CX, CY)
                continue
        break
    PROGUserIDUC = Answer.strip().upper()
    PROGUserIDLC = Answer.strip().lower()
    return (0, )
################################################
# BEGIN: setPROGStartDir(Which, WarnQuit = True)
# FUNC:setPROGStartDir():2018.334
#   The lines are about 60 chars long.


def setPROGStartDir(Which, WarnQuit=True):
    # 60 chars.
    # PETM
    if Which == 0:
        Answer = formMYDF(Root, 1, "Pick A Starting Directory",
                          PROGSetupsDirVar.get(), "",
                          "This may be the first time this program has been "
                          "started on\nthis computer or in this account. "
                          "Select a directory for\nthe program to use as the "
                          "directory where all of its files\nshould start out "
                          "being saved. Click the OK button if the\ndisplayed "
                          "directory is OK to use.")
    # POCUS, CHANGEO, HOCUS...
    elif Which == 1:
        Answer = formMYDF(Root, 1, "Pick A Starting Directory",
                          PROGSetupsDirVar.get(), "",
                          "This may be the first time this program has been "
                          "started on\nthis computer or in this account. "
                          "Select a directory for\nthe program to use as the "
                          "directory where all of the bookkeeping\nfiles for "
                          "the program should start out being saved -- NOT\n"
                          "necessarily where any data files may be. We'll get "
                          "to that\nlater. Click the OK button if the "
                          "displayed directory is\nOK to use.")
    elif Which == 2:
        # LOGPEEK, QPEEK (no messages file)
        Answer = formMYDF(Root, 1, "Where's The Data?",
                          PROGSetupsDirVar.get(), "",
                          "This may be the first time this program has been "
                          "started\non this computer or in this account. "
                          "Select a directory\nfor the program to use as a "
                          "starting point -- generally\nwhere some data files "
                          "to read are located.\nClick the OK button to use "
                          "the displayed\ndirectory.")
    # This is here just so they all say the same thing. The caller should call
    # the quitter (didn't want to do that here). The caller can supress the
    # message if it just wants to set it's own defaults.
    if WarnQuit is True and len(Answer) == 0:
        formMYD(Root, (("(OK)", TOP, "ok"), ), "ok", "YB",
                "Have It Your Way.",
                "You didn't enter anything, so I'm quitting.")
        quit(0)
    return Answer
# END: startupThings


#################
# BEGIN: intt(In)
# LIB:intt():2018.257
#   Handles all of the annoying shortfalls of the int() function (vs C).
def intt(In):
    In = str(In).strip()
    if len(In) == 0:
        return 0
    # Let the system try it first.
    try:
        return int(In)
    except ValueError:
        Number = ""
        for c in In:
            if c.isdigit():
                Number += c
            elif (c == "-" or c == "+") and len(Number) == 0:
                Number += c
            elif c == ",":
                continue
            else:
                break
        try:
            return int(Number)
        except ValueError:
            return 0
# END: intt


###################################################################
# BEGIN: labelEntry2(Sub, Format, LabTx, TTLen, TTTx, TxVar, Width)
# LIB:labelEntry2():2018.236
#   For making simple  Label(): Entry() pairs.
#   Format: 10 = Aligned LEFT-RIGHT, entry field is disabled
#           11 = Aligned LEFT-RIGHT, entry field is normal
#           20 = Aligned TOP-BOTTOM, entry field disabled
#           21 = Aligned TOP-BOTTOM, entry field is normal
#    LabTx = the text of the label. "" LabTx = no label
#    TTLen = The length of the tooltip text
#     TTTx = The text of the label's tooltip. "" TTTx = no tooltip
#    TxVar = Var for the entry field
#    Width = Width to make the field, 0 = .pack(side=LEFT, expand=YES, fill=X)
#            Width should only be 0 if Format is 10 or 11.
def labelEntry2(Sub, Format, LabTx, TTLen, TTTx, TxVar, Width):
    if len(LabTx) != 0:
        if len(TTTx) != 0:
            Lab = Label(Sub, text=LabTx)
            if Format == 11 or Format == 10:
                Lab.pack(side=LEFT)
            elif Format == 21 or Format == 20:
                Lab.pack(side=TOP)
            ToolTip(Lab, TTLen, TTTx)
        else:
            # Don't create the extra object if we don't have to.
            if Format == 11 or Format == 10:
                Label(Sub, text=LabTx).pack(side=LEFT)
            elif Format == 21 or Format == 20:
                Label(Sub, text=LabTx).pack(side=TOP)
    if Width == 0:
        Ent = Entry(Sub, textvariable=TxVar)
    else:
        Ent = Entry(Sub, textvariable=TxVar, width=Width + 1)
    if Format == 11 or Format == 10:
        if Width != 0:
            Ent.pack(side=LEFT)
        else:
            Ent.pack(side=LEFT, expand=YES, fill=X)
    elif Format == 21 or Format == 20:
        Ent.pack(side=TOP)
    if Format == 10 or Format == 20:
        Ent.configure(state=DISABLED, bg=Clr["D"])
    return Ent
# END: labelEntry2


####################################################
# BEGIN: labelTip(Sub, LText, Side, TTWidth, TTText)
# LIB:labelTip():2006.262
#   Creates a label and assignes the passed ToolTip to it. Returns the Label()
#   widget.
def labelTip(Sub, LText, Side, TTWidth, TTText):
    Lab = Label(Sub, text=LText)
    Lab.pack(side=Side)
    ToolTip(Lab, TTWidth, TTText)
    return Lab
# END: labelTip


###########################
# BEGIN: line2ASCII(InLine)
# LIB:line2ASCII():2019.014
#   Returns the passed line with anything not an ASCII character as a ?.
def line2ASCII(InLine):
    try:
        InLine.encode("ascii")
        return InLine
    except Exception:
        OutLine = ""
        for C in InLine:
            try:
                # Value = ord(C)
                OutLine += C
            except Exception:
                OutLine += "?"
        return OutLine
# END: line2ASCII


########################################################################
# BEGIN: list2Str(TheList, Delim = ", ", Sort = True, DelBlanks = False)
# LIB:list2Str():2018.235
def list2Str(TheList, Delim=", ", Sort=True, DelBlanks=False):
    if isinstance(TheList, list) is False:
        return TheList
    if Sort is True:
        TheList.sort()
    Ret = ""
    # If there is any funny-business (which has been seen).
    for Item in TheList:
        try:
            if DelBlanks is True:
                if len(str(Item)) == 0:
                    continue
            Ret += str(Item) + Delim
        except UnicodeEncodeError:
            Ret += "Error" + Delim
    return Ret[:-(len(Delim))]
# END: list2Str


#########################
# BEGIN: listKnownChans()
# FUNC:listKnownChans():2018.310
def listKnownChans():
    Chans = sorted(QPChans.keys())
    # See formMYDT().
    Msg = ["}"]
    Msg.append("These are the channels that QPEEK knows how to deal with. If "
               "a channel needs to be added to this list it requires changing "
               "the program.")
    for Chan in Chans:
        QPC = QPChans[Chan]
        if QPC[QPCHANS_PLOT] == 0:
            continue
        Msg.append("")
        Msg.append(QPC[QPCHANS_LABEL])
        Msg.append("   Desc: %s" % QPC[QPCHANS_DESC])
        Msg.append("   Units: %s" % QPC[QPCHANS_UNITS])
        Value = QPC[QPCHANS_DECODE]
        if Value == 1:
            Msg.append("   Decode: 1 count = 1 unit")
        elif Value == 2:
            Msg.append("   Decode: 1 count = .150 units")
        elif Value == 3:
            Msg.append("   Decode: 1 count = .1 units")
        elif Value == 4:
            Msg.append("   Decode: 1 count = 1 unit, but kept >= 0")
        elif Value == 5:
            Msg.append("   Decode: 1 count = .1 unit, but kept >= 0")
        elif Value == 6:
            Msg.append("   Decode: 1 count = .001 (Divide counts by 1000)")
        elif Value == 7:
            Msg.append("   Decode: 1 count = .000001 (Divide counts by "
                       "1000000)")
        elif Value == 99:
            Msg.append("   Decode: Different multipliers depending on make"
                       "/model for voltages")
        elif Value == 999:
            Msg.append("   Decode: Different multipliers depending on make/"
                       "model for mass positions")
        Value = QPC[QPCHANS_PLOTTER]
        if Value == "mln":
            Msg.append("   Plot: multi-line normal, one color, line and "
                       "maybe dots")
        elif Value == "mlmp":
            Msg.append("   Plot: multi-line mass position, multi-color, "
                       "always line and dots")
        elif Value == "mll":
            Msg.append("   Plot: multi-line line, one color, line only")
        elif Value == "mlls":
            Msg.append("   Plot: multi-line line seismic, one color, line "
                       "only, can apply bit weights")
        elif Value == "mp":
            Msg.append("   Plot: mass position, multi-color, single line")
        elif Value == "strz":
            Msg.append("   Plot: single line, \"good\"=nothing or green, "
                       "\"note\"=cyan, \"hey!\"=yellow, \"bad\"=red, "
                       "\"yikes!\"=magenta")
        elif Value == "tri":
            Msg.append("   Plot: single line, 0=red, 1=yellow, 2=green "
                       "(e.g. Off/Unlocked/Locked)")
        elif Value == "tri2":
            Msg.append("   Plot: single line, 0=not plotted, 1=red, 2=magenta "
                       "(e.g. Ok/Bad/Worse)")
    formMYDT(Root, (("(OK)", TOP, "ok"), ), "ok", "WB", True,
             "Known Channels", WORD, Msg)
    return
# END: listKnownChans


##############################
# BEGIN: loadChanPrefsSetups()
# FUNC:loadChanPrefsSetups():2019.028
#   This is basically the same as loadPROGSetups(), but only for the Channel
#   Preferences information. If anything goes wrong with loading the regular
#   setups it will probably go wrong here too, but this function will just
#   return.
ChanPrefsFilespec = ""


def loadChanPrefsSetups():
    global ChanPrefsFilespec
    Debug = False
    SetupsDir = PROGSetupsDirVar.get()
    if len(SetupsDir) == 0:
        SetupsDir = "%s%s" % (abspath("."), sep)
    ChanPrefsFilespec = "%sset%schanprefs%s.set" % (SetupsDir, PROG_NAMELC,
                                                    PROGUserIDLC)
    # The .set file may not be there.
    if exists(ChanPrefsFilespec) is False:
        if Debug is True:
            stdout.write("%s not found.\n" % ChanPrefsFilespec)
        return
    Ret = readFileLinesRB(ChanPrefsFilespec)
    if Ret[0] != 0:
        if Debug is True:
            stdout.write("Error opening setups file\n   %s\n   %s\n" %
                         (ChanPrefsFilespec, Ret[5]))
        return
    Lines = Ret[1]
    Found = False
    VersGood = False
    for Line in Lines:
        if len(Line.strip()) == 0:
            continue
        if Line.startswith(PROG_NAME + ":"):
            Parts = Line.split()
            for Index in arange(0, len(Parts)):
                Parts[Index] = Parts[Index].strip()
            # In case an old setups file is read that does not have the version
            # item.
            Parts += [""] * (3 - len(Parts))
            # If the version doesn't match what the program wants then don't
            # set Vers and everything will be left with their default values.
            if Parts[2] == PROG_CPSETUPSVERS:
                VersGood = True
            Found = True
            continue
        if Found is True and VersGood is True:
            Parts = Line.split(";", 1)
            for Index in arange(0, len(Parts)):
                Parts[Index] = Parts[Index].strip()
            for Item in ChanPrefsSetups:
                if Parts[0] == Item:
                    if isinstance(eval(Item), StringVar):
                        eval(Item).set(Parts[1])
                        break
                    elif isinstance(eval(Item), IntVar):
                        eval(Item).set(int(Parts[1]))
                        break
    if Found is False:
        if Debug is True:
            stdout.write("No Chan Prefs setups found in\n   %s\n" %
                         ChanPrefsFilespec)
    else:
        if VersGood is False:
            if Debug is True:
                stdout.write("Chan Prefs setups version mismatch.\n")
            return
        if Debug is True:
            stdout.write("Setups loaded from\n   %s\n" % ChanPrefsFilespec)
    return
##############################
# BEGIN: saveChanPrefsSetups()
# FUNC:saveChanPrefsSetups():2018.310


def saveChanPrefsSetups():
    Debug = False
    try:
        Fp = open(ChanPrefsFilespec, "w")
    except Exception as e:
        if Debug is True:
            stdout.write(
                "Error opening chan prefs setups file\n   %s\n   %s\n" %
                (ChanPrefsFilespec, e))
    try:
        Fp.write("%s: %s %s\n" % (PROG_NAME, PROG_VERSION, PROG_CPSETUPSVERS))
        for Item in ChanPrefsSetups:
            if isinstance(eval(Item), StringVar):
                Fp.write("%s; %s\n" % (Item, eval(Item).get()))
            elif isinstance(eval(Item), IntVar):
                Fp.write("%s; %d\n" % (Item, eval(Item).get()))
        Fp.close()
        if Debug is True:
            stdout.write("Setups saved to\n   %s\n" % ChanPrefsFilespec)
        return
    except Exception as e:
        Fp.close()
        if Debug is True:
            stdout.write("Error saving setups to\n   %s\n   %s\n" %
                         (ChanPrefsFilespec, e))
# END: loadChanPrefsSetups


#################################
# BEGIN: loadSourceFiles(Filebox)
# FUNC:loadSourceFiles():2018.310
#   Normal mode:
#       Looks through the directory specified in the PROGDataDirVar and
#       creates a list of the files that it comes across which it places in
#       Filebox.
#   AntRT mode:
#       (db/YEAR/DOY/files) Looks for the directory "db" then tries to read
#       the db/YEAR/DOY/files and put a list of stations in the Filebox split
#       up by YEAR.
#   B44Stick mode:
#       Just shows a list of the sub-directories of the PROGDataDirVar.
#       It's up to the user to select the one that is the Baler memory stick.
#   SC3 mode:
#       (year/net/sta/chan/data) Looks through the directories starting at the
#       [user selected] year directory level then lists the next two sub-
#       directories that should be network/station code names. The channels
#       and then day-long data files for each channel should be in the next
#       two directories.
# Don't remember the value of this in PROGSetups.
OPTCalcFileSizesCVar = IntVar()


def loadSourceFiles(Filebox):
    Filebox.delete(0, END)
    setMsg("MF", "CB", "Working...")
    Dir = PROGDataDirVar.get()
    # You never know about those users.
    if Dir.endswith(sep) is False:
        PROGDataDirVar.set(Dir + sep)
        Dir += sep
    CalcFileSizes = OPTCalcFileSizesCVar.get()
    # Not all listboxes will have a find field.
    try:
        FileFind = MFLbxFindVar.get().strip().upper()
    except Exception:
        FileFind = ""
    if exists(Dir) is False:
        setMsg("MF", "RW", "Entered data directory does not exist.", 2)
        return
    OpMode = OPTOpModeRVar.get()
    if OpMode == "Normal":
        Files = sorted(listdir(Dir))
        BMSFound = 0
        ALLFound = 0
        SOHFound = 0
        SDRFound = 0
        NANFound = 0
        ANTFound = 0
        # Go through the files and pick out each type.
        for File in Files:
            if File.startswith(".") or File.startswith("_"):
                continue
            FileLC = File.lower()
            if FileLC.endswith(".bms"):
                if CalcFileSizes == 1:
                    Size = getFolderSize(Dir + File)
                    Filename = "%s (%s)" % (File, diskSizeFormat("b", Size))
                else:
                    Filename = File
                Filebox.insert(END, Filename)
                BMSFound += 1
        if BMSFound > 0:
            Filebox.insert(END, "")
        for File in Files:
            if File.startswith(".") or File.startswith("_"):
                continue
            FileLC = File.lower()
            if FileLC.endswith(".all"):
                if CalcFileSizes == 1:
                    Size = getsize(Dir + File)
                    Filename = "%s (%s)" % (File, diskSizeFormat("b", Size))
                else:
                    Filename = File
                Filebox.insert(END, Filename)
                ALLFound += 1
        if ALLFound > 0:
            Filebox.insert(END, "")
        for File in Files:
            if File.startswith(".") or File.startswith("_"):
                continue
            FileLC = File.lower()
            if FileLC.endswith(".soh"):
                if CalcFileSizes == 1:
                    Size = getsize(Dir + File)
                    Filename = "%s (%s)" % (File, diskSizeFormat("b", Size))
                else:
                    Filename = File
                Filebox.insert(END, Filename)
                SOHFound += 1
        if SOHFound > 0:
            Filebox.insert(END, "")
        for File in Files:
            if File.startswith(".") or File.startswith("_"):
                continue
            FileLC = File.lower()
            if FileLC.endswith(".sdr"):
                if CalcFileSizes == 1:
                    Size = getFolderSize(Dir + File)
                    Filename = "%s (%s)" % (File, diskSizeFormat("b", Size))
                else:
                    Filename = File
                Filebox.insert(END, Filename)
                SDRFound += 1
        if SDRFound > 0:
            Filebox.insert(END, "")
        for File in Files:
            if File.startswith(".") or File.startswith("_"):
                continue
            FileLC = File.lower()
            if FileLC.endswith(".nan"):
                if CalcFileSizes == 1:
                    Size = getFolderSize(Dir + File)
                    Filename = "%s (%s bytes)" % (File, diskSizeFormat("b",
                                                                       Size))
                else:
                    Filename = File
                Filebox.insert(END, Filename)
                NANFound += 1
        if NANFound > 0:
            Filebox.insert(END, "")
        for File in Files:
            if File.startswith(".") or File.startswith("_"):
                continue
            FileLC = File.lower()
            if FileLC.endswith(".ant"):
                if CalcFileSizes == 1:
                    Size = getFolderSize(Dir + File)
                    Filename = "%s (%s bytes)" % (File, diskSizeFormat("b",
                                                                       Size))
                else:
                    Filename = File
                Filebox.insert(END, Filename)
                ANTFound += 1
        if BMSFound == 0 and ALLFound == 0 and SOHFound == 0 and \
                SDRFound == 0 and NANFound == 0 and ANTFound == 0:
            Filebox.insert(END, "Op Mode: Normal")
            Filebox.insert(END, "Looking for:")
            Filebox.insert(END, "")
            Filebox.insert(END, " .all = EZBaler ALL file")
            Filebox.insert(END, " .ant = Antelope files folder")
            Filebox.insert(END, "        (channel files and/or")
            Filebox.insert(END, "        multiplexed)")
            Filebox.insert(END, " .bms = Baler memory stick folder")
            Filebox.insert(END, "        (with data and/or sdata)")
            Filebox.insert(END, " .nan = Nanometrics folder of")
            Filebox.insert(END, "        seed or mini-seed channel")
            Filebox.insert(END, "        files maybe with some")
            Filebox.insert(END, "        .csv files")
            Filebox.insert(END, " .sdr = sdrsplit files folder")
            Filebox.insert(END, "        (separated by channel)")
            Filebox.insert(END, " .soh = EZBaler SOH only file")
            setMsg("MF", "YB",
                   "No recognized data source files found in\n   %s" % Dir)
        else:
            setMsg("MF", "WB",
                   "Found   .all files: %d   .ant folders: %d   .bms folders: "
                   "%d   .nan folders: %d   .sdr folders: %d   .soh files: "
                   "%d" % (ALLFound, ANTFound, BMSFound, NANFound, SDRFound,
                           SOHFound))
        # Add any files we may be looking for in their own section at the
        # bottom of the list.
        if len(FileFind) != 0:
            Finds = []
            for Index in arange(0, Filebox.size()):
                File = Filebox.get(Index)
                if FileFind in File.upper():
                    Finds.append(File)
            if len(Finds) > 0:
                Finds.sort()
                Finds.reverse()
                Filebox.insert(0, "")
                for Find in Finds:
                    Filebox.insert(0, Find)
    elif OpMode == "B44Stick":
        Filebox.insert(END, "B44 Memory Stick")
        setMsg("MF")
    elif OpMode == "AntRT":
        Files = listdir(Dir)
        Found = False
        for File in Files:
            if File == "db":
                Found = True
                break
        if Found is False:
            Filebox.insert(END, "Op Mode: AntRT")
            Filebox.insert(END, "")
            Filebox.insert(END, "No 'db' directory.")
            setMsg("MF")
            return
        # Scan for the year directories and throw out anything that doesn't
        # look like one.
        Ret = listdir(Dir + "db")
        Years = []
        for Year in Ret:
            if len(Year) == 4 and intt(Year) != 0:
                Years.append(Year)
        # Now build a list of what should be the DOY directories.
        Doys = []
        for Year in Years:
            Ret = listdir(Dir + "db" + sep + Year)
            for Doy in Ret:
                if len(Doy) == 3 and intt(Doy) != 0:
                    Doys.append(Doy)
        # NOW go through what should be the file directories and collect what
        # might be the station names. If there extraneous files in here then
        # there will be false station names.
        Years.sort()
        Doys.sort()
        Stations = []
        FileCount = {}
        for Year in Years:
            for Doy in Doys:
                FDir = Dir + "db" + sep + Year + sep + Doy + sep
                # For multi-year datasets there may be a 2011/001 directory,
                # but there might not be a 2010/001 directory. So try.
                try:
                    Files = listdir(FDir)
                except OSError:
                    continue
                for File in Files:
                    Parts = File.split(".")
                    if len(Parts[0]) != 0:
                        Station = "%s-%s" % (Year, Parts[0])
                        if Station not in Stations:
                            Stations.append(Station)
                        if Station in FileCount:
                            FileCount[Station] += 1
                        else:
                            FileCount[Station] = 1
        StationsCount = len(Stations)
        if StationsCount == 0:
            Filebox.insert(END, "No stations found.")
        else:
            Stations.sort()
            for Station in Stations:
                if CalcFileSizes == 0:
                    Filebox.insert(END, Station)
                else:
                    Filebox.insert(END, "%s  (%d %s)" %
                                   (Station, FileCount[Station],
                                    sP(FileCount[Station], ("file", "files"))))
        setMsg("MF", "", "Data files for %d %s found." %
               (StationsCount, sP(StationsCount, ("station", "stations"))))
    elif OpMode == "NanoCard":
        Filebox.insert(END, "Nano Card")
        setMsg("MF")
    elif OpMode == "SC3":
        # %%%%% not well tested
        Files = listdir(Dir)
        Found = False
        # Just look for something that looks like "2017".
        for File in Files:
            if len(File) == 4 and intt(File) != 0:
                Found = True
                break
        if Found is False:
            Filebox.insert(END, "Op Mode: SC3")
            Filebox.insert(END, "")
            Filebox.insert(END, "Not in archive's")
            Filebox.insert(END, "YEAR directory?")
            setMsg("MF")
            return
        # Collect the year directories.
        Years = []
        for File in Files:
            if len(File) == 4 and intt(File) != 0:
                Years.append(File)
        # Go through each year and get the network codes in that year.
        NetCodes = []
        for Year in Years:
            Files = listdir(Dir + Year)
            for File in Files:
                if len(File) == 2:
                    NetCodes.append(File)
        # Now find all of the stations.
        StaCodes = []
        for Year in Years:
            for NetCode in NetCodes:
                Files = listdir(Dir + Year + sep + NetCode)
                for File in Files:
                    if len(File) > 1:
                        StaCodes.append(File)
        # NOW go through all of the combinations and figure out which ones
        # actually exist.
        Stations = []
        for Year in Years:
            for NetCode in NetCodes:
                for StaCode in StaCodes:
                    if exists(Dir + Year + sep + NetCode + sep + StaCode):
                        Stations.append(Year + sep + NetCode + sep + StaCode)
        StationsCount = len(Stations)
        if StationsCount == 0:
            Filebox.insert(END, "No stations found.")
        else:
            Stations.sort()
            for Station in Stations:
                # No CalcFileSizes here.
                Filebox.insert(END, Station)
        setMsg("MF", "", "Data files for %d %s found." %
               (StationsCount, sP(StationsCount, ("station", "stations"))))
    return
##############################################
# BEGIN: loadSourceFilesCmd(Filebox, e = None)
# FUNC:loadSourceFilesCmd():2014.099


def loadSourceFilesCmd(Filebox, e=None):
    setMsg("INFO", "", "")
    setMsg("MF", "", "")
    loadSourceFiles(Filebox)
    return
########################################################
# BEGIN: loadSourceFilesClearLbxFindVar(Filebox, Reload)
# FUNC:loadSourceFilesClearLbxFindVar()2014.099


def loadSourceFilesClearLbxFindVar(Filebox, Reload):
    MFLbxFindVar.set("")
    if Reload is True:
        loadSourceFiles(Filebox)
    return
# END: loadSourceFiles


###################################
# BEGIN: dirParts(Path, Base, Subs)
# LIB:dirParts():2018.310
#   When Path is
#     /opt/seiscomp3/var/lib/archive/2017/ZZ/SLEP/ACE.T/ZZ.SLEP..ACE.T.2017.306
#   and Base is
#     /opt/seiscomp3/var/lib/archive/2017/
#   and Subs is 2 then this will be returned
#     ZZ/SLEP
#   If there are not Subs sub-directories (or files) beyond the base then *
#   will be returned like
#     ZZ/*  or  */*
#   Just "" will be returned if things go wrong.
def dirParts(Path, Base, Subs):
    if Base.endswith(sep) is False:
        Base += sep
    if Path.startswith(Base) is False:
        return ""
    Remainder = Path[len(Base):]
    Parts = Remainder.split(sep)
    Index = 0
    Return = ""
    while Index < Subs:
        if len(Return) != 0:
            Return += sep
        try:
            Return += Parts[Index]
        except IndexError:
            Return += "*"
        Index += 1
    return Return
# END: dirParts


##################################
# BEGIN: mouseWheel(Who, e = None)
# LIB:mouseWheel():2015.177
#    Bind <MouseWheel> for Windows and <Button-4>/<Button-5> for others to
#    this to get button scrolling.
def mouseWheel(Who, e=None):
    # Some systems don't call this with the right values (it depends on how the
    # trackpad/mouse is set up), so just do nothing if that happens.
    Dir = 0
    if e.num == 5 or e.delta == -120:
        Dir = 1
    if e.num == 4 or e.delta == 120:
        Dir = -1
    Who.yview_scroll(Dir, "units")
    return
# END: mouseWheel


#########################
# BEGIN: msgCenter(Which)
# FUNC:msgCenter():2012.325
def msgCenter(Which):
    if Which == "spikes":
        if OPTWhackAntSpikesCVar.get() == 0:
            setMsg("MF", "",
                   "Turned off spike whacking. Must Read source data to show "
                   "change.", 1)
        else:
            setMsg("MF", "",
                   "Turned on spike whacking. Must Read source data to show "
                   "change.", 1)
    return
# END: msgCenter


##############################
# BEGIN: nsew2pm(Which, Value)
# LIB:nsew2pm():2012.089
def nsew2pm(Which, Value):
    if Value == "?":
        return "?"
    if Which == "lat":
        try:
            if Value[0] == "N":
                return Value[1:]
            elif Value[0] == "S":
                return "-" + Value[1:]
            # Allow positions that are already + or -.
            elif Value[0].isdigit() or Value[0] == "-" or Value[0] == "+":
                return Value
            else:
                return "?"
        except IndexError:
            return "?"
    elif Which == "lon":
        try:
            if Value[0] == "E":
                return Value[1:]
            elif Value[0] == "W":
                return "-" + Value[1:]
            elif Value[0].isdigit() or Value[0] == "-" or Value[0] == "+":
                return Value
            else:
                return "?"
        except IndexError:
            return "?"
###############################
# BEGIN: pm2nsew(Which, SValue)
# FUNC:pm2nsew():2012.089
#   A little trickier that originally suspected since Value can be a number or
#   a string, and if a string it may or may not have a leading + sign, and we
#   don't just want to convert it to a float and go from there because of
#   introducing binary conversion errors and extra digits, and we never want
#   the - sign.


def pm2nsew(Which, Value):
    if Value == "?":
        return "?"
    Value = str(Value)
    if Value.startswith("+"):
        Value = Value[1:]
    FValue = floatt(Value)
    if Which == "lat":
        # Just in case the caller passes the wrong thing.
        if Value.startswith("N") or Value.startswith("S"):
            return Value
        if FValue >= 0.0:
            return "N%09.6f" % FValue
        else:
            return "S%09.6f" % abs(FValue)
    elif Which == "lon":
        if Value.startswith("E") or Value.startswith("W"):
            return Value
        if FValue >= 0.0:
            return "E%010.6f" % FValue
        else:
            return "W%010.6f" % abs(FValue)
# END: nsew2pm


###########################
# BEGIN: openDDIR(e = None)
# FUNC:openDDIR():2014.323
#   Just for macOS. The user can right-click on the DDIR entry field and open
#   a Finder window on the directory. Don't know if it works across networks
#   and stuff.
def openDDIR(e=None):
    Cmd = "open %s" % PROGDataDirVar.get()
    call(Cmd, shell=True)
    return
################################
# BEGIN: openDDIRPopup(e = None)
# FUNC:openDDIRPopup():2013.282


def openDDIRPopup(e=None):
    Xx = Root.winfo_pointerx()
    Yy = Root.winfo_pointery()
    PMenu = Menu(e.widget, font=PROGOrigPropFont, tearoff=0,
                 bg=Clr["D"], bd=2, relief=GROOVE)
    PMenu.add_command(label="Open This Directory", command=openDDIR)
    PMenu.tk_popup(Xx, Yy)
    return
# END: openDDIR


###############################
# BEGIN: openFile(Filespec, RW)
# LIB:openFile():2018.344
#   For "text" files. Opens everything as latin-1 so everyone can just get
#   along, or else. No clue what happens if the file is corrupted.
def openFile(Filespec, RW):
    # Any exception will just be passed back up.
    if PROG_PYVERS == 3:
        Fp = open(Filespec, RW, encoding="latin-1")
    return Fp
# END: openFile


########################################
# BEGIN: overwriteFile(Parent, Filespec)
# LIB:overwriteFile():2018.234
#   Returns False if the file does not exist, or the Answer.
def overwriteFile(Parent, Filespec):
    if exists(Filespec):
        if isinstance(Parent, astring):
            Parent = PROGFrm[Parent]
        Answer = formMYD(Parent, (("Overwrite", LEFT, "over"),
                                  ("(Stop)", LEFT, "stop")),
                         "stop", "YB", "Well?",
                         "File %s already exists. Do you want to overwrite it "
                         "or stop?" % basename(Filespec))
        return Answer
    return False
# END: overwriteFile


#########################
# BEGIN: plotMF(e = None)
# FUNC:plotMF():2019.232
# These are the earliest and latest times of the source data file. These will
# also be affected by any date entries in the From and To fields. These get
# set in fileSelected().
QPEarliestData = 0.0
QPLatestData = 0.0
# These are the earlist and latest times of what was last plotted. These will
# change with zooming in and out.
QPEarliestCurr = 0.0
QPLatestCurr = 0.0
MFBufLeft = 0
MFPWidth = 0
MFTimeLineTop = 0
MFTimeLineBottom = 0
MFPlotTop = 0
MFPlotBottom = 0
# Tells the control-click time function how to display the time based on how
# long the stuff plotted covers.
MFTimeMode = ""
# Gets set after something has been plotted.
MFPlotted = False


def plotMF(e=None):
    global QPEarliestCurr
    global QPLatestCurr
    global QPUserChannels
    global MFBufLeft
    global MFPWidth
    global MFTimeLineTop
    global MFTimeLineBottom
    global MFPlotTop
    global MFPlotBottom
    global MFPlotted
    LCan = PROGCan["MF"]
    # Set the background color et al.
    setColors()
    LCan.configure(bg=Clr[DClr["MF"]])
    MFPlotted = False
    setMsg("MF", "CB", "Working...")
    LCanW = LCan.winfo_width()
    MagnifyL = intt(OPTMagnifyLRVar.get())
    MagnifyH = intt(OPTMagnifyHRVar.get())
    # The value may be bad for some reason.
    if MagnifyL != 0:
        LCanW = LCanW * MagnifyL
    # Same here.
    if MagnifyH == 0:
        MagnifyH = 1
    # The sample counts could get large. +5 after the end of the plot and 5
    # pixels before the right edge.
    BufRight = PROGPropFont.measure(" 000000 ") + 5 + 5
    # The left buffer will be determined by the label lengths in QPChans of the
    # things that were found to plot below.
    MFBufLeft = 0
    # Go through the channels that the user asked to plot to establish the left
    # edge of the plotting area.
    for Chan in QPUserChannels:
        # These don't get "plotted", so skip them.
        if QPChans[Chan[:3]][QPCHANS_PLOT] == 0:
            continue
        # We want to find the QPChans info for the 3-char version of Chan and
        # then replace the first 3 chrs of the label with the full version of
        # Chan. These will get built again down in the plotting sections.
        NewLabel = QPChans[Chan[:3]][QPCHANS_LABEL]
        NewLabel = Chan + NewLabel[3:]
        if PROGPropFont.measure(NewLabel) > MFBufLeft:
            MFBufLeft = PROGPropFont.measure(NewLabel)
    # Everything starts 5 pixels from the edge and there is 10 pixels after the
    # labels before the plotting starts.
    MFBufLeft += 5 + 10
    # This can't get too small or a long date/time in the timeline may run into
    # the label like "Hours" at the beginning of the time line. So just make
    # this the lower limit.
    if MFBufLeft < 125:
        MFBufLeft = 125
    # The actual width of the plots proper.
    MFPWidth = LCanW - MFBufLeft - BufRight
    # Get local variable versions of these to make things go a bit faster.
    EarliestCurr = QPEarliestCurr
    LatestCurr = QPLatestCurr
    TimeRange = LatestCurr - EarliestCurr
    # FINISHME - check for small timerange?
    # Keeps track of where to plot things in Y. There will be a title and a
    # blank line for the time at the top of the clock rule in 3 lines,
    # so advance .5 lines to get to the vertical center of the title line,
    # plus 5 pixels to keep the title off the top.
    PY = PROGPropFontHeight / 2 + 5
    # Don't list all of the files.
    File = QPFilesProcessed[0]
    if len(QPFilesProcessed) > 1:
        File += "+"
    Message = ("Read: %s   Station: %s   %s  to  %s  (%s)" %
               (File, list2Str(QPStaIDs), dt2Time(-1, 80, EarliestCurr),
                dt2Time(-1, 80, LatestCurr),
                timeRangeFormat(LatestCurr - EarliestCurr)))
    if len(QPPlotRanges) > 0:
        Message += "  (Zoomed %d)" % len(QPPlotRanges)
    canText(LCan, 5, PY, DClr["TX"], Message)
    # Time line for Control-Click rule.
    PY += PROGPropFontHeight
    MFTimeLineTop = PY
    # Date/time line. This and the ticks line will be opposite at the end.
    PY += PROGPropFontHeight
    # Ticks line.
    PYT = PY + PROGPropFontHeight
    plotMFTimeMarks("lines", LCan, PY, PYT, EarliestCurr, LatestCurr)
    # Jump over the ticks.
    PY += PROGPropFontHeight
    # Where the plots really start.
    MFPlotTop = PY
    # This is the vertical center line of a 1-line plot (like LCQ) or the top
    # border of a multi-line plot (like LCE).
    PY += PROGPropFontHeight
    # Plotting of the gaps comes first if the checkbutton is on.
    GapsDetect = OPTGapsDetectCVar.get()
    if GapsDetect == 1:
        setMsg("MF", "CB", "Plotting Gaps...")
        LCan.create_line(MFBufLeft, PY, MFBufLeft + MFPWidth, PY,
                         fill=DClr["P0"])
        canText(LCan, 5, PY, DClr["LB"], "Gaps (%s)" % PROGGapsLengthVar.get())
        # The format of the Var has already been checked, so just get it.
        GapsLength = dt2Timedhms2Secs(PROGGapsLengthVar.get())
        Skip = int(GapsLength / 60)
        # Go through each day of the range of days that we are going to
        # plot and then see if there was data for each 1-minute period
        # (or 1h or 2h periods, etc.) that covers that time range.
        # This should match the day indecies in QPGaps.
        DaysEarliest = int(EarliestCurr / 86400.0)
        DaysLatest = int(LatestCurr / 86400.0)
        Skipped = 0
        SkipStart = 0.0
        GapCount = 0
        # If a day is missing just use this to fool the routine.
        M5sEmpty = [0] * 1440
        for DayKey in arange(DaysEarliest, DaysLatest + 1):
            DayStart = DayKey * 86400.0
            DayEnd = DayStart + 86400.0
            # If we're zoomed in skip the earlier days.
            if DayEnd < EarliestCurr:
                continue
            # If we're zoomed in we're finished.
            if DayStart > LatestCurr:
                # Check to see if we were in the middle of a down time.
                if Skipped > 0:
                    XX1 = MFBufLeft + MFPWidth * (SkipStart -
                                                  EarliestCurr) / TimeRange
                    XX2 = MFBufLeft + MFPWidth * (LatestCurr -
                                                  EarliestCurr) / TimeRange
                    if (XX2 - XX1) < 3.0:
                        ID = LCan.create_rectangle(XX1 - 1, PY - 1, XX1 + 1,
                                                   PY + 1, fill=DClr["GG"],
                                                   outline=DClr["GG"])
                    else:
                        ID = LCan.create_rectangle(XX1, PY - 1, XX2, PY + 1,
                                                   fill=DClr["GG"],
                                                   outline=DClr["GG"])
                    GapCount += 1
                    LCan.tag_bind(ID, "<Button-1>",
                                  Command(plotMFGapClick, GapCount, SkipStart,
                                          LatestCurr))
                break
            try:
                M5s = QPGaps[DayKey]
            except KeyError:
                if DayStart >= EarliestCurr and DayEnd <= LatestCurr:
                    if Skipped == 0:
                        SkipStart = DayStart
                    Skipped += 288
                    continue
                M5s = M5sEmpty
            Index = -1
            for M5 in M5s:
                Index += 1
                M5Start = DayStart + (Index * 60.0)
                M5End = M5Start + 60.0
                if M5Start > LatestCurr:
                    if Skipped >= Skip:
                        XX1 = MFBufLeft + MFPWidth * (SkipStart -
                                                      EarliestCurr) / TimeRange
                        XX2 = MFBufLeft + MFPWidth * (LatestCurr -
                                                      EarliestCurr) / TimeRange
                        if (XX2 - XX1) < 3.0:
                            ID = LCan.create_rectangle(XX1 - 1, PY - 1,
                                                       XX1 + 1, PY + 1,
                                                       fill=DClr["GG"],
                                                       outline=DClr["GG"])
                        else:
                            ID = LCan.create_rectangle(XX1, PY - 1, XX2,
                                                       PY + 1, fill=DClr["GG"],
                                                       outline=DClr["GG"])
                        GapCount += 1
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFGapClick, GapCount,
                                              SkipStart, LatestCurr))
                    break
                if M5End < EarliestCurr:
                    continue
                if M5Start < EarliestCurr:
                    M5Start = EarliestCurr
                if M5End > LatestCurr:
                    M5End = LatestCurr
                if M5 == 0:
                    if Skipped == 0:
                        SkipStart = M5Start
                    Skipped += 1
                    continue
                elif Skipped > 0:
                    if Skipped >= Skip:
                        XX1 = MFBufLeft + MFPWidth * (SkipStart -
                                                      EarliestCurr) / TimeRange
                        XX2 = MFBufLeft + MFPWidth * (M5Start -
                                                      EarliestCurr) / TimeRange
                        if (XX2 - XX1) < 3.0:
                            ID = LCan.create_rectangle(XX1 - 1, PY - 1,
                                                       XX1 + 1, PY + 1,
                                                       fill=DClr["GG"],
                                                       outline=DClr["GG"])
                        else:
                            ID = LCan.create_rectangle(XX1, PY - 1, XX2,
                                                       PY + 1, fill=DClr["GG"],
                                                       outline=DClr["GG"])
                        GapCount += 1
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFGapClick, GapCount,
                                              SkipStart, M5End))
                    Skipped = 0
                    continue
        ID = canText(LCan, MFBufLeft + MFPWidth + 5,
                     PY, DClr["TX"], "%d" % GapCount)
        LCan.tag_bind(ID, "<Button-1>", Command(plotMFShowChan, "Gap"))
        PY += PROGPropFontHeight
    # --- Where the regular plots start.
    for Chan in QPUserChannels:
        QPC = QPChans[Chan[:3]]
        # There may be an entry for these in QPChans, but there won't be
        # anything in them and they won't get plotted.
        if QPC[QPCHANS_PLOT] == 0:
            continue
        # These may have data, but will not be plotted (like GPS positions
        # that get turned into LOG lines).
        if QPC[QPCHANS_PLOT] == 2:
            continue
        # Where's My Water?
        if Chan not in QPData:
            QPErrors.append((1, "YB",
                             "Channel '%s' in Channel Preferences list, but "
                             "not found in data." % Chan, 2))
            continue
        PlotType = QPC[QPCHANS_PLOTTER]
        # -------- "mln" Multi-line normal (dots and/or line) plot.
        # -------- "mll" Multi-line line (no dots) plot.
        # ---------"mlls" Multi-line seismic (no dots and applies the bit
        # weight) plot.
        if PlotType == "mln" or PlotType == "mll" or PlotType == "mlls":
            setMsg("MF", "CB", "Plotting %s..." % Chan)
            Height = PROGPropFontHeight * QPC[QPCHANS_HEIGHT] * MagnifyH
            Bottom = PY + Height
            MidY = PY + Height / 2
            LCan.create_line(MFBufLeft, PY, MFBufLeft + MFPWidth, PY,
                             fill=DClr["P0"])
            LCan.create_line(MFBufLeft, Bottom, MFBufLeft + MFPWidth, Bottom,
                             fill=DClr["P0"])
            NewLabel = QPC[QPCHANS_LABEL]
            NewLabel = Chan + NewLabel[3:]
            canText(LCan, 5, MidY, DClr["LB"], NewLabel)
            Min = maxFloat
            Max = -maxFloat
            # Find the min and max values of what we are going to plot for the
            # Y range. Keep a count of how many points there will be so we know
            # if we are going to be binning or not. Find the index of the first
            # point we are going to plot.
            # FINISHME - make a note that this may filter out some severe
            # timing problems or even not plot a lot of data if there are
            # severe timing problems.
            Plotting = 0
            Index = -1
            StartIndex = -1
            for Data in QPData[Chan]:
                Index += 1
                Time = Data[0]
                if Time < EarliestCurr:
                    continue
                if Time > LatestCurr:
                    break
                if StartIndex == -1:
                    StartIndex = Index
                Value = Data[1]
                if Value < Min:
                    Min = Value
                if Value > Max:
                    Max = Value
                Plotting += 1
            # If there were no points in the specified time range set these so
            # we don't put maxInt and -maxInt up for the plot range.
            if Plotting == 0:
                Min = 0.0
                Max = 0.0
            Range = float(Max - Min)
            if Range == 0.0:
                Range = 1.0
            XY = []
            # Get the color.
            if PROGColorModeRVar.get() == "B":
                C = QPC[QPCHANS_BBG]
            else:
                C = QPC[QPCHANS_WBG]
            # If there aren't any points don't start looping through things
            # that don't exist.
            if Plotting == 0:
                pass
            # To bin or not to bin?
            # Fewer points than pixels don't bin. Oversample/overplot a bit
            # (MFPWidth*X) to make the extra effort of binning worth it.
            # Worring about 1.5 points per pixel is a little lame. Plotting
            # 1000's of points doesn't seem to take any effort at all, but up
            # into the higher 10.000's does.
            elif Plotting <= MFPWidth * 3:
                for Data in QPData[Chan][StartIndex:]:
                    Time = Data[0]
                    # We already know we are past the start of the time range.
                    if Time > LatestCurr:
                        break
                    Value = Data[1]
                    XX = MFBufLeft + MFPWidth * \
                        ((Time - EarliestCurr) / TimeRange)
                    YY = Bottom - Height * ((Value - Min) / Range)
                    XY += XX, YY
                if Plotting > 1:
                    if PlotType == "mln" or PlotType == "mlng":
                        LCan.create_line(XY, fill=Clr[C])
                        for i in arange(0, len(XY), 2):
                            XX = XY[i]
                            YY = XY[i + 1]
                            ID = LCan.create_rectangle(XX - 1, YY - 1, XX + 1,
                                                       YY + 1, fill=Clr[C],
                                                       outline=Clr[C])
                            LCan.tag_bind(ID, "<Button-1>",
                                          Command(plotMFPointClick, Chan,
                                                  StartIndex))
                            if B2Glitch is True:
                                LCan.tag_bind(ID, "<Button-2>",
                                              Command(plotMFPointZap, Chan,
                                                      StartIndex))
                            LCan.tag_bind(ID, "<Button-3>",
                                          Command(plotMFPointZap, Chan,
                                                  StartIndex))
                            StartIndex += 1
                    elif PlotType == "mll" or PlotType == "mlls":
                        LCan.create_line(XY, fill=Clr[C])
                elif Plotting == 1:
                    XX = XY[0]
                    YY = XY[1]
                    ID = LCan.create_rectangle(XX - 1, YY - 1, XX + 1, YY + 1,
                                               fill=Clr[C], outline=Clr[C])
                    LCan.tag_bind(ID, "<Button-1>", Command(plotMFPointClick,
                                                            Chan, StartIndex))
            # Binning.
            else:
                BinMin = maxFloat
                BinMax = -maxFloat
                # Start plotting at the first point's position, which may not
                # be at the far left edge.
                LastXX = int(MFBufLeft + MFPWidth *
                             ((QPData[Chan][StartIndex][0] - EarliestCurr) /
                              TimeRange))
                for Data in QPData[Chan][StartIndex:]:
                    Time = Data[0]
                    # We already know we are past the start time, so just check
                    # this.
                    if Time > LatestCurr:
                        break
                    # When we get to a point that is farther than LastXX then
                    # we create points for a vertical line from BinMax to
                    # BinMin.
                    XX = int(MFBufLeft + MFPWidth *
                             ((Time - EarliestCurr) / TimeRange))
                    # We don't care about the Time after this since we are
                    # plotting by X.
                    # This point must be in the next bin.
                    if XX > LastXX:
                        YY = Bottom - Height * ((BinMax - Min) / Range)
                        XY += LastXX, YY
                        YY = Bottom - Height * ((BinMin - Min) / Range)
                        XY += LastXX, YY
                        BinMin = maxFloat
                        BinMax = -maxFloat
                        LastXX = XX
                        # We might lose a few points here, but that's kindof
                        # what the overplotting math above covers up.
                        if LastXX > (MFBufLeft + MFPWidth):
                            break
                    Value = Data[1]
                    if Value < BinMin:
                        BinMin = Value
                    if Value > BinMax:
                        BinMax = Value
                # If we are binning any channel there will only be a line.
                LCan.create_line(XY, fill=Clr[C])
            # Not all channels use/have this field.
            try:
                Fix = QPC[QPCHANS_FIX]
            except Exception:
                Fix = 0
            if PlotType == "mln" or PlotType == "mll" or \
                    PlotType == "mlng" or PlotType == "mlmp":
                if Fix == 0:
                    Str1 = "%s%s" % (Max, QPC[QPCHANS_UNITS])
                    Str2 = "%s%s" % (Min, QPC[QPCHANS_UNITS])
                else:
                    Str1 = "%.*f%s" % (Fix, Max, QPC[QPCHANS_UNITS])
                    Str2 = "%.*f%s" % (Fix, Min, QPC[QPCHANS_UNITS])
            elif PlotType == "mlls":
                if OPTBitWeightRVar.get() == "low":
                    if Fix == 0:
                        Str1 = "%sV" % (Max)
                        Str2 = "%sV" % (Min)
                    else:
                        Str1 = "%.*fV" % (Fix, Max)
                        Str2 = "%.*fV" % (Fix, Min)
                elif OPTBitWeightRVar.get() == "high":
                    if Fix == 0:
                        Str1 = "%sV" % (Max)
                        Str2 = "%sV" % (Min)
                    else:
                        Str1 = "%.*fV" % (Fix, Max)
                        Str2 = "%.*fV" % (Fix, Min)
                else:
                    if Fix == 0:
                        Str1 = "%s%s" % (Max, QPC[QPCHANS_UNITS])
                        Str2 = "%s%s" % (Min, QPC[QPCHANS_UNITS])
                    else:
                        Str1 = "%.*f%s" % (Fix, Max, QPC[QPCHANS_UNITS])
                        Str2 = "%.*f%s" % (Fix, Min, QPC[QPCHANS_UNITS])
            canText(LCan, MFBufLeft - PROGPropFont.measure(Str1) - 5, PY,
                    DClr["TX"], Str1)
            canText(LCan, MFBufLeft - PROGPropFont.measure(Str2) - 5, Bottom,
                    DClr["TX"], Str2)
            ID = canText(LCan, MFBufLeft + MFPWidth + 5, MidY, DClr["TX"],
                         "%d" % Plotting)
            LCan.tag_bind(ID, "<Button-1>",
                          Command(plotMFShowChan, "%s:  Max: %s   Min: %s" %
                                  (NewLabel, Str1, Str2)))
            # All channels should be in QPSampRates, but just in case...
            if QPC[QPCHANS_SHOWSR] == 1 and Chan in QPSampRates:
                Rate = QPSampRates[Chan]
                if Rate >= 1.0:
                    Str = "%dsps" % Rate
                else:
                    Str = "%gsps" % Rate
                # Items showing the sample rate will need to be at least 4
                # units tall (like the seismic crowd), otherwise this will
                # overwrite something.
                canText(LCan, 5, MidY + PROGPropFontHeight, DClr["TX"], Str)
            PY += Height + PROGPropFontHeight * 1.5
        # -------- "mp" Single-line Mass Position-type.
        elif PlotType == "mp":
            setMsg("MF", "CB", "Plotting %s..." % Chan)
            # Figure out which voltages to check for.
            Range = OPTMPVoltageRangeRVar.get()
            if Range not in ("Regular", "Trillium"):
                QPErrors.append((1, "MW",
                                 "%s: Select the Mass Position color range "
                                 "again." % Chan, 3))
                continue
            MPVR = MPVoltageRanges[Range]
            # The color pallet to use.
            MPCP = MPColorPallets[PROGColorModeRVar.get()]
            NewLabel = QPC[QPCHANS_LABEL]
            NewLabel = Chan + NewLabel[3:]
            canText(LCan, 5, PY, DClr["LB"], NewLabel)
            LCan.create_line(MFBufLeft, PY, MFBufLeft + MFPWidth, PY,
                             fill=DClr["P0"])
            Plotted = 0
            StartIndex = -1
            Index = -1
            for Data in QPData[Chan]:
                Index += 1
                Time = Data[0]
                if Time < EarliestCurr:
                    continue
                if Time > LatestCurr:
                    break
                if StartIndex == -1:
                    StartIndex = Index
                Value = abs(Data[1])
                XX = MFBufLeft + MFPWidth * ((Time - EarliestCurr) / TimeRange)
                # Trying to make problems stick out more. Make the "OK" dots
                # smaller than the others.
                if Value <= MPVR[0]:
                    ID = LCan.create_rectangle(XX - 1, PY - 1, XX + 1, PY + 1,
                                               fill=Clr[MPCP[0]],
                                               outline=Clr[MPCP[0]])
                elif Value <= MPVR[1]:
                    ID = LCan.create_rectangle(XX - 1, PY - 1, XX + 1, PY + 1,
                                               fill=Clr[MPCP[1]],
                                               outline=Clr[MPCP[1]])
                elif Value <= MPVR[2]:
                    ID = LCan.create_rectangle(XX - 2, PY - 2, XX + 2, PY + 2,
                                               fill=Clr[MPCP[2]],
                                               outline=Clr[MPCP[2]])
                elif Value <= MPVR[3]:
                    ID = LCan.create_rectangle(XX - 2, PY - 2, XX + 2, PY + 2,
                                               fill=Clr[MPCP[3]],
                                               outline=Clr[MPCP[3]])
                else:
                    ID = LCan.create_rectangle(XX - 3, PY - 3, XX + 3, PY + 3,
                                               fill=Clr[MPCP[4]],
                                               outline=Clr[MPCP[4]])
                LCan.tag_bind(ID, "<Button-1>",
                              Command(plotMFPointClick, Chan, StartIndex))
                if B2Glitch is True:
                    LCan.tag_bind(ID, "<Button-2>",
                                  Command(plotMFPointZap, Chan, StartIndex))
                LCan.tag_bind(ID, "<Button-3>",
                              Command(plotMFPointZap, Chan, StartIndex))
                StartIndex += 1
                Plotted += 1
            ID = canText(LCan, MFBufLeft + MFPWidth + 5, PY, DClr["TX"],
                         "%d" % Plotted)
            LCan.tag_bind(ID, "<Button-1>", Command(plotMFShowChan,
                                                    "%s" % NewLabel))
            PY += PROGPropFontHeight * 1.5
        # -------- Multi-line Mass Position-type
        elif PlotType == "mlmp":
            setMsg("MF", "CB", "Plotting %s..." % Chan)
            # Figure out which voltages to check for.
            Range = OPTMPVoltageRangeRVar.get()
            # If this isn't set just make a QPErrors entry for each channel on
            # go on.
            if Range not in ("Regular", "Trillium"):
                QPErrors.append((2, "MW",
                                 "%s: Select the Mass Position color range "
                                 "again." % Chan, 3))
                continue
            MPVR = MPVoltageRanges[Range]
            # The color pallet to use.
            MPCP = MPColorPallets[PROGColorModeRVar.get()]
            Height = PROGPropFontHeight * QPC[QPCHANS_HEIGHT] * MagnifyH
            Bottom = PY + Height
            MidY = PY + Height / 2
            LCan.create_line(MFBufLeft, PY, MFBufLeft + MFPWidth, PY,
                             fill=DClr["P0"])
            LCan.create_line(MFBufLeft, Bottom, MFBufLeft + MFPWidth, Bottom,
                             fill=DClr["P0"])
            NewLabel = QPC[QPCHANS_LABEL]
            NewLabel = Chan + NewLabel[3:]
            canText(LCan, 5, MidY, DClr["LB"], NewLabel)
            # Find the min and max values of what we are going to plot for the
            # Y range.
            Min = maxFloat
            Max = -maxFloat
            Plotting = 0
            StartIndex = -1
            Index = -1
            for Data in QPData[Chan]:
                Index += 1
                Time = Data[0]
                if Time < EarliestCurr:
                    continue
                if Time > LatestCurr:
                    break
                if StartIndex == -1:
                    StartIndex = Index
                Value = Data[1]
                if Value < Min:
                    Min = Value
                if Value > Max:
                    Max = Value
                Plotting += 1
            # If there were no points in the specified time range set these so
            # we don't put maxInt and -maxInt up for the plot range.
            if Plotting == 0:
                Min = 0.0
                Max = 0.0
            Range = float(Max - Min)
            if Range == 0.0:
                Range = 1.0
            if Plotting == 0:
                pass
            else:
                XY = []
                # Where the color for each point will be kept.
                CList = []
                for Data in QPData[Chan][StartIndex:]:
                    Time = Data[0]
                    if Time > LatestCurr:
                        break
                    Value = Data[1]
                    AValue = abs(Value)
                    # Get the color based on the Value.
                    if AValue <= MPVR[0]:
                        # We can't keep these in the XY List, because the
                        # create_lines won't like it.
                        CList += [MPCP[0]]
                    elif AValue <= MPVR[1]:
                        CList += MPCP[1]
                    elif AValue <= MPVR[2]:
                        CList += [MPCP[2]]
                    elif AValue <= MPVR[3]:
                        CList += [MPCP[3]]
                    else:
                        CList += [MPCP[4]]
                    XX = MFBufLeft + MFPWidth * \
                        ((Time - EarliestCurr) / TimeRange)
                    YY = Bottom - Height * ((Value - Min) / Range)
                    XY += XX, YY
                if Plotting > 1:
                    # If there are are less than MFPWidth*3 points make it
                    # standard line and dots.
                    # If there are more than that make line segments the color
                    # that the dots would be.
                    if Plotting <= MFPWidth * 3:
                        LCan.create_line(XY, fill=DClr["PL"])
                        CIndex = 0
                        for i in arange(0, len(XY), 2):
                            XX = XY[i]
                            YY = XY[i + 1]
                            C = CList[CIndex]
                            ID = LCan.create_rectangle(XX - 1, YY - 1, XX + 1,
                                                       YY + 1, fill=Clr[C],
                                                       outline=Clr[C])
                            LCan.tag_bind(ID, "<Button-1>",
                                          Command(plotMFPointClick, Chan,
                                                  StartIndex))
                            if B2Glitch is True:
                                LCan.tag_bind(ID, "<Button-2>",
                                              Command(plotMFPointZap, Chan,
                                                      StartIndex))
                            LCan.tag_bind(ID, "<Button-3>",
                                          Command(plotMFPointZap, Chan,
                                                  StartIndex))
                            StartIndex += 1
                            CIndex += 1
                    else:
                        CStart = 0
                        CurrClr = CList[CStart]
                        CIndex = 0
                        for i in arange(0, len(XY) - 2, 2):
                            CIndex += 1
                            C = CList[CIndex]
                            if C == CurrClr:
                                continue
                            # When the color changes draw a line from CStart to
                            # CIndex. Just skip changes that are only 1 dot
                            # long. Eveything will be so squished if we are
                            # here that it won't matter. These skipped dots
                            # will show up when the user zooms in and
                            # plotting changes to individual samples (above).
                            if CIndex - CStart > 1:
                                # CIndex points to X value to plot next, the +2
                                # gets the Y value after that included in the
                                # slice.
                                LCan.create_line(XY[CStart * 2:CIndex * 2 + 2],
                                                 fill=Clr[CurrClr])
                            CurrClr = C
                            CStart = CIndex
                        # This will plot the last segment or the whole plot if,
                        # for example, the mass position just happens to be
                        # green for the whole plotting period. Same thing
                        # applies about not plotting just one last dot of a
                        # given color by itself.
                        if CIndex - CStart > 1:
                            LCan.create_line(XY[CStart * 2:CIndex * 2],
                                             fill=Clr[CurrClr])
                elif Plotting == 1:
                    XX = XY[0]
                    YY = XY[1]
                    C = CList[0]
                    ID = LCan.create_rectangle(XX - 1, YY - 1, XX + 1, YY + 1,
                                               fill=Clr[C], outline=Clr[C])
                    LCan.tag_bind(ID, "<Button-1>",
                                  Command(plotMFPointClick, Chan, StartIndex))
            Str1 = "%s%s" % (Max, QPC[QPCHANS_UNITS])
            Str2 = "%s%s" % (Min, QPC[QPCHANS_UNITS])
            canText(LCan, MFBufLeft - PROGPropFont.measure(Str1) - 5, PY,
                    DClr["TX"], Str1)
            canText(LCan, MFBufLeft - PROGPropFont.measure(Str2) - 5, Bottom,
                    DClr["TX"], Str2)
            ID = canText(LCan, MFBufLeft + MFPWidth + 5, MidY, DClr["TX"],
                         "%d" % Plotting)
            LCan.tag_bind(ID, "<Button-1>",
                          Command(plotMFShowChan, "%s:  Max: %s   Min: %s" %
                                  (NewLabel, Str1, Str2)))
            PY += Height + PROGPropFontHeight * 1.5
        # -------- "strz" Single-line, multi-value, Very Large Array/Very Long
        #          Baseline Array star coloring scheme: -1=good/not plotted,
        #          0=good/green, 1=cyan, 2=yellow, 3=red, 4=magenta
        # Just use the appropriate values to get a "binary", on/off, good/bad
        # effect.
        # (There used to be a "bin" type.)
        elif PlotType == "strz":
            setMsg("MF", "CB", "Plotting %s..." % Chan)
            CLabel = QPC[QPCHANS_LABEL]
            canText(LCan, 5, PY, DClr["LB"], CLabel)
            LCan.create_line(MFBufLeft, PY, MFBufLeft + MFPWidth, PY,
                             fill=DClr["P0"])
            Plotted = 0
            StartIndex = -1
            Index = -1
            # Figure out how many points are going to be plotted. <=MFPWidth*3
            # do the rectangles, otherwise just do a colored line.
            for Data in QPData[Chan]:
                Index += 1
                if Data[0] < EarliestCurr:
                    continue
                if Data[0] > LatestCurr:
                    break
                # This will be the QPData index of the first plotted value.
                # We'll use it below.
                if StartIndex == -1:
                    StartIndex = Index
                Value = Data[1]
                # These won't be plotted, so don't count them as part of
                # the limit.
                if Data[1] == -1:
                    continue
                Plotted += 1
            # There wasn't anything in the time range to plot.
            if Plotted == 0:
                pass
            elif Plotted <= MFPWidth * 3:
                Plotted = 0
                # The QPData index value for the click-on dot information.
                # It gets incremented right away in the loop.
                Index = StartIndex - 1
                for Data in QPData[Chan][StartIndex:]:
                    Index += 1
                    Time = Data[0]
                    # We only need to check when to jump out. StartIndex is the
                    # first point in the time range.
                    if Time > LatestCurr:
                        break
                    Value = Data[1]
                    # Don't plot these.
                    if Value == -1:
                        continue
                    XX = MFBufLeft + MFPWidth * \
                        ((Time - EarliestCurr) / TimeRange)
                    if Value == 0:
                        ID = LCan.create_rectangle(XX - 2, PY - 1, XX + 2,
                                                   PY + 1, fill=Clr["G"],
                                                   outline=Clr["G"])
                    elif Value == 1:
                        ID = LCan.create_rectangle(XX - 2, PY - 2, XX + 2,
                                                   PY + 2, fill=Clr["C"],
                                                   outline=Clr["C"])
                    elif Value == 2:
                        ID = LCan.create_rectangle(XX - 2, PY - 2, XX + 2,
                                                   PY + 2, fill=Clr["Y"],
                                                   outline=Clr["Y"])
                    elif Value == 3:
                        ID = LCan.create_rectangle(XX - 2, PY - 3, XX + 2,
                                                   PY + 3, fill=Clr["R"],
                                                   outline=Clr["R"])
                    else:
                        ID = LCan.create_rectangle(XX - 2, PY - 3, XX + 2,
                                                   PY + 3, fill=Clr["M"],
                                                   outline=Clr["M"])
                    LCan.tag_bind(ID, "<Button-1>", Command(plotMFPointClick,
                                                            Chan, Index))
                    if B2Glitch is True:
                        LCan.tag_bind(ID, "<Button-2>",
                                      Command(plotMFPointZap, Chan, Index))
                    LCan.tag_bind(ID, "<Button-3>", Command(plotMFPointZap,
                                                            Chan, Index))
                    Plotted += 1
            else:
                Plotted = 0
                # The first point's color. When it changes we'll draw a line.
                Value = QPData[Chan][StartIndex][1]
                if Value == -1:
                    CurrClr = ""
                elif Value == 0:
                    CurrClr = "G"
                elif Value == 1:
                    CurrClr = "C"
                elif Value == 2:
                    CurrClr = "Y"
                elif Value == 3:
                    CurrClr = "R"
                else:
                    CurrClr = "M"
                # There is no Index here. These won't be clickable.
                # Collect the XY values in here for each line segment.
                XY = []
                Plotted = 0
                for Data in QPData[Chan][StartIndex:]:
                    Time = Data[0]
                    if Time > LatestCurr:
                        break
                    Plotted += 1
                    Value = Data[1]
                    if Value == -1:
                        NewClr = ""
                    elif Value == 0:
                        NewClr = "G"
                    elif Value == 1:
                        NewClr = "C"
                    elif Value == 2:
                        NewClr = "Y"
                    elif Value == 3:
                        NewClr = "R"
                    else:
                        NewClr = "M"
                    # Add this point to the list, then check to see if we
                    # should plot.
                    XX = MFBufLeft + MFPWidth * \
                        ((Time - EarliestCurr) / TimeRange)
                    XY += XX, PY
                    if NewClr != CurrClr:
                        # There may only be 1 point this color if the voltages,
                        # or whatever, were varying a lot, so fake it.
                        if len(CurrClr) != 0:
                            if len(XY) >= 4:
                                LCan.create_line(XY, fill=Clr[CurrClr],
                                                 width=3)
                            else:
                                LCan.create_line(XY[0], XY[1], XY[0], XY[1],
                                                 fill=Clr[CurrClr], width=3)
                        del XY[:]
                        # Now add this same point to the new list as the first
                        # point of the new color.
                        XY += XX, PY
                        CurrClr = NewClr
                # Plot any leftovers.
                if len(CurrClr) != 0:
                    if len(XY) >= 4:
                        LCan.create_line(XY, fill=Clr[CurrClr], width=3)
                    else:
                        LCan.create_line(XY[0], XY[1], XY[0], XY[1],
                                         fill=Clr[CurrClr], width=3)
            ID = canText(LCan, MFBufLeft + MFPWidth + 5, PY, DClr["TX"],
                         "%d" % Plotted)
            LCan.tag_bind(ID, "<Button-1>", Command(plotMFShowChan,
                                                    "%s" % CLabel))
            PY += PROGPropFontHeight * 1.5
        # -------- "tri" Single-line, multi-value, 0/1/2, red/yellow/green,
        #          Off/Unlocked/Locked coloring scheme.
        elif PlotType == "tri":
            setMsg("MF", "CB", "Plotting %s..." % Chan)
            CLabel = QPC[QPCHANS_LABEL]
            canText(LCan, 5, PY, DClr["LB"], CLabel)
            LCan.create_line(MFBufLeft, PY, MFBufLeft + MFPWidth, PY,
                             fill=DClr["P0"])
            Plotted = 0
            StartIndex = -1
            Index = -1
            # Figure out how many points are going to be plotted. <=MFPWidth*3
            # do the rectangles, otherwise just do a colored line.
            for Data in QPData[Chan]:
                Index += 1
                if Data[0] < EarliestCurr:
                    continue
                if Data[0] > LatestCurr:
                    break
                # This will be the QPData index of the first plotted value.
                # We'll use it below.
                if StartIndex == -1:
                    StartIndex = Index
                Value = Data[1]
                # These won't be plotted, so don't count them as part of the
                # limit.
                if Data[1] == -1:
                    continue
                Plotted += 1
            # There wasn't anything in the time range to plot.
            if Plotted == 0:
                pass
            elif Plotted <= MFPWidth * 3:
                Plotted = 0
                # The QPData index value for the click-on dot information.
                # It gets incremented
                # right away in the loop.
                Index = StartIndex - 1
                for Data in QPData[Chan][StartIndex:]:
                    Index += 1
                    Time = Data[0]
                    # We only need to check when to jump out. StartIndex is the
                    # first point in the time range.
                    if Time > LatestCurr:
                        break
                    Value = Data[1]
                    XX = MFBufLeft + MFPWidth * \
                        ((Time - EarliestCurr) / TimeRange)
                    if Value == 0:
                        ID = LCan.create_rectangle(XX - 2, PY - 1, XX + 2,
                                                   PY + 1, fill=Clr["R"],
                                                   outline=Clr["R"])
                    elif Value == 1:
                        ID = LCan.create_rectangle(XX - 2, PY - 2, XX + 2,
                                                   PY + 2, fill=Clr["Y"],
                                                   outline=Clr["Y"])
                    elif Value == 2:
                        ID = LCan.create_rectangle(XX - 2, PY - 3, XX + 2,
                                                   PY + 3, fill=Clr["G"],
                                                   outline=Clr["G"])
                    else:
                        ID = LCan.create_rectangle(XX - 2, PY - 3, XX + 2,
                                                   PY + 3, fill=Clr["M"],
                                                   outline=Clr["M"])
                    LCan.tag_bind(ID, "<Button-1>", Command(plotMFPointClick,
                                                            Chan, Index))
                    if B2Glitch is True:
                        LCan.tag_bind(ID, "<Button-2>",
                                      Command(plotMFPointZap, Chan, Index))
                    LCan.tag_bind(ID, "<Button-3>", Command(plotMFPointZap,
                                                            Chan, Index))
                    Plotted += 1
            else:
                Plotted = 0
                # The first point's color. When it changes we'll draw a line.
                Value = QPData[Chan][StartIndex][1]
                if Value == 0:
                    CurrClr = "R"
                elif Value == 1:
                    CurrClr = "Y"
                elif Value == 2:
                    CurrClr = "G"
                else:
                    CurrClr = "M"
                # There is no Index here. These won't be clickable.
                # Collect the XY values in here for each line segment.
                XY = []
                Plotted = 0
                for Data in QPData[Chan][StartIndex:]:
                    Time = Data[0]
                    if Time > LatestCurr:
                        break
                    Plotted += 1
                    Value = Data[1]
                    if Value == 0:
                        NewClr = "R"
                    elif Value == 1:
                        NewClr = "Y"
                    elif Value == 2:
                        NewClr = "R"
                    else:
                        NewClr = "M"
                    # Add this point to the list, then check to see if we
                    # should plot.
                    XX = MFBufLeft + MFPWidth * \
                        ((Time - EarliestCurr) / TimeRange)
                    XY += XX, PY
                    if NewClr != CurrClr:
                        # There may only be 1 point this color if the voltages,
                        # or whatever, were varying a lot, so fake it.
                        if len(CurrClr) != 0:
                            if len(XY) >= 4:
                                LCan.create_line(XY, fill=Clr[CurrClr],
                                                 width=3)
                            else:
                                LCan.create_line(XY[0], XY[1], XY[0], XY[1],
                                                 fill=Clr[CurrClr], width=3)
                        del XY[:]
                        # Now add this same point to the new list as the first
                        # point of the new color.
                        XY += XX, PY
                        CurrClr = NewClr
                # Plot any leftovers.
                if len(CurrClr) != 0:
                    if len(XY) >= 4:
                        LCan.create_line(XY, fill=Clr[CurrClr], width=3)
                    else:
                        LCan.create_line(XY[0], XY[1], XY[0], XY[1],
                                         fill=Clr[CurrClr], width=3)
            ID = canText(LCan, MFBufLeft + MFPWidth + 5, PY, DClr["TX"],
                         "%d" % Plotted)
            LCan.tag_bind(ID, "<Button-1>", Command(plotMFShowChan,
                                                    "%s" % CLabel))
            PY += PROGPropFontHeight * 1.5
        # -------- "tri2" Single-line, multi-value, 0/1/2, not
        # plotted/red/magenta,
        # OK/Bad/Worse coloring scheme.
        elif PlotType == "tri2":
            setMsg("MF", "CB", "Plotting %s..." % Chan)
            CLabel = QPC[QPCHANS_LABEL]
            canText(LCan, 5, PY, DClr["LB"], CLabel)
            LCan.create_line(MFBufLeft, PY, MFBufLeft + MFPWidth, PY,
                             fill=DClr["P0"])
            Plotted = 0
            StartIndex = -1
            Index = -1
            # Figure out how many points are going to be plotted. <=MFPWidth*3
            # do the rectangles, otherwise just do a colored line.
            for Data in QPData[Chan]:
                Index += 1
                if Data[0] < EarliestCurr:
                    continue
                if Data[0] > LatestCurr:
                    break
                # This will be the QPData index of the first plotted value.
                # We'll use it below.
                if StartIndex == -1:
                    StartIndex = Index
                Value = Data[1]
                # These won't be plotted, so don't count them as part of the
                # limit.
                if Data[1] == -1:
                    continue
                Plotted += 1
            # There wasn't anything in the time range to plot.
            if Plotted == 0:
                pass
            elif Plotted <= MFPWidth * 3:
                Plotted = 0
                # The QPData index value for the click-on dot information.
                # It gets incremented right away in the loop.
                Index = StartIndex - 1
                for Data in QPData[Chan][StartIndex:]:
                    Index += 1
                    Time = Data[0]
                    # We only need to check when to jump out. StartIndex is
                    # the first point in the time range.
                    if Time > LatestCurr:
                        break
                    Value = Data[1]
                    XX = MFBufLeft + MFPWidth * \
                        ((Time - EarliestCurr) / TimeRange)
                    if Value == 0:
                        continue
                    elif Value == 1:
                        ID = LCan.create_rectangle(XX - 2, PY - 2, XX + 2,
                                                   PY + 2, fill=Clr["R"],
                                                   outline=Clr["R"])
                    elif Value == 2:
                        ID = LCan.create_rectangle(XX - 2, PY - 3, XX + 2,
                                                   PY + 3, fill=Clr["M"],
                                                   outline=Clr["M"])
                    else:
                        ID = LCan.create_rectangle(XX - 2, PY - 3, XX + 2,
                                                   PY + 3, fill=Clr["M"],
                                                   outline=Clr["M"])
                    LCan.tag_bind(ID, "<Button-1>", Command(plotMFPointClick,
                                                            Chan, Index))
                    if B2Glitch is True:
                        LCan.tag_bind(ID, "<Button-2>",
                                      Command(plotMFPointZap, Chan, Index))
                    LCan.tag_bind(ID, "<Button-3>", Command(plotMFPointZap,
                                                            Chan, Index))
                    Plotted += 1
            else:
                Plotted = 0
                # The first point's color. When it changes we'll draw a line.
                Value = QPData[Chan][StartIndex][1]
                if Value == 0:
                    CurrClr = "R"
                elif Value == 1:
                    CurrClr = "Y"
                elif Value == 2:
                    CurrClr = "G"
                else:
                    CurrClr = "M"
                # There is no Index here. These won't be clickable.
                # Collect the XY values in here for each line segment.
                XY = []
                Plotted = 0
                for Data in QPData[Chan][StartIndex:]:
                    Time = Data[0]
                    if Time > LatestCurr:
                        break
                    Plotted += 1
                    Value = Data[1]
                    if Value == 0:
                        NewClr = "R"
                    elif Value == 1:
                        NewClr = "Y"
                    elif Value == 2:
                        NewClr = "R"
                    else:
                        NewClr = "M"
                    # Add this point to the list, then check to see if we
                    # should plot.
                    XX = MFBufLeft + MFPWidth * \
                        ((Time - EarliestCurr) / TimeRange)
                    XY += XX, PY
                    if NewClr != CurrClr:
                        # There may only be 1 point this color if the voltages,
                        # or whatever, were varying a lot, so fake it.
                        if len(CurrClr) != 0:
                            if len(XY) >= 4:
                                LCan.create_line(XY, fill=Clr[CurrClr],
                                                 width=3)
                            else:
                                LCan.create_line(XY[0], XY[1], XY[0], XY[1],
                                                 fill=Clr[CurrClr], width=3)
                        del XY[:]
                        # Now add this same point to the new list as the first
                        # point of the new color.
                        XY += XX, PY
                        CurrClr = NewClr
                # Plot any leftovers.
                if len(CurrClr) != 0:
                    if len(XY) >= 4:
                        LCan.create_line(XY, fill=Clr[CurrClr], width=3)
                    else:
                        LCan.create_line(XY[0], XY[1], XY[0], XY[1],
                                         fill=Clr[CurrClr], width=3)
            ID = canText(LCan, MFBufLeft + MFPWidth + 5, PY, DClr["TX"],
                         "%d" % Plotted)
            LCan.tag_bind(ID, "<Button-1>", Command(plotMFShowChan,
                                                    "%s" % CLabel))
            PY += PROGPropFontHeight * 1.5
    # Where the plots really end.
    MFPlotBottom = PY
    PYT = PY + PROGPropFontHeight
    # PYT,PY opposite order from beginning.
    plotMFTimeMarks("lines", LCan, PYT, PY, EarliestCurr, LatestCurr)
    MFTimeLineBottom = PYT + PROGPropFontHeight
    L, T, R, B = LCan.bbox(ALL)
    # Last item, plus space for the rule time, plus a little.
    LCan.configure(scrollregion=(0, 0, R + 10, B + PROGPropFontHeight + 10))
    MFPlotted = True
    # This might get overwritten by fileSelected() with more news, but that's
    # OK.
    if GapsDetect == 1 and len(QPGaps) == 0:
        QPErrors.append((1, "YB",
                         "It looks like gap detection was not originally on "
                         "when the source data was read.\nRe-Read the source "
                         "data with Detect Gaps selected.", 1))
    return
#######################################################################
# BEGIN: plotMFTimeMarks(What, LCan, PY, PYT, EarliestTime, LatestTime)
# FUNC:plotMFTimeMarks():2013.045


def plotMFTimeMarks(What, LCan, PY, PYT, EarliestTime, LatestTime):
    global MFTimeMode
    # Plot tick marks showing months, days, hours, minutes or seconds depending
    # on the range of the currently displayed plot.
    TimeRange = LatestTime - EarliestTime
    if TimeRange >= 2592000.0:
        if What == "lines":
            canText(LCan, 5, PY, DClr["T0"], "10 Days")
            MFTimeMode = "DD"
        # Time of the nearest midnight before the earliest time.
        Time = (EarliestTime // 86400.0) * 86400
        Add = 864000.0
    elif TimeRange >= 864000.0:
        if What == "lines":
            canText(LCan, 5, PY, DClr["T0"], "Days")
            MFTimeMode = "D"
        Time = (EarliestTime // 86400.0) * 86400
        Add = 86400.0
    elif TimeRange >= 3600.0:
        if What == "lines":
            canText(LCan, 5, PY, DClr["T0"], "Hours")
            MFTimeMode = "H"
        # Nearest hour.
        Time = (EarliestTime // 3600.0) * 3600
        Add = 3600.0
    elif TimeRange >= 60.0:
        if What == "lines":
            canText(LCan, 5, PY, DClr["T0"], "Minutes")
            MFTimeMode = "M"
        # Nearest minute.
        Time = (EarliestTime // 60.0) * 60
        Add = 60.0
    else:
        if What == "lines":
            canText(LCan, 5, PY, DClr["T0"], "Seconds")
            MFTimeMode = "S"
        Time = (EarliestTime // 1)
        Add = 1.0
    # Tick marks line.
    if What == "lines":
        LCan.create_line(MFBufLeft, PYT, MFBufLeft + MFPWidth, PYT,
                         fill=DClr["TL"])
    # When we get to the tick mark that is beyond the end of the time line we
    # will put the last (end) time or the last grid line here.
    LastXX = 0.0
    # LastTime = 0.0
    NextTimeX = 0
    while True:
        Time += Add
        if Time < LatestTime:
            if Time >= EarliestTime:
                XX = MFBufLeft + MFPWidth * (Time - EarliestTime) / TimeRange
                if What == "lines":
                    LCan.create_line(XX, PYT - 2, XX, PYT + 2, fill=DClr["TL"])
                elif What == "grid":
                    LCan.create_line(XX, MFPlotTop, XX, MFPlotBottom,
                                     fill=DClr["GR"], tags="grid")
                # LastTime = Time
                LastXX = XX
                if What == "lines" and XX > NextTimeX:
                    # Returns to the milli-seconds. Chop off what we don't
                    # want.
                    TheTime = dt2Time(-1, 80, Time)
                    if MFTimeMode == "DD" or MFTimeMode == "D":
                        TheTime = TheTime[:-13]
                    elif MFTimeMode == "H":
                        TheTime = TheTime[:-7]
                    elif MFTimeMode == "M" or MFTimeMode == "S":
                        TheTime = TheTime[:-4]
                    canText(LCan, XX, PY, DClr["T0"], TheTime, "c")
                    # Make bigger ticks where the times are written.
                    LCan.create_line(XX, PYT - 3, XX, PYT + 3, width=3,
                                     fill=DClr["TL"])
                    # Take where this time ended up, and add to that the width
                    # of what was printed.
                    # When the XX above gets past that point then set WriteTime
                    # to True and write another time.
                    NextTimeX = CANTEXTLastX + CANTEXTLastWidth
        else:
            if What == "grid":
                LCan.create_line(LastXX, MFPlotTop, LastXX, MFPlotBottom,
                                 fill=DClr["GR"], tags="grid")
            break
    return
######################################
# BEGIN: plotMFShowChan(Str, e = None)
# FUNC:plotMFShowChan():2012.311
#   Handles clicking on the plot point totals at the ends of the plots and
#   showing information in the messages section.


def plotMFShowChan(Str, e=None):
    setMsg("MF", "", Str)
    return
################################################
# BEGIN: plotMFPointClick(Chan, Index, e = None)
# FUNC:plotMFPointClick():2018.310
#   Handles clicking on a data point.


def plotMFPointClick(Chan, Index, e=None):
    Time = QPData[Chan][Index][0]
    Value = QPData[Chan][Index][1]
    # Sometimes there will be a value like "Locked".
    try:
        Value2 = QPData[Chan][Index][2]
    except Exception:
        Value2 = ""
    if len(Value2) == 0:
        setMsg("MF", "", "Channel: %s   Point: %d   Time: %s   Value: %g%s" %
               (Chan, Index + 1, dt2Time(-1, 80, Time), Value,
                QPChans[Chan[:3]][QPCHANS_UNITS]))
    else:
        setMsg("MF", "",
               "Channel: %s   Point: %d   Time: %s   Value: %g%s (%s)" %
               (Chan, Index + 1, dt2Time(-1, 80, Time), Value,
                QPChans[Chan[:3]][QPCHANS_UNITS], Value2))
    # If there is a third item it should be the QPLogs index number, which
    # should be the line number in formLOG().
    try:
        Line = QPData[Chan][Index][2]
        if isinstance(Line, anint):
            # WARNING
            # This is a special one-time deal only for the "strz"-type plot
            # dots. Keep these colors the same as the stuff in the plotMF()
            # "strz" section.
            if QPChans[Chan[:3]][QPCHANS_PLOTTER] == "strz":
                # The -1's should never come here (they don't get plotted),
                # but we're covered anyway.
                if Value == -1 or Value == 0:
                    # We're assuming (uh huh) a white formLOG() background.
                    HClr = "GB"
                elif Value == 1:
                    HClr = "CB"
                elif Value == 2:
                    HClr = "YB"
                elif Value == 3:
                    HClr = "RW"
                else:
                    HClr = "MW"
            # Same special two-time deal for the "tri"-type plot dots.
            elif QPChans[Chan[:3]][QPCHANS_PLOTTER] == "tri":
                if Value == 0:
                    # We're assuming (uh huh) a white formLOG() background.
                    HClr = "RW"
                elif Value == 1:
                    HClr = "YB"
                elif Value == 2:
                    HClr = "GB"
                else:
                    HClr = "MW"
            # Same special three-time deal for the "tri2"-type plot dots.
            elif QPChans[Chan[:3]][QPCHANS_PLOTTER] == "tri2":
                # Should never show up.
                if Value == 0:
                    # We're assuming (uh huh) a white formLOG() background.
                    HClr = "GB"
                elif Value == 1:
                    HClr = "RW"
                elif Value == 2:
                    HClr = "MW"
                else:
                    HClr = "MW"
            else:
                HClr = QPChans[Chan[:3]][QPCHANS_LOGBF]
            formLOGHilite(Line, HClr)
    except Exception:
        pass
    return
################################################
# BEGIN: plotMFGapClick(Gap, From, To, e = None)
# FUNC:plotMFGapClick():2013.045


def plotMFGapClick(Gap, From, To, e=None):
    setMsg("MF", "", "Gap %d: %s  to  %s  (%s)" %
           (Gap, dt2Time(-1, 80, From), dt2Time(-1, 80, To),
            timeRangeFormat(To - From)))
    return
##############################################
# BEGIN: plotMFPointZap(Chan, Index, e = None)
# FUNC:plotMFPointZap():2012.334
#   Handles right-clicking on points. I'm not even going to advertise this.
#   With having to replot (because we just deleted an element, so we can't do
#   another one without re-indexing) it's kinda cumbersome.


def plotMFPointZap(Chan, Index, e=None):
    if len(QPData[Chan]) > 1:
        del QPData[Chan][Index]
    plotMFReplot()
    return


###########################################
# BEGIN: plotMFTimeRuleGrid(LCan, e = None)
# FUNC:plotMFTimeRuleGrid():2014.078
#   For time rule clicks (i.e. Control-Button-1) on the MF Canvas to draw the
#   time and/or the time grids.
MFGridOn = False


def plotMFTimeRuleGrid(LCan, e=None):
    global MFGridOn
    if MFPlotted is False:
        beep(1)
        return
    CX = LCan.canvasx(e.x)
    CY = LCan.canvasy(e.y)
    Time = plotMFEpochAtX(CX, CY)
    if Time == -10 or Time == -20 or Time == -30 or Time == -40:
        LCan.delete("rule")
        LCan.delete("grid")
        # We'll throw this in here so the user doesn't have to go all of the
        # way back to (probably) the TPS plot to get rid of the line.
        LCan.delete("orule")
        setMsg("MF", "", "")
        return
    if Time == -100 or Time == -200:
        LCan.delete("rule")
        LCan.delete("orule")
        setMsg("MF", "", "")
        if MFGridOn is False:
            plotMFTimeMarks("grid", LCan, 0, 0, QPEarliestCurr, QPLatestCurr)
            MFGridOn = True
        else:
            LCan.delete("grid")
            MFGridOn = False
        return
    LCan.delete("rule")
    LCan.delete("orule")
    TheTime = dt2Time(-1, 80, Time)
    if MFTimeMode == "DD" or MFTimeMode == "D" or MFTimeMode == "H":
        TheTime = TheTime[:-7]
    elif MFTimeMode == "M":
        TheTime = TheTime[:-4]
    elif MFTimeMode == "S":
        pass
    canText(LCan, CX, MFTimeLineTop, DClr["TM"], TheTime, "c", "rule")
    LCan.create_line(CX, MFPlotTop, CX, MFPlotBottom, fill=DClr["TR"],
                     tags="rule")
    canText(LCan, CX, MFTimeLineBottom, DClr["TM"], TheTime, "c", "rule")
    setMsg("MF", "", "Time rule: %s" % TheTime)
    return
###############################
# BEGIN: plotMFEpochAtX(CX, CY)
# FUNC:plotMFEpochAtX():2014.078
#    Takes the passed CX pixel location and returnsa code if the clicking is
#    off the grid, or the epoch time at that plot location on the main form.


def plotMFEpochAtX(CX, CY):
    # Way left.
    if CX < MFBufLeft - 25:
        return -10
    # Just left.
    if CX < MFBufLeft:
        return -20
    # Just right.
    if CX > (MFBufLeft + MFPWidth) and CX <= (MFBufLeft + MFPWidth + 25):
        return -30
    # Way right.
    if CX > (MFBufLeft + MFPWidth + 25):
        return -40
    # Off top.
    if CY < MFPlotTop:
        return -100
    # Off bottom.
    if CY > MFPlotBottom:
        return -200
    return QPEarliestCurr + (((CX - MFBufLeft) / MFPWidth) *
                             (QPLatestCurr - QPEarliestCurr))


###########################
# BEGIN: plotMFZoomClick(e)
# FUNC:plotMFZoomClick():2012.329
#   Handles shift-clicks for zooming on the MF Canvas.
MFSelectX1 = 0
MFSelectTime1 = 0.0


def plotMFZoomClick(e):
    global MFSelectX1
    global MFSelectTime1
    global QPEarliestCurr
    global QPLatestCurr
    global QPPlotRanges
    if MFPlotted is False:
        beep(1)
        return
    CX = LCan.canvasx(e.x)
    CY = LCan.canvasy(e.y)
    Time = plotMFEpochAtX(CX, CY)
    # %%%% check these
    # Must be the first click.
    if MFSelectX1 == 0:
        # Way left, try to zoom out.
        if Time == -10:
            if len(QPPlotRanges) == 0:
                setMsg("MF", "", "Already zoomed all of the way out.", 1)
                return
            Range = QPPlotRanges[-1]
            QPEarliestCurr = Range[0]
            QPLatestCurr = Range[1]
            QPPlotRanges.remove(Range)
            plotMFReplot()
        # Way right, top or bottom.
        elif Time == -40 or Time == -100 or Time == -200:
            beep(1)
        else:
            # If close to the left edge of the plots then assume they wanted
            # to click at the left edge of the plots.
            # Just left.
            if Time == -20:
                Time = QPEarliestCurr
                CX = MFBufLeft
            # Just right.
            elif Time == -30:
                Time = QPLatestCurr
                CX = MFBufLeft + MFPWidth
            LCan.create_line(CX, MFPlotTop, CX, MFPlotBottom,
                             fill=DClr["SR"], tags="sel")
            MFSelectX1 = CX
            MFSelectTime1 = Time
    else:
        # Second select.
        # Way left or way right, erase the first line.
        if Time == -10 or Time == -40:
            LCan.delete("sel")
            MFSelectX1 = 0
        # Top or bottom.
        elif Time == -100 or Time == -200:
            beep(1)
        else:
            # If we click in the exact same X then erase the first line.
            if MFSelectTime1 == Time:
                LCan.delete("sel")
                MFSelectX1 = 0
                return
            # Just left.
            if Time == -20:
                Time = QPEarliestCurr
                CX = MFBufLeft
            # Just right.
            elif Time == -30:
                Time = QPLatestCurr
                CX = MFBufLeft + MFPWidth
            LCan.create_line(CX, MFPlotTop, CX, MFPlotBottom,
                             fill=DClr["SR"], tags="sel")
            updateMe(0)
            sleep(.2)
            LCan.delete("sel")
            MFSelectX1 = 0
            # Record these before we change them.
            QPPlotRanges.append((QPEarliestCurr, QPLatestCurr))
            if MFSelectTime1 < Time:
                QPEarliestCurr = MFSelectTime1
                QPLatestCurr = Time
            else:
                QPEarliestCurr = Time
                QPLatestCurr = MFSelectTime1
            plotMFReplot()
    return
#######################################
# BEGIN: plotMFShowTimeRule(Who, Epoch)
# FUNC:plotMFShowTimeRule():2013.045
#   For external callers to use to show and clear the "orule" rule (generally
#   caused by clicking on the TPS plot).


def plotMFShowTimeRule(Who, Epoch):
    LCan = PROGCan["MF"]
    LCan.delete("orule")
    setMsg("MF", "", "")
    if Epoch == -1:
        return
    if Epoch < QPEarliestCurr:
        setMsg("MF", "", "Time is before current time range.", 1)
        return
    if Epoch > QPLatestCurr:
        setMsg("MF", "", "Time is after current time range.", 1)
        return
    XX = MFBufLeft + MFPWidth * ((Epoch - QPEarliestCurr) /
                                 (QPLatestCurr - QPEarliestCurr))
    LCan.create_line(XX, MFPlotTop, XX, MFPlotBottom, fill=DClr["OR"],
                     tags="orule")
    setMsg("MF", "", "%s Time: %s" % (Who, dt2Time(-1, 80, Epoch)[:-4]))
    return
###########################
# BEGIN: plotMFMindTheGap()
# FUNC:plotMFMindTheGap():2013.045


def plotMFMindTheGap():
    if OPTGapsDetectCVar.get() == 1:
        GapsLength = dt2Timedhms2Secs(PROGGapsLengthVar.get())
        if GapsLength == 0:
            setMsg("MF", "RW", "The Gap Length field value is 0.", 2)
            return False
        if GapsLength % 60 != 0:
            setMsg("MF", "RW",
                   "The Gap Length field value is not a multiple of 1 "
                   "minute.", 2)
            return False
    return True
#####################
# BEGIN: plotMFPlot()
# FUNC:plotMFPlot():2012.334


def plotMFPlot():
    global QPEarliestCurr
    global QPLatestCurr
    Root.focus_set()
    # Only fileSelected() calls this so this must be the right thing to do.
    QPEarliestCurr = QPEarliestData
    QPLatestCurr = QPLatestData
    plotMF()
    # Set this back to Working... and fileSelected() will set it to Done when
    # everything has finished plotting.
    setMsg("MF", "CB", "Working...")
    return
#######################
# BEGIN: plotMFReplot()
# FUNC:plotMFReplot():2016.005


def plotMFReplot():
    global QPErrors
    Root.focus_set()
    if len(QPData) == 0:
        setMsg("MF", "RW", "No file(s) have been read.", 2)
        return False
    PROGCan["MF"].delete(ALL)
    # Don't reset the scroll region here so the display will stay end up
    # somewhere near where it was and the user won't have to scroll back down
    # to see what they were looking at.
    # Get the list again. The user may have rearranged things.
    Ret = setQPUserChannels("replot")
    if Ret[0] != 0:
        setMsg("MF", Ret)
        return False
    # Check this here in case the user changed it.
    if plotMFMindTheGap() is False:
        return False
    formQPERRClear()
    del QPErrors[:]
    plotMF()
    if len(QPErrors) == 0:
        formClose("QPERR")
    else:
        formQPERR()
    setMsg("MF", "", "Done.")
    return
######################
# BEGIN: plotMFClear()
# FUNC:plotMFClear():2012.334


def plotMFClear():
    PROGCan["MF"].delete(ALL)
    PROGCan["MF"].configure(scrollregion=(0, 0, 1, 1))
    return
# END: plotMF


# TESTING - This variable is all there is right now. :)
TryPyDecode = False
###########################################################################
# BEGIN: q330Process(Filespec, FromEpoch, ToEpoch, StaIDFilter, GapsDetect,
#                WhackAntSpikes, SkipFile, StopBut, Running, WhereMsg,
#                ListChannels)
# FUNC:q330Process():2019.231
#   Handles getting the information from the passed mini-seed format Filespec
#   decoded and left in the global QP variables.


def q330Process(Filespec, FromEpoch, ToEpoch, StaIDFilter, GapsDetect,
                WhackAntSpikes, SkipFile, StopBut, Running, WhereMsg,
                ListChannels):
    global QPData
    global QPLogs
    global QPGaps
    global QPErrors
    global QPStaIDs
    global QPNetCodes
    global QPTagNos
    global QPSWVers
    global QPLocIDs
    global QPSampRates
    global QPUserChannels
    global QPAntSpikesWhacked
    global QPUnknownChanIDs
    global QPUnknownBlocketteType
    global QPListChannels
    Iam = stack()[0][3]
    SupressUBM = OPTSupressUBMCVar.get()
    try:
        Fp = open(Filespec, "rb")
    except Exception as e:
        return (1, "MW", "%s: Error opening file. %s" % (Iam, e), 3, Filespec)
    # This is used to unset a gap's 1 value if we are whacking large Antelope
    # values.
    GapWas = 0
    Ret = findRecordSize(Fp)
    if Ret[0] != 0:
        return (1, Ret[1], "%s: %s" % (basename(Filespec), Ret[2]), Ret[3])
    RecordSize = Ret[1]
    Fp.seek(0)
    # ---------------
    # FINISHME - I think this is where the C code for decoding will be called?
    # Maybe I'll just pass each chunk a little further down, instead. We'll
    # have to see what works best.
    # ---------------
    # Read the file in 10 record chunks to make it faster.
    RecordsSize = RecordSize * 10
    RecordsReads = 0
    # Some may use this for messages.
    NoOfRecords = int(getsize(Filespec) / RecordSize)
    if ListChannels != 0:
        if Filespec not in QPListChannels:
            QPListChannels[Filespec] = []
        RSize = "%dB" % RecordSize
        if RecordSize not in QPListChannels[Filespec]:
            QPListChannels[Filespec].append(RecordSize)
    while True:
        Records = Fp.read(RecordsSize)
        # We're done with this file.
        if len(Records) == 0:
            Fp.close()
            return (0, )
        RecordsReads += 10
        if RecordsReads % 10000 == 0:
            # If WhereMsg is None then the caller will handle stopping suddenly
            # (like by not calling this function anymore), otherwise this
            # function will check and generate the progress messages (like for
            # when reading an all-in-one file).
            if WhereMsg is not None:
                StopBut.update()
                if Running == 0:
                    # Nothing wrong here. The caller will handle the actual
                    # stopping.
                    return (0, )
                setMsg(WhereMsg, "CB", "Reading record %d of %d..." %
                       (RecordsReads, NoOfRecords))
        for i in arange(0, 10):
            Ptr = RecordSize * i
            Record = Records[Ptr:Ptr + RecordSize]
            # Need another set of Records.
            if len(Record) < RecordSize:
                break
            if PROG_PYVERS == 3:
                ChanID = Record[15:18].decode("latin-1")
                ChanID3 = ChanID[:3]
                LocID = Record[13:15].decode("latin-1").strip()
                ChanID = ChanID + LocID
                StaID = Record[8:13].decode("latin-1").strip()
            # The Q330 may create "empty" files that usually are all 00H and
            # then not finish filling them in. The read()s keep reading, but
            # there's nothing to process. Apparently the files can also be
            # slight gibberish. This trys to detect this and goes on to the
            # next file. This may only happen in .bms-type data, but also
            # showed up in some SeisComp3-created files.
            if ChanID.isalnum() is False or StaID.isalnum() is False:
                QPErrors.append((4, "YB", "File ends empty. Not used.", 2,
                                 Filespec))
                return (0, )
            if len(StaIDFilter) != 0 and StaID != StaIDFilter:
                continue
            if ListChannels != 0:
                if ChanID not in QPListChannels[Filespec]:
                    QPListChannels[Filespec].append(ChanID)
            # Just to minimize CPU cycles we'll only check all of these from
            # the beginning of each file that we get passed.
            if RecordsReads == 10:
                if StaID not in QPStaIDs:
                    QPStaIDs.append(StaID)
                if PROG_PYVERS == 3:
                    NetCode = Record[18:20].decode("latin-1")
                if NetCode not in QPNetCodes:
                    QPNetCodes.append(NetCode)
# ----------------------------------------------------------
# Don't go any further if the user didn't request it. Maybe.
# ----------------------------------------------------------
            if GapsDetect == 0:
                # Delay doing this check until after we decode the record time
                # if we are looking for gaps. This check will be repeated
                # below.
                if ChanID not in QPUserChannels:
                    # Just to keep from getting a zillion error messages.
                    if ChanID not in QPUnknownChanIDs:
                        QPUnknownChanIDs.append(ChanID)
                        # Only complain if the channel is plottable.
                        # The user may have ignored the warning about channels
                        # that QPEEK does not know anything about when canning
                        # the file, so try and leave a message. Again.
                        try:
                            if QPChans[ChanID3][QPCHANS_PLOT] == 1:
                                QPErrors.append((1, "YB",
                                                 "Channel-location '%s' found "
                                                 "in data, but not in channel "
                                                 "list." % ChanID, 2))
                        except Exception:
                            QPErrors.append((1, "YB",
                                             "Channel-location '%s' found in "
                                             "data, but QPEEK does not know "
                                             "how to handle it." % ChanID, 2))
                    # If this is something like an sdrsplit-produced file that
                    # has only one kind of channel, and the caller has
                    # authorized it, then just return back for the next file
                    # since this one doesn't have anything we want.
                    if SkipFile is True:
                        return (0, )
                    continue
            # Everybody will need the time.
            Year, Doy, Hour, Mins, Secs, Tttt = unpack(">HHBBBxH",
                                                       Record[20:30])
            M, D = q330yd2md(Year, Doy)
            RecordStartTimeEpoch = q330ydhmst2Epoch(Year, Doy, Hour, Mins,
                                                    Secs, Tttt)
            # FINISHME - This will need to be moved to the frame/sample level
            # at some point.
            if RecordStartTimeEpoch < FromEpoch:
                continue
            if RecordStartTimeEpoch > ToEpoch:
                return (0, )
            # FINISHME - this will move too.
            # QPGaps will be  int(Epoch//86400.0):[1m, 1m, 1m, ...] where each
            # 1m \ represents each 1 minute of the day and will be 0 or 1 --
            # there was no data for that 1 minute, or there was.
            if GapsDetect == 1:
                Days = RecordStartTimeEpoch / 86400.0
                DayIndex = int(Days)
                M5 = int((Days - DayIndex) * 1440)
                try:
                    GapWas = QPGaps[DayIndex][M5]
                    QPGaps[DayIndex][M5] = 1
                except Exception:
                    QPGaps[DayIndex] = [0] * 1440
                    QPGaps[DayIndex][M5] = 1
                    GapWas = 0
                # Now do this part that we skipped above.
                if ChanID not in QPUserChannels:
                    if ChanID not in QPUnknownChanIDs:
                        QPErrors.append((1, "YB",
                                         "Channel-location '%s' found in "
                                         "data, but not in channel list." %
                                         ChanID, 2))
                        QPUnknownChanIDs.append(ChanID)
                    # Don't check SkipFile here. As punishment for detecting
                    # gaps we will read through the whole file even if we are
                    # not interested.
                    continue
            # Set up to start collecting for this channel. A LOG (and ACE and
            # OCF -- not plottable channels -- for Q330s) entry will be made
            # here, but won't end up having anything in it.
            if ChanID not in QPData:
                QPData[ChanID] = []
            # LOG records are a special case. We don't need to know anything
            # else about them to process them.
            # All LOG messages will be written together (there's only one form
            # to show them on) even if there may be a location ID associated
            # with them.
            if ChanID.startswith("LOG"):
                # An RT130-style header line just to break up the monotony.
                # The date matches
                # the Q330 YYY-MM-DD format.
                QPLogs.append("State of Health   %d-%02d-%02d %02d:%02d:"
                              "%02d.%03d   ST: %s" % (Year, M, D, Hour, Mins,
                                                      Secs, Tttt, StaID))
                # All of this binary gibberish to save space and then they
                # waste a byte here and all of the space after the messages
                # below.
                if PROG_PYVERS == 3:
                    Lines = Record[56:].decode("latin-1").split("\r\n")
                # Most of the time there is wasted space for the rest of the
                # record.
                if Lines[-1].startswith("\x00"):
                    Lines = Lines[:-1]
                # Pick the stuff out of the lOG messages that we are interested
                # in here.
                for Line in Lines:
                    try:
                        Index = Line.index("KMI Property Tag Number:")
                        # 24 = len("KMI...Number:")
                        Value = Line[Index + 24:].strip()
                        if Value not in QPTagNos:
                            QPTagNos.append(Value)
                    except Exception:
                        pass
                    try:
                        Index = Line.index("System Software Version:")
                        Value = Line[Index + 24:].strip()
                        if Value not in QPSWVers:
                            QPSWVers.append(Value)
                    except Exception:
                        pass
                    QPLogs.append(Line)
                QPLogs.append("")
                continue
            # This will be used by many.
            QPC = QPChans[ChanID[:3]]
            # We can't check for this until here, because of the LOG stuff
            # above. Again, in some cases there won't be anything in the file
            # we want. The caller will decide.
            if QPC[QPCHANS_PLOT] == 0:
                if SkipFile is True:
                    return (0, )
                continue
            # OK. We have (probably) 4K of data with a given ChanID like HHZ
            # or VKI. We will get a bit more info from the fixed header then
            # we will see what is next. What needs to be next is a 1000
            # blockette followed by a 1001 blockette. The 1001 blockette will
            # tell us how many frames of 64 bytes of Steim compressed data
            # samples there is going to be in the rest of the 4K.
            # With just Python I don't have a lot of CPU cycles to decode
            # everything, so I'll only grab the first sample of the first
            # 64byte frame from each 4K block. It's the only one that must be
            # an absolute value and not a difference.
            # If the global TryPyDecode is True, then I will try to decode all
            # of the data points in the record, and keep the first samples
            # from each frame.
            BeginDataOffset = 0
            NumberOfSamples = unpack(">H", Record[30:32])[0]
            if PROG_PYVERS == 3:
                NumberOfBlockettes = Record[39]
            # Where all of the blockettes end and the data begins.
            BlocketteOffset = unpack(">H", Record[46:48])[0]
            for i in arange(0, NumberOfBlockettes):
                BlocketteType = unpack(">H",
                                       Record[BlocketteOffset:
                                              BlocketteOffset + 2])[0]
                if BlocketteType == 1000:
                    Blockette = Record[BlocketteOffset:BlocketteOffset + 8]
                    EncodingFormat = Blockette[4]
                    if EncodingFormat != 10 and EncodingFormat != 11:
                        stdout.write("1000: %s EncodingFormat not 10 or 11: "
                                     "%s\n" % (ChanID, EncodingFormat))
                        break
                    if ChanID not in QPSampRates:
                        SampleRateFactor = float(unpack(">h",
                                                        Record[32:34])[0])
                        SampleRateMult = float(unpack(">h",
                                                      Record[34:36])[0])
                        try:
                            if SampleRateFactor > 0:
                                if SampleRateMult >= 0:
                                    SampleRate = SampleRateFactor * \
                                        SampleRateMult
                                else:
                                    SampleRate = -1 * SampleRateFactor / \
                                        SampleRateMult
                            else:
                                if SampleRateMult >= 0:
                                    SampleRate = -1 * SampleRateMult / \
                                        SampleRateFactor
                                else:
                                    SampleRate = 1 / (SampleRateFactor *
                                                      SampleRateMult)
                        # The record contains LOG messages or other non-sampled
                        # data (except we won't even be here if it is a LOG
                        # record - that was handled above), or it's corrupted.
                        except Exception:
                            SampleRate = 0.0
                        QPSampRates[ChanID] = SampleRate
                    BeginDataOffset = unpack(">H", Record[44:46])[0]
                    # Next blockette. This will be in Record offset like it was
                    # before we entered the loop.
                    BlocketteOffset = unpack(">H", Blockette[2:4])[0]
                    # TESTING - If the Record only has a blockette 1000 get
                    # the data?
                    if BeginDataOffset != 0 and BlocketteOffset == 0:
                        # FINISHME - there may be more stuff in here when we
                        # figure out the TitanSMA and Apollo data layout.
                        FrameCount = 0
                        break
                    continue
                if BlocketteType == 1001:
                    Blockette = Record[BlocketteOffset:BlocketteOffset + 8]
                    FrameCount = Blockette[7]
                    BlocketteOffset = unpack(">H", Blockette[2:4])[0]
                    # Next blockette. This will be in Record offset like it
                    # was before we entered the loop.
                    continue
                # Just jump over these. The Record sample rate seems to be
                # fine for our stuff.
                # These are not in Q330 data, but in Antelope data.
                if BlocketteType == 100:
                    Blockette = Record[BlocketteOffset:BlocketteOffset + 12]
                    BlocketteOffset = unpack(">H", Blockette[2:4])[0]
                    continue
                # 2018: There has been blockette 201's and 320's, and that's
                # about it so far.
                # I think they are TA-realated.
                else:
                    # Well, we tried. Put the details to stdout. There may be
                    # a lot of them.
                    if SupressUBM == 0:
                        stdout.write("%s: Unhandled blockette at %d-%02d-%02d "
                                     "%02d:%02d: %d-%d\n" % (ChanID, Year, M,
                                                             D, Hour, Mins, i,
                                                             BlocketteType))
                        if BlocketteType not in QPUnknownBlocketteType:
                            QPErrors.append((4, "RW",
                                             "%s: Unhandled blockette: %d" %
                                             (ChanID, BlocketteType), 2,
                                             Filespec))
                            QPUnknownBlocketteType.append(BlocketteType)
                    continue
            if BeginDataOffset != 0:
                FrameTime = RecordStartTimeEpoch
                FrameOffset = BeginDataOffset + 4
                # Get the decode code for this channel. This keeps us from
                # having a big if statement below to decide how to decode the
                # information in this record.
                DecodeCode = QPC[QPCHANS_DECODE]
                # There may be a small detour to get the right code for the
                # data for this make/model of recorder.
                if DecodeCode > 89:
                    if DecodeCode == 99:
                        DecodeCode = QPChansDecodeV[QPCDSelect]
                    elif DecodeCode == 999:
                        DecodeCode = QPChansDecodeMP[QPCDSelect]
                if TryPyDecode is False:
                    Value = unpack(
                        ">l", Record[FrameOffset:FrameOffset + 4])[0]
                    # If we get a large sample value and we are whacking
                    # Antelope spikes (where the large value probably came
                    # from) then if we are detecting gaps we need to set the
                    # 5-minute block for this sample back to 0 if it was before
                    # this sample, because we are going to just not record this
                    # sample and if there are enough of them we'll want a gap
                    # to show up.
                    if WhackAntSpikes == 1 and Value >= AntSpikesValue:
                        if GapsDetect == 1:
                            if GapWas == 0:
                                QPGaps[DayIndex][M5] = 0
                        QPAntSpikesWhacked += 1
                        continue
                    if DecodeCode == 1:
                        QPData[ChanID].append((FrameTime, Value))
                    elif DecodeCode == 2:
                        QPData[ChanID].append((FrameTime, Value * .150))
                    elif DecodeCode == 3:
                        QPData[ChanID].append((FrameTime, Value * .1))
                    elif DecodeCode == 4:
                        # Can't have (-) current, but it can show up that way
                        # (1 count negative).
                        if Value < 0:
                            Value = 0
                        QPData[ChanID].append((FrameTime, Value))
                    elif DecodeCode == 5:
                        # Some channels that represent things like percent can
                        # be a negative number.
                        Value = Value * .1
                        if Value < 0:
                            Value = 0.0
                        QPData[ChanID].append((FrameTime, Value))
                    elif DecodeCode == 6:
                        Value = Value / 1000.0
                        QPData[ChanID].append((FrameTime, Value))
                    elif DecodeCode == 7:
                        Value = Value / 1000000.0
                        QPData[ChanID].append((FrameTime, Value))
                    else:
                        stdout.write("Unknown DecodeCode: %d\n" % DecodeCode)
                # FINISHME - future. Get the first value from each frame.
                else:
                    # We are going to cheat by figuring out how long a record
                    # is in time and then just dividing that by the number of
                    # frames to get the first sample time of each frame.
                    # This will make sample times wrong (for sure), but will
                    # make things a bit quicker. The constant sample rate/not a
                    # lot of variation in amplitude stuff will be close, but
                    # the seismic data will be a bit off.
                    TimePerFrame = (NumberOfSamples * SampleRate) / FrameCount
                    # Visit each 64 byte frame.
                    for f in arange(0, FrameCount):
                        FrameOffset += 64 * f
                        FrameTime += TimePerFrame * f
                        if EncodingFormat == 10:
                            pass
                        elif EncodingFormat == 11:
                            pass
                        # FINISHME - will all change
                        if f == 0:
                            Value = unpack(">l", Record[Offset:Offset + 4])[0]
                            # If we get a large sample value and we are
                            # whacking Antelope spikes (where the large value
                            # probably came from) then if we are detecting gaps
                            # we need to set the 5-minute block for this sample
                            # back to 0 if it was before this sample, because
                            # we are going to just not record this sample and
                            # if there are enough of them we'll want a gap to
                            # show up.
                            if WhackAntSpikes == 1 and Value >= AntSpikesValue:
                                if GapsDetect == 1:
                                    if GapWas == 0:
                                        QPGaps[DayIndex][M5] = 0
                                QPAntSpikesWhacked += 1
                                continue
                        if DecodeCode == 1:
                            QPData[ChanID].append((FrameTime, Value))
                        elif DecodeCode == 2:
                            QPData[ChanID].append((FrameTime, Value * .150))
                        elif DecodeCode == 3:
                            QPData[ChanID].append((FrameTime, Value * .1))
                        elif DecodeCode == 4:
                            # Can't have (-) current, but it can show up that
                            # way (1 count negative).
                            if Value < 0:
                                Value = 0
                            QPData[ChanID].append((FrameTime, Value))
                        elif DecodeCode == 5:
                            # Some channels that represent things like percent
                            # can be a negative number.
                            Value = Value * .1
                            if Value < 0:
                                Value = 0.0
                            QPData[ChanID].append((FrameTime, Value))
                        elif DecodeCode == 6:
                            Value = Value / 1000.0
                            QPData[ChanID].append((FrameTime, Value))
                        elif DecodeCode == 7:
                            Value = Value / 1000000.0
                            QPData[ChanID].append((FrameTime, Value))
                        else:
                            stdout.write(
                                "1000/1001: Unknown DecodeCode: %d\n" %
                                DecodeCode)
    return (0, )
# END: q330Process


#############################
# BEGIN: q330yd2md(YYYY, DOY)
# LIB:q330yd2md():2013.023
#   Converts YYYY,DOY to Month, Day. Faster than using using ydhmst2Time().
#   Expects a 4-digit YYYY.
def q330yd2md(YYYY, DOY):
    if DOY < 1 or DOY > 366:
        return 0, 0
    if DOY < 32:
        return 1, DOY
    elif DOY < 60:
        return 2, DOY - 31
    if YYYY % 4 != 0:
        Leap = 0
    elif YYYY % 100 != 0 or YYYY % 400 == 0:
        Leap = 1
    else:
        Leap = 0
    # Check for this special day.
    if Leap == 1 and DOY == 60:
        return 2, 29
    # The PROG_FDOM values for Mar-Dec are set up for non-leap years. If it is
    # a leap year and the date is going to be Mar-Dec (it is if we have made
    # it this far), subtract Leap from the day.
    DOY -= Leap
    # We start through PROG_FDOM looking for dates in March.
    Month = 3
    for FDOM in PROG_FDOM[4:]:
        # See if the DOY is less than the first day of next month.
        if DOY <= FDOM:
            # Subtract the DOY for the month that we are in.
            return Month, DOY - PROG_FDOM[Month]
        Month += 1
    return 0, 0
# END: q330yd2md


#####################################################
# BEGIN: q330ydhmst2Epoch(YYYY, DOY, HH, MM, SS, TTT)
# LIB:q330ydhmst2Epoch():2018.310
#   Converts the passed date to seconds since Jan 1, 1970.
#   Same as the epoch section of dt2Time(), but just for Q330 binary time
#   conversion to make things faster.
def q330ydhmst2Epoch(YYYY, DOY, HH, MM, SS, TTT):
    global Y2EPOCH
    Epoch = 0
    try:
        Epoch = Y2EPOCH[YYYY]
    except KeyError:
        for YYY in arange(1970, YYYY):
            if YYY % 4 != 0:
                Epoch += 31536000
            elif YYY % 100 != 0 or YYY % 400 == 0:
                Epoch += 31622400
            else:
                Epoch += 31536000
        Y2EPOCH[YYYY] = Epoch
    Epoch += (DOY - 1) * 86400
    Epoch += HH * 3600
    Epoch += MM * 60
    Epoch += SS
    Epoch += TTT / 10000.0
    return Epoch
# END: q330ydhmst2Epoch


##########################
# BEGIN: progControl(What)
# FUNC:progControl():2018.310
PROGRunning = 0


def progControl(What):
    global PROGRunning
    if What == "go":
        buttonBG(PROGReadBut, "G", NORMAL)
        buttonBG(PROGStopBut, "R", NORMAL)
        PROGRunning = 1
        busyCursor(1)
        return
    elif What == "stop":
        if PROGRunning == 0:
            beep(1)
        else:
            buttonBG(PROGStopBut, "Y", NORMAL)
            # The main form Stop button can control the TPS form activity, but
            # not the other way around.
            if PROGFrm["TPS"] is not None and TPSRunning != 0:
                formTPSControl("stop")
    elif What == "stopped":
        buttonBG(PROGReadBut, "D", NORMAL)
        buttonBG(PROGStopBut, "D", DISABLED)
        if PROGFrm["TPS"] is not None and TPSRunning != 0:
            # Let the form fully stop itself.
            formTPSControl("stop")
    PROGRunning = 0
    busyCursor(0)
    return
# END: progControl


##########################
# BEGIN: progQuitter(Save)
# FUNC:progQuitter():2016.005
def progQuitter(Save):
    if Save is True:
        # Load this with the current main display position and size.
        PROGGeometryVar.set(Root.geometry())
        Ret = savePROGSetups()
        if Ret[0] != 0:
            formMYD(Root, (("(OK)", TOP, "ok"),), "ok", Ret[1], "Oh Oh.",
                    Ret[2])
        formCPREFControl("save")
    setMsg("MF", "CB", "Quitting...")
    Ret = formCloseAll()
    if Ret[0] == 2:
        setMsg("MF", "", "Not quitting...")
        return
    quit()
# END: progQuitter


###########################################################
# BEGIN: readFileLinesRB(Filespec, Strip = False, Bees = 0)
# LIB:readFileLinesRB():2019.239
#   This is the same idea as readFileLines(), but the Filespec is passed and
#   the file is treated as a 'hostile text file' that may be corrupted. This
#   can be used any time, but was developed for reading Reftek LOG files which
#   can be corrupted, or just have a lot of extra junk added by processing
#   programs.
#   This is based on the method used in rt72130ExtractLogData().
#   The return value is (0, [lines]) if things go OK, or a standard error
#   message if not, except the "e" of the exception also will be returned
#   after the passed Filespec, so the caller can construct their own error
#   message if needed.
#   If Bees is not 0 then that many bytes of the file will be returned and
#   converted to lines. If Bees is less than the size of the file the last
#   line will be discarded since it's a good bet that it will be a partial
#   line.
#   Weird little kludge: Setting Bees to -42 tells the function that Filespec
#   contains a bunch of text and that it should be split up into lines and
#   returned just as if the text had come from reading a file.
#   If Filespec has "http:" or "https:" in it then urlopen() will be used.
def readFileLinesRB(Filespec, Strip=False, Bees=0):
    Lines = []
    if Bees != -42:
        try:
            if Filespec.find("http:") == -1 and Filespec.find("https:") == -1:
                # These should be text files, but there's no way to know if
                # they are ASCII or Unicode or garbage, especially if they are
                # corrupted, so open binarially.
                Fp = open(Filespec, "rb")
            else:
                Fp = urlopen(Filespec)
            # This will be trouble if the file is huge, but that should be
            # rare. Bees can be used if the file is known to be yuge. This
            # should result in one long string. This and the "rb" above seems
            # to work on Py2 and 3.
            if Bees == 0:
                Raw = Fp.read().decode("latin-1")
            else:
                Raw = Fp.read(Bees).decode("latin-1")
            Fp.close()
            if len(Raw) == 0:
                return (0, Lines)
        except Exception as e:
            try:
                Fp.close()
            except Exception:
                pass
            return (1, "MW", "%s: Error opening/reading file.\n%s" %
                    (basename(Filespec), e), 3, Filespec, e)
    else:
        Raw = Filespec
        Filespec = "PassedLines"
    # Yes, this is weird. These should be "text" files and in a non-corrupted
    # file there should be either all \n or all \r or the same number of \r\n
    # and \n and \r. Try and split the file up based on these results.
    RN = Raw.count("\r\n")
    N = Raw.count("\n")
    R = Raw.count("\r")
    # Just one line by itself with no delimiter? OK.
    if RN == 0 and N == 0 and R == 0:
        return (0, [Raw])
    # Perfect \n. May be the most popular, so we'll check for it first.
    if N != 0 and R == 0 and RN == 0:
        RawLines = Raw.split("\n")
    # Perfect \r\n file. We checked for RN=0 above.
    elif RN == N and RN == R:
        RawLines = Raw.split("\r\n")
    # Perfect \r.
    elif R != 0 and N == 0 and RN == 0:
        RawLines = Raw.split("\r")
    else:
        # There was something in the file, so make a best guess based on the
        # largest number. It might be complete crap, but what else can we do?
        if N >= RN and N >= R:
            RawLines = Raw.split("\n")
        elif N >= RN and N >= R:
            RawLines = Raw.split("\r\n")
        elif R >= N and R >= RN:
            RawLines = Raw.split("\n")
        # If all of those if's couldn't figure it out.
        else:
            return (1, "RW", "%s: Unrecognized file format." %
                    basename(Filespec), 2, Filespec)
    # If Bees is not 0 then throw away the last line if the file is larger than
    # the number of bytes requested.
    if Bees != 0 and Bees < getsize(Filespec):
        RawLines = RawLines[:-1]
    # Get rid of trailing empty lines. They can sneak in from various places
    # depending on who wrote the file. Do the strip in case there are something
    # like leftover \r's when \n was used for splitting.
    while RawLines[-1].strip() == "":
        RawLines = RawLines[:-1]
        # It must be all the file had in it.
        if len(RawLines) == 0:
            return (0, Lines)
    # If the caller doesn't want anything else then just go through and get rid
    # of any trailing spaces, else get rid of all the spaces.
    if Strip is False:
        for Line in RawLines:
            Lines.append(Line.rstrip())
    else:
        for Line in RawLines:
            Lines.append(Line.strip())
    return (0, Lines)
# END: readFileLinesRB


#########################
# BEGIN: returnReadDir(e)
# FUNC:returnReadDir():2018.310
#   Needed to handle the Return key press in the DDIR field.
def returnReadDir(e):
    setMsg("MF", "", "")
    PROGDataDirVar.set(PROGDataDirVar.get().strip())
    if len(PROGDataDirVar.get()) == 0:
        if PROGSystem == "dar" or PROGSystem == "lin" or PROGSystem == "sun":
            PROGDataDirVar.set(sep)
        elif PROGSystem == "win":
            # For a lack of anyplace else.
            PROGDataDirVar.set("C:\\")
    if PROGDataDirVar.get().endswith(sep) is False:
        PROGDataDirVar.set(PROGDataDirVar.get() + sep)
    loadSourceFilesCmd(MFFiles)
    PROGEnt["DDIR"].icursor(END)
    return
# END: returnReadDir


######################################
# BEGIN: rtnPattern(In, Upper = False)
# LIB:rtnPattern():2006.114
def rtnPattern(In, Upper=False):
    Rtn = ""
    for c in In:
        if c.isdigit():
            Rtn += "0"
        elif c.isupper():
            Rtn += "A"
        elif c.islower():
            Rtn += "a"
        else:
            Rtn += c
    # So the A/a chars will always be A, so the caller knows what to look for.
    if Upper is True:
        return Rtn.upper()
    return Rtn
# END: rtnPattern


####################
# BEGIN: setColors()
# FUNC:setColors():2013.039
#   Uses the value of PROGColorModeRVar and sets the colors in the global DClr
#   dictionary for the non-data plotted stuff.
DClr = {}


def setColors():
    if PROGColorModeRVar.get() == "B":
        # Main form background
        DClr["MF"] = "B"
        # GPS background and dots.
        DClr["GS"] = Clr["K"]
        DClr["GD"] = Clr["W"]
        # Time rule time and rule.
        DClr["TM"] = "W"
        DClr["TR"] = Clr["Y"]
        # Time grid lines.
        DClr["GR"] = Clr["y"]
        # Date/time and ticks. Different things want the colors expressed in
        # different ways.
        DClr["T0"] = "W"
        DClr["TL"] = Clr["W"]
        # Labels.
        DClr["LB"] = "C"
        # Text like the title.
        DClr["TX"] = "W"
        # The plot center line or the upper and lower bounds lines.
        DClr["P0"] = Clr["A"]
        # Line connecting dots.
        DClr["PL"] = Clr["A"]
        # Selector rules
        DClr["SR"] = Clr["Y"]
        # TPS Canvas
        DClr["TC"] = Clr["B"]
        # The main plot time rule created by others (like by TPS clicking).
        DClr["OR"] = Clr["O"]
        # Gap Gap and Overlap.
        DClr["GG"] = Clr["R"]
        DClr["GO"] = Clr["R"]
    elif PROGColorModeRVar.get() == "W":
        DClr["MF"] = "W"
        DClr["GS"] = Clr["W"]
        DClr["GD"] = Clr["B"]
        DClr["TM"] = "B"
        DClr["TR"] = Clr["U"]
        DClr["GR"] = Clr["E"]
        DClr["T0"] = "B"
        DClr["TL"] = Clr["B"]
        DClr["LB"] = "B"
        DClr["TX"] = "B"
        DClr["P0"] = Clr["A"]
        DClr["PL"] = Clr["E"]
        DClr["SR"] = Clr["A"]
        DClr["TC"] = Clr["W"]
        DClr["OR"] = Clr["U"]
        DClr["GG"] = Clr["R"]
        DClr["GO"] = Clr["R"]
    return
# END: setColors


########################################################################
# BEGIN: setMsg(WhichMsg, Colors = "", Message = "", Beep = 0, e = None)
# LIB:setMsg():2018.236
#   Be careful to pass all of the arguments if this is being called by an
#   event.
def setMsg(WhichMsg, Colors="", Message="", Beep=0, e=None):
    # So callers don't have to always be checking for this.
    if WhichMsg is None:
        return
    # This might get called when a window is not, or never has been up so try
    # everything.
    try:
        # This will be the common way to call it.
        if isinstance(WhichMsg, astring):
            LMsgs = [PROGMsg[WhichMsg]]
        elif isinstance(WhichMsg, (tuple, list)):
            LMsgs = []
            for Which in WhichMsg:
                if isinstance(Which, astring):
                    LMsgs.append(PROGMsg[Which])
                else:
                    LMsgs.append(Which)
        else:
            LMsgs = [WhichMsg]
        # Colors may be a standard error message. If it is break it up such
        # that the rest of the function won't know the difference.
        if isinstance(Colors, tuple):
            # Some callers may not pass a Beep value if it is 0.
            try:
                Beep = Colors[3]
            except IndexError:
                Beep = 0
            # The passed Message may not be "". If it is 1 append the [4] part
            # of the standard error message to part [2] with a space, if it is
            # 2 append it with a \n, and if 3 append it with '\n   '. Leave the
            # results in Message.
            if isinstance(Message, anint) is False and len(Message) == 0:
                Message = Colors[2]
            elif Message == 1:
                Message = Colors[2] + " " + Colors[4]
            elif Message == 2:
                Message = Colors[2] + "\n" + Colors[4]
            elif Message == 3:
                Message = Colors[2] + "\n   " + Colors[4]
            Colors = Colors[1]
        for LMsg in LMsgs:
            try:
                LMsg.configure(state=NORMAL)
                LMsg.delete("0.0", END)
                # This might get passed. Just ignore it in this function.
                if Colors.find("X") != -1:
                    Colors = Colors.replace("X", "")
                if len(Colors) == 0:
                    LMsg.configure(bg=Clr["W"], fg=Clr["B"])
                else:
                    LMsg.configure(bg=Clr[Colors[0]], fg=Clr[Colors[1]])
                LMsg.insert(END, Message)
                LMsg.update()
                LMsg.configure(state=DISABLED)
            except Exception:
                pass
        # This may get called from a generated event with no Beep value set.
        if isinstance(Beep, anint) and Beep > 0:
            beep(Beep)
        updateMe(0)
    except (KeyError, TclError):
        pass
    return
########################################################################
# BEGIN: setTxt(WhichTxt, Colors = "", Message = "", Beep = 0, e = None)
# FUNC:setTxt():2018.236
#   Same as above, but for Text()s.


def setTxt(WhichTxt, Colors="", Message="", Beep=0, e=None):
    if WhichTxt is None:
        return
    try:
        if isinstance(WhichTxt, astring):
            LTxt = PROGTxt[WhichTxt]
        else:
            LTxt = WhichTxt
        if isinstance(Colors, tuple):
            Message = Colors[2]
            try:
                Beep = Colors[3]
            except IndexError:
                Beep = 0
            Colors = Colors[1]
        LTxt.delete("0.0", END)
        if Colors.find("X") != -1:
            Colors = Colors.replace("X", "")
        if len(Colors) == 0:
            LTxt.configure(bg=Clr["W"], fg=Clr["B"])
        else:
            LTxt.configure(bg=Clr[Colors[0]], fg=Clr[Colors[1]])
        LTxt.insert(END, Message)
        LTxt.update()
        if isinstance(Beep, anint) and Beep > 0:
            beep(Beep)
        updateMe(0)
    except (KeyError, TclError):
        pass
    return
# END: setMsg


#####################
# BEGIN: showUp(Fram)
# LIB:showUp():2018.236
def showUp(Fram):
    # If anything should go wrong just close the form and let the caller fix it
    # (i.e. redraw it).
    try:
        if PROGFrm[Fram] is not None:
            PROGFrm[Fram].deiconify()
            PROGFrm[Fram].lift()
            PROGFrm[Fram].focus_set()
            return True
    except TclError:
        # This call makes sure that the PROGFrm[] value gets set to None.
        formClose(Fram)
    return False
# END: showUp


#################################################################
# BEGIN: nanProcessCSV(Filespec, FromEpoch, ToEpoch, StaIDFilter)
# LIB:nanProcessCSV():2019.024
#   Reads the .csv files from Nanometrics recorders. It leaves the decoded data
#   in the QPData if the stuff it finds is in the list of channels the user
#   wants.
def nanProcessCSV(Filespec, FromEpoch, ToEpoch, StaIDFilter):
    global QPData
    global QPLogs
    global QPErrors
    global QPInstruments
    Iam = stack()[0][3]
    # If the station name is not in the file name then we don't want this file.
    # WARNING: This might cause a problem by including files whose file name's
    # just happen to have the station name in them, but it's the best we can
    # do.
    if len(StaIDFilter) != 0:
        if Filespec.find(StaIDFilter) == -1:
            return (0, )
    Ret = readFIleLinesRB(Filespec)
    if Ret[0] != 0:
        return (2, "MW", "%s: Error opening file. %s" % (Iam, e), 3, Filespec)
    Lines = Ret[1]
    # If there ends up being no column header line don't read the file.
    if Lines[0].startswith("Time") is False:
        return (1, "RW", "%s: No column header line in file." % Iam, 2,
                Filespec)
    # Check to see if any of the things we extract from csv files are in here,
    # and if the user wants to look at them.
    Line = Lines[0].lower()
    Parts = Line.split(",")
    for Index in arange(0, len(Parts)):
        Parts[Index] = Parts[Index].strip()
    # If this fails forget the rest.
    try:
        TimeIndex = Parts.index("time(utc)")
    except Exception as e:
        stdout.write("%s\n" % e)
        return (1, "RW", "%s: Bad timestamp in CSV file." % Iam, 2, Filespec)
    # Collect the things to look for in the lines of this file in here.
    PartsList = {}
    if "gps receiver status" in Parts:
        if "NGS" in QPUserChannels:
            PartsList["NGS"] = Parts.index("gps receiver status")
    if "timing phase lock" in Parts:
        if "NPL" in QPUserChannels:
            PartsList["NPL"] = Parts.index("timing phase lock")
    if "phase lock loop status" in Parts:
        if "NPL" in QPUserChannels:
            PartsList["NPL"] = Parts.index("phase lock loop status")
    if "timing uncertainty(ns)" in Parts:
        if "NTU" in QPUserChannels:
            PartsList["NTU"] = Parts.index("timing uncertainty(ns)")
    if "time uncertainty(ns)" in Parts:
        if "NTU" in QPUserChannels:
            PartsList["NTU"] = Parts.index("time uncertainty(ns)")
    if "supply voltage(v)" in Parts:
        if "NSV" in QPUserChannels:
            PartsList["NSV"] = Parts.index("supply voltage(v)")
            SupplyVoltsMV = False
    if "supply voltage(mv)" in Parts:
        if "NSV" in QPUserChannels:
            PartsList["NSV"] = Parts.index("supply voltage(mv)")
            SupplyVoltsMV = True
    if "total current(a)" in Parts:
        if "NTC" in QPUserChannels:
            PartsList["NTC"] = Parts.index("total current(a)")
    if "temperature(&deg;c)" in Parts:
        if "NTM" in QPUserChannels:
            PartsList["NTM"] = Parts.index("temperature(&deg;c)")
    if "timing dac count" in Parts:
        if "NTD" in QPUserChannels:
            PartsList["NTD"] = Parts.index("timing dac count")
    if "sensor soh voltage 1(v)" in Parts:
        if "NM1" in QPUserChannels:
            PartsList["NM1"] = Parts.index("sensor soh voltage 1(v)")
    if "sensor soh voltage 2(v)" in Parts:
        if "NM2" in QPUserChannels:
            PartsList["NM2"] = Parts.index("sensor soh voltage 2(v)")
    if "sensor soh voltage 3(v)" in Parts:
        if "NM3" in QPUserChannels:
            PartsList["NM3"] = Parts.index("sensor soh voltage 3(v)")
    if "timing error(ns)" in Parts:
        if "NTE" in QPUserChannels:
            PartsList["NTE"] = Parts.index("timing error(ns)")
    if "time error(ns)" in Parts:
        if "NTE" in QPUserChannels:
            PartsList["NTE"] = Parts.index("time error(ns)")
    if "time quality(&#37;)" in Parts:
        if "NTQ" in QPUserChannels:
            PartsList["NTQ"] = Parts.index("time quality(&#37;)")
    if "controller current(ma)" in Parts:
        if "NCC" in QPUserChannels:
            PartsList["NCC"] = Parts.index("controller current(ma)")
    if "digitizer current(ma)" in Parts:
        if "NDC" in QPUserChannels:
            PartsList["NDC"] = Parts.index("digitizer current(ma)")
    if "nmx bus current(ma)" in Parts:
        if "NNC" in QPUserChannels:
            PartsList["NNC"] = Parts.index("nmx bus current(ma)")
    if "sensor current(ma)" in Parts:
        if "NSC" in QPUserChannels:
            PartsList["NSC"] = Parts.index("sensor current(ma)")
    if "serial port current(ma)" in Parts:
        if "NSP" in QPUserChannels:
            PartsList["NSP"] = Parts.index("serial port current(ma)")
    # Always collect the positions if we come across them so they can be
    # converted into LOG lines and give the GPS display something to plot.
    if "earth location" in Parts:
        PartsList["GPS"] = Parts.index("earth location")
    # These may not be in the CSV file.
        try:
            PartsList["GSU"] = Parts.index("gps satellites used")
        except Exception:
            pass
    if "instrument" in Parts:
        PartsList["I"] = Parts.index("instrument")
    # Nothing of interest.
    if len(PartsList) == 0:
        return (0, )
    # Only record the GPS position every hour. There's just plain too much of
    # everything in these CSV files. Every minute is crazy.
    LastGPSPos = -1
    for Line in Lines[1:]:
        Parts = Line.split(",")
        # Convert the date/time and check to see if we want this line's info.
        try:
            DT = Parts[TimeIndex]
            if rtnPattern(DT) != "0000-00-00 00:00:00":
                return (1, "RW", "%s: Bad timestamp in CSV file." % Iam, 2,
                        Filespec)
            Epoch = dt2Time(22, -1, DT)
        except Exception:
            # This is impossible, so this line will be skipped.
            Epoch = -1.0
        # We'll assume the lines are in time order.
        if Epoch < FromEpoch:
            continue
        # We're done with this file.
        if Epoch > ToEpoch:
            return (0, )
        # Default to "on". Some recorders will have GPS status messages that
        # will change this and some won't.
        GPSStatOn = True
        # Extract the stuff from each line that we are interested in.
        ChanKeys = sorted(PartsList.keys())
        # Sort these so we look for NGS before we look for NPL so GPSStatOn
        # will be set correctly before doing the NPL thing.
        for Chan in ChanKeys:
            if Chan not in QPData:
                QPData[Chan] = []
            Value = Parts[PartsList[Chan]]
            if Chan == "NGS":
                # Locked is good and we don't need to plot anything.
                # Same for Off.
                # "GPS locked"
                if Value.startswith("GPS l"):
                    # These won't be plotted.
                    QPData["NGS"].append([Epoch, -1, Value])
                # Not good - red.
                # "GPS unlocked"
                # %%% put msgs in help.
                elif Value.startswith("GPS u"):
                    QPData["NGS"].append([Epoch, 3, Value])
                # "Off" - Apollo
                elif Value.startswith("Off"):
                    QPData["NGS"].append([Epoch, -1, Value])
                    GPSStatOn = False
                # "Doing fixes" - Apollo
                elif Value.startswith("Doing"):
                    QPData["NGS"].append([Epoch, 1, Value])
                    GPSStatOn = True
                # "No satellites"/"No GPS time" - Apollo
                elif Value.startswith("No"):
                    QPData["NGS"].append([Epoch, 2, Value])
                    GPSStatOn = True
                else:
                    QPData["NGS"].append([Epoch, 4, Value])
            elif Chan == "NPL" and GPSStatOn is True:
                # Finelock is good.
                if Value.startswith("Fine"):
                    QPData["NPL"].append([Epoch, -1, Value])
                elif Value.startswith("Coarse"):
                    QPData["NPL"].append([Epoch, 2, Value])
                elif Value.startswith("No"):
                    QPData["NPL"].append([Epoch, 3, Value])
                # "Free running" or anything else.
                else:
                    QPData["NPL"].append([Epoch, 4, Value])
            elif Chan == "NTU":
                IValue = intt(Value)
                # The popular choices for this are 100,000,000,000 or 100ns,
                # so we'll temper that a bit.
                if IValue > 10000:
                    IValue = 10000
                QPData["NTU"].append([Epoch, IValue])
            elif Chan == "NSV":
                if SupplyVoltsMV is False:
                    QPData[Chan].append([Epoch, round(floatt(Value), 1)])
                else:
                    QPData[Chan].append([Epoch, round(floatt(Value) / 1000.0,
                                                      1)])
            elif Chan == "NTC" or Chan == "NTM" or Chan == "NCC" or \
                    Chan == "NDC" or Chan == "NNC" or Chan == "NSC" or \
                    Chan == "NSP":
                QPData[Chan].append([Epoch, round(floatt(Value), 1)])
            elif Chan == "NM1" or Chan == "NM2" or Chan == "NM3":
                QPData[Chan].append([Epoch, round(floatt(Value), 2)])
            elif Chan == "NTE" or Chan == "NTQ" or Chan == "NTD":
                QPData[Chan].append([Epoch, intt(Value)])
            elif Chan == "GPS" and GPSStatOn is True:
                if Epoch - LastGPSPos > 1800:
                    try:
                        Sats = Parts[PartsList["GSU"]]
                    except Exception:
                        Sats = "0"
                    QPLogs.append("%s GPSPOS: %s SATS: %s" % (DT, Value, Sats))
                    LastGPSPos = Epoch
        # This gets done outside of the channel empire.
        if "I" in PartsList:
            Value = Parts[PartsList["I"]]
            if len(Value) != 0 and Value not in QPInstruments:
                QPInstruments.append(Value)
    return (0, )
# END: nanProcessCSV


###########################
# BEGIN: sP(Count, Phrases)
# LIB:sP():2012.223
def sP(Count, Phrases):
    if Count == 1 or Count == -1:
        return Phrases[0]
    else:
        return Phrases[1]
# END: sP


###############################
# BEGIN: timeRangeFormat(Range)
# LIB:timeRangeFormat():2012.323
def timeRangeFormat(Range):
    if Range > 86400:
        return "%.2f days" % (Range / 86400.0)
    elif Range > 3600:
        return "%.2f hours" % (Range / 3600.0)
    elif Range > 60:
        return "%.2f minutes" % (Range / 60.0)
    else:
        return "%.3f seconds" % Range
# END: timeRangeFormat


######################
# BEGIN: class ToolTip
# LIB:ToolTip():2019.220
#   Add tooltips to objects.
#   Usage: ToolTip(obj, Len, "text")
#   Nice and clever.
#   Starting the text with a ^ signals that the cursor for this item should be
#   set to right_ptr black white when the widget is entered.
class ToolTipBase:
    def __init__(self, button):
        self.button = button
        self.tipwindow = None
        self.id = None
        self.x = self.y = 0
        self.button.bind("<Enter>", self.enter)
        self.button.bind("<Leave>", self.leave)
        self.button.bind("<ButtonPress>", self.leave)
        return

    def enter(self, event=None):
        self.schedule()
        if self.text.startswith("^"):
            self.button.config(cursor="right_ptr black white")
        return

    def leave(self, event=None):
        self.unschedule()
        self.hidetip()
        self.button.config(cursor="")
        return

    def schedule(self):
        self.unschedule()
        self.id = self.button.after(500, self.showtip)
        return

    def unschedule(self):
        id = self.id
        self.id = None
        if id:
            self.button.after_cancel(id)
        return

    def showtip(self):
        if self.text == "^":
            return
        if self.tipwindow:
            return
        # The tip window must be completely clear of the mouse pointer so
        # offset the x and y a little. This is all in a try because I started
        # getting TclErrors to the effect that the window no longer existed by
        # the time the geometry and deiconify functions were reached after
        # adding the 'keep the tooltip off the edge of the display' stuff. I
        # think this additional stuff was adding enough time to the whole
        # process that it would get caught trying to bring up a tip that was no
        # longer needed as the user quickly moved the pointer around the
        # display.
        try:
            self.tipwindow = tw = Toplevel(self.button)
            tw.withdraw()
            tw.wm_overrideredirect(1)
            self.showcontents()
            x = self.button.winfo_pointerx()
            y = self.button.winfo_pointery()
            # After much trial and error...keep the tooltip away from the right
            # edge of the screen and the bottom of the screen.
            tw.update()
            if x + tw.winfo_reqwidth() + 5 > PROGScreenWidthNow:
                x -= (tw.winfo_reqwidth() + 5)
            else:
                x += 5
            if y + tw.winfo_reqheight() + 5 > PROGScreenHeightNow:
                y -= (tw.winfo_reqheight() + 5)
            else:
                y += 5
            tw.wm_geometry("+%d+%d" % (x, y))
            tw.deiconify()
            # FINISHME - May need to be removed if the tooltip 'flashes' when
            # it shows up, or I may have recently added this, because without
            # it the tooltips don't work in py3.
            tw.lift()
        except TclError:
            self.hidetip()
        return

    def showcontents(self, Len, text, BF):
        # Break up the incoming message about every Len characters.
        if Len > 0 and len(text) > Len:
            Mssg = ""
            Count = 0
            for c in text:
                if Count == 0 and c == " ":
                    continue
                if Count > Len and c == " ":
                    Mssg += "\n"
                    Count = 0
                    continue
                if c == "\n":
                    Mssg += c
                    Count = 0
                    continue
                Count += 1
                Mssg += c
            text = Mssg
        # Override this in derived class.
        Lab = Label(self.tipwindow, text=text, justify=LEFT,
                    bg=Clr[BF[0]], fg=Clr[BF[1]], bd=1, relief=SOLID,
                    padx=3, pady=3)
        Lab.pack()
        return

    def hidetip(self):
        # If it is already gone then just go back.
        try:
            tw = self.tipwindow
            self.tipwindow = None
            if tw:
                tw.destroy()
        except TclError:
            pass
        return


class ToolTip(ToolTipBase):
    def __init__(self, button, Len, text, BF="YB"):
        # If the caller doesn't pass any text then don't get this started.
        if len(text) == 0:
            return
        ToolTipBase.__init__(self, button)
        self.Len = Len
        self.text = text
        self.BF = BF
        return

    def showcontents(self):
        if self.text.startswith("^") is False:
            ToolTipBase.showcontents(self, self.Len, self.text, self.BF)
        else:
            ToolTipBase.showcontents(self, self.Len, self.text[1:], self.BF)
        return
# END: ToolTip


########################
# BEGIN: updateMe(Which)
# FUNC:updateMe():2006.112
def updateMe(Which):
    if Which == 0:
        Root.update_idletasks()
        Root.update()
    return
# END: updateMe


#####################################################
# BEGIN: walkDirs(Dir, IncHidden, End, Walk, IncDirs)
# LIB:walkDirs():2016.223
#   Walks the passed Dir and returns a list of every file in Dir with full
#   path.
#   IncHidden is True/False.
#   If End is not "" then only files ending with End will be passed back.
#   If Walk is 0 then files only in Dir will be returned and no
#   sub-directories will be walked.
#   If IncDirs is True then sub-directory entries will be be included with the
#   files.
def walkDirs(Dir, IncHidden, End, Walk, IncDirs):
    if (Dir.startswith(".") or Dir.startswith("_")) and IncHidden is False:
        return (1, "RW", "Directory %s is hidden or special." % Dir, 2, "")
    if exists(Dir) is False:
        return (1, "RW", "Directory %s does not exist." % Dir, 2, "")
    if isdir(Dir) is False:
        return (1, "RW", "%s is not a directory." % Dir, 2, "")
    Files = []
    if Walk == 0:
        if Dir.endswith(sep) is False:
            Dir += sep
        for Name in listdir(Dir):
            # No hidden files or directories, please.
            if (Name.startswith(".") or Name.startswith("_")) and \
                    IncHidden is False:
                continue
            Fullpath = Dir + Name
            if isdir(Fullpath):
                # The caller may not want to walk into the directories, but
                # may still want a list of them in the current directory.
                if IncDirs is True:
                    Files.append(Fullpath + sep)
                continue
            if Fullpath.endswith(End):
                Files.append(Fullpath)
    elif Walk != 0:
        # Collect the directories we come across them in here.
        Dirs = []
        Dirs.append(Dir)
        while len(Dirs) > 0:
            Dir = Dirs.pop()
            if Dir.endswith(sep) is False:
                Dir += sep
            if IncDirs is True:
                if Dir not in Files:
                    Files.append(Dir)
            for Name in listdir(Dir):
                # No hidden files or directories, please.
                if (Name.startswith(".") or Name.startswith("_")) and \
                        IncHidden is False:
                    continue
                Fullpath = Dir + Name
                # Save this so we can pop it and do a listdir() on it.
                if isdir(Fullpath):
                    Dirs.append(Fullpath)
                    continue
                if Fullpath.endswith(End):
                    Files.append(Fullpath)
    Files.sort()
    return (0, Files)
#####################################################
# BEGIN: walkDirs2(Dir, IncHidden, Middle, End, Walk)
# FUNC:walkDirs2():2015.149
#   Walks the passed Dir and returns a list of every file in Dir with full
#   path.
#   If Middle is not "" then only full paths containing Middle will be
#   kept.
#   If End is not "" then only files ending with End will be passed
#   back.
#   If Walk is False then files only in Dir will be returned and no
#   sub-directories will be walked.


def walkDirs2(Dir, IncHidden, Middle, End, Walk):
    if (Dir.startswith(".") or Dir.startswith("_")) and IncHidden is False:
        return (1, "RW", "Directory %s is hidden or special." % Dir, 2, "")
    if exists(Dir) is False:
        return (1, "RW", "Directory %s does not exist." % Dir, 2, "")
    if isdir(Dir) is False:
        return (1, "RW", "%s is not a directory." % Dir, 2, "")
    Files = []
    if Walk == 0:
        if Dir.endswith(sep) is False:
            Dir += sep
        Names = listdir(Dir)
        for Name in Names:
            # No hidden files or directories, please.
            if (Name.startswith(".") or Name.startswith("_")) and \
                    IncHidden is False:
                continue
            Fullpath = Dir + Name
            if isdir(Fullpath):
                continue
            # If Middle and/or End are "" this will still work.
            if Fullpath.find(Middle) != -1 and Fullpath.endswith(End):
                Files.append(Fullpath)
    elif Walk == 1:
        # Collect the directories we come across in here.
        Dirs = []
        Dirs.append(Dir)
        while len(Dirs) > 0:
            Dir = Dirs.pop()
            if Dir.endswith(sep) is False:
                Dir += sep
            for Name in listdir(Dir):
                # No hidden files or directories, please.
                if (Name.startswith(".") or Name.startswith("_")) and \
                        IncHidden is False:
                    continue
                Fullpath = Dir + Name
                # Save this so we can pop it and do a listdir() on it.
                if isdir(Fullpath):
                    Dirs.append(Fullpath)
                    continue
                if Fullpath.find(Middle) == -1:
                    continue
                if Fullpath.endswith(End):
                    Files.append(Fullpath)
    Files.sort()
    return (0, Files)
# END: walkDirs


# ============================================
# BEGIN: =============== SETUP ===============
# ============================================


###############################
# BEGIN: changeOpMode(e = None)
# FUNC:changeOpMode(e = None):2014.233
def changeOpMode(e=None):
    loadSourceFilesCmd(MFFiles)
    Root.title(makeTitle())
    return
# END: changeOpMode


####################
# BEGIN: makeTitle()
# FUNC:makeTitle():2018.310
def makeTitle():
    Title2 = ""
    if len(PROGHostname) != 0:
        Title2 += "h:%s" % PROGHostname
    if len(PROGUserIDLC) != 0:
        if len(Title2) == 0:
            Title2 = "ID:%s" % PROGUserIDLC
        else:
            Title2 += " ID:%s" % PROGUserIDLC
    if len(Title2) == 0:
        Title2 = "m:%s" % OPTOpModeRVar.get()
    else:
        Title2 += " m:%s" % OPTOpModeRVar.get()
    return "%s - %s (%s)" % (PROG_NAME, PROG_VERSION, Title2)
# END: makeTitle


######################
# BEGIN: menuMake(Win)
# FUNC:makeMenu():2016.084
OPTOpModeRVar = StringVar()
OPTOpModeRVar.set("Normal")
OPTDateFormatRVar = StringVar()
OPTDateFormatRVar.set("YYYY-MM-DD")
OPTBitWeightRVar = StringVar()
OPTBitWeightRVar.set("none")
OPTMPVoltageRangeRVar = StringVar()
OPTMPVoltageRangeRVar.set("Regular")
OPTSupressUBMCVar = IntVar()
OPTDebugCVar = IntVar()
PROGSetups += ["OPTOpModeRVar", "OPTDateFormatRVar", "OPTMPVoltageRangeRVar",
               "OPTSupressUBMCVar", "OPTDebugCVar"]
# Don't remember this so it always goes back to off.
OPTWhackAntSpikesCVar = IntVar()
OPTSortAfterReadCVar = IntVar()
# Always reset this.
OPTListChannelsCVar = IntVar()
OPTListChannelsCVar.set(0)
MENUMenus = {}
MENUFont = PROGOrigPropFont


def menuMake(Win):
    global MENUMenus
    MENUMenus.clear()
    Top = Menu(Win, font=MENUFont)
    Win.configure(menu=Top)
    Fi = Menu(Top, font=MENUFont, tearoff=0)
    MENUMenus["File"] = Fi
    Top.add_cascade(label="File", menu=Fi)
    Fi.add_command(label="Delete Setups File", command=deletePROGSetups)
    Fi.add_separator()
    Fi.add_command(label="Quit %s" % PROG_NAME,
                   command=Command(progQuitter, True))
    Co = Menu(Top, font=MENUFont, tearoff=0)
    MENUMenus["Commands"] = Co
    Top.add_cascade(label="Commands", menu=Co)
    Co.add_command(label="LOG Search", command=formLOGSRCH)
    Co.add_separator()
    Co.add_command(label="GPS Data Plotter", command=formGPS)
    Co.add_command(label="TPS Plot", command=formTPSOpen)
    Co.add_separator()
    Co.add_command(label="Scan For Channel IDs", command=formSFCI)
    Co.add_command(label="Channel Preferences", command=formCPREF)
    Op = Menu(Top, font=MENUFont, tearoff=0)
    MENUMenus["Options"] = Op
    Top.add_cascade(label="Options", menu=Op)
    Op.add_radiobutton(label="Op Mode: Normal", command=changeOpMode,
                       variable=OPTOpModeRVar, value="Normal")
    Op.add_radiobutton(label="Op Mode: AntRT, db/YEAR/DOY/data",
                       command=changeOpMode, variable=OPTOpModeRVar,
                       value="AntRT")
    Op.add_radiobutton(label="Op Mode: Inside B44 Memory Stick",
                       command=changeOpMode, variable=OPTOpModeRVar,
                       value="B44Stick")
    Op.add_radiobutton(label="Op Mode: Inside Nanometrics SD Card",
                       command=changeOpMode, variable=OPTOpModeRVar,
                       value="NanoCard")
    # Removed until we do more testing.
    # Op.add_radiobutton(label = "Op Mode: SeisComp3,"
    #                            "YEAR/NetCode/StaID/Chan/data",
    #                    command = changeOpMode, variable = OPTOpModeRVar,
    #                    value = "SC3")
    Op.add_separator()
    Op.add_radiobutton(label="Show Seismic Data In Counts",
                       variable=OPTBitWeightRVar, value="none")
    Op.add_radiobutton(label="Use Q330 Low Gain",
                       variable=OPTBitWeightRVar, value="low", state=DISABLED)
    Op.add_radiobutton(label="Use Q330 High Gain",
                       variable=OPTBitWeightRVar, value="high", state=DISABLED)
    Op.add_separator()
    Message = "MP Coloring: %s (Regular)" % list2Str(
        MPVoltageRanges["Regular"])
    Op.add_radiobutton(label=Message, variable=OPTMPVoltageRangeRVar,
                       value="Regular")
    Message = "MP Coloring: %s (Trillium)" % \
        list2Str(MPVoltageRanges["Trillium"])
    Op.add_radiobutton(label=Message, variable=OPTMPVoltageRangeRVar,
                       value="Trillium")
    Op.add_separator()
    Op.add_radiobutton(label="Show YYYY-MM-DD Dates",
                       command=Command(changeDateFormat, "MF"),
                       variable=OPTDateFormatRVar, value="YYYY-MM-DD")
    Op.add_radiobutton(label="Show YYYYMMMDD Dates",
                       command=Command(changeDateFormat, "MF"),
                       variable=OPTDateFormatRVar, value="YYYYMMMDD")
    Op.add_radiobutton(label="Show YYYY:DOY Dates",
                       command=Command(changeDateFormat, "MF"),
                       variable=OPTDateFormatRVar, value="YYYY:DOY")
    Op.add_separator()
    Op.add_checkbutton(label="Calculate File Sizes",
                       variable=OPTCalcFileSizesCVar)
    Op.add_checkbutton(label="Sort Data By Time After Read",
                       variable=OPTSortAfterReadCVar)
    Op.add_checkbutton(label="Whack Antelope Spikes",
                       variable=OPTWhackAntSpikesCVar,
                       command=Command(msgCenter, "spikes"))
    Op.add_checkbutton(label="Supress Unhandled Blockette Messages",
                       variable=OPTSupressUBMCVar)
    Op.add_separator()
    Op.add_command(label="Set Font Sizes",
                   command=Command(formFONTSZ, Root, 0))
    Op.add_separator()
    Op.add_checkbutton(label="List Each File's Channels",
                       variable=OPTListChannelsCVar)
    Op.add_checkbutton(label="Debug", variable=OPTDebugCVar)
    Fo = Menu(Top, font=MENUFont, tearoff=0, postcommand=menuMakeForms)
    MENUMenus["Forms"] = Fo
    Top.add_cascade(label="Forms", menu=Fo)
    Hp = Menu(Top, font=MENUFont, tearoff=0)
    MENUMenus["Help"] = Hp
    Top.add_cascade(label="Help", menu=Hp)
    Hp.add_command(label="Help", command=Command(formHELP, Root))
    Hp.add_command(label="Known Channels", command=listKnownChans)
    Hp.add_command(label="Calendar", command=Command(formCAL, Root))
    Hp.add_command(label="Check For Updates", command=checkForUpdates)
    Hp.add_command(label="About", command=formABOUT)
    # TESTING
    #    Hp.add_command(label = "Test", command = testCmd)
    return
###############################################
# BEGIN: menuMakeSet(MenuText, ItemText, State)
# FUNC:menuMakeSet():2018.235


def menuMakeSet(MenuText, ItemText, State):
    try:
        Menu = MENUMenus[MenuText]
        Indexes = Menu.index("end") + 1
        for Item in arange(0, Indexes):
            Type = Menu.type(Item)
            if Type in ("tearoff", "separator"):
                continue
            if Menu.entrycget(Item, "label") == ItemText:
                Menu.entryconfigure(Item, state=State)
                break
    # Just in case. This should never go off if I've done my job.
    except Exception:
        stdout.write("menuMakeSet: %s, %s\n\a" % (MenuText, ItemText))
    return
################################
# BEGIN: menuMakeForms(e = None)
# FUNC:menuMakeForms():2018.247qpeek


def menuMakeForms(e=None):
    FMenu = MENUMenus["Forms"]
    FMenu.delete(0, END)
    # Build the list of forms so they can be sorted alphabetically.
    Forms = []
    for Frmm in list(PROGFrm.keys()):
        try:
            if PROGFrm[Frmm] is not None:
                Forms.append([PROGFrm[Frmm].title(), Frmm])
        except TclError:
            stdout.write("%s\n" % Frmm)
            formClose(Frmm)
    if len(Forms) == 0:
        FMenu.add_command(label="No Forms Are Open", state=DISABLED)
    else:
        Forms.sort()
        for Title, Frmm in Forms:
            FMenu.add_command(label=Title, command=Command(showUp, Frmm))
    # FOR QPEEK.
    #        FMenu.add_separator()
    #        FMenu.add_command(label = "Close All Forms",
    #                          command = formCloseAll)
    return
# END: menuMake


##################
# BEGIN: testCmd()
# FUNC:testCmd():2016.236
def testCmd():
    stdout.write("%s\n" % basename("http://1234/e.py"))
    return
# END: testCmd


###############
# BEGIN: main()
# FUNC:main():2019.024
PROGDataDirVar = StringVar()
PROGGeometryVar = StringVar()
FromDateVar = StringVar()
ToDateVar = StringVar()
MFLbxFindVar = StringVar()
OPTBMSDataDirRVar = StringVar()
OPTBMSDataDirRVar.set("sdata")
PROGColorModeRVar = StringVar()
PROGColorModeRVar.set("B")
OPTPlotTPSCVar = IntVar()
OPTPlotTPSChansVar = StringVar()
OPTMagnifyLRVar = StringVar()
OPTMagnifyLRVar.set("1")
OPTMagnifyHRVar = StringVar()
OPTMagnifyHRVar.set("1")
MFPSFilespecVar = StringVar()
OPTGapsDetectCVar = IntVar()
PROGGapsLengthVar = StringVar()
PROGStaIDFilterVar = StringVar()
PROGSetups += ["PROGDataDirVar", "PROGGeometryVar", "FromDateVar",
               "ToDateVar", "MFLbxFindVar", "OPTBMSDataDirRVar",
               "PROGColorModeRVar", "OPTMagnifyLRVar", "OPTMagnifyHRVar",
               "OPTPlotTPSCVar", "OPTPlotTPSChansVar", "MFPSFilespecVar",
               "OPTGapsDetectCVar", "PROGGapsLengthVar", "PROGStaIDFilterVar"]
# Antelope can be set to stick in huge (16384**2) values into data files when
# data is missing, instead of just leaving a gap. The Options menu item Whack
# Antelope Spikes can be used to not collect those values and leave a gap.
AntSpikesValue = 16384**2
PROGClipboardVar = StringVar()
MPVoltageRanges = \
    {
        "Regular": [0.5, 2.0, 4.0, 7.0],
        "Trillium": [0.5, 1.8, 2.4, 3.5]
    }
# First value is the color to use when below the first value in the Lists in
# MPVoltageRanges. Second color when between the first and second voltages,
# and so on. Last color is used when above last voltage.
MPColorPallets = \
    {
        "B": ["C", "G", "Y", "R", "M"], \
        # FINISHME - change these
        "W": ["B", "B", "B", "B", "B"]
    }
# Information about each channel we know how to decode and plot.
# FINISHME - someday QPChans might be editable as a preferences-like thing, but
# not until the program is older to keep a little control over what is going
# on. Besides, I'm not sure how you could specify the _DECODE part without
# having to write the code to do the math or whatever.
QPCHANS_PLOT = 0      # 0=Don't plot, 1=Plottable, 2=collect data, but don't
# plot.
QPCHANS_TYPE = 1      # "S"=seismic data, "E"=engineering data
# "E" data won't be plottable by TPS, for example, even
# if an E-channel ends up in the TPS channel list.
# QPCHANS_DECODE: How to decode.
# WARNING: Update Known Channels stuff if these change.
# 1 = 1 for 1, 1 count = 1%, 1 byte, etc.
# 2 = 1 count = 150mV
# 3 = 1 count = .1%
# 4 = 1 for 1, but kept >= 0
# 5 = 1 count = .1%, but kept >= 0
# 6 = 1 count = .001 counts (divide by 1000)
# 7 = 1 count = .0000001 counts (divide by 1000000) (microdegrees, micrometers)
# 99 = special case based on the type of DAS for VEI (See QPChansDecodeV)
# 999 = special case based on the type of DAS for mass positions (See
#       QPChansDecodeMP)
QPCHANS_DECODE = 2
QPCHANS_DESC = 3      # A description for the list of known channels function.
QPCHANS_LABEL = 4     # Plot canvas label (may have 3-char code modified when
# plotted to include location code). Will also be the
# tooltip for channel scanner buttons
QPCHANS_UNITS = 5     # Value units: volts, micro-seconds, etc.
QPCHANS_HEIGHT = 6    # Number of PROGPropFontHeight lines height the plot is.
# Gets doubled id Options|2x Plot Heights is selected.
# QPCHANS_PLOTTER: Plotting type to use
# mln  = multi-line normal, one color, line and maybe dots
# mlmp = multi-line mass position, multi-color, always line and dots until
#         <=MFPWidth*3 points, then colored line
# mll  = multi-line line, one color, line only
# mlls = multi-line line seismic, one color, line only, can apply bit weights
# mp   = mass position, multi-color, single line
# strz = the code must create data values to get the desired color dots or
#        lines (if there are too many dots). -1=not plotted, 0=green, 1=cyan,
#        2=yellow, 3=red, 4=magenta. Dots get larger green to magenta.
# tri  = single line plot. 0=red, 1=yellow, 2=green (e.g. off/unlocked/locked).
#        Dots get larger red to green.
# tri2 = single line plot. 0=not plotted, 1=red, 2=magenta (e.g. ok/bad/worse)
#        Dots get larger red to magenta.
QPCHANS_PLOTTER = 7
QPCHANS_SHOWSR = 8    # 1=show sample rate on plot (not used for mlmp plots)
QPCHANS_BBG = 9       # Black background color to plot for one color plots
QPCHANS_WBG = 10      # White background color to plot
QPCHANS_LOGBF = 11    # Clicking on a point LOG line bg/fg colors (if the
# channel does not come from LOG information then
# don't need, or can be "" if using _FIX.
QPCHANS_FIX = 12      # 0=display as is. 1=.1 for floats, 2=.01, etc. for
# Min/Max values. For the mln/mlls-type plots. Defaults
# to 0.
# Generally the first group are all from miniseed files. Was just the Q330
# section, but others are starting to use some of the same codes.
QPChans = \
    {"LCE": [1, "E", 1, "Clock phase error",
             "LCE-Phase Error", "us", 3, "mln", 0, "Y", "B"],
     "VCO": [1, "E", 1,
             "Voltage controlled oscillator contol voltage value",
             "VCO-VCO Control", "", 2, "mln", 0, "W", "B"],
     "LCQ": [1, "E", 1, "Clock quality",
             "LCQ-Clock Qual", "%", 3, "mln", 0, "Y", "B"],
     "VEA": [1, "E", 4, "Antenna amperage",
             "VEA-Ant Amps", "mA", 2, "mln", 0, "R", "B"],
     "VEC": [1, "E", 1, "Total system amperage",
             "VEC-Sys Amps", "mA", 3, "mln", 0, "R", "B"],
     "VEP": [1, "E", 2, "System input supply voltage",
             "VEP-Input Volts", "V", 3, "mln", 0, "G", "B", "", 2],
     "VKI": [1, "E", 1, "System temperature",
             "VKI-Sys Temp", "C", 3, "mln", 0, "C", "B"],
     "VPB": [1, "E", 3, "Percentage of memory buffer used",
             "VPB-Buffer Used", "%", 3, "mln", 0, "W", "B"],
     "VFP": [1, "E", 3, "Percentage of memory buffer used",
             "VFP-Buffer Used", "%", 3, "mln", 0, "W", "B"],
     "VMZ": [1, "E", 3, "Sensor mass position",
             "VMZ-MassPos Z", "V", 2, "mlmp", 0, "", "", "", 1],
     "VMN": [1, "E", 999, "Sensor mass position",
             "VMN-MassPos NS", "V", 2, "mlmp", 0, "", "", "", 1],
     "VME": [1, "E", 999, "Sensor mass position",
             "VME-MassPos EW", "V", 2, "mlmp", 0, "", "", "", 1],
     "VMU": [1, "E", 999, "Sensor mass position",
             "VMU-MassPos U", "V", 2, "mlmp", 0, "", "", "", 1],
     "VMV": [1, "E", 999, "Sensor mass position",
             "VMV-MassPos V", "V", 2, "mlmp", 0, "", "", "", 1],
     "VMW": [1, "E", 999, "Sensor mass position",
             "VMW-MassPos W", "V", 2, "mlmp", 0, "", "", "", 1],
     "VM0": [1, "E", 999, "Sensor mass position",
             "VM0-MassPos", "V", 2, "mlmp", 0, "", "", "", 1],
     "VM1": [1, "E", 999, "Sensor mass position",
             "VM1-MassPos", "V", 2, "mlmp", 0, "", "", "", 1],
     "VM2": [1, "E", 999, "Sensor mass position",
             "VM2-MassPos", "V", 2, "mlmp", 0, "", "", "", 1],
     "VM3": [1, "E", 999, "Sensor mass position",
             "VM3-MassPos", "V", 2, "mlmp", 0, "", "", "", 1],
     "VM4": [1, "E", 999, "Sensor mass position",
             "VM4-MassPos", "V", 2, "mlmp", 0, "", "", "", 1],
     "VM5": [1, "E", 999, "Sensor mass position",
             "VM5-MassPos", "V", 2, "mlmp", 0, "", "", "", 1],
     "VM6": [1, "E", 999, "Sensor mass position",
             "VM6-MassPos", "V", 2, "mlmp", 0, "", "", "", 1],
     "LHZ": [1, "S", 1, "Seismic data",
             "LHZ-V", "", 4, "mlls", 1, "G", "B"],
     "LHN": [1, "S", 1, "Seismic data",
             "LHN-NS", "", 4, "mlls", 1, "G", "B"],
     "LHE": [1, "S", 1, "Seismic data",
             "LHE-EW", "", 4, "mlls", 1, "G", "B"],
     "BHZ": [1, "S", 1, "Seismic data",
             "BHZ-V", "", 4, "mlls", 1, "G", "B"],
     "BHN": [1, "S", 1, "Seismic data",
             "BHN-NS", "", 4, "mlls", 1, "G", "B"],
     "BHE": [1, "S", 1, "Seismic data",
             "BHE-EW", "", 4, "mlls", 1, "G", "B"],
     "EHZ": [1, "S", 1, "Seismic data",
             "EHZ-V", "", 4, "mlls", 1, "G", "B"],
     "EHN": [1, "S", 1, "Seismic data",
             "EHN-NS", "", 4, "mlls", 1, "G", "B"],
     "EHE": [1, "S", 1, "Seismic data",
             "EHE-EW", "", 4, "mlls", 1, "G", "B"],
     "ELZ": [1, "S", 1, "Seismic data",
             "ELZ-V", "", 4, "mlls", 1, "G", "B"],
     "EL1": [1, "S", 1, "Seismic data",
             "EL1-NS", "", 4, "mlls", 1, "G", "B"],
     "EL2": [1, "S", 1, "Seismic data",
             "EL2-EW", "", 4, "mlls", 1, "G", "B"],
     "HHZ": [1, "S", 1, "Seismic data",
             "HHZ-V", "", 4, "mlls", 1, "G", "B"],
     "HHN": [1, "S", 1, "Seismic data",
             "HHN-NS", "", 4, "mlls", 1, "G", "B"],
     "HHE": [1, "S", 1, "Seismic data",
             "HHE-EW", "", 4, "mlls", 1, "G", "B"],
     "UHZ": [1, "S", 1, "Seismic data",
             "UHZ-V", "", 4, "mlls", 1, "G", "B"],
     "UHN": [1, "S", 1, "Seismic data",
             "UHN-NS", "", 4, "mlls", 1, "G", "B"],
     "UHE": [1, "S", 1, "Seismic data",
             "UHE-EW", "", 4, "mlls", 1, "G", "B"],
     "VHZ": [1, "S", 1, "Seismic data",
             "VHZ-V", "", 4, "mlls", 1, "G", "B"],
     "VHN": [1, "S", 1, "Seismic data",
             "VHN-NS", "", 4, "mlls", 1, "G", "B"],
     "VHE": [1, "S", 1, "Seismic data",
             "VHE-EW", "", 4, "mlls", 1, "G", "B"],

     # Started showing up with Nanometrics Meridian miniseed files.
     "CHZ": [1, "S", 1, "Seismic data",
             "CHZ-V", "", 4, "mlls", 1, "G", "B"],
     "CH1": [1, "S", 1, "Seismic data",
             "CH1-NS", "", 4, "mlls", 1, "G", "B"],
     "CH2": [1, "S", 1, "Seismic data",
             "CH2-EW", "", 4, "mlls", 1, "G", "B"],

     # Nanometrics Meridian/Centaur SOH miniseed.
     "GAN": [1, "E", 1, "GPS antenna status",
             "GAN-Ant Status", "", 1, "tri2", 0, "", ""],
     # The GEL GLA GLO get turned into LOG entries, instead of being plotted.
     "GEL": [2, "E", 7, "GPS Elevation",
             "GLA-Elev", "m", 2, "mln", 0, "Y", "B"],
     "GLA": [2, "E", 7, "GPS Latitude",
             "GLA-Lat", "d", 2, "mln", 0, "Y", "B"],
     "GLO": [2, "E", 7, "GPS Longitude",
             "GLA-Long", "d", 2, "mln", 0, "Y", "B"],
     "GNS": [1, "E", 1, "GPS number of sats used",
             "GNS-Sats Used", "", 3, "mln", 0, "Y", "B"],
     "GPL": [1, "E", 1, "GPS phase lock status",
             "GPL-Phase Lock", "", 1, "strz", 0, "", ""],
     "GST": [1, "E", 1, "GPS status indicator",
             "GST-GPS Off/Un/Lk", "", 1, "tri", 0, "", ""],
     "VDT": [1, "E", 6, "Digitizer system temperature",
             "VDT-Temp", "C", 3, "mln", 0, "C", "B"],
     "VEI": [1, "E", 99, "Input system voltage",
             "VEI-Supply V", "V", 6, "mln", 0, "G", "B"],

     # These showed up "somewhere", but I can't remember where.
     # A GLISN station? Maybe one of the Danish ones?
     "LCC": [1, "E", 1, "Clock (GPS) quality",
             "LCC-Clk Qual", "", 3, "mln", 0, "Y", "B"],
     "LCL": [1, "E", 1, "Time since GPS lost lock",
             "LCL-Since Last Lk", "", 3, "mln", 0, "Y", "B"],
     "LPL": [1, "E", 1, "Clock phase lock loop status",
             "LPL-PLL Status", "", 3, "mln", 0, "Y", "B"],

     # Started with GLISN stuff, because the sensors are not really always
     # North and East (boreholes).
     "LH1": [1, "S", 1, "Borehole seismic data",
             "LH1-NS", "", 4, "mlls", 1, "G", "B"],
     "LH2": [1, "S", 1, "Borehole seismic data",
             "LH2-EW", "", 4, "mlls", 1, "G", "B"],
     "BH1": [1, "S", 1, "Borehole seismic data",
             "BH1-NS", "", 4, "mlls", 1, "G", "B"],
     "BH2": [1, "S", 1, "Borehole seismic data",
             "BH2-EW", "", 4, "mlls", 1, "G", "B"],
     "UH1": [1, "S", 1, "Borehole seismic data",
             "UH1-NS", "", 4, "mlls", 1, "G", "B"],
     "UH2": [1, "S", 1, "Borehole seismic data",
             "UH2-EW", "", 4, "mlls", 1, "G", "B"],
     "EH1": [1, "S", 1, "Borehole seismic data",
             "EH1-NS", "", 4, "mlls", 1, "G", "B"],
     "EH2": [1, "S", 1, "Borehole seismic data",
             "EH2-EW", "", 4, "mlls", 1, "G", "B"],
     "HH1": [1, "S", 1, "Borehole seismic data",
             "HH1-NS", "", 4, "mlls", 1, "G", "B"],
     "HH2": [1, "S", 1, "Borehole seismic data",
             "HH2-EW", "", 4, "mlls", 1, "G", "B"],
     "VH1": [1, "S", 1, "Borehole seismic data",
             "VH1-NS", "", 4, "mlls", 1, "G", "B"],
     "VH2": [1, "S", 1, "Borehole seismic data",
             "VH2-EW", "", 4, "mlls", 1, "G", "B"],

     # Used on the bench at PASSCAL.
     "BLZ": [1, "S", 1, "Channel 4-6 bench data",
             "BLZ-V", "", 4, "mlls", 1, "G", "B"],
     "BLN": [1, "S", 1, "Channel 4-6 bench data",
             "BLN-NS", "", 4, "mlls", 1, "G", "B"],
     "BLE": [1, "S", 1, "Channel 4-6 bench data",
             "BLE-EW", "", 4, "mlls", 1, "G", "B"],

     # TitanSMA stuff.
     "BNZ": [1, "S", 1, "Nanometrics seismic data",
             "BNZ-V", "", 4, "mlls", 1, "G", "B"],
     "BNN": [1, "S", 1, "Nanometrics seismic data",
             "BNN-NS", "", 4, "mlls", 1, "G", "B"],
     "BNE": [1, "S", 1, "Nanometrics seismic data",
             "BNE-EW", "", 4, "mlls", 1, "G", "B"],
     "HNZ": [1, "S", 1, "Nanometrics seismic data",
             "HNZ-V", "", 4, "mlls", 1, "G", "B"],
     "HNN": [1, "S", 1, "Nanometrics seismic data",
             "HNN-NS", "", 4, "mlls", 1, "G", "B"],
     "HNE": [1, "S", 1, "Nanometrics seismic data",
             "HNE-EW", "", 4, "mlls", 1, "G", "B"],

     # Filled in from the Nanometrics .csv SOH file info.
     "NSV": [1, "E", 1, "(CSV) Nanometrics input supply voltage",
             "NSV-Supply V", "V", 3, "mln", 0, "G", "B", "GB", 2],
     "NTC": [1, "E", 1, "(CSV) Nanometrics total system current",
             "NTC-Total Curr", "mA", 3, "mln", 0, "R", "B", "RW"],
     "NTM": [1, "E", 1, "(CSV) Nanometrics system temperature",
             "NTM-Temp", "C", 3, "mln", 0, "C", "B", "CB"],
     "NCC": [1, "E", 1, "(CSV) Nanometrics controller current",
             "NCC-Ctrlr Curr", "mA", 3, "mln", 0, "R", "B", "RW"],
     "NDC": [1, "E", 1, "(CSV) Nanometrics digitizer current",
             "NDC-Digtzr Curr", "mA", 3, "mln", 0, "R", "B", "RW"],
     "NNC": [1, "E", 1, "(CSV) Nanometrics NMX bus current",
             "NNC-NMXB Curr", "mA", 3, "mln", 0, "R", "B", "RW"],
     "NSC": [1, "E", 1, "(CSV) Nanometrics sensor current",
             "NSC-Sensor Curr", "mA", 3, "mln", 0, "R", "B", "RW"],
     "NSP": [1, "E", 1, "(CSV) Nanometrics serial port current",
             "NSP-SerPort Curr", "mA", 3, "mln", 0, "R", "B", "RW"],
     "NTU": [1, "E", 1, "(CSV) Nanometrics timing uncertanty",
             "NTU-Time Uncert", "ns", 3, "mln", 0, "Y", "B", "YB"],
     "NTE": [1, "E", 1, "(CSV) Nanometrics timing error",
             "NTE-Time Error", "ns", 3, "mln", 0, "Y", "B", "YB"],
     "NTQ": [1, "E", 1, "(CSV) Nanometrics timing quality",
             "NTQ-Time Qual", "%", 3, "mln", 0, "Y", "B", "YB"],
     "NTD": [1, "E", 1, "(CSV) Nanometrics timing DAC count",
             "NTD-Timing DAC Count", "", 3, "mln", 0, "Y", "B", "YB"],
     "NM1": [1, "E", 3, "(CSV) Nanometrics sensor mass position voltage",
             "NM1-MassPos Ch1", "V", 2, "mlmp", 0, "", "", "", 1],
     "NM2": [1, "E", 3, "(CSV) Nanometrics sensor mass position voltage",
             "NM2-MassPos Ch2", "V", 2, "mlmp", 0, "", "", "", 1],
     "NM3": [1, "E", 3, "(CSV) Nanometrics sensor mass position voltage",
             "NM3-MassPos Ch3", "V", 2, "mlmp", 0, "", "", "", 1],
     # Color items are not used for these. The LOG line bg/fg color is a
     # compromise that will usually not match the dot color.
     "NGS": [1, "E", 1, "Nanometrics GPS lock/unlock indicator",
             "NGS-GPS Lk/Ulk", "", 1, "strz", 0, "", "", "RW"],
     "NPL": [1, "E", 1, "Nanometrics phase lock indicator",
             "NPL-Phase Lock", "", 1, "strz", 0, "", "", "RW"],

     # Realtime Antelope "Q-channels".
     "QBD": [1, "E", 1, "RTAnt Q-Channel: Q330 Reboots last 24 hours",
             "QBD-Reboots", "", 3, "mln", 0, "W", "B"],
     "QBP": [1, "E", 1,
             "RTAnt Q-Channel: Logical port buffer percent full from "
             "real-time status",
             "QBP-LP Buff Full", "%", 3, "mln", 0, "W", "B"],
     "QDG": [1, "E", 1, "RTAnt Q-Channel: Data gap lengths",
             "QDG-Gap Length", "sec", 3, "mln", 0, "Y", "B"],
     "QDL": [1, "E", 1, "RTAnt Q-Channel: Current data latency",
             "QDL-Latency", "sec", 3, "mln", 0, "Y", "B"],
     "QDR": [1, "E", 1, "RTAnt Q-Channel: Data rate", "QDR-Data Rate",
             "b/sec", 3, "mln", 0, "O", "B"],
     "QEF": [1, "E", 3,
             "RTAnt Q-Channel: Overall communications efficiency",
             "QEF-Comm Eff", "%", 3, "mln", 0, "O", "B"],
     "QG1": [1, "E", 1, "RTAnt Q-Channel: Data gaps last hour",
             "QG1-Gaps 1H", "", 3, "mln", 0, "O", "B"],
     "QGD": [1, "E", 1, "RTAnt Q-Channel: Data gaps last 24 hours",
             "QGD-Gaps 24H", "", 3, "mln", 0, "O", "B"],
     "QID": [1, "E", 1,
             "RTAnt Q-Channel: Data logger IP address changes last 24 hours",
             "QID-IP Changes", "", 3, "mln", 0, "W", "B"],
     "QLD": [1, "E", 1, "RTAnt Q-Channel: Comm link cycles last 24 hours",
             "QLD-Link Cycles", "", 3, "mln", 0, "O", "B"],
     "QPC": [1, "E", 1, "RTAnt Q-Channel: Packets with checksum errors",
             "QPC-Chksum Errs", "", 3, "mln", 0, "O", "B"],
     "QPD": [1, "E", 1,
             "RTAnt Q-Channel: Number of POCs received last 24 hours",
             "QPD-POCs 24H", "", 3, "mln", 0, "W", "B"],
     "QPK": [1, "E", 1, "RTAnt Q-Channel: Packets processed",
             "QPK-Total Packets", "", 3, "mln", 0, "W", "B"],
     "QPS": [1, "E", 1, "RTAnt Q-Channel: Packets with wrong sizes",
             "QPS-Wrong Sizes", "", 3, "mln", 0, "W", "B"],
     "QRD": [1, "E", 1, "RTAnt Q-Channel: Bytes read last 24 hours",
             "QRD-Bytes Read", "", 3, "mln", 0, "W", "B"],
     "QRT": [1, "E", 1, "RTAnt Q-Channel: Current Run Time",
             "QRT-Run Time", "sec", 3, "mln", 0, "Y", "B"],
     "QTH": [1, "E", 1, "RTAnt Q-Channel: Current Throttling Setting",
             "QTH-Throttling", "b/sec", 3, "mln", 0, "O", "B"],
     "QTP": [1, "E", 1,
             "RTAnt Q-Channel: Ratio of seconds read to realtime clock",
             "QTP-Ratio Read", "", 3, "mln", 0, "Y", "B"],
     "QWD": [1, "E", 1, "RTAnt Q-Channel: Bytes written last 24 hours",
             "QRD-Bytes Written", "", 3, "mln", 0, "W", "B"],

     # These are not plottable things from Q330s. They are here to keep the
     # error messages from popping up about them. LOG is a slightly special
     # case. It gets handled separately in the code.
     "LOG": [0, "E", 0, "Q330 LOG channel messages (not plotted)",
             "LOG-Not Plotted", "", 0, "", 0, "", ""],
     "ACE": [0, "E", 0, "Q330 ACE channel items (not plotted)",
             "ACE-Not Plotted"],
     "OCF": [0, "E", 0, "Q330 OCP channel items (not plotted)",
             "OCF-Not Plotted"]}
# I think Nanometrics has, once again, changed gears, so this had to be added.
# QPChansDecodeX = Handles mapping QPCHANS_DECODE values to the make/model of
#                 DAS data being read. Q330s are set to report mass positions
#                 in mv, and it looks like Nanometrics Meridians are set to
#                 report it in volts, for example.
QPCDSelect = "Other"
# _DECODE = 99 - voltage
QPChansDecodeV = {"Other": 1, "NM": 6}
# _DECODE = 999 - mass postions
QPChansDecodeMP = {"Other": 3, "NM": 7}
# QData = "ChanID":[time series]. All of the data read from the station.
# [time series] will usually be [Time, Value], [Time, Value], ...
QPData = {}
# ...except for log messages. They go here.
QPLogs = []
# An attempt to catch gaps.
# FINISHME - >2hour gaps in point timestamps for the LCQ channel. It gets
# determined while plotting LCQ and then drawn right below the LCQ plot.
# The time gap detection will shrink, but I think it will have to stay tied
# to a specific channel. LCQ always seems to be there.
QPGaps = {}
# Standard errors/messages about stuff the program finds as it reads the data,
# but does not stop because of.
QPErrors = []
# The name of the station whose data we just read. It SHOULD end up with only
# one ID.
QPStaIDs = []
# Where the found network codes are kept. There SHOULD only be one.
QPNetCodes = []
# What should be any tag/serial numbers found in the LOG messages.
QPTagNos = []
# Any system software versions found in the LOG messages.
QPSWVers = []
# We will combine these with the channel IDs to look for data in QPData.
# We will first look for the Chan+"" channels, then anything Chan+anything
# else that might be in there.
QPLocIDs = []
# Sample rates. Where the sample rate for each channel is collected for
# printing on the plots.
QPSampRates = {}
# From the Nanometrics CSV files Instrument column.
QPInstruments = []
# Just for the end of fileSelected() message.
QPAntSpikesWhacked = 0
# For error message control.
QPUnknownChanIDs = []
QPUnknownBlocketteType = []
# The list of files processed the last time fileSelected() was called.
QPFilesProcessed = []
# Where the zooming in and out time ranges are kept.
QPPlotRanges = []
# If the ListChannels option is on the channels found in each file will be
# kept in here.
QPListChannels = {}
# Start creating the display.
Root.protocol("WM_DELETE_WINDOW", Command(progQuitter, True))
menuMake(Root)
# If I don't create a sub-frame of Root resizing the main window causes a lot
# of plot updates to be caught and executed causing the plot to be redrawn
# a number of times before reaching the new size of the window (on some OS's
# and setups).
SubRoot = Frame(Root)
SubRoot.pack(fill=BOTH, expand=YES)
# ----- Current directory area -----
Sub = Frame(SubRoot)
LBu = BButton(Sub, text="Main Data Directory", command=changeDir)
LBu.pack(side=LEFT)
ToolTip(LBu, 30, "Click to change the directory.")
LEnt = PROGEnt["DDIR"] = Entry(Sub, textvariable=PROGDataDirVar)
LEnt.pack(side=LEFT, fill=X, expand=YES)
compFsSetup(Root, LEnt, PROGDataDirVar, None, "MF")
LEnt.bind('<Return>', returnReadDir)
if PROGSystem == "dar":
    # At this point this is overkill. If it is "dar" this will be True.
    if B2Glitch is True:
        LEnt.bind("<Button-2>", openDDIRPopup)
    LEnt.bind("<Button-3>", openDDIRPopup)
Lb = Label(Sub, text="From:")
Lb.pack(side=LEFT)
ToolTip(Lb, 30,
        "Fill in the 'From' and 'To' fields to the right with start and end "
        "dates to read in from the selected file.")
LEnt = Entry(Sub, width=11, textvariable=FromDateVar)
LEnt.pack(side=LEFT)
BButton(Sub, text="C",
        command=Command(formPCAL, Root, "C", "MF", FromDateVar, LEnt, False,
                        OPTDateFormatRVar)).pack(side=LEFT)
Label(Sub, text=" To ").pack(side=LEFT)
LEnt = Entry(Sub, width=11, textvariable=ToDateVar)
LEnt.pack(side=LEFT)
BButton(Sub, text="C",
        command=Command(formPCAL, Root, "C", "MF", ToDateVar, LEnt, False,
                        OPTDateFormatRVar)).pack(side=LEFT)
LLb = Label(Sub, text=" Hints ")
LLb.pack(side=LEFT)
ToolTip(LLb, 45,
        "PLOT AREA HINTS:\n--Clicking on a point: Clicking on a plot point "
        "will display the time and data value associated with that point in "
        "the messages are below the plot area.\n\n--Clicking on totals: "
        "Clicking on one of the total points values at the right end of a "
        "plot will display the identity of that plot's channel in the message "
        "area.\n\n--Control-clicking: Control-clicking in the plotting area "
        "will draw a line and show the date/time at that point at the top and "
        "bottom of the plotting area.\n\n--Control-clicking: Control-clicking "
        "above or below the plots will draw lines between the tick marks on "
        "the top and bottom time lines. Control-clicking again will erase the "
        "lines.\n\n--Shift-click: Shift-clicking in the plotting area the "
        "first time will display a vertical selection rule at that point. "
        "Shift-clicking again will display a second selection rule at the "
        "new point, then QPEEK will zoom in on the area between the rules. "
        "Shift-clicking in the area of the plot labels when the first "
        "selection rule is visible will cancel the selection operation.\n\n"
        "--Shift-click on labels: If zoomed in (described above) shift-"
        "clicking in the area of the plot labels will zoom back out. Each "
        "step when zooming in will be repeated when zooming out.")
Sub.pack(side=TOP, fill=X, expand=NO, padx=3)
# Files list and plotting area.
Sub = Frame(SubRoot)
# ----- File list, scroll bar and text area -----
SSub = Frame(Sub)
# ----- File list and the scroll bars -----
SSSub = Frame(SSub)
SSSSub = Frame(SSSub)
MFFiles = Listbox(SSSSub, relief=SUNKEN, bd=2, height=5,
                  selectmode=EXTENDED)
MFFiles.pack(side=LEFT, fill=BOTH, expand=YES)
MFFiles.bind("<Double-Button-1>", fileSelected)
LSb = Scrollbar(SSSSub, command=MFFiles.yview, orient=VERTICAL)
LSb.pack(side=RIGHT, fill=Y, expand=NO)
MFFiles.configure(yscrollcommand=LSb.set)
SSSSub.pack(side=TOP, fill=BOTH, expand=YES)
LSb = Scrollbar(SSSub, command=MFFiles.xview, orient=HORIZONTAL)
LSb.pack(side=TOP, fill=X, expand=NO)
MFFiles.configure(xscrollcommand=LSb.set)
SSSub.pack(side=TOP, fill=BOTH, expand=YES)
SSSub = Frame(SSub)
LEnt = labelEntry2(SSSub, 11, "Find:=", 35,
                   "[Reload] Show file names containing this at the top.",
                   MFLbxFindVar, 6)
LEnt.bind("<Return>", Command(loadSourceFilesCmd, MFFiles))
LEnt.bind("<KP_Enter>", Command(loadSourceFilesCmd, MFFiles))
BButton(SSSub, text="Clear", fg=Clr["U"],
        command=Command(loadSourceFilesClearLbxFindVar, MFFiles,
                        True)).pack(side=LEFT)
BButton(SSSub, text="Reload", command=Command(loadSourceFilesCmd,
                                              MFFiles)).pack(side=LEFT)
SSSub.pack(side=TOP)
SSSub = Frame(SSub)
labelTip(SSSub, "Station:", LEFT, 40,
         "(Read) If anything is in this field QPEEK will check each record's "
         "station ID value to see if it matches and only process those that "
         "do. This only applies to seed/mini-seed data. Nanometrics CSV files "
         "from different stations cannot be mixed -- they will all just get "
         "plotted together.")
Entry(SSSub, width=12, textvariable=PROGStaIDFilterVar).pack(side=LEFT)
Label(SSSub, text=" ").pack(side=LEFT)
BButton(SSSub, text="Replot", command=plotMFReplot).pack(side=LEFT)
SSSub.pack(side=TOP)
SSSub = Frame(SSub)
labelTip(SSSub, "Background:", LEFT, 30,
         "(Replot)\nB = Black background for all plot areas\nW = White "
         "background")
Radiobutton(SSSub, text="B", variable=PROGColorModeRVar,
            command=setColors, value="B").pack(side=LEFT)
Radiobutton(SSSub, text="W", variable=PROGColorModeRVar,
            command=setColors, value="W").pack(side=LEFT)
SSSub.pack(side=TOP)
SSSub = Frame(SSub)
labelTip(SSSub, "Magnify:", LEFT, 35, "(Replot) 1xL/2xL/4xL: Plot the "
         "information on a canvas the width of the plotting area, or 2x or "
         "4x the width to spread out the points.\n1xH/2xH: Make the plots 1x "
         "or 2x the normal height (this sometimes helps for the seismic data "
         "plots).")
SSSSub = Frame(SSSub)
SSSSSub = Frame(SSSSub)
Radiobutton(SSSSSub, text="1xL", variable=OPTMagnifyLRVar,
            value="1").pack(side=LEFT)
Radiobutton(SSSSSub, text="2xL", variable=OPTMagnifyLRVar,
            value="2").pack(side=LEFT)
Radiobutton(SSSSSub, text="4xL", variable=OPTMagnifyLRVar,
            value="4").pack(side=LEFT)
SSSSSub.pack(side=TOP)
SSSSSub = Frame(SSSSub)
Radiobutton(SSSSSub, text="1xH", variable=OPTMagnifyHRVar,
            value="1").pack(side=LEFT)
Radiobutton(SSSSSub, text="2xH", variable=OPTMagnifyHRVar,
            value="2").pack(side=LEFT)
SSSSSub.pack(side=TOP)
SSSSub.pack(side=LEFT)
SSSub.pack(side=TOP)
SSSub = Frame(SSub)
BButton(SSSub, text="S", command=formSFCI).pack(side=LEFT)
BButton(SSSub, text="C", command=formCPREF).pack(side=LEFT)
labelTip(SSSub, " Curr:", LEFT, 35,
         "(Read) The currently selected Channel Preferences slot.")
Entry(SSSub, textvariable=CPREFCurrNameVar, width=14,
      state=DISABLED).pack(side=LEFT)
SSSub.pack(side=TOP)
SSSub = Frame(SSub)
labelTip(SSSub, ".bms Dir:", LEFT, 35,
         "(Read) Select which directory on a baler memory stick to read if "
         "applicable.")
Radiobutton(SSSub, text="data" + sep, variable=OPTBMSDataDirRVar,
            value="data").pack(side=LEFT)
Radiobutton(SSSub, text="sdata" + sep, variable=OPTBMSDataDirRVar,
            value="sdata").pack(side=LEFT)
SSSub.pack(side=TOP)
SSSub = Frame(SSub)
LCb = Checkbutton(SSSub, text="Detect Gaps:", variable=OPTGapsDetectCVar)
LCb.pack(side=LEFT)
ToolTip(LCb, 35,
        "(Read or Replot) When selected all data records read will be used "
        "to detect gaps. Must Reread if you just turned it on.")
labelTip(SSSub, "Len:", LEFT, 35,
         "(Replot) Enter the minimum length of time a gap must be to be "
         "detected like 5m or 1h. Must be a multiple of a whole minute.")
Entry(SSSub, width=7, textvariable=PROGGapsLengthVar).pack(side=LEFT)
SSSub.pack(side=TOP)
SSSub = Frame(SSub)
LCb = Checkbutton(SSSub, text="TPS:", variable=OPTPlotTPSCVar)
LCb.pack(side=LEFT)
ToolTip(LCb, 45,
        "(Read) Opens a form and plots the Time-Power-Squared information "
        "when data source reading is finished. Only the channels in the Chans "
        "list field to the right will be plotted.")
labelTip(SSSub, "Chans:", LEFT, 35,
         "(Read or Replot) Enter the channels to plot separated by a comma. "
         "Wildcards are allowed. See the Help.")
Entry(SSSub, textvariable=OPTPlotTPSChansVar, width=15).pack(side=LEFT)
SSSub.pack(side=TOP)
SSSub = Frame(SSub)
PROGReadBut = BButton(SSSub, text="Read", command=fileSelected)
PROGReadBut.pack(side=LEFT)
PROGStopBut = BButton(SSSub, text="Stop", state=DISABLED,
                      command=Command(progControl, "stop"))
PROGStopBut.pack(side=LEFT)
BButton(SSSub, text="Write .ps",
        command=Command(formWritePS, Root, "MF",
                        MFPSFilespecVar)).pack(side=LEFT)
SSSub.pack(side=TOP, pady=3)
# ----- Information area -----
SSSub = Frame(SSub)
LMsg = PROGMsg["INFO"] = Text(SSSub, cursor="", font=PROGPropFont,
                              height=10, wrap=WORD)
LMsg.pack(side=LEFT, fill=X, expand=YES)
LSb = Scrollbar(SSSub, orient=VERTICAL, command=LMsg.yview)
LSb.pack(side=TOP, fill=Y, expand=YES)
LMsg.configure(yscrollcommand=LSb.set)
SSSub.pack(side=TOP, fill=X, expand=NO)
SSub.pack(side=LEFT, fill=Y, expand=NO, padx=3, pady=3)
# ----- The plot area -----
SSub = Frame(Sub, bd=2, relief=SUNKEN)
SSSub = Frame(SSub)
# Don't specify the color here.
LCan = PROGCan["MF"] = Canvas(SSSub)
LCan.pack(side=LEFT, expand=YES, fill=BOTH)
LCan.bind("<Shift-Button-1>", plotMFZoomClick)
LCan.bind("<Control-Button-1>", Command(plotMFTimeRuleGrid, LCan))
# With Windows OS.
LCan.bind("<MouseWheel>", Command(mouseWheel, LCan))
# With Linux OS.
LCan.bind("<Button-4>", Command(mouseWheel, LCan))
LCan.bind("<Button-5>", Command(mouseWheel, LCan))
LSb = Scrollbar(SSSub, orient=VERTICAL, command=LCan.yview)
LSb.pack(side=RIGHT, fill=Y, expand=NO)
LCan.configure(yscrollcommand=LSb.set)
SSSub.pack(side=TOP, expand=YES, fill=BOTH)
LSb = Scrollbar(SSub, orient=HORIZONTAL, command=LCan.xview)
LSb.pack(side=BOTTOM, fill=X, expand=NO)
LCan.configure(xscrollcommand=LSb.set)
SSub.pack(side=RIGHT, fill=BOTH, expand=YES)
Sub.pack(side=TOP, fill=BOTH, expand=YES)
# ----- Status message field -----
Sub = Frame(SubRoot)
PROGMsg["MF"] = Text(Sub, cursor="", font=PROGPropFont, height=3,
                     wrap=WORD)
PROGMsg["MF"].pack(side=LEFT, fill=X, expand=YES)
Button(Sub, text="Replot", command=plotMFReplot).pack(side=LEFT)
Sub.pack(side=TOP, fill=X, expand=NO)
# Begin startup sequence.
# Ask who the user is so we can load their setups.
Ret = setUserID()
if Ret[0] != 0:
    updateMe(0)
    progQuitter(False)
Ret = setPROGSetupsDir()
if Ret[0] != 0:
    formMYD(None, (("(OK)", TOP, "ok"),), "ok", Ret[1], "That's Not Good.",
            Ret[2])
    progQuitter(False)
# We need to call this to get the setups loaded, but there may be errors, so
# Ret will be looked at after the main form is positioned and lifted below.
Ret = loadPROGSetups()
# Sneak this in before we check the return value of loadPROGSetups(). If it
# fails it will probably be for the same reason as loadPROGSetups(), so it
# doesn't even return errors.
loadChanPrefsSetups()
# Set these now that the setups have been loaded (if they were), but before we
# deiconify.
fontSetSize()
setColors()
# Now we can do this.
PROGCan["MF"].configure(bg=Clr[DClr["MF"]])
# If there is something wrong it will be caught later.
changeDateFormat(None)
Root.title(makeTitle())
# If the loaded PROGScreenWidthSaved (and Height) values are the same as they
# are now then use the PROGGeometryVar value that was last saved to set the
# window, otherwise set it to new values.
if PROGIgnoreGeometry is False:
    if PROGScreenWidthSaved.get() == PROGScreenWidthNow and \
            PROGScreenHeightSaved.get() == PROGScreenHeightNow and \
            len(PROGGeometryVar.get()) != 0:
        Root.geometry(PROGGeometryVar.get())
    else:
        # My 27" iMac.
        if PROGScreenWidthNow <= 2560:
            FW = int(PROGScreenWidthNow * .75)
            FH = int(PROGScreenHeightNow * .75)
            FX = int(PROGScreenWidthNow / 2 - FW / 2)
            if FX < 0:
                FX = 0
            # Sometimes (OS) measurements don't include the size of the title
            # bar, so the -20 compensates for that a bit.
            FY = int(PROGScreenHeightNow / 2 - FH / 2 - 20)
            if FY < 0:
                FY = 0
            Root.geometry("%dx%d+%d+%d" % (FW, FH, FX, FY))
        else:
            # Just let it come up however it wants. It will be too small,
            # but after the user resizes it it will remember.
            pass
else:
    # We'll just make a guess.
    FW = int(PROGScreenWidthNow * .75)
    FH = int(PROGScreenHeightNow * .75)
    FX = int(PROGScreenWidthNow / 2 - FW / 2)
    if FX < 0:
        FX = 0
    FY = int(PROGScreenHeightNow / 2 - FH / 2 - 20)
    if FY < 0:
        FY = 0
    Root.geometry("%dx%d+%d+%d" % (FW, FH, FX, FY))
Root.update()
# Set these so they get saved when we quit (even if they were the same).
PROGScreenHeightSaved.set(PROGScreenHeightNow)
PROGScreenWidthSaved.set(PROGScreenWidthNow)
Root.deiconify()
Root.lift()
# NOW look at any loadPROGSetups() messages.
# Setups file could not be found.
if Ret[0] == 1:
    formMYD(Root, (("(OK)", TOP, "ok"), ), "ok", Ret[1], "Really?", Ret[2])
# Error opening setups file. We'll just continue.
elif Ret[0] == 2 or Ret[0] == 3 or Ret[0] == 4:
    formMYD(Root, (("(OK)", TOP, "ok"), ), "ok", Ret[1], "Oh Oh. Maybe.",
            Ret[2])
# Something is wrong so quit and don't save the setups.
elif Ret[0] == 5:
    formMYD(Root, (("(Quit)", TOP, "quit"), ), "quit", Ret[1], "Oh Oh.",
            Ret[2])
    progQuitter(False)
# This will be true if there was no setups file.
if len(PROGDataDirVar.get()) == 0:
    Ret = setPROGStartDir(2, True)
    if len(Ret) == 0:
        progQuitter(False)
    elif len(Ret) != 0:
        PROGDataDirVar.set(Ret)
# This really isn't necessary since the file name gets set when a file is
# selected, but it will keep the user out of trouble before anything is
# plotted.
if len(MFPSFilespecVar.get()) == 0:
    MFPSFilespecVar.set(PROGDataDirVar.get())
loadSourceFilesCmd(MFFiles)
formCPREFCurrName()
# Turns on epoch time displaying for troubleshooting.
# OPTDateFormatRVar.set("")
if PROG_PYVERS == 3:
    formMYD(Root, (("(OK)", TOP, "ok"),), "ok", "YB", "Be Careful.",
            "Running under Python 3 is new, and possibly exciting, so be sure "
            "to report any bugs to PASSCAL.", "", 1)
# TESTING-PROFILE. Uncomment this and comment out 'Root.mainloop()' line.
# profile.run("Root.mainloop()")
Root.mainloop()
# END: main
# END PROGRAM: QPEEK

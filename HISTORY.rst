=======
History
=======

2016.235 (2018-06-07)
---------------------
* First release on new build system.

2016.235 (2016-08-22)
---------------------
* .txt files can now be in .sdr directories.

2017.102 (2017-04-19)
---------------------
* A little more informative handling of unknown blockettes.

2017.103 (2017-04-21)
---------------------
* May finally handle small data files better. It's been a problem.

2017.125 (2017-05-05)
---------------------
* Added Find-stuff to the list of known channels form.

2017.205 (2017-07-24)
---------------------
* GPS plot display was not clearing when reading of the next data
  files started.

2017.306 (2017-11-02)
---------------------
* Added the SeisComp3 operating mode. It's not heavily tested, and I
  don't know if the system at PASSCAL is typical or special, but it
  seems to work.

2018.179 (2018-06-28)
---------------------
* Added EL2, EL1 and EL2 channels.

2019.060 (2019-02-28)
---------------------
* Should be ready for Python 2 and 3.
* The menu item to change font sizes is now Change Font Sizes, instead
  of Fonts BIGGER and Fonts smaller.

2019.232 (2019-08-20)
---------------------
* Started an exclusion list for folders/files on Nanometric Centaur
  SD cards that do not have seismic/recorder data in them. Most of
  them are created by "others" (like Windows).
* Adjusted the program so that the channel IDs found when scanning
  match the channel IDs found when reading Centaur SD card files
  when reading them in the Nano Card mode.
* Reading Centaur SD cards copied to a folder ending with .nan and
  using the Normal program mode works best at this point.

2019.262 (2019-09-19)
---------------------
* Just LIB function changes from other programs.

2020.220 (2020-08-07)
---------------------
* Updated to work with Python 3
* Added a unit test to test qpeek import
* Updated list of platform specific dependencies to be installed when
  installing qpeek in dev mode (see setup.py)
* Installed and tested qpeek against Python3.[6,7,8] using tox
* Formatted Python code to conform to the PEP8 style guide
* Created conda packages for qpeek that can run on Python3.[6,7,8]
* Updated .gitlab-ci.yml to run a linter and unit tests for Python3.[6,7,8]
  in GitLab CI pipeline

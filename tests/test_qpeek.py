#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `qpeek` package."""

import unittest
import sys

from unittest.mock import patch


class TestQpeek(unittest.TestCase):
    """Tests for `qpeek` package."""

    def test_import(self):
        """Test qpeek import"""
        with patch.object(sys, 'argv', ['qpeek', '-#']):
            with self.assertRaises(SystemExit) as cmd:
                try:
                    import qpeek.qpeek as qp
                    self.assertTrue(qp.PROG_NAME, 'QPEEK')
                except ImportError as e:
                    print(e)
                    self.fail("qpeek import failed")
            self.assertEqual(cmd.exception.code, 0, "sys.exit(0) never called "
                             "- Failed to excercise qpeek")
